/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.helpers.IOUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.common.JPairtree;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceConfigurator;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceUtilities;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudStorageAbs;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceStorageLocalFileImpl;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceUtilities;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudTextGridObject;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2022-05-24 - Funk - Fix some SpotBugs issues.
 * 
 * 2017-02-24 - Funk - Copied from TestGatImpl.
 */

/**
 * <p>
 * JUnit class for testing the local file storage implementation.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-05-24
 * @since 2017-02-24
 */

public class TestTGCrudServiceStorageLocalFileImpl {

  // **
  // STATIC FINALS
  // **

  private static final String PROPERTIES_FILE = "tgcrud-junit.localfile.properties";
  private static final String META_FILE = "test-item.meta.xml";
  private static final String DATA_FILE = "test-item.png";
  private static final String TMP_DIR = "./target/tmp";
  private static final String PAIRTREE_ROOT = "/pairtree_root";

  private static final String THE_ABSOLUTE_PATH = "THE_ABSOLUTE_PATH";
  private static final String DATA_STORAGE_URI = "DEFAULT_DATA_STORAGE_URI";
  private static final String DATA_STORAGE_URI_PUBLIC = "DEFAULT_DATA_STORAGE_URI_PUBLIC";

  // **
  // STATICS
  // **

  private static CrudServiceConfigurator conf;
  private static String absTempPath;

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Load the TG-crud configurations.
    conf = CrudServiceConfigurator
        .getInstance(CrudServiceUtilities.getResource(PROPERTIES_FILE).toString());

    // Adapt the relatively given storage location pathes.
    absTempPath = new File(TMP_DIR).toURI().normalize().toASCIIString();

    System.out.println(absTempPath);

    System.out.println(conf.getDEFAULTdATAsTORAGEuRI().toASCIIString()
        .replaceFirst(THE_ABSOLUTE_PATH, absTempPath));

    conf.setProperty(DATA_STORAGE_URI, conf.getDEFAULTdATAsTORAGEuRI().toASCIIString()
        .replaceFirst(THE_ABSOLUTE_PATH, absTempPath));
    conf.setProperty(DATA_STORAGE_URI_PUBLIC, conf.getDEFAULTdATAsTORAGEuRIpUBLIC().toASCIIString()
        .replaceFirst(THE_ABSOLUTE_PATH, absTempPath));

    System.out.println("DEFAULT_DATA_STORAGE_URI set to:    " + conf.getDEFAULTdATAsTORAGEuRI());
    System.out.println(
        "DEFAULT_DATA_STORAGE_URI_PUBLIC set to:  " + conf.getDEFAULTdATAsTORAGEuRIpUBLIC());
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {

    // Create all the test dirs.
    System.out.println("Creating temporary directories");

    // Create original and replica folders for first storage location.
    new File(new URI(conf.getDEFAULTdATAsTORAGEuRI() + PAIRTREE_ROOT)).mkdirs();
    for (String s : conf.getREPLICAdATAsTORAGEuRIS()) {
      new File(new URI(s + PAIRTREE_ROOT)).mkdirs();
    }

    // Create original and replica folders for second storage location.
    new File(new URI(conf.getDEFAULTdATAsTORAGEuRIpUBLIC() + PAIRTREE_ROOT)).mkdirs();
    for (String s : conf.getREPLICAdATAsTORAGEuRISpUBLIC()) {
      new File(new URI(s + PAIRTREE_ROOT)).mkdirs();
    }
  }

  /**
   * <p>
   * Delete all the temporary directories.
   * </p>
   * 
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {

    System.out.println(new File(TMP_DIR));

    // Delete temp dir.
    File f = new File(TMP_DIR);
    boolean result = delete(f);

    if (result) {
      System.out.println("Deleted temp dir: " + f.getAbsolutePath());
    } else {
      System.out.println("Could NOT delete temp dir: " + conf.getDEFAULTdATAsTORAGEuRI());
      assertTrue(false);
    }
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Tests the <b>create()</b> using the default storage location.
   * </p>
   * 
   * @throws IoFault
   * @throws IOException
   * @throws InterruptedException
   */
  @Test
  public void testCreate() throws IoFault, IOException, InterruptedException {

    System.out.println("Testing #CREATE");

    MetadataContainerType metadata = testCreate(conf);
    URI uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    System.out.println("\tURI: " + uri);
    JPairtree jpt = new JPairtree(uri);
    String ppath = jpt.getPtreePathString() + jpt.getPtreeEncodedId();
    System.out.println("\tPairtreePath: " + ppath);

    // Test if files were created.
    File dataFile = new File(URI.create(
        conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath));
    File metadataFile = new File(URI.create(
        conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath + conf.getMETADATAfILEsUFFIX()));
    if (dataFile.exists()) {
      System.out.println("\tData file created:\n\t\t" + dataFile.getAbsolutePath());
    } else {
      System.out.println("\tData file NOT created:\n\t\t" + dataFile.getAbsolutePath());
      assertTrue(false);
    }
    if (metadataFile.exists()) {
      System.out.println("\tMetadata file created:\n\t\t" + metadataFile.getAbsolutePath());
    } else {
      System.out.println("\tMetadata file NOT created:\n\t\t" + metadataFile.getAbsolutePath());
      assertTrue(false);
    }

    // Test for file size and message digest.
    BigInteger fileSizeFromFile = BigInteger.valueOf(dataFile.length());
    BigInteger fileSizeFromMetadata = metadata.getObject().getGeneric().getGenerated().getExtent();

    System.out.println("\tFile size from file:\t\t" + fileSizeFromFile);
    System.out
        .println("\tFile size from metadata:\t" + fileSizeFromMetadata);

    if (fileSizeFromFile.compareTo(fileSizeFromMetadata) != 0) {
      System.out.println("\tARGL! FILE SIZES DO NOT MATCH!!");
      assertTrue(false);
    }

    // Compute MD5 checksum of data file.
    FileInputStream fis = new FileInputStream(dataFile);
    String messageDigestFromFile = DigestUtils.md5Hex(IOUtils.readBytesFromStream(fis));
    fis.close();
    String messageDigestFromMetadata =
        metadata.getObject().getGeneric().getGenerated().getFixity().get(0).getMessageDigest();

    System.out.println("\tMessage digest from file:\t" + messageDigestFromFile);
    System.out.println("\tMessage digest from metadata:\t" + messageDigestFromMetadata);

    if (!messageDigestFromFile.equals(messageDigestFromMetadata)) {
      System.out.println("\tARGL! CHECKSUMS DO NOT MATCH!");
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Tests the <b>createMetadataPublic()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testCreatePublic() {}

  /**
   * <p>
   * Tests the <b>createMetadata()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testCreateMetadata() {}

  /**
   * <p>
   * Tests the <b>createMetadataPublic()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testCreateMetadataPublic() {}

  /**
   * <p>
   * Tests the <b>retrieve()</b>.
   * </p>
   * 
   * @throws IOException
   * @throws IoFault
   */
  @Ignore
  @Test
  public void testRetrieve() throws IoFault, IOException {}

  /**
   * <p>
   * Tests the <b>retrievePublic()</b>.
   * </p>
   * 
   * @throws IOException
   * @throws IoFault
   */
  @Ignore
  @Test
  public void testRetrievePublic() throws IoFault, IOException {}

  /**
   * <p>
   * Tests the <b>retrieveMetadata()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testRetrieveMetadata() {}

  /**
   * <p>
   * Tests the <b>retrieveMetadataPublic()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testRetrieveMetadataPublic() {}

  /**
   * <p>
   * Tests the <b>update()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testUpdate() {}

  /**
   * <p>
   * Tests the <b>updateMetadata()</b>.
   * </p>
   */
  @Ignore
  @Test
  public void testUpdateMetadata() {}

  /**
   * <p>
   * Tests the <b>delete()</b>.
   * </p>
   * 
   * @throws IOException
   * @throws IoFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   */
  @Test
  public void testDelete() throws IOException, IoFault, RelationsExistFault, InterruptedException {

    System.out.println("Testing #DELETE");

    // Create file first.
    System.out.println("\tCreating file first...");

    MetadataContainerType metadata = testCreate(conf);
    URI uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    System.out.println("\tURI: " + uri);
    JPairtree jpt = new JPairtree(uri);
    String ppath = jpt.getPtreePathString() + jpt.getPtreeEncodedId();
    System.out.println("\tPairtreePath: " + ppath);

    // Call #DELETE.
    testDelete(conf, uri);

    // Test if files were deleted.
    File dataFile = new File(URI.create(conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath));
    File metadataFile = new File(
        URI.create(conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath
            + conf.getMETADATAfILEsUFFIX()));
    if (!dataFile.exists()) {
      System.out.println("\tData file deleted:\n\t\t" + dataFile.getAbsolutePath());
    } else {
      System.out.println("\tData file NOT deleted:\n\t\t" + dataFile.getAbsolutePath());
      assertTrue(false);
    }
    if (!metadataFile.exists()) {
      System.out.println("\tMetadata file deleted:\n\t\t" + metadataFile.getAbsolutePath());
    } else {
      System.out.println("\tMetadata file NOT deleted:\n\t\t" + metadataFile.getAbsolutePath());
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Tests the <b>move()</b>.
   * </p>
   * 
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   * @throws InterruptedException
   */
  @Test
  public void testMovePublic()
      throws IoFault, ObjectNotFoundFault, IOException, InterruptedException {

    System.out.println("Testing #MOVE");

    // Create file first.
    System.out.println("\tCreating file first...");

    MetadataContainerType metadata = testCreate(conf);
    URI uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    System.out.println("\tURI: " + uri);
    JPairtree jpt = new JPairtree(uri);
    String ppath = jpt.getPtreePathString() + jpt.getPtreeEncodedId();
    System.out.println("\tPairtreePath: " + ppath);

    // Call #MOVE.
    testMove(conf, uri);

    // Test if files were deleted from first storage location and created at
    // the second storage location.
    File dataFile1 = new File(URI.create(conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath));
    File metaFile1 = new File(URI.create(
        conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath + conf.getMETADATAfILEsUFFIX()));
    File dataFile2 =
        new File(URI.create(conf.getDEFAULTdATAsTORAGEuRIpUBLIC().toASCIIString() + ppath));
    File metaFile2 = new File(URI.create(conf.getDEFAULTdATAsTORAGEuRIpUBLIC().toASCIIString()
        + ppath + conf.getMETADATAfILEsUFFIX()));
    if (!dataFile1.exists() && dataFile2.exists()) {
      System.out.println("\tData file moved to:\n\t\t" + dataFile2.getAbsolutePath());
    } else {
      System.out.println("\tData file not moved:\n\t\t" + dataFile2.getAbsolutePath());
      assertTrue(false);
    }
    if (!metaFile1.exists() && metaFile2.exists()) {
      System.out.println("\tMetadata file moved to:\n\t\t" + metaFile2.getAbsolutePath());
    } else {
      System.out.println("\tMetadata file not moved:\n\t\t" + metaFile2.getAbsolutePath());
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Tests the <b>copy()</b>.
   * </p>
   * 
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   * @throws InterruptedException
   */
  @Test
  public void testCopyPublic()
      throws IoFault, ObjectNotFoundFault, IOException, InterruptedException {

    System.out.println("Testing #COPY");

    // Create file first.
    System.out.println("\tCreating file first...");

    MetadataContainerType metadata = testCreate(conf);
    URI uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    System.out.println("\tURI: " + uri);
    JPairtree jpt = new JPairtree(uri);
    String ppath = jpt.getPtreePathString() + jpt.getPtreeEncodedId();
    System.out.println("\tPairtreePath: " + ppath);

    // Call #COPY.
    testCopy(conf, uri);

    // Test if files were deleted from first storage location and created at
    // the second storage location.
    File dataFile1 = new File(URI.create(conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath));
    File metaFile1 = new File(URI.create(
        conf.getDEFAULTdATAsTORAGEuRI().toASCIIString() + ppath + conf.getMETADATAfILEsUFFIX()));
    File dataFile2 =
        new File(URI.create(conf.getDEFAULTdATAsTORAGEuRIpUBLIC().toASCIIString() + ppath));
    File metaFile2 = new File(URI.create(conf.getDEFAULTdATAsTORAGEuRIpUBLIC().toASCIIString()
        + ppath + conf.getMETADATAfILEsUFFIX()));
    if (dataFile1.exists() && dataFile2.exists()) {
      System.out.println("\tData file copied to:\n\t\t" + dataFile2.getAbsolutePath());
    } else {
      System.out.println(
          "\tData file not copied or not existing or both:\n\t\t" + dataFile2.getAbsolutePath());
      assertTrue(false);
    }
    if (metaFile1.exists() && metaFile2.exists()) {
      System.out.println("\tMetadata file copied to:\n\t\t" + metaFile2.getAbsolutePath());
    } else {
      System.out.println(
          "\tMetadata not not copied or not existing or both:\n\t\t" + metaFile2.getAbsolutePath());
      assertTrue(false);
    }
  }

  /**
   * @throws IoFault
   */
  @Test
  public void testGetPairtreePathFolderURI() throws IoFault {

    URI uri = URI.create("textgrid:12345.0");
    URI storageUri = URI.create("/storage/bla/blubb/");

    URI resultPath = URI.create("/storage/bla/blubb/pairtree_root/te/xt/gr/id/+1/23/45/,0/");

    System.out.println("URI:    " + uri);
    System.out.println("Storage URI: " + storageUri);

    URI path = TGCrudServiceStorageLocalFileImpl.getPairtreePathFolderURI(uri, storageUri);

    System.out.println("Path: " + path);

    if (path.equals(resultPath)) {
      System.out.println(path + " = " + resultPath);
    } else {
      assertTrue(false);
    }
  }

  /**
   * @throws URISyntaxException
   * @throws IoFault
   * @throws UnsupportedEncodingException
   */
  @Test
  public void testGetGatPathFromUri()
      throws UnsupportedEncodingException, IoFault, URISyntaxException {

    URI uri = URI.create("textgrid:12345.0");
    URI storageUri = URI.create("/storage/bla/blubb/");

    URI resultPath =
        URI.create("/storage/bla/blubb/pairtree_root/te/xt/gr/id/+1/23/45/,0/textgrid+12345,0");

    System.out.println("URI: " + uri);
    System.out.println("Storage URI: " + storageUri);

    URI path = TGCrudServiceStorageLocalFileImpl.getPairtreeObjectUri(uri, storageUri);

    System.out.println("Path: " + path);

    if (path.equals(resultPath)) {
      System.out.println(path + " = " + resultPath);
    } else {
      assertTrue(false);
    }
  }

  // **
  // PRIVATE METHODS
  // *

  /**
   * <p>
   * Tests the #CREATE with a given TG-crud configuration.
   * </p>
   * 
   * @param theConfiguration
   * @return
   * @throws IOException
   * @throws IoFault
   */
  private static MetadataContainerType testCreate(CrudServiceConfigurator theConfiguration)
      throws IOException, IoFault {

    MetadataContainerType result;

    // Get storage implementation.
    CrudStorageAbs<MetadataContainerType> storageImplementation = getStorageImplementation();

    System.out
        .println("\tCreating metadata from file: " + CrudServiceUtilities.getResource(META_FILE));

    // Create metadata from file.
    result = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(META_FILE)));

    System.out.println("\tCreating data from file:     "
        + CrudServiceUtilities.getResource(DATA_FILE).getAbsolutePath());

    // Create data from file.
    DataHandler data =
        new DataHandler(new FileDataSource(CrudServiceUtilities.getResource(DATA_FILE)));

    // Create test object.
    TGCrudTextGridObject testObject = new TGCrudTextGridObject(result, data);

    // Do #CREATE.
    storageImplementation.create(testObject);

    return result;
  }

  /**
   * <p>
   * Tests the #DELETE with a given TG-crud configuration.
   * </p>
   * 
   * @param theConfiguration
   * @param theUri
   * @throws IOException
   * @throws IoFault
   * @throws RelationsExistFault
   */
  private static void testDelete(CrudServiceConfigurator theConfiguration, URI theUri)
      throws IOException, IoFault, RelationsExistFault {

    // Get storage implementation.
    CrudStorageAbs<MetadataContainerType> storageImplementation = getStorageImplementation();

    // Do #DELETE.
    storageImplementation.delete(new TGCrudTextGridObject(theUri));
  }

  /**
   * <p>
   * Tests the #MOVE with a given TG-crud configuration.
   * </p>
   * 
   * @param theConfiguration
   * @param theUri
   * @throws IOException
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  private static void testMove(CrudServiceConfigurator theConfiguration, URI theUri)
      throws IOException, IoFault, ObjectNotFoundFault {

    // Get storage implementation.
    CrudStorageAbs<MetadataContainerType> storageImplementation = getStorageImplementation();

    System.out.println("\tSource location:      " + conf.getDEFAULTdATAsTORAGEuRI());
    System.out.println("\tDestination location: " + conf.getDEFAULTdATAsTORAGEuRIpUBLIC());

    // Do #MOVE.
    storageImplementation.move(theUri);
  }

  /**
   * <p>
   * Tests the #COPY with a given TG-crud configuration.
   * </p>
   * 
   * @param theConfiguration
   * @param theUri
   * @throws IOException
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  private static void testCopy(CrudServiceConfigurator theConfiguration, URI theUri)
      throws IOException, IoFault, ObjectNotFoundFault {

    // Get storage implementation.
    CrudStorageAbs<MetadataContainerType> storageImplementation = getStorageImplementation();

    System.out.println("\tSource location:      " + conf.getDEFAULTdATAsTORAGEuRI());
    System.out.println("\tDestination location: " + conf.getDEFAULTdATAsTORAGEuRIpUBLIC());

    // Do #COPY.
    storageImplementation.copy(theUri);
  }

  /**
   * <p>
   * Returns the storage implementation.
   * </p>
   * 
   * @return
   */
  private static CrudStorageAbs<MetadataContainerType> getStorageImplementation() {

    // Get JavaGAT storage implementation.
    CrudStorageAbs<MetadataContainerType> storageImplementation =
        new TGCrudServiceStorageLocalFileImpl();
    storageImplementation.setConfiguration(conf);

    return storageImplementation;
  }

  /**
   * <p>
   * Deletes all the temp files.
   * </p>
   * 
   * @param theFolder
   */
  private static boolean delete(File theFile) {

    boolean result = false;

    // Delete recursively.
    if (theFile != null) {
      File files[] = theFile.listFiles();
      if (files != null && files.length > 0) {
        for (File f : files) {
          if (f != null) {
            boolean deleted = delete(f);

            if (deleted) {
              System.out.println("DELETED: " + f.getAbsolutePath());
            } else {
              System.out.println("NOT DELETED: " + f.getAbsolutePath());
            }
          }
        }
      }

      result = theFile.delete();
    }

    return result;
  }

}
