/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funksub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertEquals;
import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import javax.xml.datatype.XMLGregorianCalendar;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceUtilities;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceUtilities;
import jakarta.xml.bind.JAXB;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2023-02-16 - Funk - Complete tests. Finally learned to add correct assert methods!
 * 
 * 2023-02-15 - Funk - First version.
 */

/**
 * <p>
 * JUnit class for testing the general crud service utilities.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */

public class TestCrudServiceUtilities {

  // https://hdl.handle.net/api/handles/21.11113/00-1734-0000-0005-1424-B?type=FILESIZE
  private static final String FILESIZE_JSON_1743 =
      "{\"responseCode\":1,\"handle\":\"21.11113/00-1734-0000-0005-1424-B\",\"values\":[{\"index\":2,\"type\":\"FILESIZE\",\"data\":{\"format\":\"string\",\"value\":\"-1\"},\"ttl\":86400,\"timestamp\":\"2017-09-20T12:04:26Z\"}]}";
  // https://hdl.handle.net/api/handles/21.11113/00-1734-0000-0005-1424-B?type=CHECKSUM
  private static final String CHECKSUM_JSON_1734 =
      "{\"responseCode\":200,\"values\":[],\"handle\":\"21.11113/00-1734-0000-0005-1424-B\"}";
  // https://hdl.handle.net/api/handles/21.11113/0000-0006-9F65-6?type=FILESIZE
  private static final String FILESIZE_JSON_0006 =
      "{\"responseCode\":1,\"handle\":\"21.11113/0000-0006-9F65-6\",\"values\":[{\"index\":3,\"type\":\"FILESIZE\",\"data\":{\"format\":\"string\",\"value\":\"1003377\"},\"ttl\":86400,\"timestamp\":\"2017-08-01T21:32:56Z\"}]}";
  // https://hdl.handle.net/api/handles/21.11113/0000-0006-9F65-6?type=CHECKSUM
  private static final String CHECKSUM_JSON_0006 =
      "{\"responseCode\":1,\"handle\":\"21.11113/0000-0006-9F65-6\",\"values\":[{\"index\":4,\"type\":\"CHECKSUM\",\"data\":{\"format\":\"string\",\"value\":\"md5:5a3af94cc5e48220cc1754d99e2558e2\"},\"ttl\":86400,\"timestamp\":\"2017-08-01T21:32:56Z\"}]}";
  // https://hdl.handle.net/api/handles/21.11113/0000-000F-F5FF-E?type=FILESIZE
  private static final String FILESIZE_JSON_21 =
      "{\"responseCode\":1,\"handle\":\"21.11113/0000-000F-F5FF-E\",\"values\":[{\"index\":3,\"type\":\"FILESIZE\",\"data\":{\"format\":\"string\",\"value\":\"312800\"},\"ttl\":86400,\"timestamp\":\"2023-06-09T13:33:16Z\"}]}";
  // https://hdl.handle.net/api/handles/21.11113/0000-000F-F5FF-E?type=CHECKSUM
  private static final String CHECKSUM_JSON_21 =
      "{\"responseCode\":1,\"handle\":\"21.11113/0000-000F-F5FF-E\",\"values\":[{\"index\":4,\"type\":\"CHECKSUM\",\"data\":{\"format\":\"string\",\"value\":\"md5:d330bae4c1b9610358f8589756342990\"},\"ttl\":86400,\"timestamp\":\"2023-06-09T13:33:16Z\"}]}";


  /**
   * <p>
   * Wikipedia: Use for checking, please see <https://de.wikipedia.org/wiki/Unixzeit>.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testGetXmlGregorianCalendar() throws IoFault {

    long millies;
    String expectedDate;
    XMLGregorianCalendar cal;
    String cetString;

    // 1111111111 / 423A35C7 --> 18. März 2005 01:58:31 UTC (see
    // <https://de.wikipedia.org/wiki/Unixzeit#Besondere_Werte>)
    millies = 1111111111l * 1000;
    expectedDate = "2005-03-18T02:58:31.000+01:00";
    cal = CrudServiceUtilities.getXMLGregorianCalendar(millies);
    cetString = CrudServiceUtilities.getCETDateString(cal);
    assertEquals(cetString, expectedDate);

    // 4294967295 / FFFFFFFF --> 7. Feb. 2106 06:28:15 UTC (see
    // <https://de.wikipedia.org/wiki/Unixzeit#Besondere_Werte>)
    millies = 4294967295l * 1000;
    expectedDate = "2106-02-07T07:28:15.000+01:00";
    cal = CrudServiceUtilities.getXMLGregorianCalendar(millies);
    cetString = CrudServiceUtilities.getCETDateString(cal);
    assertEquals(cetString, expectedDate);

    // !Sommerzeit value!
    // 1600000000 / 5F5E1000 13. Sep. 2020 12:26:40 UTC (see
    // <https://de.wikipedia.org/wiki/Unixzeit#Besondere_Werte>)
    millies = 1600000000l * 1000;
    expectedDate = "2020-09-13T14:26:40.000+02:00";
    cal = CrudServiceUtilities.getXMLGregorianCalendar(millies);
    cetString = CrudServiceUtilities.getCETDateString(cal);
    assertEquals(cetString, expectedDate);
  }

  /**
   * @throws IoFault
   */
  @Test
  public void testGetDateString() throws IoFault {

    long millies;
    String expectedDate;
    String dateString;

    // 1111111111 / 423A35C7 --> 18. März 2005 01:58:31 UTC
    millies = 1111111111l * 1000;
    expectedDate = "2005-03-18T02:58:31.000+01:00";
    dateString = CrudServiceUtilities
        .getCETDateString(CrudServiceUtilities.getXMLGregorianCalendar(millies));
    assertEquals(dateString, expectedDate);

    // 4294967295 / FFFFFFFF --> 7. Feb. 2106 06:28:15 UTC
    millies = 4294967295l * 1000;
    expectedDate = "2106-02-07T07:28:15.000+01:00";
    dateString = CrudServiceUtilities
        .getCETDateString(CrudServiceUtilities.getXMLGregorianCalendar(millies));
    assertEquals(dateString, expectedDate);

    // !Sommerzeit value!
    // 1600000000 / 5F5E1000 13. Sep. 2020 12:26:40 UTC (see
    // <https://de.wikipedia.org/wiki/Unixzeit#Besondere_Werte>)
    millies = 1600000000l * 1000;
    expectedDate = "2020-09-13T14:26:40.000+02:00";
    dateString = CrudServiceUtilities
        .getCETDateString(CrudServiceUtilities.getXMLGregorianCalendar(millies));
    assertEquals(dateString, expectedDate);
  }

  /**
   * 
   */
  @Test
  public void testGetDateFromCETString() {

    String dateString;
    Date expectedDate;
    Date date;

    // 1111111111 / 423A35C7 --> 18. März 2005 01:58:31 UTC
    dateString = "2005-03-18T01:58:31.000Z";
    expectedDate = Date.from(Instant.parse(dateString));
    date = CrudServiceUtilities.getDateFromCETString(dateString);
    assertEquals(date, expectedDate);

    // 4294967295 / FFFFFFFF --> 7. Feb. 2106 06:28:15 UTC
    dateString = "2106-02-07T06:28:15.000Z";
    expectedDate = Date.from(Instant.parse(dateString));
    date = CrudServiceUtilities.getDateFromCETString(dateString);
    assertEquals(date, expectedDate);

    // !Sommerzeit value!
    // 1600000000 / 5F5E1000 13. Sep. 2020 12:26:40 UTC (see
    // <https://de.wikipedia.org/wiki/Unixzeit#Besondere_Werte>)
    dateString = "2020-09-13T12:26:40.000Z";
    expectedDate = Date.from(Instant.parse(dateString));
    date = CrudServiceUtilities.getDateFromCETString(dateString);
    assertEquals(date, expectedDate);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testCheckHandleMetadataJSON1734Filesize() throws JSONException, ParseException {

    boolean result = CrudServiceUtilities.checkTypeInHandleMetadata(FILESIZE_JSON_1743,
        TGCrudServiceUtilities.HDL_FILESIZE);
    assertEquals(result, true);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testCheckHandleMetadataJSON1734Checksum() throws JSONException, ParseException {

    boolean result = CrudServiceUtilities.checkTypeInHandleMetadata(CHECKSUM_JSON_1734,
        TGCrudServiceUtilities.HDL_CHECKSUM);
    assertEquals(result, false);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testGetHandleMetadataJSON1734Filesize() throws JSONException, ParseException {

    String result = CrudServiceUtilities.getHandleMetadata(FILESIZE_JSON_1743,
        TGCrudServiceUtilities.HDL_FILESIZE);
    assertEquals(result, "-1");
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testGetHandleMetadataJSON1734Checksum() throws JSONException, ParseException {

    String result = CrudServiceUtilities.getHandleMetadata(CHECKSUM_JSON_1734,
        TGCrudServiceUtilities.HDL_CHECKSUM);
    assertEquals(result, "");
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testCheckHandleMetadataJSON0006Filesize() throws JSONException, ParseException {

    boolean result = CrudServiceUtilities.checkTypeInHandleMetadata(FILESIZE_JSON_0006,
        TGCrudServiceUtilities.HDL_FILESIZE);
    assertEquals(result, true);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testCheckHandleMetadataJSON0006Checksum() throws JSONException, ParseException {

    boolean result = CrudServiceUtilities.checkTypeInHandleMetadata(CHECKSUM_JSON_0006,
        TGCrudServiceUtilities.HDL_CHECKSUM);
    assertEquals(result, true);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testGetHandleMetadataJSON0006Filesize() throws JSONException, ParseException {

    String result = CrudServiceUtilities.getHandleMetadata(FILESIZE_JSON_0006,
        TGCrudServiceUtilities.HDL_FILESIZE);
    assertEquals(result, "1003377");
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testGetHandleMetadataJSON0006Checksum() throws JSONException, ParseException {

    String result = CrudServiceUtilities.getHandleMetadata(CHECKSUM_JSON_0006,
        TGCrudServiceUtilities.HDL_CHECKSUM);
    assertEquals(result, "md5:5a3af94cc5e48220cc1754d99e2558e2");
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testCheckHandleMetadataJSON21Filesize() throws JSONException, ParseException {

    boolean result = CrudServiceUtilities.checkTypeInHandleMetadata(FILESIZE_JSON_21,
        TGCrudServiceUtilities.HDL_FILESIZE);
    assertEquals(result, true);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testCheckHandleMetadataJSON21Checksum() throws JSONException, ParseException {

    boolean result = CrudServiceUtilities.checkTypeInHandleMetadata(CHECKSUM_JSON_21,
        TGCrudServiceUtilities.HDL_CHECKSUM);
    assertEquals(result, true);
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testGetHandleMetadataJSON21Filesize() throws JSONException, ParseException {

    String result = CrudServiceUtilities.getHandleMetadata(FILESIZE_JSON_21,
        TGCrudServiceUtilities.HDL_FILESIZE);
    assertEquals(result, "312800");
  }

  /**
   * @throws ParseException
   * @throws JSONException
   */
  @Test
  public void testGetHandleMetadataJSON21Checksum() throws JSONException, ParseException {

    String result = CrudServiceUtilities.getHandleMetadata(CHECKSUM_JSON_21,
        TGCrudServiceUtilities.HDL_CHECKSUM);
    assertEquals(result, "md5:d330bae4c1b9610358f8589756342990");
  }

  /**
   * @throws JSONException
   */
  @Test
  public void testDecodeBase64ForESMetadataRetrieval() throws JSONException {

    String jsonMetadata =
        "{\"extent\":3414,\"item\":{\"rightsHolder\":\"ELTeC conversion\"},\"original_metadata\":\"PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/Pgo8bWV0YWRhdGFDb250YWluZXJUeXBlIHhtbG5zOnRnPSJodHRwOi8vdGV4dGdyaWQuaW5mby9yZWxhdGlvbi1ucyMiIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyIgeG1sbnM6bnM0PSJodHRwOi8vdGV4dGdyaWQuaW5mby9uYW1lc3BhY2VzL21ldGFkYXRhL2NvcmUvMjAxMCI+CiAgICA8bnM0Om9iamVjdD4KICAgICAgICA8bnM0OmdlbmVyaWM+CiAgICAgICAgICAgIDxuczQ6cHJvdmlkZWQ+CiAgICAgICAgICAgICAgICA8bnM0OnRpdGxlPkVMVGVDIFRlc3QubWQ8L25zNDp0aXRsZT4KICAgICAgICAgICAgICAgIDxuczQ6Zm9ybWF0PnRleHQvbWFya2Rvd248L25zNDpmb3JtYXQ+CiAgICAgICAgICAgIDwvbnM0OnByb3ZpZGVkPgogICAgICAgICAgICA8bnM0OmdlbmVyYXRlZD4KICAgICAgICAgICAgICAgIDxuczQ6Y3JlYXRlZD4yMDIwLTA3LTAxVDA5OjQwOjUyLjM5MCswMjowMDwvbnM0OmNyZWF0ZWQ+CiAgICAgICAgICAgICAgICA8bnM0Omxhc3RNb2RpZmllZD4yMDIwLTA3LTAxVDA5OjQwOjUyLjM5MCswMjowMDwvbnM0Omxhc3RNb2RpZmllZD4KICAgICAgICAgICAgICAgIDxuczQ6aXNzdWVkPjIwMjAtMDctMDFUMDk6NDA6NTIuMzkwKzAyOjAwPC9uczQ6aXNzdWVkPgogICAgICAgICAgICAgICAgPG5zNDp0ZXh0Z3JpZFVyaSBleHRSZWY9IiI+dGV4dGdyaWQ6M3RoeHMuMDwvbnM0OnRleHRncmlkVXJpPgogICAgICAgICAgICAgICAgPG5zNDpyZXZpc2lvbj4wPC9uczQ6cmV2aXNpb24+CiAgICAgICAgICAgICAgICA8bnM0OnBpZCBwaWRUeXBlPSJoYW5kbGUiPmhkbDoyMS5UMTE5OTEvMDAwMC0wMDFBLTcyOEEtODwvbnM0OnBpZD4KICAgICAgICAgICAgICAgIDxuczQ6cGlkIHBpZFR5cGU9ImhhbmRsZSI+aGRsOjIxLlQxMTk5MS8wMDAwLTAwMUEtNzQzOC0zPC9uczQ6cGlkPgogICAgICAgICAgICAgICAgPG5zNDpleHRlbnQ+MzQxNDwvbnM0OmV4dGVudD4KICAgICAgICAgICAgICAgIDxuczQ6Zml4aXR5PgogICAgICAgICAgICAgICAgICAgIDxuczQ6bWVzc2FnZURpZ2VzdEFsZ29yaXRobT5tZDU8L25zNDptZXNzYWdlRGlnZXN0QWxnb3JpdGhtPgogICAgICAgICAgICAgICAgICAgIDxuczQ6bWVzc2FnZURpZ2VzdD5iYTk1NmFjMTA2YzIwNGMwNGQ5NDdmNjEwODUyY2RlNDwvbnM0Om1lc3NhZ2VEaWdlc3Q+CiAgICAgICAgICAgICAgICAgICAgPG5zNDptZXNzYWdlRGlnZXN0T3JpZ2luYXRvcj5jcnVkLWJhc2UgMTAuMi4xMC1FUzYtU05BUFNIT1Q8L25zNDptZXNzYWdlRGlnZXN0T3JpZ2luYXRvcj4KICAgICAgICAgICAgICAgIDwvbnM0OmZpeGl0eT4KICAgICAgICAgICAgICAgIDxuczQ6ZGF0YUNvbnRyaWJ1dG9yPjhmODNlNzAxYjYwNzRlNWE2YjFmZjFkYjM0M2MzMDRlZjkxNDc4M2JiNWQyZWE0ZjgyNjljMTZmOTFhYjAzNDRAZGFyaWFoLmV1PC9uczQ6ZGF0YUNvbnRyaWJ1dG9yPgogICAgICAgICAgICAgICAgPG5zNDpwcm9qZWN0IGlkPSJUR1BSLTU5NzIyZTI5LWVmY2ItMzMwZi1iOWI0LTVlZjJmYjA4ZWRhYiI+RUxUZUMgVGVzdDwvbnM0OnByb2plY3Q+CiAgICAgICAgICAgICAgICA8bnM0OmF2YWlsYWJpbGl0eT5wdWJsaWM8L25zNDphdmFpbGFiaWxpdHk+CiAgICAgICAgICAgIDwvbnM0OmdlbmVyYXRlZD4KICAgICAgICA8L25zNDpnZW5lcmljPgogICAgICAgIDxuczQ6aXRlbT4KICAgICAgICAgICAgPG5zNDpyaWdodHNIb2xkZXIgaWQ9Imh0dHBzOi8vZGlzdGFudHJlYWRpbmcuZ2l0aHViLmlvLyI+RUxUZUMgY29udmVyc2lvbjwvbnM0OnJpZ2h0c0hvbGRlcj4KICAgICAgICA8L25zNDppdGVtPgogICAgPC9uczQ6b2JqZWN0Pgo8L21ldGFkYXRhQ29udGFpbmVyVHlwZT4K\",\"created\":\"2020-07-01T09:40:52.390+02:00\",\"format\":\"text/markdown\",\"project\":{\"id\":\"TGPR-59722e29-efcb-330f-b9b4-5ef2fb08edab\",\"value\":\"ELTeC Test\"},\"xmlns:ns3\":\"http://textgrid.info/namespaces/metadata/core/2010\",\"pid\":[{\"pidType\":\"handle\",\"value\":\"hdl:21.T11991/0000-001A-728A-8\"},{\"pidType\":\"handle\",\"value\":\"hdl:21.T11991/0000-001A-7438-3\"}],\"availability\":\"public\",\"title\":\"ELTeC Test.md\",\"textgridUri\":\"textgrid:3thxs.0\",\"dataContributor\":\"8f83e701b6074e5a6b1ff1db343c304ef914783bb5d2ea4f8269c16f91ab0344@dariah.eu\",\"revision\":0,\"baseUri\":\"textgrid:3thxs\",\"fixity\":{\"messageDigestAlgorithm\":\"md5\",\"messageDigest\":\"ba956ac106c204c04d947f610852cde4\"},\"lastModified\":\"2020-07-01T09:40:52.390+02:00\",\"issued\":\"2020-07-01T09:40:52.390+02:00\"}";
    JSONObject o = new JSONObject(jsonMetadata);

    String oMetadataString = o.getString("original_metadata");
    System.out.println(oMetadataString);

    byte decoded[] = Base64.getDecoder().decode(oMetadataString);
    MetadataContainerType t =
        JAXB.unmarshal(new ByteArrayInputStream(decoded), MetadataContainerType.class);

    JAXB.marshal(t, System.out);
  }

}
