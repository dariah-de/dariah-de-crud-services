/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceStorageElasticSearchImpl;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceStorageSesameImpl;

/**
 * <p>
 * Test the service storage Sesame implementation.
 * </p>
 */
public class TestTGCrudServiceStorageElasticSearchImpl {

  /**
   * @throws ParseException
   * @throws IoFault
   * @throws IOException
   */
  @Test
  public void testEmptySPO() throws ParseException {

    String triple = "<urg:a> <arg:b> <aua:c> .";
    boolean test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (!test) {
      assertFalse("Triple " + triple + " should be valid!", true);
    }

    triple =
        "<textgrid:41vjb.0> <http://textgrid.info/relation-ns#inProject> <textgrid:project:TGPR-027dd56b-9666-7747-6bba-5b3ce8414cc7> .";
    test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (!test) {
      assertFalse("Triple " + triple + " is valid, but should not be!", true);
    }

    triple = "<urg:a> <arg:b> <> .";
    test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (test) {
      assertFalse("Triple " + triple + " is valid, but should not be!", true);
    }

    triple = "<urg:a> <> <aua:c> .";
    test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (test) {
      assertFalse("Triple " + triple + " is valid, but should not be!", true);
    }

    triple = "<> <arg:b> <aua:c> .";
    test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (test) {
      assertFalse("Triple " + triple + " is valid, but should not be!", true);
    }

    triple = "<> <> <>";
    test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (test) {
      assertFalse("Triple " + triple + " is valid, but should not be!", true);
    }

    triple = "<> <>";
    test = TGCrudServiceStorageSesameImpl.validateN3(triple);
    if (test) {
      assertFalse("Triple " + triple + " is valid, but should not be!", true);
    }
  }

  /**
   * @throws MalformedURLException
   */
  @Test
  public void testCheckIndexName() throws MalformedURLException {

    URL idxdbServiceURL = new URL("http://searchindex-main:9200/textgrid-public");
    String expectedIndeName = "textgrid-public";
    String indexName = TGCrudServiceStorageElasticSearchImpl.getIndexName(idxdbServiceURL);
    assertTrue(expectedIndeName.equals(indexName));

    idxdbServiceURL = new URL("http://searchindex-main:9200/textgrid-public/");
    expectedIndeName = "textgrid-public";
    indexName = TGCrudServiceStorageElasticSearchImpl.getIndexName(idxdbServiceURL);
    assertTrue(expectedIndeName.equals(indexName));

    idxdbServiceURL = new URL("http://searchindex-main:9200/textgrid-public/krams/");
    expectedIndeName = "textgrid-public";
    indexName = TGCrudServiceStorageElasticSearchImpl.getIndexName(idxdbServiceURL);
    assertTrue(expectedIndeName.equals(indexName));

    idxdbServiceURL = new URL("http://searchindex-main:9200/textgrid-public/krams");
    expectedIndeName = "textgrid-public";
    indexName = TGCrudServiceStorageElasticSearchImpl.getIndexName(idxdbServiceURL);
    assertTrue(expectedIndeName.equals(indexName));

    idxdbServiceURL = new URL("http://searchindex-main:9200/textgrid-public/krams/kakki");
    expectedIndeName = "textgrid-public";
    indexName = TGCrudServiceStorageElasticSearchImpl.getIndexName(idxdbServiceURL);
    assertTrue(expectedIndeName.equals(indexName));

    idxdbServiceURL = new URL("http://searchindex-main:9200/textgrid-public/krams/kakki/");
    expectedIndeName = "textgrid-public";
    indexName = TGCrudServiceStorageElasticSearchImpl.getIndexName(idxdbServiceURL);
    assertTrue(expectedIndeName.equals(indexName));
  }

}
