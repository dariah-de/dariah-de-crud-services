/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceAai;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceConfigurator;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceUtilities;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudStorageAbs;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceUtilities;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.ValidationEvent;
import jakarta.xml.bind.util.ValidationEventCollector;

/**
 * 
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2024-08-19 - Funk - Add test for invalid RDF relation data.
 * 
 * 2024-04-03 - Funk - Add test for validateMetadata().
 * 
 * 2023-04-24 - Funk - Add test for RDF and about="" setting in RDF metadata (see #313).
 * 
 * 2021-06-21 - Funk - Add method for filename creation from metadata.
 * 
 * 2013-04-03 - Funk - Adapt to new IoFault from isBaseUri() method from Utilities class.
 * 
 * 2012-03-23 - Funk - Add tests for strip revisions on UUID URIs.
 * 
 * 2012-02-07 - Funk - Add tests for getRevision.
 * 
 * 2010-09-03 - Funk - First version.
 */

/**
 * <p>
 * JUnit class for testing the TG-crud service utilities.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-04-24
 * @since 2010-09-03
 */

public class TestTGCrudServiceUtilities {

  // **
  // STATIC FINALS
  // **

  private static final String META_FILE = "test-item.meta.xml";
  private static final String HEIDI_EDITION_META_FILE = "heidi.edition.meta.xml";
  private static final String ARCHITRAVE_COLLECTION_META_FILE = "architrave.collection.meta.xml";
  private static final String HEIDI_AGGREGATION_META_FILE = "heidi.aggregation.meta.xml";
  private static final String HEIDI_WORK_META_FILE = "heidi.work.meta.xml";
  private static final String HEIDI_META_FILE = "heidi.meta.xml";
  private static final String RDF_METADATA_FILE_ABOUT_EMPTY = "por032_about_empty.work.meta.xml";
  private static final String RDF_NTRIPLES_FILE_ABOUT_EMPTY = "por032_about_empty.work.meta.xml.nt";
  private static final String RDF_METADATA_FILE_ABOUT_FILLED = "por032_about_filled.work.meta.xml";
  private static final String RDF_NTRIPLES_FILE_ABOUT_FILLED =
      "por032_about_filled.work.meta.xml.nt";
  private static final String OBJECT_METADATA_ONLY = "test-file-object-metadata.meta.xml";
  private static final String NO_GENERIC = "test-file-eltec-nogeneric.meta.xml";
  private static final String NO_PROVIDED = "test-file-eltec-noprovided.meta.xml";
  private static final String NO_TITLE = "test-file-eltec-notitle.meta.xml";
  private static final String NO_FORMAT = "test-file-eltec-noformat.meta.xml";
  private static final String INVALID_AGENT = "test-file-eltec-invalid-agent-id.meta.xml";
  private static final String INVALID_DATE = "test-file-eltec-invalid-date.meta.xml";
  private static final String VALID_METADATA = "test-file-eltec-valid-metadata.meta.xml";
  private static final String INVALID_GENRE = "test-file-eltec-invalid-genre.meta.xml";
  private static final String MERKWUERDISCHE_METADATA = "no-metadata-file.meta.xml";

  // **
  // STATICS
  // **

  private static CrudServiceConfigurator conf;

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Get config file.
    String configFile =
        CrudServiceUtilities.getResource("tgcrud-junit.localfile.properties").toString();

    // Load the TG-crud configuration.
    conf = CrudServiceConfigurator.getInstance(configFile);
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Tests the generation of the metadata ObjectType.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testCreateGereratedType() throws IoFault {

    // Set some test values.
    long createdDate = System.currentTimeMillis();
    long lastModifiedDate = System.currentTimeMillis();
    URI uri = URI.create("my:test:uri");
    int revision = 0;
    String projectId = "TGPR1234";
    long size = 1234567890l;
    URI ownerId = URI.create("my.textgrid.account@textgrid.de");
    String projectName = "My new and funny test project";

    // Create the middleware metadata (ok, ok, the generated type :-)
    GeneratedType generated = TGCrudServiceUtilities.createGereratedType(createdDate,
        lastModifiedDate, uri, revision, projectId, size, ownerId, projectName);

    // Check the objectType's content.
    // TODO LOG!
    System.out.println("Checking createdDate:");
    System.out.println("\t" + generated.getCreated().toString() + " =");
    System.out.println("\t" + CrudServiceUtilities.getXMLGregorianCalendar(createdDate));

    if (!generated.getCreated().equals(CrudServiceUtilities.getXMLGregorianCalendar(createdDate))) {
      assertTrue(false);
    }

    // TODO LOG!
    System.out.println("Checking lastModifiedDate:");
    System.out.println("\t" + generated.getLastModified().toString() + " =");
    System.out.println("\t" + CrudServiceUtilities.getXMLGregorianCalendar(lastModifiedDate));

    if (!generated.getLastModified()
        .equals(CrudServiceUtilities.getXMLGregorianCalendar(lastModifiedDate))) {
      assertTrue(false);
    }

    // TODO LOG!
    System.out.println("Checking URI:");
    System.out.println("\t" + generated.getTextgridUri().getValue() + " =");
    System.out.println("\t" + uri);

    if (!generated.getTextgridUri().getValue().equals(uri)) {
      assertTrue(false);
    }

    // TODO LOG!
    System.out.println("Checking revision:");
    System.out.println("\t" + generated.getRevision() + " =");
    System.out.println("\t" + revision);

    if (generated.getRevision() != revision) {
      assertTrue(false);
    }

    // TODO LOG!
    System.out.println("Checking size:");
    System.out.println("\t" + generated.getExtent() + " =");
    System.out.println("\t" + size);

    if (!generated.getExtent().toString().equals(String.valueOf(size))) {
      assertTrue(false);
    }

    // TODO LOG!
    System.out.println("Checking owner ID:");
    System.out.println("\t" + generated.getDataContributor() + " =");
    System.out.println("\t" + ownerId);

    if (!generated.getDataContributor().equals(ownerId)) {
      assertTrue(false);
    }

    // TODO LOG!
    System.out.println("Checking permissions:");
    System.out.println("\t" + generated.getProject().getValue() + " =");
    System.out.println("\t" + projectName);

    if (!generated.getProject().getValue().equals(projectName)) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Tests THE GRID storage service instantiation.
   * </p>
   * 
   * @throws IoFault
   * @throws ConfigurationFault
   */
  @Test
  public void testGRIDStorageImplementation() throws IoFault {

    System.out.println(
        "Instantiating class: " + conf.getDATAsTORAGEiMPLEMENTATION());

    CrudStorageAbs<MetadataContainerType> gridStorageImplementation =
        TGCrudServiceUtilities.getDataStorageImplementation(conf);
    gridStorageImplementation.getConfiguration();
  }

  /**
   * <p>
   * Tests the URI service instantiation.
   * </p>
   * 
   * @throws ConfigurationFault
   * @throws IoFault
   */
  @Test
  public void testGetIdentifierImplementation() throws IoFault {

    System.out.println("Instantiating class: " + conf.getIDiMPLEMENTATION());

    CrudServiceIdentifier uriImplementation = CrudServiceUtilities.getIdImplementation(conf);
    uriImplementation.getConfiguration();
  }

  /**
   * <p>
   * Tests the AAI service instantiation.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testGetAaiImplementation() throws IoFault {

    System.out.println("Instantiating class: " + conf.getAAIiMPLEMENTATION());

    CrudServiceAai securityImplementation = CrudServiceUtilities.getAaiImplementation(conf);
    securityImplementation.getConfiguration();
  }

  /**
   * <p>
   * Tests the Index DB service instantiation.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testGetIndexStorageImplementation() throws IoFault {

    System.out.println("Instantiating class: " + conf.getIDXDBsTORAGEiMPLEMENTATION());

    CrudStorageAbs<MetadataContainerType> xmldbStorageImplementation =
        TGCrudServiceUtilities.getIndexStorageImplementation(conf);
    xmldbStorageImplementation.getConfiguration();
  }

  /**
   * <p>
   * Tests the RDFDB service instantiation.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testGetRdfStorageImplementation() throws IoFault {

    System.out.println("Instantiating class: " + conf.getRDFDBsTORAGEiMPLEMENTATION());

    CrudStorageAbs<MetadataContainerType> rdfdbStorageImplementation =
        TGCrudServiceUtilities.getRdfStorageImplementation(conf);
    rdfdbStorageImplementation.getConfiguration();
  }

  /**
   * <p>
   * Tests creating TextGrid metadata from a file, getting a byte array out of that metadata, and
   * finally getting metadata back from that byte array.
   * </p>
   * 
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   */
  @Test
  public void testMetadataTransformations()
      throws IOException, IoFault, MetadataParseFault {

    System.out.println("Creating TextGrid metadata container from metadata file:");

    // Get metadata container.
    MetadataContainerType container = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(META_FILE)));

    System.out.println(
        "\tTitle is '" + container.getObject().getGeneric().getProvided().getTitle().get(0) + "'.");
    System.out.println("Creating byte array out of metadata container:");

    // Get a byte array.
    byte b[] = TGCrudServiceUtilities.getByteArray(container, conf);

    System.out.println("\tByte array length is " + b.length + " bytes.");

    String metadataString = new String(b, conf.getDEFAULTeNCODING());

    System.out.println("Metadata string:");
    System.out.println(metadataString.trim());

    // Transforming back.
    System.out.println("Transforming metadata string back to metadata container:");

    container =
        TGCrudServiceUtilities.createMetadataContainer(new String(b, conf.getDEFAULTeNCODING()));

    System.out.println("\tTitle is still '"
        + container.getObject().getGeneric().getProvided().getTitle().get(0) + "'.");
  }

  /**
   * <p>
   * Tests if a revision number is correctly stripped from the TextGrid URI string.
   * </p>
   * 
   * TODO Tests should be done in the NOID URI implementation?
   */
  @Test
  public void testStripRevision() {

    URI baseUri = URI.create("textgrid:1234");
    List<URI> uris = new ArrayList<URI>();
    uris.add(URI.create(baseUri.toASCIIString()));
    uris.add(URI.create(baseUri.toASCIIString() + ".0"));
    uris.add(URI.create(baseUri.toASCIIString() + ".1"));
    uris.add(URI.create(baseUri.toASCIIString() + ".11652"));

    System.out.println("TextGrid URIs to strip: " + uris);

    for (URI u : uris) {
      try {
        URI strippedUri = TGCrudServiceUtilities.stripRevision(u);

        System.out.println("TextGrid URI '" + u.toASCIIString() + "' stripped to '"
            + strippedUri.toASCIIString() + "'");

        if (!strippedUri.equals(baseUri)) {
          System.out.println("Stripping error with TextGrid URI: " + u);
          assertTrue(false);
        }
      } catch (IoFault e) {
        System.out.println(e.getMessage());
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Tests if a revision number is correctly stripped from the UUID URI string.
   * </p>
   * 
   * TODO Tests should be done in the UUID URI implementation?
   */
  @Test
  public void testStripRevisionWithUuidUris() {

    URI baseUri = URI.create("textgrid:32f9a0b6-aacc-4ce4-9e79-3b105bfbe880");
    List<URI> uris = new ArrayList<URI>();
    uris.add(URI.create(baseUri.toASCIIString()));
    uris.add(URI.create(baseUri.toASCIIString() + ".0"));
    uris.add(URI.create(baseUri.toASCIIString() + ".1"));
    uris.add(URI.create(baseUri.toASCIIString() + ".11652"));

    System.out.println("UUID URIs to strip: " + uris);

    for (URI u : uris) {
      try {
        URI strippedUri = TGCrudServiceUtilities.stripRevision(u);

        System.out.println("UUID URI '" + u.toASCIIString() + "' stripped to '"
            + strippedUri.toASCIIString() + "'");

        if (!strippedUri.equals(baseUri)) {
          System.out.println("Stripping error with UUID URI: " + u);
          assertTrue(false);
        }
      } catch (IoFault e) {
        System.out.println(e.getMessage());
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Tests if a given URI is a base URI.
   * </p>
   */
  @Test
  public void testIsBaseUri() {

    // Check for NPE.
    try {
      TGCrudServiceUtilities.isBaseUri(null);
    } catch (IoFault e) {
      System.out.println("Null URI catched (" + e.getMessage() + ")");
    }

    // Create and check base URIs.
    List<URI> baseUris = new ArrayList<URI>();
    baseUris.add(URI.create("textgrid:1234"));
    baseUris.add(URI.create("textgrid:12345"));
    baseUris.add(URI.create("textgrid:asdf"));
    baseUris.add(URI.create("textgrid:qwertzuiopueh"));

    System.out.println("Base URIs checked for base URI status: " + baseUris);

    for (URI u : baseUris) {
      try {
        boolean isBaseUri = TGCrudServiceUtilities.isBaseUri(u);

        if (isBaseUri) {
          System.out.println("URI '" + u.toASCIIString() + "' is a base URI");
        } else {
          System.out.println("URI '" + u.toASCIIString() + "' is NOT a base URI, but should be!");
          assertTrue(false);
        }
      } catch (IoFault e) {
        System.out.println(e.getMessage());
        assertTrue(false);
      }
    }

    // Create and check revisionURIs.
    List<URI> uris = new ArrayList<URI>();
    uris.add(URI.create("textgrid:1234.1"));
    uris.add(URI.create("textgrid:12345.12"));
    uris.add(URI.create("textgrid:asdf.123"));
    uris.add(URI.create("textgrid:qwertzuiopueh.1234"));

    System.out.println("Revision URIs checked for base URI status: " + uris);

    for (URI u : uris) {
      try {
        boolean isBaseUri = TGCrudServiceUtilities.isBaseUri(u);

        if (!isBaseUri) {
          System.out.println("URI '" + u.toASCIIString() + "' is not a base URI");
        } else {
          System.out.println("URI '" + u.toASCIIString() + "' IS a base URI, but should be NOT!");
          assertTrue(false);
        }
      } catch (IoFault e) {
        System.out.println(e.getMessage());
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Tests if a revision number is correctly returned.
   * </p>
   */
  @Test
  public void testGetRevision() {

    URI baseUri = URI.create("textgrid:1234");
    List<Integer> revisions = new ArrayList<Integer>(Arrays.asList(1, 6, 777, 198462, 0));

    // Get revision for revision URIs.
    for (int r : revisions) {
      URI uri =
          URI.create(baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + r);
      int revision = TGCrudServiceUtilities.getRevision(uri);

      System.out.println("URI '" + uri.toASCIIString() + "' has got revision number: " + revision);

      if (revision != r) {
        System.out
            .println("Got wrong revision " + revision + " for URI: " + uri + ", should be " + r);
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Tests getRevision using a baseURI.
   * </p>
   */
  @Test(expected = NumberFormatException.class)
  public void testGetRevisionWithBaseUri() throws NumberFormatException {

    URI baseUri = URI.create("textgrid:1234");

    System.out.println("Testing base URI for revision: " + baseUri);

    TGCrudServiceUtilities.getRevision(baseUri);
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testGetDataFilenameFromMetadataEDITION() throws FileNotFoundException, IOException {

    String expectedFilename = "Heidi_kann_brauchen_was_es_gelernt_hat.vqn0.0.edition";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(HEIDI_EDITION_META_FILE)));

    String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata);

    System.out.println(HEIDI_EDITION_META_FILE + " --> "
        + metadata.getObject().getGeneric().getProvided().getFormat() + " --> " + filename);

    if (!expectedFilename.equals(filename)) {
      System.out.println("'" + filename + "' !=");
      System.out.println("'" + expectedFilename + "'");
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws MetadataParseFault
   */
  @Test
  public void testGetDataFilenameFromMetadataCOLLECTION()
      throws FileNotFoundException, IOException, MetadataParseFault {

    String expectedFilename = "Collection_of_subsequent_added_scans.3rht4.0.collection";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(ARCHITRAVE_COLLECTION_META_FILE)));

    String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata);

    System.out.println(ARCHITRAVE_COLLECTION_META_FILE + " --> "
        + metadata.getObject().getGeneric().getProvided().getFormat() + " --> " + filename);

    if (!expectedFilename.equals(filename)) {
      System.out.println("'" + filename + "' !=");
      System.out.println("'" + expectedFilename + "'");
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testGetDataFilenameFromMetadataWORK() throws FileNotFoundException, IOException {

    String expectedFilename = "Heidi_kann_brauchen_was_es_gelernt_hat.vqmx.0.work";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(HEIDI_WORK_META_FILE)));

    System.out.println(metadata);

    String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata);

    System.out.println(HEIDI_WORK_META_FILE + " --> "
        + metadata.getObject().getGeneric().getProvided().getFormat() + " --> " + filename);

    if (!expectedFilename.equals(filename)) {
      System.out.println("'" + filename + "' !=");
      System.out.println("'" + expectedFilename + "'");
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testGetDataFilenameFromMetadataMETA() throws FileNotFoundException, IOException {

    String expectedFilename = "Heidi_kann_brauchen_was_es_gelernt_hat.vqmz.0.xml";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(HEIDI_META_FILE)));

    String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata);

    System.out.println(HEIDI_META_FILE + " --> "
        + metadata.getObject().getGeneric().getProvided().getFormat() + " --> " + filename);

    if (!expectedFilename.equals(filename)) {
      System.out.println("'" + filename + "' !=");
      System.out.println("'" + expectedFilename + "'");
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testGetDataFilenameFromMetadataAGGREGATION()
      throws FileNotFoundException, IOException {

    String expectedFilename = "Romane.vqn1.0.aggregation";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(HEIDI_AGGREGATION_META_FILE)));

    String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata);

    System.out.println(HEIDI_AGGREGATION_META_FILE + " --> "
        + metadata.getObject().getGeneric().getProvided().getFormat() + " --> " + filename);

    if (!expectedFilename.equals(filename)) {
      System.out.println("'" + filename + "' !=");
      System.out.println("'" + expectedFilename + "'");
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testGetListFromConfigString() {

    String val = "9202 9203";
    List<String> result = TGCrudServiceUtilities.getListFromConfigString(val);
    System.out.println(result);

    val = "9202";
    result = TGCrudServiceUtilities.getListFromConfigString(val);
    System.out.println(result);
  }

  /**
   * @throws IoFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   */
  @Test
  public void testAssembleRelationsForCreationAboutEmpty()
      throws IoFault, FileNotFoundException, IOException, ParseException {

    URI uri = URI.create("textgrid:1234.0");

    // Get expected model from file.
    String ntriples = IOUtils.readStringFromStream(
        new FileInputStream(CrudServiceUtilities.getResource(RDF_NTRIPLES_FILE_ABOUT_EMPTY)));
    Model expectedModel = RDFUtils.readModel(ntriples, RDFConstants.NTRIPLES);

    // Get model from metadata.
    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(RDF_METADATA_FILE_ABOUT_EMPTY)));
    // Assemble relations list!
    List<String> relationList = TGCrudServiceUtilities.assembleRelationsForCreation(uri,
        conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadata);
    // Transfer all relations to String.
    try (StringWriter relations = new StringWriter()) {
      for (String s : relationList) {
        relations.append(s);
      }
      Model model = RDFUtils.readModel(relations.toString(), RDFConstants.NTRIPLES);
      relations.close();

      // Compare models and test assertion!
      assertTrue(model.isIsomorphicWith(expectedModel));
    }
  }

  /**
   * @throws IoFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   */
  @Test
  public void testAssembleRelationsForCreationAboutFilled()
      throws IoFault, FileNotFoundException, IOException, ParseException {

    URI uri = URI.create("textgrid:1234.0");

    // Get expected model from file.
    String ntriples = IOUtils.readStringFromStream(
        new FileInputStream(CrudServiceUtilities.getResource(RDF_NTRIPLES_FILE_ABOUT_FILLED)));
    Model expectedModel = RDFUtils.readModel(ntriples, RDFConstants.NTRIPLES);

    // Get model from metadata.
    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(RDF_METADATA_FILE_ABOUT_FILLED)));
    // Assemble relations list!
    List<String> relationList = TGCrudServiceUtilities.assembleRelationsForCreation(uri,
        conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadata);
    // Transfer all relations to String.
    try (StringWriter relations = new StringWriter()) {
      for (String s : relationList) {
        relations.append(s);
      }
      Model model = RDFUtils.readModel(relations.toString(), RDFConstants.NTRIPLES);
      relations.close();

      // Compare models and test assertion!
      assertTrue(model.isIsomorphicWith(expectedModel));
    }
  }

  /**
   * <p>
   * Test invalid RDF input in relation metadata, see
   * https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/349, should throw an IoFault!
   * </p>
   * 
   * @throws IoFault
   */
  @Test(expected = IoFault.class)
  public void testAssembleRelationsForCreationIssue349() throws IoFault {

    // Get false metadata from string.
    // TODO Bring in using metadata file and crud client configuration!
    String metadata =
        """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010"
                xmlns:ns2="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                <object>
                    <generic>
                        <provided>
                            <title>1965_Interview_Manthey_1_S14</title>
                            <format>image/jpeg</format>
                            <notes/>
                        </provided>
                        <generated>
                            <created>2021-12-03T17:28:04.557+01:00</created>
                            <lastModified>2024-08-14T11:56:39.994Z</lastModified>
                            <textgridUri extRef="">textgrid:3rghf.1</textgridUri>
                            <revision>1</revision>
                            <extent>582737</extent>
                            <fixity>
                                <messageDigestAlgorithm>md5</messageDigestAlgorithm>
                                <messageDigest>45dce3d6cfe47c89cab759c67628119c</messageDigest>
                                <messageDigestOriginator>crud-base 11.3.0-TG-RELEASE</messageDigestOriginator>
                            </fixity>
                            <dataContributor>MaxZeterberg@dariah.eu</dataContributor>
                            <project id="TGPR-4d929bb8-db00-b972-da3a-58b452527262">KMG</project>
                            <permissions>read write delegate publish</permissions>
                        </generated>
                    </generic>
                    <item>
                        <rightsHolder id="https://d-nb.info/gnd/2020450-4" isCorporateBody="true"
                            >Georg-August-Universität Göttingen Stiftung Öffentlichen Rechts: Niedersächsische
                            Staats- und Universitätsbibliothek Göttingen</rightsHolder>
                        <rightsHolder id="" isCorporateBody="false">Manthey</rightsHolder>
                    </item>
                    <custom>
                        <ns1:templateEditor xmlns:ns1="http://textgrid.info/custom-namespace/2009-01-01"
                            xmlns:ns0="http://textgrid.info/namespaces/metadata/core/2010"
                            xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010"/>
                    </custom>
                    <relations>
                        <ns2:RDF>
                            <ns1:Description xmlns:ns1="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                                xmlns:ns0="http://textgrid.info/namespaces/metadata/core/2010"
                                xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010"
                                ns1:about="textgrid:3rghf.1">
                                <ns2:height xmlns:ns2="http://www.w3.org/2003/12/exif/ns#"
                                    ns1:datatype="http://www.w3.org/2001/XMLSchema#integer">644</ns2:height>
                                <ns2:width xmlns:ns2="http://www.w3.org/2003/12/exif/ns#"
                                    ns1:datatype="http://www.w3.org/2001/XMLSchema#integer">864</ns2:width>
                            </ns1:Description>
                        </ns2:RDF>
                    </relations>
                </object>
            </MetadataContainerType>
            """;

    MetadataContainerType metadataType =
        JAXB.unmarshal(new StringReader(metadata), MetadataContainerType.class);

    // Assemble relations list!
    TGCrudServiceUtilities.assembleRelationsForCreation(
        metadataType.getObject().getGeneric().getGenerated().getTextgridUri().getValue(),
        conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadataType);
  }

  /**
   * <p>
   * Validate RDF input in relation metadata, see
   * https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/349
   * </p>
   * 
   * @throws IoFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   */
  @Test
  public void textCheckRelationRDFMetadata()
      throws IoFault, FileNotFoundException, IOException, ParseException {

    // Get false metadata from string.
    // TODO Bring in using metadata file and crud client configuration!
    String metadata =
        """
            <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <MetadataContainerType xmlns="http://textgrid.info/namespaces/metadata/core/2010"
                xmlns:ns2="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                <object>
                    <generic>
                        <provided>
                            <title>1965_Interview_Manthey_1_S14</title>
                            <format>image/jpeg</format>
                            <notes/>
                        </provided>
                        <generated>
                            <created>2021-12-03T17:28:04.557+01:00</created>
                            <lastModified>2024-08-14T11:56:39.994Z</lastModified>
                            <textgridUri extRef="">textgrid:3rghf.1</textgridUri>
                            <revision>1</revision>
                            <extent>582737</extent>
                            <fixity>
                                <messageDigestAlgorithm>md5</messageDigestAlgorithm>
                                <messageDigest>45dce3d6cfe47c89cab759c67628119c</messageDigest>
                                <messageDigestOriginator>crud-base 11.3.0-TG-RELEASE</messageDigestOriginator>
                            </fixity>
                            <dataContributor>MaxZeterberg@dariah.eu</dataContributor>
                            <project id="TGPR-4d929bb8-db00-b972-da3a-58b452527262">KMG</project>
                            <permissions>read write delegate publish</permissions>
                        </generated>
                    </generic>
                    <item>
                        <rightsHolder id="https://d-nb.info/gnd/2020450-4" isCorporateBody="true"
                            >Georg-August-Universität Göttingen Stiftung Öffentlichen Rechts: Niedersächsische
                            Staats- und Universitätsbibliothek Göttingen</rightsHolder>
                        <rightsHolder id="" isCorporateBody="false">Manthey</rightsHolder>
                    </item>
                    <custom>
                        <ns1:templateEditor xmlns:ns1="http://textgrid.info/custom-namespace/2009-01-01"
                            xmlns:ns0="http://textgrid.info/namespaces/metadata/core/2010"
                            xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010"/>
                    </custom>
                    <relations>
                        <ns2:RDF>
                            <ns1:Description xmlns:ns1="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                                xmlns:ns0="http://textgrid.info/namespaces/metadata/core/2010"
                                xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010"
                                ns1:about="textgrid:3rghf.1">
                                <ns2:height xmlns:ns2="http://www.w3.org/2003/12/exif/ns#"
                                    ns1:datatype="http://www.w3.org/2001/XMLSchema#integer">644</ns2:height>
                                <ns2:width xmlns:ns2="http://www.w3.org/2003/12/exif/ns#"
                                    ns1:datatype="http://www.w3.org/2001/XMLSchema#integer">864</ns2:width>
                            </ns1:Description>
                        </ns2:RDF>
                    </relations>
                </object>
            </MetadataContainerType>
            """;
    String expectedWarningString =
        """
            Error validating relations RDF due to a org.apache.jena.riot.RiotException (EXIF information will be rewritten!): [line: 4, col: 129] {E201} The attributes on this property element, are not permitted with any content; expecting end element tag. in <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
            <RDF xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                <ns1:Description ns2:about="textgrid:3rghf.1" xmlns="http://textgrid.info/namespaces/metadata/core/2010" xmlns:ns0="http://textgrid.info/namespaces/metadata/core/2010" xmlns:ns2="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:ns1="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010">
                    <ns2:height ns2:datatype="http://www.w3.org/2001/XMLSchema#integer" xmlns:ns2="http://www.w3.org/2003/12/exif/ns#">644</ns2:height>
                    <ns2:width ns2:datatype="http://www.w3.org/2001/XMLSchema#integer" xmlns:ns2="http://www.w3.org/2003/12/exif/ns#">864</ns2:width>
                            </ns1:Description>
            </RDF>
            """;

    MetadataContainerType metadataType =
        JAXB.unmarshal(new StringReader(metadata), MetadataContainerType.class);

    // Validate metadata!
    List<Warning> warnings = TGCrudServiceUtilities.checkRelationRDFMetadata(metadataType);
    assertFalse(warnings.size() != 1);
    assertTrue(expectedWarningString.trim().equals(warnings.get(0).getValue().trim()));

    System.out.println("Got " + warnings.size() + " warning: " + warnings.get(0).getValue());
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test(expected = MetadataParseFault.class)
  public void testCheckMetadataObjectOnly() throws MetadataParseFault, IOException {
    MetadataContainerType objectMetadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(OBJECT_METADATA_ONLY)));
    TGCrudServiceUtilities.checkMetadata(objectMetadata);
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test(expected = MetadataParseFault.class)
  public void testCheckMetadataNoTitle() throws MetadataParseFault, IOException {
    MetadataContainerType metadata = TGCrudServiceUtilities
        .createMetadataContainer(new FileInputStream(CrudServiceUtilities.getResource(NO_TITLE)));
    TGCrudServiceUtilities.checkMetadata(metadata);
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test(expected = MetadataParseFault.class)
  public void testCheckMetadataNoFormat() throws MetadataParseFault, IOException {
    MetadataContainerType metadata = TGCrudServiceUtilities
        .createMetadataContainer(new FileInputStream(CrudServiceUtilities.getResource(NO_FORMAT)));
    TGCrudServiceUtilities.checkMetadata(metadata);
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test(expected = MetadataParseFault.class)
  public void testCheckMetadataNoGeneric() throws MetadataParseFault, IOException {
    MetadataContainerType metadata = TGCrudServiceUtilities
        .createMetadataContainer(new FileInputStream(CrudServiceUtilities.getResource(NO_GENERIC)));
    TGCrudServiceUtilities.checkMetadata(metadata);
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test(expected = MetadataParseFault.class)
  public void testCheckMetadataNoProvided() throws MetadataParseFault, IOException {
    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(NO_PROVIDED)));
    TGCrudServiceUtilities.checkMetadata(metadata);
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test
  public void testCheckMetadataInvalidDate() throws MetadataParseFault, IOException {

    String expectedWarningString =
        "metadata validation issue: jakarta.xml.bind.UnmarshalException: cvc-datatype-valid.1.2.3: '1876-1878' is not a valid value of union type 'isoDate'.";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(INVALID_DATE)));
    List<Warning> warnings = TGCrudServiceUtilities.checkMetadata(metadata);

    showWarnings(warnings);

    assertTrue(
        !warnings.isEmpty() && warnings.get(0).getValue().trim().equals(expectedWarningString));
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test
  @Ignore // Ignore as long as JAXB ignores some validation errors, such as invalid agent IDs like
  // id="viaf:54272906 wikidata:Q2975144" already on client side marshaling!
  public void testCheckMetadataInvalidAgentID() throws MetadataParseFault, IOException {

    String expectedWarningString = "";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(INVALID_AGENT)));
    List<Warning> warnings = TGCrudServiceUtilities.checkMetadata(metadata);

    showWarnings(warnings);

    assertTrue(
        !warnings.isEmpty() && warnings.get(0).getValue().trim().equals(expectedWarningString));
  }

  /**
   * @throws FileNotFoundException
   * @throws MetadataParseFault
   * @throws IOException
   * @throws SAXException
   * @throws JAXBException
   */
  @Test
  public void testCheckEnhancedJAXBMetadataUnmarshallingValidationAgent()
      throws FileNotFoundException, IOException, MetadataParseFault, SAXException, JAXBException {

    // NOTE This is only working on client side with a metadata string. JAXB client side marshaling
    // takes out the agent ID already! :-(

    // Set schema and context.
    SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
    Schema tgSchema =
        sf.newSchema(new URL("https://textgridlab.org/schema/textgrid-metadata_2010.xsd"));
    JAXBContext tgContext = JAXBContext.newInstance(ObjectType.class);

    // Create Unmarshaller and set TextGrid schema.
    Unmarshaller tgUnmarshaller = tgContext.createUnmarshaller();
    tgUnmarshaller.setSchema(tgSchema);

    ValidationEventCollector validationCollector = new ValidationEventCollector();
    tgUnmarshaller.setEventHandler(validationCollector);

    tgUnmarshaller.unmarshal(CrudServiceUtilities.getResource(INVALID_AGENT));

    if (validationCollector.hasEvents()) {
      for (ValidationEvent event : validationCollector.getEvents()) {
        String msg = event.getMessage();
        System.out.println(msg);
      }
    }
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test
  public void testCheckEltecMetadataValid() throws MetadataParseFault, IOException {
    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(VALID_METADATA)));
    TGCrudServiceUtilities.checkMetadata(metadata);
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test
  public void testCheckEltecInvalidGenre() throws MetadataParseFault, IOException {

    String expectedWarningString =
        "metadata validation issue: jakarta.xml.bind.UnmarshalException: cvc-enumeration-valid: Value 'fugu' is not facet-valid with respect to enumeration '[drama, prose, verse, reference work, non-fiction, non-text, other]'. It must be a value from the enumeration.";

    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(INVALID_GENRE)));
    List<Warning> warnings = TGCrudServiceUtilities.checkMetadata(metadata);

    showWarnings(warnings);

    assertTrue(
        !warnings.isEmpty() && warnings.get(0).getValue().trim().equals(expectedWarningString));
  }

  /**
   * @throws MetadataParseFault
   * @throws IOException
   */
  @Test(expected = MetadataParseFault.class)
  public void testCheckMerkwuerdischeMetadata() throws MetadataParseFault, IOException {
    MetadataContainerType metadata = TGCrudServiceUtilities.createMetadataContainer(
        new FileInputStream(CrudServiceUtilities.getResource(MERKWUERDISCHE_METADATA)));
    TGCrudServiceUtilities.checkMetadata(metadata);
  }

  // **
  // PRIVATE
  // **

  /**
   * @param theWarnings
   */
  private static void showWarnings(List<Warning> theWarnings) {
    for (Warning w : theWarnings) {
      System.out.println(w.getValue());
    }
  }

}
