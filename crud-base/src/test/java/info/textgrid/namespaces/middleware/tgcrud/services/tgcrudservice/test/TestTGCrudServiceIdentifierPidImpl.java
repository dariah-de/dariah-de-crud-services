/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license Lesser General Public License v3 ( http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertEquals;
import java.net.URI;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceIdentifierPidImpl;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2024-06-28 - Funk - First version.
 **/

/***
 * <p>
 * JUnit class for testing the TG-crud HDL metadata extraction.
 * </p>
 **/

public class TestTGCrudServiceIdentifierPidImpl {

  // **
  // TESTS
  // **

  /**
   * 
   */
  @Test
  public void testFixHandlePrefix() {

    URI uri = URI.create("hdl:11858/00-1734-0000-0005-1424-B");
    String expectedUri = "21.11113/00-1734-0000-0005-1424-B";
    String result = TGCrudServiceIdentifierPidImpl.fixHandlePrefix(uri);
    assertEquals(result, expectedUri);

    uri = URI.create("11858/00-1734-0000-0005-1424-B");
    expectedUri = "21.11113/00-1734-0000-0005-1424-B";
    result = TGCrudServiceIdentifierPidImpl.fixHandlePrefix(uri);
    assertEquals(result, expectedUri);

    uri = URI.create("hdl:11378/0000-0006-9F65-6");
    expectedUri = "21.11113/0000-0006-9F65-6";
    result = TGCrudServiceIdentifierPidImpl.fixHandlePrefix(uri);
    assertEquals(result, expectedUri);

    uri = URI.create("11378/0000-0006-9F65-6");
    expectedUri = "21.11113/0000-0006-9F65-6";
    result = TGCrudServiceIdentifierPidImpl.fixHandlePrefix(uri);
    assertEquals(result, expectedUri);

    uri = URI.create("hdl:21.11113/0000-000F-F5FF-E");
    expectedUri = "21.11113/0000-000F-F5FF-E";
    result = TGCrudServiceIdentifierPidImpl.fixHandlePrefix(uri);
    assertEquals(result, expectedUri);

    uri = URI.create("21.11113/0000-000F-F5FF-E");
    expectedUri = "21.11113/0000-000F-F5FF-E";
    result = TGCrudServiceIdentifierPidImpl.fixHandlePrefix(uri);
    assertEquals(result, expectedUri);
  }

}
