/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertTrue;
import java.net.URI;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceUtilities;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceImpl;

/**
 * TODOLOG
 * 
 * TODO Add check for logging tests!
 * 
 **
 * CHANGELOG
 * 
 * 2022-02-10 - Funk - Add logging tests.
 * 
 * 2015-05-13 - Funk - First version.
 *
 */

/**
 * <p>
 * JUnit class for testing all the TG-crud REST service methods.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2019-12-20
 * @since 2015-05-13
 */

public class TestTGCrudServiceImpl {

  // **
  // STATICS
  // **

  private static TGCrudServiceImpl serviceInstance;

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    // Get service instance.
    String configFile =
        CrudServiceUtilities.getResource("tgcrud-junit.localfile.properties").toString();
    serviceInstance = new TGCrudServiceImpl(configFile);
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Tests the number for NOID method.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testN2N() throws IoFault {

    // Set parameters.
    URI uri = URI.create("textgrid:24krd.0");
    String expected = "24krd = 2*29^4 + 4*29^3 + 17*29^2 + 22*29^1 + 12*29^0 = 1527065";

    // Call n2n.
    String noid = serviceInstance.number4noidREST(uri);

    // Check.
    System.out.print(expected);
    if (noid.equals(expected)) {
      System.out.println(" == \n" + noid);
    } else {
      System.out.println(" != \n" + noid);
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Tests logging with N4N method.
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testLoggingWithN4N() throws IoFault {

    // Set parameters.
    URI uri = URI.create("textgrid:24krd.0");

    // Call n2n.
    serviceInstance.number4noidREST(uri);

    // TODO Check! Maybe using a file handler?
  }

  /**
   * <p>
   * Tests logging with getVersion().
   * </p>
   * 
   * @throws IoFault
   */
  @Test
  public void testLoggingWithVersionRequest() throws IoFault {
    serviceInstance.getVersion();

    // TODO Check! Maybe using a file handler?
  }

}
