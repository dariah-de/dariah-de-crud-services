/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license Lesser General Public License v3 ( http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertTrue;
import java.net.URI;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceConfigurator;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceUtilities;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceIdentifierUuidImpl;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2012-02-06 - Funk - Added locking tests.
 * 
 * 2011-05-05 - Funk - Changed to UUID generation implementation.
 * 
 * 2010-09-03 - Funk - First version.
 **/

/***
 * <p>
 * JUnit class for testing the TG-crud metadata URI generation.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2023-09-08
 * @since 2010-09-03
 **/

public class TestTGCrudServiceIdentifierUuidImpl {

  // **
  // FINALS
  // **

  private static final String TEST_USER = "test@tgcrud";

  // **
  // STATICS
  // **

  private static CrudServiceConfigurator conf;

  // **
  // SET UPS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    // Get config file.
    String configFile =
        CrudServiceUtilities.getResource("tgcrud-junit.localfile.properties").toString();

    // Load the TG-crud configuration.
    conf = CrudServiceConfigurator.getInstance(configFile);
  }

  /**
   * @throws java.lang.Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Tests the generation of URIs from TextGrid Metadata.
   * </p>
   * 
   * @throws IoFault
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testUuidUriGeneration() throws IoFault, DatatypeConfigurationException {

    // Create UUID URI instance.
    CrudServiceIdentifier uuidUriService = new TGCrudServiceIdentifierUuidImpl();
    uuidUriService.setConfiguration(conf);
    List<URI> result = uuidUriService.getUris(7);

    System.out.println("Creating UUID URIs:");
    System.out.println("\tUUID URI = " + result);
  }

  /**
   * @throws IoFault
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testUuidUriLockingAndUnlocking() throws IoFault, DatatypeConfigurationException {

    // Create UUID URI instance.
    CrudServiceIdentifier uuidUriService = new TGCrudServiceIdentifierUuidImpl();
    uuidUriService.setConfiguration(conf);
    List<URI> result = uuidUriService.getUris(1);

    // Now lock the URI created.
    URI uri = result.get(0);
    boolean success = uuidUriService.lock(uri, TEST_USER);
    if (success) {
      System.out.println("URI locked by " + TEST_USER + ": " + uri);
    } else {
      assertTrue(false);
    }

    // And again unlock it.
    uuidUriService.unlock(uri, TEST_USER);
    System.out.println("URI unlocked by " + TEST_USER + ": " + uri);
  }

  /**
   * @throws IoFault
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testUuidUriLockingTwice() throws IoFault, DatatypeConfigurationException {

    // Create UUID URI instance.
    CrudServiceIdentifier uuidUriService = new TGCrudServiceIdentifierUuidImpl();
    uuidUriService.setConfiguration(conf);
    List<URI> result = uuidUriService.getUris(1);

    // Now lock the URI created.
    URI uri = result.get(0);
    boolean success = uuidUriService.lock(uri, TEST_USER);
    if (success) {
      System.out.println("URI locked by " + TEST_USER + ": " + uri);
    } else {
      System.out.println("URI can not be locked: " + uri);
      assertTrue(false);
    }

    // And again lock the same URI.
    success = uuidUriService.lock(uri, TEST_USER);
    if (success) {
      System.out.println("URI should not be lockable again: " + uri);
      assertTrue(false);
    } else {
      System.out.println("URI can not be locked again: " + uri);
    }
  }

}
