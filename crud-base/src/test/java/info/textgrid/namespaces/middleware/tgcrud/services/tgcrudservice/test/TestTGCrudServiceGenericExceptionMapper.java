package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.test;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.cxf.helpers.IOUtils;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceGenericExceptionMapper;
import jakarta.ws.rs.core.Response.Status;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestTGCrudServiceGenericExceptionMapper {

  /**
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testCreatingXHTMLErrorPage() throws FileNotFoundException, IOException {

    String expectedErrorHTML = IOUtils
        .toString(new FileInputStream(new File("./src/test/resources/EXPECTED_ERROR_HTML.html")));

    TGCrudServiceGenericExceptionMapper tgMapper = new TGCrudServiceGenericExceptionMapper();

    String htmlFromMapper =
        tgMapper.prepareXHTMLMessage(Status.NOT_FOUND, "java.io.FileNotFoundException",
            "The request https://hdl.handle.net/api/handles/21.T11991/000-0011-B8C6-E?type=BAG for TextGrid Repository BagIt bag is not valid!",
            "tgcrud-base-9.0.7-DH+201809201218");

    if (!expectedErrorHTML.trim().equals(htmlFromMapper.trim())) {
      System.out.println("EXPECTED ERROR HTML:\n" + expectedErrorHTML);
      System.out.println("CREATED ERROR HTML:\n" + htmlFromMapper);
      assertTrue(false);
    }
  }

}
