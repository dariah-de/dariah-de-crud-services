/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import java.util.List;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 2013-12-11 - Funk - Removed public's for they are not needed anymore in interface method
 * declarations.
 * 
 * 2011-09-12 - Funk - Added checkRight() method.
 * 
 * 2011-08-10 - Funk - Added IoFaults to checkAccess() and resourceExists().
 * 
 * 2011-08-05 - Funk - Added setIsPublic() method.
 * 
 * 2010-09-04 - Funk - First version.
 * 
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * This security interface provides all issues concerning authentication and authorization.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2018-06-13
 * @since 2010-09-04
 ******************************************************************************/

public interface CrudServiceAai {

  String UNIT_NAME = "aai";
  String OPERATION_CREATE = "create";
  String OPERATION_DELEGATE = "delegate";
  String OPERATION_DELETE = "delete";
  String OPERATION_PUBLISH = "publish";
  String OPERATION_READ = "read";
  String OPERATION_WRITE = "write";

  String ROLE_ADMINISTRATOR = "Administrator";
  String ROLE_PROJECTMANAGER = "Projektleiter";
  String ROLE_EDITOR = "Bearbeiter";
  String ROLE_OBSERVER = "Beobachter";

  String RIGHT_GETURI = "getUri";
  String RIGHT_PUBLISH = "publish";

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theResource
   * @param theOperation
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   */
  void checkAccess(String theSessionId, String theLogParameter, String theResource,
      String theOperation) throws ObjectNotFoundFault, AuthFault, IoFault;

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theResource
   * @param theOperation
   * @return an object describing the object to be accessed
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   */
  Object checkAccessGetInfo(String theSessionId, String theLogParameter,
      String theResource, String theOperation)
      throws ObjectNotFoundFault, AuthFault, IoFault;

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theResource
   * @param theUri
   * @return a list of strings containing the permitted operations for the resource
   * @throws AuthFault
   * @throws IoFault
   */
  List<String> registerResource(String theSessionId, String theLogParameter,
      String theResource, URI theUri) throws AuthFault, IoFault;

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   */
  void unregisterResource(String theSessionId, String theLogParameter,
      URI theUri) throws AuthFault, ObjectNotFoundFault, IoFault;

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theResource
   * @return returns if a resource does exist or not
   * @throws AuthFault
   * @throws IoFault
   */
  boolean resourceExists(String theSessionId, String theLogParameter,
      String theResource) throws AuthFault, IoFault;

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @throws AuthFault
   * @throws IoFault
   */
  void publish(String theSessionId, String theLogParameter, URI theUri)
      throws AuthFault, IoFault;

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @throws AuthFault
   * @throws IoFault
   */
  void nearlyPublish(String theSessionId, String theLogParameter, URI theUri)
      throws AuthFault, IoFault;

  /**
   * <p>
   * Check if the user has the right to get URIs from the URI service.
   * </p>
   * 
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param theRight
   * @throws AuthFault
   */
  void checkRight(String theSessionId, String theLogParameter,
      String theProjectId, String theRight) throws AuthFault, IoFault;

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param theConfiguration
   */
  void setConfiguration(CrudServiceConfigurator theConfiguration);

  /**
   * @return the TextGrid configuration
   */
  CrudServiceConfigurator getConfiguration();

}
