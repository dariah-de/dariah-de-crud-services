/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * SUB Göttingen (https://sub.uni-goettingen.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @copyright SUB Göttingen (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.Charset;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2022-05-24 - Funk - Fix some SpotBug issues.
 *
 * 2022-02-07 - Funk - Using log4j2 now. Watching logging config file can be configured now in
 * logging config file :-D
 *
 * 2018-09-04 - Funk - Fixed threading problem: not watching config file again and again and again
 * and ... !
 *
 * 2016-11-25 - Funk - Slightly refactored message sending: If message producer session is not valid
 * anymore, connection is being reloaded once.
 *
 * 2016-09-21 - Funk - Deleting objects now also removes RDF and IDX database entries if object was
 * not found in tgauth!
 *
 * ...older change logs please look up in the GIT repository...
 *
 **/

/**
 * <p>
 * <b>The CrudServiceGeneric implementation super class</b>
 * </p>
 *
 * <p>
 * The class CrudServiceGeneric implements the service interface created by the CXF web service
 * implementation. This service is used to ingest, access, and delete TextGrid (and DARIAH) objects
 * - which actually have two components: a metadata file and a data file. It can to be used by the
 * TextGridLab software and by the TextGrid Services that are used by the TextGridLab, and of course
 * of the DARIAH repository and friends.
 * </p>
 *
 * <p>
 * All service methods can be used with the Java client classes, that are provided in the
 * middleware.tgcrud.clients.tgcrudclient folder, using the JAXB data binding, or using every client
 * to be build to serve the TG-crud's WSDL file, or using the tgcrud-client Maven module provided
 * with the parent tgcrud module. Also all methods can be used with RESTful access.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-01-17
 * @since 2007-10-30
 **/

public abstract class CrudService {

  // **
  // STATIC FINALS
  // **

  // Log levels.
  protected static final int ERROR = 1;
  protected static final int WARN = 2;
  protected static final int INFO = 3;
  protected static final int DEBUG = 4;

  // Overall error messages.
  protected static final String INTERNAL_SERVICE_ERROR = "CRUD INTERNAL SERVICE ERROR";

  protected static final String STREAM_RELEASE_COMPLETE = "#### STREAM RELEASE COMPLETE ####";
  protected static final String HOLDING_DATA_STREAM = "#### HOLDING THE DATA STREAM ####";

  // Public or not public?
  protected static final String PUBLIC = "PUBLIC";
  protected static final String NON_PUBLIC = "NON-PUBLIC";

  // Logger.
  private static final String CRUD_LOGGER_NAME = "crudLogger";
  private static final String CRUD_ROLLBACK_LOGGER_NAME = "rollbackLogger";
  private static final String CRUD_ROOT_LOGGER_NAME = "rootLogger";
  private static final String ARROWS = "----> ";

  // Miscellaneous message strings.
  private static final long SERVICE_START_TIME = System
      .currentTimeMillis();
  protected static final int WATCH_EVERY_FIVE_MINUTES = 300000;
  protected static final String URI_SEPARATOR = "...";
  protected static final String HANDLE_PREFIX = "hdl:";
  protected static final boolean LATEST_REVISION_UNFILTERED = true;
  protected static final boolean LATEST_REVISION_FILTERED = false;

  // **
  // STATICS
  // **

  // Implementation classes.
  protected static volatile CrudServiceAai aaiImplementation;
  protected static volatile CrudServiceIdentifier identifierImplementation;
  protected static volatile CrudStorage<MetadataContainerType> dataStorageImplementation;
  protected static volatile CrudStorage<MetadataContainerType> idxdbImplementation;
  protected static volatile CrudStorage<MetadataContainerType> rdfdbImplementation;

  // The TG-crud configuration class. Uses a Java properties file.
  protected static CrudServiceConfigurator conf = null;

  // The message producer.
  protected static volatile CrudServiceMessageProducer messageProducer = null;

  // **
  // CLASS
  // **

  protected String configFileLocation = null;

  // **
  // LOGGING
  // **

  /**
   * <p>
   * Handles all the logging without a given Exception.
   * </p>
   *
   * @param theLogLevel
   * @param theInfo
   * @param theLogMessage
   */
  protected static void log(int theLogLevel, CrudServiceMethodInfo theInfo, String theLogMessage) {
    CrudServiceUtilities.serviceLog(theLogLevel, theInfo.getName(), theLogMessage);
  }

  // **
  // INITIALIZATION
  // **

  /**
   * <p>
   * Initialises the service instance.
   * </p>
   *
   * TODO Check if we really need the conf.hasChanged() trigger. Must we REALLY change configuration
   * DURING runtime??
   * 
   * TODO Do we really need to configure CRUD for every method call?
   *
   * @param theInfo
   * @throws IoFault
   */
  protected void init(CrudServiceMethodInfo theInfo, CrudServiceVersion theVersion) throws IoFault {

    // Get crud configuration.
    try {
      setConfiguration(CrudServiceConfigurator.getInstance(this.configFileLocation));
    } catch (FileNotFoundException e) {
      throw CrudServiceExceptions
          .ioFault("CRUD properties file not found: " + this.configFileLocation
              + "! Please configure in beans.xml!");
    } catch (IOException e) {
      throw CrudServiceExceptions
          .ioFault("IOException while configuring CRUD service: " + e.getMessage());
    }

    // Get crud loggers.
    if (CrudServiceUtilities.crudLogger == null && CrudServiceUtilities.crudRollbackLogger == null
        && CrudServiceUtilities.rootLogger == null) {

      // Check for logging config file.
      String logfigFileLocation = conf.getLOG4JcONFIGFILE();
      try {
        if (logfigFileLocation != null && !logfigFileLocation.equals("")) {
          File loggConfigFile = new File(logfigFileLocation);

          System.out.println(ARROWS + "CRUD config file locations: ["
              + new File(this.configFileLocation).getAbsolutePath() + ", "
              + loggConfigFile.getAbsolutePath() + "]");
          System.out.println(ARROWS + "CRUD logger names: [" + CRUD_ROOT_LOGGER_NAME + ", "
              + CRUD_LOGGER_NAME + ", " + CRUD_ROLLBACK_LOGGER_NAME + "]");

          // How do I reconfigure log4j2 in code with a specific configuration file?
          // "Be aware that this LoggerContext class is not part of the public API so your code may
          // break with any minor release!"
          // <https://logging.apache.org/log4j/2.x/faq.html#config_location>
          LoggerContext context =
              (org.apache.logging.log4j.core.LoggerContext) LogManager.getContext(false);
          // Set the log4j2 config properties from configuration file given in the tgcrud.properties
          // file. Forces reconfiguration.
          context.setConfigLocation(loggConfigFile.toURI());

          // Create and configure loggers.
          CrudServiceUtilities.crudLogger = LogManager.getLogger(CRUD_LOGGER_NAME);
          CrudServiceUtilities.crudRollbackLogger = LogManager.getLogger(CRUD_ROLLBACK_LOGGER_NAME);
          CrudServiceUtilities.rootLogger = LogManager.getLogger(CRUD_ROOT_LOGGER_NAME);

          log(INFO, theInfo, "CRUD logging up and running!");

        }
      } catch (SecurityException e) {
        throw CrudServiceExceptions
            .ioFault("CRUD logging configuration file not found: " + logfigFileLocation);
      }
    }

    // Now we can log.
    log(INFO, theInfo, CrudServiceUtilities.startMethodLog(theInfo, theVersion));
    String message = "";
    if (conf.hasChanged()) {
      message = " (reloaded due to service restart or config file changes)";
    }
    log(DEBUG, theInfo,
        "CRUD configuration file: " + this.configFileLocation + message);

    // Check default charset.
    log(DEBUG, theInfo, "System's default charset is " + Charset.defaultCharset().displayName());

    // Initialise MessageProducer once.
    initMessageProducer(theInfo);

    log(INFO, theInfo, "CRUD service configuration complete");
  }

  /**
   * <p>
   * Just for runtime exception logging.
   * </p>
   *
   * @param theException
   * @param theMethodInfo
   */
  protected static synchronized void printExceptionInfos(RuntimeException theException,
      CrudServiceMethodInfo theMethodInfo) {

    theException.printStackTrace();
    log(ERROR, theMethodInfo,
        "MESSAGE: " + (theException.getMessage() == null ? "NOPE" : theException.getMessage()));
    log(ERROR, theMethodInfo, "CAUSE: "
        + (theException.getCause() == null ? "NOPE" : theException.getCause().getMessage()));
    PrintWriter printWriter = new PrintWriter(new StringWriter());
    theException.printStackTrace(printWriter);
    String s = "";
    printWriter.write(s);
    log(ERROR, theMethodInfo, "STACKTRACE: " + (s.equals("") ? "NOPE" : s));
  }

  /**
   * @param theInfo
   * @throws IoFault
   */
  protected void initMessageProducer(CrudServiceMethodInfo theInfo) throws IoFault {

    // Initialise MessageProducer once.
    if (conf.getUSEmESSAGING()) {
      if (messageProducer == null || conf.hasChanged()) {
        log(INFO, theInfo, "Initializing new crud message producer");
        messageProducer = new CrudServiceMessageProducer(conf);
      } else {
        log(DEBUG, theInfo, "Using existing crud message producer");
      }
    } else {
      log(INFO, theInfo, "Messaging is disabled");
    }
  }

  /**
   * @throws IoFault
   */
  protected abstract void initImplementationClasses() throws IoFault;

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param conf
   */
  public static void setConfiguration(CrudServiceConfigurator conf) {
    CrudService.conf = conf;
  }

  /**
   * @return The configurator object.
   */
  public static CrudServiceConfigurator getConfiguration() {
    return conf;
  }

  /**
   * @return The service start time.
   */
  public static Long getServiceStartTime() {
    return SERVICE_START_TIME;
  }

}
