/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.adaptormanager.AdaptorManager;
import jakarta.activation.DataHandler;
import jakarta.activation.MimeTypeParseException;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.xml.bind.JAXB;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2024-06-05 - Funk - Fix #342 --> add aggregation data to result list, not write directly to
 * RDFDB, will be written along with the other relations in TGCrudServiceImpl.update().
 *
 * 2022-11-22 - Funk - Fix #302 --> Do not delete existing relations type!
 * 
 * 2021-06-25 - Funk - Fix #35593 and #31709 --> EXIF and root local part extraction.
 *
 * 2020-04-06 - Funk - Using new (and much faster) EXIF extraction method now.
 *
 * 2014-12-22 - Funk - Removed extra structure handling, messaging is taking care of that now!
 *
 * 2014-12-04 - Funk - Adapted to empty string response of registerXsd() method.
 *
 * 2013-10-29 - Funk - Corrected some Sonar issues.
 *
 * 2013-10-15 - Funk - Added EXIF and DC namespace prefix handling for RDF model.
 *
 * 2013-10-09 - Funk - Added Adaptor Manager access for EXIF image metadata extraction.
 *
 * 2013-09-23 - Funk - Some name refactoring.
 *
 * 2012-04-25 - Funk - Removed slip streaming.
 *
 * 2012-03-07 - Funk - Using AdaptorManager with cloned streams now.
 *
 * 2012-02-01 - Funk - Adapted to the enhanced TextGridMetadata class.
 *
 * 2010-09-09 - Funk - First version.
 *
 */

/**
 * <p>
 * While the TG-crud takes care of all TextGrid metadata file changes (add Generated elements,
 * owners, etc.), the Adaptor Manager is used to work with the content of the TextGrid data files:
 * It is capable of extracting references to other TextGrid files from the current data file, check
 * and return namespaces, and finally is able to transform the given data file - if TEI/XML - into a
 * baseline encoded file stored to the XML database for explicit and overall search over the
 * TextGrid resources.
 * </p>
 *
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-06-25
 * @since 2010-09-09
 */

public class TGCrudServiceAdaptorManager {

  // **
  // STATIC FINALS
  // **

  private static final String METH = "go()";
  private static final String RILR_FAILED = "Registering image link relations failed";
  private static final String EXIF_FAILED = "Extracting EXIF image data failed";
  private static final String BT_FAILED = "Baseline transformation failed";
  private static final String READING_INTO_MODEL = "Reading into the Jena model";
  private static final String RELATION_TEXTGRID_NAMESPACE = "http://textgrid.info/relation-ns#";
  private static final String RELATION_EXIF_NAMESPACE = "http://www.w3.org/2003/12/exif/ns#";
  private static final String RELATION_DC_NAMESPACE = "http://purl.org/dc/elements/1.1/";
  private static final String ROOT_ELEMENT_NAMESPACE = "rootElementNamespace";
  private static final String ROOT_ELEMENT_LOCALPART = "rootElementLocalPart";
  private static final String TG_NAMESPACE_PREFIX = "tg";
  private static final String DC_NAMESPACE_PREFIX = "dc";
  private static final String EXIF_NAMESPACE_PREFIX = "exif";

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * Calls the Adaptor Manager if needed and does all the checking and storing. In this method some
   * warnings could be created that are returned.
   * </p>
   *
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @param theObject
   * @param theRdfStorage
   * @param theDataStorage
   * @param theIdxStorage
   * @param theSecurity
   * @param theConfiguration
   * @param theServiceMethod
   * @return A list of warnings to include in the metadata.
   * @throws IoFault
   */
  protected static List<String> go(String theSessionId, String theLogParameter, URI theUri,
      CrudObject<MetadataContainerType> theObject, CrudStorage<MetadataContainerType> theRdfStorage,
      CrudStorage<MetadataContainerType> theDataStorage,
      CrudStorage<MetadataContainerType> theIdxStorage, CrudServiceAai theSecurity,
      CrudServiceConfigurator theConfiguration, int theServiceMethod) throws IoFault {

    List<String> result = new ArrayList<String>();
    List<Warning> warningList = new ArrayList<Warning>();
    URI adaptorUri = null;

    //
    // Get the format of the file to adapt.
    //
    String format = theObject.getMetadata().getObject().getGeneric().getProvided().getFormat();

    CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
        "Adaptor Manager started! Detected mimetype " + format);

    //
    // -- I ---------------------------------------------------------------------------------------
    //
    // If an aggregation file is ingested (aggregation, edition or collection), put the
    // aggregations' RDF to result relations list.
    //
    // --------------------------------------------------------------------------------------------
    //
    if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {

      logRelations(theRdfStorage, theUri);

      // Add all aggregation data to the result relations list.
      try {
        Model aggregationData =
            RDFUtils.readModel(IOUtils.readStringFromStream(theObject.getData().getInputStream()),
                RDFConstants.RDF_XML);
        result.add(RDFUtils.getStringFromModel(aggregationData, RDFConstants.NTRIPLES));
      } catch (ParseException | IOException e) {
        addWarning("Extracting aggregation data failed", warningList, e);
      }

      CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
          "Added aggregation data to relations");
    }

    //
    // -- II --------------------------------------------------------------------------------------
    //
    // If an XSD file is ingested, call the AdaptorManager, get out the namespaces and put the
    // resulting relations into the RDF database.
    //
    // --------------------------------------------------------------------------------------------
    //
    else if (format.equals(TextGridMimetypes.XSD)) {

      // Add XSDs to relations string.
      try {
        String xsdNamespace =
            AdaptorManager.registerXSD(theObject.getData().getInputStream(), theUri);
        if (!xsdNamespace.equals("")) {
          result.add(xsdNamespace);

          CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
              "Added XML Schema definition to relations");
        }

      } catch (Exception e) {
        addWarning("Registering XSDs failed", warningList, e);
      }
    }

    //
    // -- III -------------------------------------------------------------------------------------
    //
    // If an Text Image Link Editor file is ingested, get out the relations to the image and
    // register the Image-Link-Editor relations to RDF database.
    //
    // --------------------------------------------------------------------------------------------
    //
    else if (format.equals(TextGridMimetypes.LINKEDITOR)) {

      // Add the link editor data to relations list.
      try {
        result.addAll(
            AdaptorManager.getImageLinkRelations(theObject.getData().getInputStream(), theUri));

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH, "RESULT: " + result);

        // Validate the URIs in the relations.
        Map<String, String> invalidUris = validateURIs(result);
        if (!invalidUris.isEmpty()) {
          for (String invalidUri : invalidUris.keySet()) {

            CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH,
                "KEY/VALUE: " + invalidUri + "/" + invalidUris.get(invalidUri));

            // Remove invalid relations.
            result.remove(invalidUris.get(invalidUri));
            addWarning(invalidUri + " in image link relation is not valid! Relation removed: "
                + invalidUris.get(invalidUri), warningList);
          }
        }

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH, "RESULT: " + result);

        CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
            "Added Text Image Link Editor relations");

      } catch (UnsupportedEncodingException e) {
        addWarning(RILR_FAILED, warningList, e);
      } catch (TransformerException e) {
        addWarning(RILR_FAILED, warningList, e);
      } catch (IOException e) {
        addWarning(RILR_FAILED, warningList, e);
      }
    }

    //
    // -- IV --------------------------------------------------------------------------------------
    //
    // If an image is ingested, do extract some mandatory EXIF metadata and put it into the
    // relations metadata as relations.RDF.Description.
    //
    // NOTE We do omit all existing RDF relations here FOR IMAGES ONLY, because for images there is
    // no need for using more RDF metadata at the moment. We may change this in the future!
    //
    // NOTE TWO: We MUST keep the TextGrid relations such as isDerivedFrom and
    // isAlternativeFormatOf!
    //
    // --------------------------------------------------------------------------------------------
    //
    else if (TextGridMimetypes.IMAGE_SET.contains(format)) {

      // Add all EXIF data we need to the relations metadata RDF type.
      try {
        // Extract the EXIF data with LTPUtils.
        List<String> exifData =
            LTPUtils.extractExifImageData(theObject.getData().getInputStream(), theUri, format);

        // NOTE No result.add() here, because we put the EXIF metadata into the RDF metadata type.
        // It will be added later on to the RDF database.

        // Metadata must be given here, too!
        if (theObject.getMetadata() == null || theObject.getMetadata().getObject() == null) {
          CrudServiceExceptions.ioFault("Metadata type missing");
        }

        // Create Jena RDF model for RDF metadata part, put prefixes into the model.
        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix(EXIF_NAMESPACE_PREFIX, RELATION_EXIF_NAMESPACE);
        model.setNsPrefix(DC_NAMESPACE_PREFIX, RELATION_DC_NAMESPACE);

        // Add relation type to metadata, if not yet existing. All existing RDF relations are
        // deleted here, because we see them as internal relations for TextGrid processing. Custom
        // RDF data must be located elsewhere...
        RelationType relations = theObject.getMetadata().getObject().getRelations();
        if (relations == null) {
          relations = new RelationType();
        }
        theObject.getMetadata().getObject().setRelations(relations);

        CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
            "Omit existing RDF relations metadata, re-creating EXIT metadata");

        // Create StringWriter.
        try (StringWriter exifWriter = new StringWriter()) {
          for (String r : exifData) {
            exifWriter.append(r);
          }
          String exifString = exifWriter.toString();

          // Read all EXIF triples into the model.
          CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH,
              READING_INTO_MODEL + " (EXIF triples): " + exifString);

          model.read(new StringReader(exifString), null, RDFConstants.NTRIPLES);
        }

        // Write model as RDF/XML into an RDF StringWriter.
        try (StringWriter rdfWriter = new StringWriter()) {
          model.write(rdfWriter, RDFConstants.RDF_XML, null);
          String rdfWriterString = rdfWriter.toString();
          rdfWriter.close();

          CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
              "Adding EXIF information to metadata");

          // Unmarshal and (re-)set RDFType!
          RdfType rdf = JAXB.unmarshal(new StringReader(rdfWriterString), RdfType.class);
          theObject.getMetadata().getObject().getRelations().setRDF(rdf);
        }
      } catch (IOException e) {
        addWarning(EXIF_FAILED, warningList, e);
      } catch (MimeTypeParseException e) {
        addWarning(EXIF_FAILED, warningList, e);
      }
    }

    //
    // -- V ---------------------------------------------------------------------------------------
    //
    // If an XML file is ingested, let the AdaptorManager extract the namespace and the root element
    // of the file and put it into the relations metadata as relations.RDF.Description.
    //
    //
    // NOTE Same here: We do omit all existing RDF part here, because only TG-crud is allowed to put
    // RDF data in here, and it is always written from scratch, if applicable!
    //
    // --------------------------------------------------------------------------------------------
    //
    else if (TextGridMimetypes.XML.equals(format)) {

      // Add XMLs namespace and root element to relations string.
      try {

        // Get QName from AdaptorManager, extract localPart and namespace, create triples.
        QName qname = AdaptorManager.getQNameFromXML(theObject.getData().getInputStream());
        List<String> qnameList = new ArrayList<String>();
        String qnameNS = "<" + theUri + "> <" + RELATION_TEXTGRID_NAMESPACE + ROOT_ELEMENT_NAMESPACE
            + "> <" + qname.getNamespaceURI() + "> .";
        String qnameLP = "<" + theUri + "> <" + RELATION_TEXTGRID_NAMESPACE + ROOT_ELEMENT_LOCALPART
            + "> \"" + qname.getLocalPart() + "\" .";

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH, "Namespace URI: " + qnameNS);
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH, "Localpart URI: " + qnameLP);

        qnameList.add(qnameNS);
        qnameList.add(qnameLP);

        // NOTE No result.add() here, because we put the EXIF metadata into the RDF metadata type.
        // It will be added later to the RDF database.

        // Metadata must be given here, too!
        if (theObject.getMetadata() == null || theObject.getMetadata().getObject() == null) {
          CrudServiceExceptions.ioFault("Metadata not omitted");
        }

        // Create Jena RDF model, put prefix into the model.
        Model model = ModelFactory.createDefaultModel();
        model.setNsPrefix(TG_NAMESPACE_PREFIX, RELATION_TEXTGRID_NAMESPACE);

        // Create and set new relation type here, if not existing yet (fixes #302)!
        if (theObject.getMetadata().getObject().getRelations() == null) {
          theObject.getMetadata().getObject().setRelations(new RelationType());
        }

        // Create StringWriter.
        try (StringWriter n3Writer = new StringWriter()) {
          for (String r : qnameList) {
            n3Writer.append(r);
          }
          String n3WriterString = n3Writer.toString();
          n3Writer.close();

          // Read the QName triples into the model.
          model.read(new StringReader(n3WriterString), null, RDFConstants.NTRIPLES);

          CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH,
              "NTRIPLES read into the Jena model: " + n3WriterString);
        }

        // Write model as RDF/XML into the StringWriter.
        try (StringWriter rdfWriter = new StringWriter()) {
          model.write(rdfWriter, RDFConstants.RDF_XML, null);
          String rdfWriterString = rdfWriter.toString();
          rdfWriter.close();

          // Unmarshal and set RDFType again.
          RdfType rdf = JAXB.unmarshal(new StringReader(rdfWriterString), RdfType.class);
          theObject.getMetadata().getObject().getRelations().setRDF(rdf);

          CrudServiceUtilities.serviceLog(CrudService.INFO, METH, "Added QName to relations");
        }
      } catch (Exception e) {
        addWarning("Extracting QName from XML data failed", warningList, e);
      }
    }

    //
    // -- VI --------------------------------------------------------------------------------------
    //
    // If an <hasAdaptor> tag is existing in the object's metadata, check if an adaptor URI is
    // existing, and do adapt.
    //
    // We assume that ALL adaptors used at this time will transform to the baseline encoding, so we
    // transform the XML object AND get out the refersTo relations!
    //
    // --------------------------------------------------------------------------------------------
    //
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH,
        "Checking for baseline adaptation");

    // Get the AdaptorManager relation out of the metadata.
    RelationType relations = theObject.getMetadata().getObject().getRelations();
    if (relations != null) {
      adaptorUri = theObject.getMetadata().getObject().getRelations().getHasAdaptor();
    }

    // If no adaptor URI is existing (anymore) in the TextGrid metadata, check for old baseline
    // entries in the XML database and delete them.
    if (adaptorUri == null || adaptorUri.toASCIIString().equals("")) {

      // Delete baseline data only if called from #UPDATE or #UPDATEMETADATA.
      if (theServiceMethod == CrudOperations.UPDATE
          || theServiceMethod == CrudOperations.UPDATEMETADATA) {
        try {
          theObject.setUri(theUri);
          theObject.setKindOfData(CrudObject.BASELINE_DATA);

          CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
              "No adaptor URI is given, deleting baseline data");

          theIdxStorage.delete(theObject);

        } catch (IoFault e) {
          CrudServiceUtilities.serviceLog(CrudService.INFO, METH, "Nothing to adapt to baseline");
        } catch (RelationsExistFault e) {
          // Not expected here.
          CrudServiceExceptions.ioFault(e, "Unexpected fault");
        }
      } else {
        CrudServiceUtilities.serviceLog(CrudService.INFO, METH, "Nothing to adapt to baseline");
      }
    }

    // If there is an adaptor URI, do adapt!
    else {

      CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
          "Baseline adaptation started with adaptor URI: " + adaptorUri);

      CrudObject<MetadataContainerType> adaptorObject = null;
      try {
        // Check access rights on the given URI. If the access is denied, put a warning into the
        // warning list.
        theSecurity.checkAccess(theSessionId, theLogParameter, adaptorUri.toString(),
            CrudServiceAai.OPERATION_READ);

        // Read the adaptor file FROM THE GRID.
        adaptorObject = theDataStorage.retrieve(adaptorUri);

        CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
            "Read adaptor file FROM THE GRID: " + adaptorUri);

      } catch (IoFault e) {
        addWarning("Could not read adaptor file", warningList, e);

        // Adaptation failed! Put the warnings into the Metadata object and return.
        theObject.getMetadata().getObject().getGeneric().getGenerated().getWarning()
            .addAll(warningList);

        return result;

      } catch (ObjectNotFoundFault e) {
        addWarning("The given adaptor file could not be found", warningList, e);

        // Adaptation failed! Put the warnings into the Metadata object and return.
        theObject.getMetadata().getObject().getGeneric().getGenerated().getWarning()
            .addAll(warningList);

        return result;

      } catch (AuthFault e) {
        addWarning("Access denied to adaptor file", warningList, e);

        // Adaptation failed! Put the warnings into the Metadata object and return.
        theObject.getMetadata().getObject().getGeneric().getGenerated().getWarning()
            .addAll(warningList);

        return result;
      }

      // Get baseline object and store it into the XML database, if Adaptor file could be read.
      if (adaptorObject.getData() != null) {

        InputStream baselineInputStream = null;
        try {
          // Get baseline data, create DataHandler.
          baselineInputStream = AdaptorManager.getBaseline(theObject.getData().getInputStream(),
              adaptorObject.getData().getInputStream(), theUri,
              theConfiguration.getVALIDATEbASELINE());
          // TODO Do use a streaming DataSource here! But with XML documents it will not really
          // matter at the moment!
          DataHandler baselineHandler = new DataHandler(
              new ByteArrayDataSource(baselineInputStream, TextGridMimetypes.OCTET_STREAM));

          // Put into the Index database.
          theIdxStorage
              .create(new TGCrudTextGridObject(theUri, baselineHandler, CrudObject.BASELINE_DATA));

          CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
              "Stored baseline object to XML database");

          // Get all (refersTo) relations from the Adaptor Manager, and put them into the result
          // stream if the baseline encoding had been successfully generated.
          if (baselineInputStream != null) {
            result =
                AdaptorManager.getBaselineRelations(theObject.getData().getInputStream(), theUri);

            CrudServiceUtilities.serviceLog(CrudService.INFO, METH,
                "Stored relations to RDF database");
          }
        } catch (SAXParseException e) {
          String message = "Baseline transformation failed in line " + e.getLineNumber()
              + " column " + e.getColumnNumber();
          addWarning(message, warningList, e);
        } catch (UnsupportedEncodingException e) {
          addWarning(BT_FAILED, warningList, e);
        } catch (TransformerException e) {
          addWarning(BT_FAILED, warningList, e);
        } catch (IOException e) {
          addWarning(BT_FAILED, warningList, e);
        } catch (SAXException e) {
          addWarning(BT_FAILED, warningList, e);
        }
      }
    }

    // Add warnings to the metadata.
    theObject.getMetadata().getObject().getGeneric().getGenerated().getWarning()
        .addAll(warningList);

    CrudServiceUtilities.serviceLog(CrudService.INFO, METH, "Adaptor Manager task complete");

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Adds a warning to the given warning list.
   * </p>
   *
   * @param theMessage
   * @param theWarningList
   */
  private static void addWarning(String theMessage, List<Warning> theWarningList) {
    addWarning(theMessage, theWarningList, null);
  }

  /**
   * <p>
   * Adds a warning to the given warning list.
   * </p>
   *
   * @param theMessage
   * @param theWarningList
   * @param e
   */
  private static void addWarning(String theMessage, List<Warning> theWarningList, Exception e) {

    Warning warning = new Warning();
    warning.setValue(theMessage + "!" + (e == null ? "" : " " + e.getClass().getName())
        + (e == null ? "" : ": " + e.getMessage()));
    theWarningList.add(warning);

    CrudServiceUtilities.serviceLog(CrudService.WARN, METH, warning.getValue());
  }

  /**
   * <p>
   * Simply logs the relations.
   * </p>
   *
   * @param theRdfStorage
   * @param theUri
   * @throws IoFault
   */
  private static void logRelations(CrudStorage<MetadataContainerType> theRdfStorage, URI theUri)
      throws IoFault {
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH,
        "Relations for URI: " + theUri + "\n" + theRdfStorage.retrieve(theUri).getRelations());
  }

  /**
   * <p>
   * Check the URIs in the relations. Simply check for correct <uri:abcd><uri:1234><uri:blablubb>
   * schema.
   * </p>
   *
   * TODO Care about formal validity of N3 triples later, seems to be too simple just to check for
   * ":" existence!
   *
   * TODO Put in TGCrudServiceUtilities later? Maybe we could use it for more than the TBLE
   * relations?
   *
   * FIXME Check why we do this!! I don't remember!
   *
   * @param theRelations
   * @return A map of some strings, I do not remember!
   */
  private static Map<String, String> validateURIs(List<String> theRelations) {

    HashMap<String, String> result = new HashMap<String, String>();

    for (String relation : theRelations) {

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH, "THE RELATION: " + relation);

      // Split and sort out end tag char.
      String uris[] = relation.split(">");
      for (String uri : uris) {
        // Sort out relation end char.
        if (uri.trim().startsWith(".")) {
          continue;
        }

        // Replace beginning tag char.
        uri = uri.replaceAll("<", "");

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, METH, "THE URI: " + uri);

        if (!uri.contains(":")) {
          result.put(uri, relation);
        }
      }
    }

    return result;
  }

}
