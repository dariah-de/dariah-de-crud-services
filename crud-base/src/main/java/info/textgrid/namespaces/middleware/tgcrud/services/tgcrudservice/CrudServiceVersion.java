/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

/**
 * TODOLOG
 * 
 ** 
 * CHANGELOG
 * 
 * 2022-05-24 - Funk - Fix some SpotBug issues.
 * 
 * 2014-10-29 - Funk - First version.
 *
 */

/**
 * <p>
 * <b>The CrudServiceVersion to implemented from the Impl classes.</b>
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-05-24
 * @since 2014-10-29
 */

public class CrudServiceVersion {

  private String version;
  private String builddate;
  private String servicename;

  /**
   * <p>
   * Constructor
   * </p>
   * 
   * @param theVersion
   * @param theBuilddate
   * @param theServicename
   */
  public CrudServiceVersion(String theVersion, String theBuilddate, String theServicename) {
    this.version = theVersion;
    this.builddate = theBuilddate;
    this.servicename = theServicename;
  }

  /**
   * @return The services' version.
   */
  public String getVERSION() {
    return this.version;
  }

  /**
   * @return The services' build date.
   */
  public String getBUILDDATE() {
    return this.builddate;
  }

  /**
   * @return The services' name.
   */
  public String getSERVICENAME() {
    return this.servicename;
  }

  /**
   * @return
   */
  public String getFULLVERSION() {
    return getSERVICENAME() + "-" + getVERSION() + "+" + getBUILDDATE();
  }

}
