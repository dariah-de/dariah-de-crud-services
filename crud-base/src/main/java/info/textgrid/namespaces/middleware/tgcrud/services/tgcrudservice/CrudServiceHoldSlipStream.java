/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import org.apache.cxf.attachment.AttachmentDataSource;
import org.apache.cxf.attachment.LazyDataSource;
import org.apache.cxf.message.Message;
import org.apache.cxf.message.MessageImpl;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2017-10-20 - Funk - Renaming to general crud service class name.
 * 
 * 2012-04-24 - Funk - First version.
 * 
 */

/**
 * <p>
 * Holds a data stream so it is rewindable.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2017-10-20
 * @since 2012-04-24
 */

public class CrudServiceHoldSlipStream {

  /**
   * <p>
   * Locking will avoid to copy data source for reuse. That's what I was looking for all the time!
   * :-)
   * </p>
   * 
   * @param theHandler The data handler to lock
   * @throws IOException
   */
  public static void lock(DataHandler theHandler) throws IOException {

    if (theHandler != null && theHandler.getDataSource() != null) {
      AttachmentDataSource ads = getAttachmentDataSource(theHandler.getDataSource());
      // Tell attachment to hold the temporary file, if not null.
      // NOTE Testing data source for NULL response should be not necessary anymore as of CXF
      // version 2.5.4/2.6.1, we do it anyway just to be sure :-)
      if (ads != null) {
        Message m = new MessageImpl();
        ads.hold(m);
      }
    }
  }

  /**
   * <p>
   * Release the hold, so the cached file can be deleted from the temp dir.
   * </p>
   * 
   * @param theHandler The data handler to release and close
   */
  public static void releaseAndClose(DataHandler theHandler) {

    if (theHandler != null && theHandler.getDataSource() != null) {
      AttachmentDataSource ads = getAttachmentDataSource(theHandler.getDataSource());
      // Tell attachment to release the temporary file.
      if (ads != null && ads.isCached()) {
        ads.release();
      }
      // Close stream to finally delete temp file.
      try {
        theHandler.getInputStream().close();
      } catch (IOException e) {
        // Ignore exception!
        e.printStackTrace();
      }
    }
  }

  /**
   * <p>
   * Gets the underlying attachment data source.
   * </p>
   * 
   * @param ds The data source
   * @return The attachment data source
   */
  private static AttachmentDataSource getAttachmentDataSource(DataSource ds) {

    if (ds instanceof LazyDataSource) {
      ds = ((LazyDataSource) ds).getDataSource();
    }
    if (ds instanceof AttachmentDataSource) {
      return (AttachmentDataSource) ds;
    }

    return null;
  }

}
