/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * TODOLOG
 *
 * TODO Interface implementations (storage, uri, and security) without hardcoding them into
 * TGCrudServiceConfigurator!
 *
 * =======
 *
 * 2024-11-08 - Funk - Add REINDEX_SECRET.
 * 
 * 2024-02-19 - Funk - Adapt to new messaging.
 * 
 * 2018-10-15 - Funk - Add TIFY_LOCATION.
 *
 * 2021-04-14 - Funk - Add DOI API URL.
 * 
 * 2020-09-21 - Funk - Add badge text.
 * 
 * 2020-09-16 - Funk - Add menu header color.
 * 
 * 2020-03-31 - Funk - Add LSR URL.
 *
 * 2018-10-15 - Funk - Add MANIFEST_LOCATION and MIRADOR_LOCATION.
 *
 * 2018-08-24 - Funk - Add DIGILIB_LOCATION and THUMB_SIZE. Removed some long unused methods.
 *
 * 2018-08-16 - Funk - Add STREAMING_SIZE.
 *
 * 2018-05-30 - Funk - Add Imprint, Privacy Policy, and Contact URLs.
 *
 * 2018-03-09 - Funk - Add PID and DOI prefixes.
 *
 * 2018-03-02 - Funk - Add FITS_CLIENT_TIMEOUT.
 **/

/**
 * <p>
 * This configurator class is just existing for CRUD configuration issues.
 * </p>
 *
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-06-28
 * @since 2008-05-21
 **/

public class CrudServiceConfigurator extends Properties {

  // **
  // STATIC FINALS
  // **

  private static final CrudServiceConfigurator configurator = new CrudServiceConfigurator();
  private static final long serialVersionUID = -6079356161844428961L;
  private static final long AAI_CLIENT_STUB_TIMEOUT = 30000l;
  private static final long ID_AUTOMAGIC_UNLOCKING_TIME = 1800000l;

  // **
  // STATICS
  // **

  private static long configfileDate = 0;
  private static String filename = "";

  // **
  // CLASS
  // **

  private boolean hasChanged;

  /**
   * CONSTRUCTOR
   */
  private CrudServiceConfigurator() {
    // Private constructor prevents instantiation by untrusted callers
  }

  // **
  // MANIPULATION
  // **

  /**
   * <p>
   * Creates an instance of this Configurator object if none is existing.
   * </p>
   *
   * TODO Check if we really need this feature!!
   *
   * @param theConfigFileLocation
   * @return A CRUD configuration object.
   * @throws IOException
   */
  public static synchronized CrudServiceConfigurator getInstance(String theConfigFileLocation)
      throws IOException {

    // Reload the configuration, if the file name or the config file's modification timestamp has
    // changed.
    String oldFile = filename;
    filename = theConfigFileLocation;
    long oldDate = configfileDate;
    configfileDate = new File(filename).lastModified();

    if (oldDate != configfileDate || !oldFile.equals(filename)) {
      configurator.load(new FileInputStream(filename));
      configurator.hasChanged = true;
    } else {
      configurator.hasChanged = false;
    }

    return configurator;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * <p>
   * Returns the filename of the current configuration file.
   * </p>
   *
   * @return The configuration filename string.
   */
  public static String getConfigurationFilename() {
    return filename;
  }

  /**
   * <p>
   * Contains the current TextGrid metadata namespace used by CRUD. Please do not change this value,
   * it is closely bind to the CRUD internal programming.
   * </p>
   *
   * TODO Do we need this anymore??
   *
   * @return The TextGrid namespace as a string.
   * @throws IoFault
   */
  public String getMETADATAnAMESPACE() throws IoFault {
    return getStringValueMandatory("METADATA_NAMESPACE");
  }

  /**
   * <p>
   * Here the default encoding of all read and written XML documents is defined, e.g. TextGrid
   * metadata. Default is UTF-8.
   * </p>
   *
   * @return The default encoding as a string.
   */
  public String getDEFAULTeNCODING() {
    return getStringValueDefault("DEFAULT_ENCODING", "UTF-8");
  }

  /**
   * <p>
   * The DEFAULT_DATA_STORAGE_URI configures the path or URL all files will be stored to, including
   * the protocol to use, only "gsiftp://" and "file://" is permitted at the moment.
   * </p>
   *
   * @return The default data storage URI.
   */
  public URI getDEFAULTdATAsTORAGEuRI() throws IoFault {

    URI result = null;

    try {
      result = new URI(getStringValueMandatory("DEFAULT_DATA_STORAGE_URI"));
    } catch (URISyntaxException e) {
      throw new IoFault("Please check the DATA storage URI", e);
    }

    return result;
  }

  /**
   * <p>
   * The DEFAULT_DATA_STORAGE_URI_PUBLIC configures the path at the second DATA storage location.
   * </p>
   *
   * @return The default public data storage URI.
   */
  public URI getDEFAULTdATAsTORAGEuRIpUBLIC() throws IoFault {

    URI result = null;

    try {
      result = new URI(getStringValueMandatory("DEFAULT_DATA_STORAGE_URI_PUBLIC"));
    } catch (URISyntaxException e) {
      throw new IoFault("Please check the public DATA storage URI", e);
    }

    return result;
  }

  /**
   * <p>
   * Within the TextGrid project the prefix "textgrid" is used. This prefix is used by CRUD for
   * TextGrid URI creation.
   * </p>
   *
   * @return The default URI prefix String.
   * @throws IoFault
   */
  public String getURIpREFIX() throws IoFault {
    return getStringValueMandatory("URI_PREFIX");
  }

  /**
   * <p>
   * Here the string is defined, that is appended to the metadata's filename, when a file is stored
   * TO THE GRID.
   * </p>
   *
   * @return The default metadata file suffix.
   * @throws IoFault
   */
  public String getMETADATAfILEsUFFIX() throws IoFault {
    return getStringValueMandatory("METADATA_FILE_SUFFIX");
  }

  /**
   * <p>
   * The URL of the Authentication service.
   * </p>
   *
   * @return The authentification service URL.
   * @throws IoFault
   */
  public URL getAAIsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("AAI_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the AAI service URL", e);
    }

    return result;
  }

  /**
   * @return
   * @throws IoFault
   */
  public URL getIMPRINTuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("IMPRINT_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the IMPRINT URL", e);
    }

    return result;
  }

  /**
   * @return
   * @throws IoFault
   */
  public URL getPRIVACYpOLICYuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("PRIVACY_POLICY_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the PRIVACY POLICY URL", e);
    }

    return result;
  }

  /**
   * @return
   * @throws IoFault
   */
  public URL getCONTACTuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("CONTACT_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the CONTACT URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The URL of the Authentication service.
   * </p>
   *
   * @return The authentification service URL.
   * @throws IoFault
   */
  public URL getAAIcRUDsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("AAI_CRUD_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the AAI_CRUD service URL", e);
    }

    return result;
  }

  /**
   * <p>
   * Contains the namespace of the RDF database's XML schema. This is used for querying and storing
   * the RDF database.
   * </p>
   *
   * @return The RDFDB XML namespace.
   * @throws IoFault
   */
  public String getRDFDBnAMESPACE() throws IoFault {
    return getStringValueMandatory("RDFDB_NAMESPACE");
  }

  /**
   * <p>
   * The RDFDB server URL.
   * </p>
   *
   * @return The RDF database service URL.
   * @throws IoFault
   */
  public URL getRDFDBsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("RDFDB_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the RDFDB service URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The RDF database's user name.
   * </p>
   *
   * @return The RDF database user name.
   * @throws IoFault
   */
  public String getRDFDBuSER() throws IoFault {
    return getStringValueMandatory("RDFDB_USER");
  }

  /**
   * <p>
   * The RDF database's password for the above mentioned user name.
   * </p>
   *
   * @return The RDF database password.
   * @throws IoFault
   */
  public String getRDFDBpASS() throws IoFault {
    return getStringValueMandatory("RDFDB_PASS");
  }

  /**
   * <p>
   * The RDFDB PUBLIC server URL.
   * </p>
   *
   * @return The RDF database public service URL.
   * @throws IoFault
   */
  public URL getRDFDBsERVICEuRLpUBLIC() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("RDFDB_SERVICE_URL_PUBLIC");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the public RDFDB service URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The RDF public database's user name.
   * </p>
   *
   * @return The RDF database public user name.
   * @throws IoFault
   */
  public String getRDFDBuSERpUBLIC() throws IoFault {
    return getStringValueMandatory("RDFDB_USER_PUBLIC");
  }

  /**
   * <p>
   * The RDF public database's password for the above mentioned user name.
   * </p>
   *
   * @return The RDF database public password.
   * @throws IoFault
   */
  public String getRDFDBpASSpUBLIC() throws IoFault {
    return getStringValueMandatory("RDFDB_PASS_PUBLIC");
  }

  /**
   * <p>
   * The SPARQL namespace for RDF queries.
   * </p>
   *
   * @return The SPARQL namespace.
   * @throws IoFault
   */
  public String getSPARQLnAMESPACE() throws IoFault {
    return getStringValueMandatory("SPARQL_NAMESPACE");
  }

  /**
   * <p>
   * The Index DB namespace.
   * </p>
   *
   * @return The IndexDB namespace (if XML based).
   * @throws IoFault
   */
  public String getIDXDBnAMESPACE() throws IoFault {
    return getStringValueMandatory("IDXDB_NAMESPACE");
  }

  /**
   * @return
   * @throws IoFault
   */
  public String getPIDpREFIX() throws IoFault {
    return getStringValueOptional("PID_PREFIX");
  }

  /**
   * @return
   * @throws IoFault
   */
  public String getDOIpREFIX() throws IoFault {
    return getStringValueOptional("DOI_PREFIX");
  }

  /**
   * <p>
   * The Index DB server URL.
   * </p>
   *
   * @return The Index DB server URL.
   * @throws IoFault
   */
  public URL getIDXDBsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("IDXDB_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the Index DB service URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The Index DB PUBLIC server URL.
   * </p>
   *
   * @return The Index DB PUBLIC server URL.
   * @throws IoFault
   */
  public URL getIDXDBsERVICEuRLpUBLIC() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("IDXDB_SERVICE_URL_PUBLIC");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the public Index DB service URL", e);
    }

    return result;
  }

  /**
   * @return
   * @throws IoFault
   */
  public List<String> getIDXDBsERVICEpORTlIST() throws IoFault {
    return getListValueMandatory("IDXDB_SERVICE_PORT_LIST");
  }

  /**
   * <p>
   * The Index DB's user name.
   * </p>
   *
   * @return The Index DB's user name.
   * @throws IoFault
   */
  public String getIDXDBuSER() throws IoFault {
    return getStringValueMandatory("IDXDB_USER");
  }

  /**
   * <p>
   * The public Index DB's user name.
   * </p>
   *
   * @return The public Index DB's user name.
   * @throws IoFault
   */
  public String getIDXDBuSERpUBLIC() throws IoFault {
    return getStringValueMandatory("IDXDB_USER_PUBLIC");
  }

  /**
   * <p>
   * The Index DB's password.
   * </p>
   *
   * @return The Index DB's password.
   * @throws IoFault
   */
  public String getIDXDBpASS() throws IoFault {
    return getStringValueMandatory("IDXDB_PASS");
  }

  /**
   * <p>
   * The public Index DB's password.
   * </p>
   *
   * @return The public Index DB's password.
   * @throws IoFault
   */
  public String getIDXDBpASSpUBLIC() throws IoFault {
    return getStringValueMandatory("IDXDB_PASS_PUBLIC");
  }

  /**
   * <p>
   * The Index DB database path, where the metadata files are stored.
   * </p>
   *
   * @return An Index DB database path.
   * @throws IoFault
   */
  public String getIDXDBmETADATApATH() throws IoFault {
    return getStringValueMandatory("IDXDB_METADATA_PATH");
  }

  /**
   * <p>
   * The Index DB database path, where the baseline data files are stored.
   * </p>
   *
   * @return An Index DB database path.
   * @throws IoFault
   */
  public String getIDXDBbASELINEpATH() throws IoFault {
    return getStringValueMandatory("IDXDB_BASELINE_PATH");
  }

  /**
   * <p>
   * The Index DB database path, where the original data files are stored.
   * </p>
   *
   * @return An Index DB database path.
   * @throws IoFault
   */
  public String getIDXDBoRIGINALpATH() throws IoFault {
    return getStringValueMandatory("IDXDB_ORIGINAL_PATH");
  }

  /**
   * <p>
   * The Index DB database path, where the aggregation files are stored.
   * </p>
   *
   * @return An Index DB database path.
   * @throws IoFault
   */
  public String getIDXDBaGGREGATIONpATH() throws IoFault {
    return getStringValueMandatory("IDXDB_AGGREGATION_PATH");
  }

  /**
   * <p>
   * The Index DB database path, where the queries are started.
   * </p>
   *
   * @return An Index DB database path.
   * @throws IoFault
   */
  public String getIDXDBqUERYpATH() throws IoFault {
    return getStringValueMandatory("IDXDB_QUERY_PATH");
  }

  /**
   * <p>
   * The Index DB cluster name.
   * </p>
   *
   * @return The IndexDB cluster name.
   * @throws IoFault
   */
  public String getIDXDBcLUSTERnAME() throws IoFault {
    return getStringValueMandatory("IDXDB_CLUSTER_NAME");
  }

  /**
   * <p>
   * The path of the GRID proxy file to authenticate to the Globus system.
   * </p>
   *
   * @return The GRID proxy file path.
   * @throws IoFault
   */
  public String getGRIDpROXYfILE() throws IoFault {
    return getStringValueOptional("GRID_PROXY_FILE");
  }

  /**
   * <p>
   * The path to the log4j config file to use with this CRUD installation.
   * </p>
   *
   * @return The path to the log4j config file.
   * @throws IoFault
   */
  public String getLOG4JcONFIGFILE() throws IoFault {
    return getStringValueMandatory("LOG4J_CONFIGFILE");
  }

  /**
   * <p>
   * A whitespace separated list of DATA storage URIs, to where all the files are replicated to.
   * Every file, processed by the CRUD will replicated to *each* of the given locations.
   * </p>
   *
   * <p>
   * Please note, that each of this URIs must be authenticated throught the given GRID proxy file.
   * </p>
   *
   * @return A list of URIs all files will be replicated to.
   * @throws IoFault
   */
  public List<String> getREPLICAdATAsTORAGEuRIS() throws IoFault {
    return getListValueOptional("REPLICA_DATA_STORAGE_URIS");
  }

  /**
   * <p>
   * A whitespace separated list of DATA storage URIs for the PUBLIC DATA storage storage location.
   * </p>
   *
   * <p>
   * Please note, that each of this URIs must be authenticated through the given GRID proxy file.
   * </p>
   *
   * @return A list of URIs all files will be replicated to.
   * @throws IoFault
   */
  public List<String> getREPLICAdATAsTORAGEuRISpUBLIC() throws IoFault {
    return getListValueOptional("REPLICA_DATA_STORAGE_URIS_PUBLIC");
  }

  /**
   * <p>
   * Check permissions for e.g. register or unregister resources.
   * </p>
   *
   * @return Allowed or not allowed...
   * @throws IoFault
   */
  public String getAAIsPECIALsECRET() throws IoFault {
    return getStringValueMandatory("AAI_SPECIAL_SECRET");
  }

  /**
   * <p>
   * Check permissions for #REINDEXMETADATA.
   * </p>
   *
   * @return Allowed or not allowed...
   * @throws IoFault
   */
  public String getREINDEXsECRET() throws IoFault {
    return getStringValueMandatory("REINDEX_SECRET");
  }

  /**
   * <p>
   * The timeout of the AAI implementation, e.g. the RBAC TgextraServiceStub.
   * </p>
   *
   * @return The AAI client stub timeout.
   * @throws IoFault
   */
  public Long getAAIcLIENTsTUBtIMEOUT() throws IoFault {
    return getLongValueDefault("AAI_CLIENT_STUB_TIMEOUT", AAI_CLIENT_STUB_TIMEOUT);
  }

  /**
   * <p>
   * The filesize crud starts to stream.
   * </p>
   *
   * @return The streaming size.
   * @throws IoFault
   */
  public Long getSTREAMINGsIZE() throws IoFault {
    return getLongValueDefault("STREAMING_SIZE", 5242880l);
  }

  /**
   * <p>
   * The timeout of the FITS service client.
   * </p>
   *
   * @return The FITS client timeout.
   * @throws IoFault
   */
  public Long getFITScLIENTsTUBtIMEOUT() throws IoFault {
    return getLongValueDefault("FITS_CLIENT_TIMEOUT", 60000l);
  }

  /**
   * <p>
   * States if the generated baseline encoded XML shall be validated by the AdaptorManager or not.
   * </p>
   *
   * @return If or if not baseline shall be validated.
   * @throws IoFault
   */
  public boolean getVALIDATEbASELINE() throws IoFault {
    return getBooleanValueDefault("VALIDATE_BASELINE", false);
  }

  /**
   * <p>
   * States if the object to create shall be directly be published into the public storage area.
   * </p>
   *
   * @return If or if not objects shall be published directly.
   * @throws IoFault
   */
  public boolean getDIRECTLYpUBLISHwITHcREATE() throws IoFault {
    return getBooleanValueDefault("DIRECTLY_PUBLISH_WITH_CREATE", false);
  }

  /**
   * <p>
   * Shall the Pairtree be used in Index DB database implementation?
   * </p>
   *
   * @return The index database pairtree usage.
   * @throws IoFault
   */
  public boolean getIDXDBpAIRTREEuSAGE() throws IoFault {
    return getBooleanValueDefault("IDXDB_PAIRTREE_USAGE", false);
  }

  /**
   * <p>
   * The MyProxy server URL.
   * </p>
   *
   * @return The MyProxy server URL.
   * @throws IoFault
   */
  public URL getMYPROXYsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("MYPROXY_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the MYPROXY service URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The MyProxy username.
   * </p>
   *
   * @return The MyProxy username.
   * @throws IoFault
   */
  public String getMYPROXYuSER() throws IoFault {
    return getStringValueOptional("MYPROXY_USER");
  }

  /**
   * <p>
   * The MyProxy password.
   * </p>
   *
   * @return The MyProxy password.
   * @throws IoFault
   */
  public String getMYPROXYpASS() throws IoFault {
    return getStringValueOptional("MYPROXY_PASS");
  }

  /**
   * <p>
   * The location of the used GRID certificate file.
   * </p>
   *
   * @return The grid certificate file.
   * @throws IoFault
   */
  public String getGRIDcERTfILE() throws IoFault {
    return getStringValueOptional("GRID_CERT_FILE");
  }

  /**
   * <p>
   * The location of the used GRID key file.
   * </p>
   *
   * @return The Grid key file.
   * @throws IoFault
   */
  public String getGRIDkEYfILE() throws IoFault {
    return getStringValueOptional("GRID_KEY_FILE");
  }

  /**
   * <p>
   * The CN of the above used GRID certificate.
   * </p>
   *
   * @return The grid certificate CN.
   * @throws IoFault
   */
  public String getGRIDcERTcN() throws IoFault {
    return getStringValueOptional("GRID_CERT_CN");
  }

  /**
   * <p>
   * The password of the above used GRID certificate.
   * </p>
   *
   * @return The grid certificate password.
   * @throws IoFault
   */
  public String getGRIDcERTpASS() throws IoFault {
    return getStringValueOptional("GRID_CERT_PASS");
  }

  /**
   * <p>
   * How shall the DATA storage authentication be done? E.g. JavaGAT allows MyProxy Authentication,
   * Proxy Certificate Authentication, and Certificate Authentication. Allowed values are: "PROXY",
   * "MYPROXY", "CERT", and now also "SLC", or of course "NONE".
   * </p>
   *
   *
   * @return The data storage authentication method.
   * @throws IoFault
   */
  public String getDATSsTORAGEaUTHENTICATION() throws IoFault {
    return getStringValueMandatory("DATA_STORAGE_AUTHENTICATION");
  }

  /**
   * <p>
   * Here the string is defined, that is appended to the rollback file's name, when a file is
   * renamed while updating.
   * </p>
   *
   * @return The default rollback file suffix.
   * @throws IoFault
   */
  public String getROLLBACKfILEsUFFIX() throws IoFault {
    return getStringValueMandatory("ROLLBACK_FILE_SUFFIX");
  }

  /**
   * <p>
   * Sets the ID implementation to use.
   * </p>
   *
   * @return The ID implementation class name.
   * @throws IoFault
   */
  public String getIDiMPLEMENTATION() throws IoFault {
    return getStringValueMandatory("ID_IMPLEMENTATION");
  }

  /**
   * <p>
   * Sets the AAI implementation to use.
   * </p>
   *
   * @return The AAI implementation class name.
   * @throws IoFault
   */
  public String getAAIiMPLEMENTATION() throws IoFault {
    return getStringValueMandatory("AAI_IMPLEMENTATION");
  }

  /**
   * <p>
   * Sets the DATA storage implementation th use.
   * </p>
   *
   * @return The storage implementation class name.
   * @throws IoFault
   */
  public String getDATAsTORAGEiMPLEMENTATION() throws IoFault {
    return getStringValueMandatory("DATA_STORAGE_IMPLEMENTATION");
  }

  /**
   * <p>
   * Sets the Index DB storage implementation th use.
   * </p>
   *
   * @return The index database implementation class name.
   * @throws IoFault
   */
  public String getIDXDBsTORAGEiMPLEMENTATION() throws IoFault {
    return getStringValueMandatory("IDXDB_STORAGE_IMPLEMENTATION");
  }

  /**
   * <p>
   * Sets the RDFDB storage implementation to use.
   * </p>
   *
   * @return The RDF database implementation class name.
   * @throws IoFault
   */
  public String getRDFDBsTORAGEiMPLEMENTATION() throws IoFault {
    return getStringValueMandatory("RDFDB_STORAGE_IMPLEMENTATION");
  }

  /**
   * <p>
   * The ID server URL.
   * </p>
   *
   * @return The ID server URL.
   * @throws IoFault
   */
  public URL getIDsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("ID_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the ID service URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The HDL resolver URL (read only).
   * </p>
   *
   * @return The HDL resolver URL.
   * @throws IoFault
   */
  public URL getHANDLErESOLVERuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("HANDLE_RESOLVER_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the HDL resolver URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The ID service user name.
   * </p>
   *
   * @return The ID service user name.
   * @throws IoFault
   */
  public String getIDsERVICEuSER() throws IoFault {
    return getStringValueMandatory("ID_SERVICE_USER");
  }

  /**
   * <p>
   * The ID service password for the above mentioned user name.
   * </p>
   *
   * @return The ID server's password.
   * @throws IoFault
   */
  public String getIDsERVICEpASS() throws IoFault {
    return getStringValueMandatory("ID_SERVICE_PASS");
  }

  /**
   * <p>
   * The secret for accessing the #MOVEPUBLIC and #CREATEPUBLIC methods of CRUD.
   * </p>
   *
   * @return The secret for moving data to the public storage location.
   * @throws IoFault
   */
  public String getPUBLISHsECRET() throws IoFault {
    return getStringValueMandatory("PUBLISH_SECRET");
  }

  /**
   * <p>
   * The time after which an ID is automagically again lockable.
   * </p>
   *
   * @return The automagic unlocking time in millis.
   * @throws IoFault
   */
  public long getIDaUTOMAGICuNLOCKINGtIME() throws IoFault {
    return getLongValueDefault("ID_AUTOMAGIC_UNLOCKING_TIME", ID_AUTOMAGIC_UNLOCKING_TIME);
  }

  /**
   * @return The messaging flag if using messaging is wanted or not.
   * @throws IoFault
   */
  public boolean getUSEmESSAGING() throws IoFault {
    return getBooleanValueDefault("USE_MESSAGING", false);
  }

  /**
   * @return The messaging provider URL.
   * @throws IoFault
   */
  public URL getMESSAGINGsERVICEuRL() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("MESSAGING_SERVICE_URL");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the messaging provider URL", e);
    }

    return result;
  }

  /**
   * @return The messaging event name.
   */
  public String getMESSAGINGeVENTnAME() {
    return getStringValueOptional("MESSAGING_EVENT_NAME");
  }

  /**
   * @return The messaging security credentials.
   * @throws IoFault
   */
  public String getMESSAGINGuSER() throws IoFault {
    return getStringValueOptional("MESSAGING_USER");
  }

  /**
   * @return The messaging security credentials.
   * @throws IoFault
   */
  public String getMESSAGINGpASS() throws IoFault {
    return getStringValueOptional("MESSAGING_PASS");
  }

  /**
   * @return The mail of the contact.
   * @throws IoFault
   */
  public String getMAILoFcONTACT() throws IoFault {
    return getStringValueOptional("MAIL_OF_CONTACT");
  }

  /**
   * @return The name of the contact.
   * @throws IoFault
   */
  public String getNAMEoFcONTACT() throws IoFault {
    return getStringValueOptional("NAME_OF_CONTACT");
  }

  /**
   * <p>
   * The DataCite metadata location.
   * </p>
   *
   * @return The DataCite metadata location URL.
   * @throws IoFault
   */
  public URL getDATACITElOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("DATACITE_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the DataCite location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The Digilib service URL.
   * </p>
   *
   * @return The Digilib service URL.
   * @throws IoFault
   */
  public URL getDIGILIBlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("DIGILIB_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the Digilib location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The Language Resource Switchboard (LRS) service URL.
   * </p>
   *
   * @return The Language Resource Switchboard service URL.
   * @throws IoFault
   */
  public URL getSWITCHBOARDlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("SWITCHBOARD_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the Language Resource Switchboard location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The IIIF manifest URL.
   * </p>
   *
   * @return The IIIF manifest URL.
   * @throws IoFault
   */
  public URL getMANIFESTlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("MANIFEST_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the IIIF manifest location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The TIFY location.
   * </p>
   *
   * @return The TIFY URL.
   * @throws IoFault
   */
  public URL getTIFYlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("TIFY_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the TIFY location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The Mirador location.
   * </p>
   *
   * @return The Mirador URL.
   * @throws IoFault
   */
  public URL getMIRADORlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("MIRADOR_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the Mirador location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The size of the landing page preview image.
   * </p>
   *
   * @return The size of the landing page preview image.
   * @throws IoFault
   */
  public String getTHMUBsIZE() throws IoFault {
    return getStringValueDefault("THUMB_SIZE", "500");
  }

  /**
   * <p>
   * The Handle resolver URL (resolving only!).
   * </p>
   *
   * @return The PID resolver URL.
   * @throws IoFault
   */
  public URL getPIDrESOLVER() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("PID_RESOLVER");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the PID resolver URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The ORCID resolver URL.
   * </p>
   *
   * @return The ORCID resolver URL.
   * @throws IoFault
   */
  public URL getORCIDrESOLVER() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("ORCID_RESOLVER");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the ORCID resolver URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The DOI resolver URL.
   * </p>
   *
   * @return The DOI resolver URL.
   * @throws IoFault
   */
  public URL getDOIrESOLVER() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("DOI_RESOLVER");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the DOI resolver URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The DOI API URL.
   * </p>
   * 
   * @return The DOI API URL.
   * @throws IoFault
   */
  public URL getDOIaPIlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("DOI_API_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the DOI API LOCATION URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The OAI-PMH host.
   * </p>
   *
   * @return The OAI-PMH host URL.
   * @throws IoFault
   */
  public URL getOAIPMHlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("OAIPMH_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the OAI-PMH location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The GND resolver URL.
   * </p>
   *
   * @return The GND resolver URL.
   * @throws IoFault
   */
  public URL getGNDrESOLVER() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("GND_RESOLVER");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the GND resolver URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The crud location used for PID metadata.
   * </p>
   *
   * @return The TG- or DH-crud location URL.
   * @throws IoFault
   */
  public URL getCRUDlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("CRUD_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the crud location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The PUBLIKATOR location.
   * </p>
   *
   * @return The location of the PUBLIKATOR.
   * @throws IoFault
   */
  public URL getPUBLIKATORlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("PUBLIKATOR_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check THE PUBLIKATOR location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The DOCUMENTATION location.
   * </p>
   *
   * @return The location of the Publikator's documentation.
   * @throws IoFault
   */
  public URL getDOCUMENTATIONlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("DOCUMENTATION_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check THE PUBLIKATOR DOCUMENTATION location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The repository API Documentation location.
   * </p>
   *
   * @return The location of the repository's API documentation.
   * @throws IoFault
   */
  public URL getAPIdOCUMENTATIONlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("API_DOCUMENTATION_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the API documentation location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The repository FAQ location.
   * </p>
   *
   * @return The location of the repository's FAQ.
   * @throws IoFault
   */
  public URL getFAQlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("FAQ_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the FAQ location URL", e);
    }

    return result;
  }

  /**
   * <p>
   * The path to the FITS.
   * </p>
   *
   * @return The FITS location URL.
   * @throws IoFault
   */
  public URL getFITSlOCATION() throws IoFault {

    URL result = null;

    try {
      result = getURLValueMandatory("FITS_LOCATION");
    } catch (MalformedURLException e) {
      throw new IoFault("Please check the fits location URL", e);
    }

    return result;
  }

  /**
   * @return The techmd extraction flag.
   * @throws IoFault
   */
  public boolean getEXTRACTtECHMD() throws IoFault {
    return getBooleanValueDefault("EXTRACT_TECHMD", false);
  }

  /**
   * <p>
   * The menu header color of the landing and index pages, if not default.
   * </p>
   * 
   * @return Hex HTML color for the menu header.
   * @throws IoFault
   */
  public String getMENUhEADERcOLOR() throws IoFault {
    return getStringValueOptional("MENU_HEADER_COLOR");
  }

  /**
   * <p>
   * The badge text of the landing and index pages, if not default.
   * </p>
   * 
   * @return Badge text string.
   * @throws IoFault
   */
  public String getBADGEtEXT() throws IoFault {
    return getStringValueOptional("BADGE_TEXT");
  }

  // **
  // INTERNAL METHODS
  // **

  /**
   * <p>
   * Returns a String value from the CRUD config file. Throws an exception if no value is set.
   * </p>
   *
   * @param theKey
   * @return A string value from the CRUD config file.
   * @throws IoFault
   */
  private String getStringValueMandatory(String theKey) throws IoFault {

    String val = ((String) this.get(theKey)).trim();

    if (val == null || val.equals("")) {
      throw new IoFault("The key '" + theKey + "' in the CRUD config file must be set");
    }

    return val;
  }

  /**
   * <p>
   * Returns a String value from the CRUD config file. Sets a default value if no value is set.
   * </p>
   *
   * @param theKey
   * @param theDefault
   * @return A string value from the CRUD config file.
   */
  private String getStringValueDefault(String theKey, String theDefault) {

    String result = theDefault;

    if (this.get(theKey) != null) {
      result = ((String) this.get(theKey)).trim();
    }

    return result;
  }

  /**
   * <p>
   * Returns a String value from the CRUD config file. Sets "" as a default value if no value is
   * set.
   * </p>
   *
   * @param theKey
   * @return A string value from the CRUD config file.
   */
  private String getStringValueOptional(String theKey) {
    return getStringValueDefault(theKey, "").trim();
  }

  /**
   * <p>
   * Returns a new URL. DO NOT ADD "/" automatically!!
   * </p>
   *
   * @param theKey
   * @return
   * @throws MalformedURLException
   * @throws IoFault
   */
  private URL getURLValueMandatory(String theKey) throws MalformedURLException, IoFault {
    return new URL(getStringValueMandatory(theKey));
  }

  /**
   * <p>
   * Returns a long value from the CRUD config file. Sets a default value if no value is set.
   * </p>
   *
   * @param theKey
   * @param theDefault
   * @return A long value from the CRUD config file.
   * @throws IoFault
   */
  private Long getLongValueDefault(String theKey, Long theDefault) throws IoFault {

    Long result;
    Object val = this.get(theKey);

    if (val == null) {
      return theDefault;
    }

    try {
      result = Long.parseLong(((String) val).trim());
    } catch (NumberFormatException e) {
      throw new IoFault("Value of key '" + theKey + "' can not be parsed to a Long value", e);
    }

    return result;
  }

  /**
   * <p>
   * Gets a boolean value from the config file. Default is FALSE.
   * </p>
   *
   * @param theKey
   * @param theDefault
   * @return The boolean value of the configuration key, taken from the configuration file. If TRUE
   *         is not given in the config file, FALSE is returned.
   */
  private boolean getBooleanValueDefault(String theKey, final boolean theDefault) {

    Object val = this.get(theKey);

    if (val == null) {
      return theDefault;
    }

    return (boolean) Boolean.parseBoolean((String) val);
  }

  /**
   * <p>
   * Gets a list of strings from the config file.
   * </p>
   *
   * @param theKey
   * @return The List of the configuration key, taken from the configuration file.
   */
  private List<String> getListValueOptional(String theKey) {

    List<String> result = new ArrayList<String>();
    Object val = this.get(theKey);

    result = TGCrudServiceUtilities.getListFromConfigString((String) val);

    return result;
  }

  /**
   * <p>
   * Gets a list of strings from the config file.
   * </p>
   *
   * @param theKey
   * @return The List of the configuration key, taken from the configuration file.
   * @throws IoFault
   */
  private List<String> getListValueMandatory(String theKey) throws IoFault {

    List<String> result = new ArrayList<String>();
    Object val = this.get(theKey);

    if (val == null || val.equals("")) {
      throw new IoFault("The key '" + theKey + "' in the CRUD config file must be set");
    }

    result = TGCrudServiceUtilities.getListFromConfigString((String) val);

    return result;
  }

  /**
   * <p>
   * Is set to TRUE if the service configuration file has changed since the last configuration
   * initialisation call.
   * </p>
   *
   * @return TRUE if the config file has changed, FALSE otherwise.
   */
  public boolean hasChanged() {
    return this.hasChanged;
  }

}
