/**
 * This software is copyright (c) 2015 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * TODOLOG
 * 
 * TODO Test internal locking.
 * 
 **
 * CHANGELOG
 * 
 * 2013-10-29 - Funk Corrected some Sonar issues.
 * 
 * 2012-04-19 - Funk - Added internal locking.
 * 
 * 2011-05-05 - Funk - First version.
 */

/**
 * <p>
 * This URI implementation creates URIs from random UUIDs.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2014-12-22
 * @since 2011-05-05
 */

public class TGCrudServiceIdentifierUuidImpl extends CrudServiceIdentifierAbs {

  // **
  // FINALS
  // **

  private static final String INTERNAL = "INTERNALUSER";

  // **
  // STATICS
  // **

  /**
   * <p>
   * The set of current URIs is only used to check, if an URI is currently processed by another
   * instance of the TG-crud. The URI is put in, if an instance starts working with it and the URI
   * is removed, if the processing has been completed (or an error occurred).
   * </p>
   */
  private static volatile Set<URI> currentUris = new HashSet<URI>();

  // **
  // METHODS TO IMPLEMENT
  // **

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#getUris
   * (int)
   */
  @Override
  public List<URI> getUris(int theAmount) throws IoFault {

    List<URI> result = new ArrayList<URI>();

    // Some checks.
    if (this.conf == null) {
      throw CrudServiceExceptions.ioFault("The configuration must not be null");
    }

    // Create the URIs.
    for (int i = 0; i < theAmount; i++) {
      result.add(URI.create(this.conf.getURIpREFIX() + ":" + UUID.randomUUID().toString()));
    }

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * TGCrudServiceIdentifier#updatePidMetadata(java.net.URI)
   */
  @Override
  public void updatePidMetadata(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * TGCrudServiceUri#uriExists(java.net.URI)
   */
  @Override
  public boolean uriExists(URI theUri) throws IoFault {
    // In this UUID URI implementation every URI is existing, because we
    // have no service to check, so we return TRUE by default.
    return true;
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#lock(
   * java.net.URI, java.lang.String)
   */
  @Override
  public boolean lock(URI theUri, String theUser) throws IoFault {
    synchronized (currentUris) {
      if (currentUris.contains(theUri)) {
        return false;
      }

      currentUris.add(theUri);
      return true;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * TGCrudServiceUri#isLocked(java.net.URI, java.lang.String)
   */
  @Override
  public String isLockedBy(URI theUri) throws IoFault {
    synchronized (currentUris) {
      if (currentUris.contains(theUri)) {
        return "NO_USER";
      } else {
        return "";
      }
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#unlock(
   * java.net.URI, java.lang.String)
   */
  @Override
  public boolean unlock(URI theUri, String theUser) throws IoFault {
    synchronized (currentUris) {
      currentUris.remove(theUri);
      return true;
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * TGCrudServiceUri#lockInternal(java.net.URI)
   */
  @Override
  public boolean lockInternal(URI theUri) throws IoFault {
    synchronized (currentUris) {
      return lock(theUri, INTERNAL);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * TGCrudServiceUri#unlockInternal(java.net.URI)
   */
  @Override
  public boolean unlockInternal(URI theUri) throws IoFault {
    synchronized (currentUris) {
      return unlock(theUri, INTERNAL);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifierAbs#
   * setKeyValuePairs(java.util.HashMap)
   */
  @Override
  public void setKeyValuePairs(HashMap<String, String> theKeyValueMap) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * getHandleMetadata(java.net.URI, java.lang.String)
   */
  @Override
  public String getHandleMetadata(URI theUri, String theType) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

}
