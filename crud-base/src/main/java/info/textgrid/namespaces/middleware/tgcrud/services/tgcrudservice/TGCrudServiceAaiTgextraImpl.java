/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import java.net.URL;
import java.util.List;
import info.textgrid.middleware.tgauth.tgauthclient.TGAuthClientCrudUtilities;
import info.textgrid.middleware.tgauth.tgauthclient.TGAuthClientUtilities;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.RolesetResponse;
import info.textgrid.namespaces.middleware.tgauth.TgAssignedRolesRequest;
import info.textgrid.namespaces.middleware.tgauth.TgCheckAccessRequest;
import info.textgrid.namespaces.middleware.tgauth.UnknownResourceFault;
import info.textgrid.namespaces.middleware.tgauth_crud.NearlyPublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgauth_crud.PublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.RbacFault;
import info.textgrid.namespaces.middleware.tgauth_crud.RegisterResourceRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.TgCrudCheckAccessRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.TgCrudCheckAccessResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.UnregisterResourceRequest;
import jakarta.xml.ws.WebServiceException;

/**
 * CHANGELOG
 * 
 * 2024-02-28 - Funk - Edit the workaround for #TG-635.
 *
 * 2022-02-10 - Funk - Remove deprecated get SLC method.
 *
 * 2019-02-18 - Funk - Added better error message if checkRights() checks do fail.
 * 
 * 2019-02-18 - Funk - Marked SLC method deprecated.
 * 
 * 2016-08-11 - Funk - Using TG-auth client from TG-auth service utilities now!
 * 
 * 2015-02-16 - Funk - Added SID purging.
 * 
 * 2012-04-20 - Funk - Adapted to new tgextra and tgextra-crud stubs.
 * 
 * 2012-04-02 - Funk - Added workaround for TG-1732.
 * 
 * 2012-02-07 - Funk - Added TG-extra generation methods from TGCrudServiceUtils.
 * 
 * 2012-12-21 - Funk - Added SLC method.
 * 
 * 2011-12-13 - Funk - Divided publish() into publish() and nearlyPublish().
 * 
 * 2011-09-13 - Funk - Added TG-auth CRUD service stub and changed publish()'s work to
 * nearlyPublish!
 * 
 * 2011-09-13 - Funk - Added method checkRights().
 * 
 * 2011-08-10 - Funk - Added creation of tgextra from TGEXTRA_SERVICE_URL, instead of trusting the
 * WSDL.
 * 
 * 2011-05-11 - Funk - Added duration for all tgextra calls.
 * 
 * 2011-05-02 - Funk - Throw the SOAPFaults for the workaround, they must not get lost!
 * 
 * 2011-01-19 - Funk - Changed the Tgextra stub to be static and get it as a Singleton.
 * 
 * 2010-09-07 - Funk - First version.
 */

/**
 * <p>
 * This security implementation uses the TG-auth service of TextGrid.
 * </p>
 * 
 * TODO Check workaround for TG-635!! Do we need more SOAP faults to be catched?? Could we use the
 * FaultNo instead of the exception message string??
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-02-28
 * @since 2010-09-07
 */

public class TGCrudServiceAaiTgextraImpl extends CrudServiceAaiAbs {

  // **
  // STATIC FINALS
  // **

  public static final String UNIT_NAME = "tgextra";

  public static final String TGEXTRA_UNKNOWNRESOURCE_STRING =
      "The given resource is unknown to the system";

  public static final String ERROR_REGISTER = "Registering resource failed";
  public static final String ERROR_UNREGISTER = "Unregistering resource failed";
  public static final String ERROR_ACCESSDENIED = "Access denied";
  public static final String ERROR_PUBLISH = "Publishing failed";
  public static final String ERROR_UNKNOWNRESOURCE = "Resource is unknown to the TG-auth";

  // **
  // CLASS VARIABLES
  // **

  protected static PortTgextra tgextra = null;
  protected static PortTgextraCrud tgextraCrud = null;

  // **
  // METHODS TO IMPLEMENT
  // **

  /**
   *
   */
  @Override
  public void checkAccess(String theSessionId, String theLogParameter, String theResource,
      String theOperation) throws ObjectNotFoundFault, AuthFault, IoFault {

    String meth = UNIT_NAME + ".checkAccess()";
    long startTime = System.currentTimeMillis();

    // Get the one and only tgextra stub.
    initTgextraService(this.conf.getAAIsERVICEuRL());

    // Create checkAccess request.
    TgCheckAccessRequest params = new TgCheckAccessRequest();
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setOperation(theOperation);
    params.setResource(theResource);

    logStartParameters(meth, theSessionId, theLogParameter, theOperation,
        theResource);

    // Call TG-auth.
    try {
      // Throw AuthFault if SID is not valid.
      if (!tgextra.tgCheckAccess(params).isResult()) {
        throw new AuthFault("No " + theOperation.toUpperCase()
            + " permission is granted for resource: " + theResource);
      }
    } catch (UnknownResourceFault e) {
      throw CrudServiceExceptions.objectNotFoundFault(e,
          ERROR_UNKNOWNRESOURCE + ": " + theResource);
    } catch (AuthFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_ACCESSDENIED);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "tgextra query duration: "
        + CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime));
  }

  /**
   *
   */
  @Override
  public TgCrudCheckAccessResponse checkAccessGetInfo(String theSessionId, String theLogParameter,
      String theResource, String theOperation) throws ObjectNotFoundFault, AuthFault, IoFault {

    String meth = UNIT_NAME + ".checkAccessGetInfo()";
    long startTime = System.currentTimeMillis();

    // Get the one and only tgextra-crud stub.
    initTgextraCrudService(this.conf.getAAIcRUDsERVICEuRL());

    // Create checkAccess request.
    TgCrudCheckAccessRequest params = new TgCrudCheckAccessRequest();
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setOperation(theOperation);
    params.setResource(theResource);
    params.setSecret(this.conf.getAAIsPECIALsECRET());

    logStartParameters(meth, theSessionId, theLogParameter, theOperation, theResource);

    // Call TG-auth.
    TgCrudCheckAccessResponse result = null;
    try {
      result = tgextraCrud.tgCrudCheckAccess(params);

      // Throw AuthFault if SID is not valid.
      if (!result.isResult()) {
        throw new AuthFault(
            "No " + theOperation.toUpperCase() + " permission is granted for: " + theResource);
      }

    } catch (WebServiceException e) {
      // TODO Remove workaround for TG-635, if fixed!!
      if (e.getMessage().contains(TGEXTRA_UNKNOWNRESOURCE_STRING)) {
        throw CrudServiceExceptions.objectNotFoundFault(e,
            ERROR_UNKNOWNRESOURCE + ": " + theResource);
      }
      throw e;
    } catch (info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault e) {
      throw CrudServiceExceptions.objectNotFoundFault(e,
          ERROR_UNKNOWNRESOURCE + ": " + theResource);
    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      throw CrudServiceExceptions.ioFault(e,
          ERROR_ACCESSDENIED + "! BTW: Who is not cruddy enough??");
    } catch (AuthFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_ACCESSDENIED);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "tgextra query duration: "
        + CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime));

    return result;
  }

  /**
   *
   */
  @Override
  public List<String> registerResource(String theSessionId, String theLogParameter,
      String theResource, URI theUri) throws AuthFault, IoFault {

    List<String> result;

    String meth = UNIT_NAME + ".registerResource()";
    long startTime = System.currentTimeMillis();

    // Get the one and only tgextra-crud stub.
    initTgextraCrudService(this.conf.getAAIcRUDsERVICEuRL());

    // Create register resource request.
    RegisterResourceRequest params = new RegisterResourceRequest();
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setProject(theResource);
    params.setUri(theUri.toString());
    params.setSecret(this.conf.getAAIsPECIALsECRET());
    // FIXME Store the absolute path of the GRID resource.
    // params.setUuid(the_absolute_grid_path);

    logStartParameters(meth, theSessionId, theLogParameter, theResource, theUri);

    try {
      result = tgextraCrud.registerResource(params).getOperation();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Sucessfully registered: " + theUri);

    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_REGISTER);
    } catch (RbacFault e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_REGISTER);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "tgextra query duration: "
            + CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime));

    return result;
  }

  /**
   *
   */
  @Override
  public void unregisterResource(String theSessionId, String theLogParameter, URI theUri)
      throws AuthFault, ObjectNotFoundFault, IoFault {

    String meth = UNIT_NAME + ".unregisterResource()";
    long startTime = System.currentTimeMillis();

    // Get the one and only tgextra-crud stub.
    initTgextraCrudService(this.conf.getAAIcRUDsERVICEuRL());

    // Create register resource request.
    UnregisterResourceRequest params = new UnregisterResourceRequest();
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setUri(theUri.toString());
    params.setSecret(this.conf.getAAIsPECIALsECRET());

    logStartParameters(meth, theSessionId, theLogParameter, "", theUri);

    try {
      if (!(tgextraCrud.unregisterResource(params).isResult())) {
        throw new IoFault(ERROR_UNREGISTER + theUri.toString());
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Sucessfully unregistered: " + theUri);

    } catch (WebServiceException e) {
      // TODO Remove workaround for TG-635, if fixed!!
      if (e.getMessage().contains(TGEXTRA_UNKNOWNRESOURCE_STRING)) {
        throw CrudServiceExceptions.objectNotFoundFault(e, ERROR_UNKNOWNRESOURCE + ": " + theUri);
      }
      throw e;
    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_UNREGISTER);
    } catch (info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault e) {
      throw CrudServiceExceptions.objectNotFoundFault(e, ERROR_UNKNOWNRESOURCE + ": " + theUri);
    } catch (IoFault e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_UNREGISTER);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "tgextra query duration: "
        + CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime));
  }

  /**
   *
   */
  @Override
  public boolean resourceExists(String theSessionId, String theLogParameter, String theResource)
      throws AuthFault, IoFault {

    boolean result;

    String meth = UNIT_NAME + ".resourceExists()";
    long startTime = System.currentTimeMillis();

    // Get the one and only tgextra stub.
    initTgextraService(this.conf.getAAIsERVICEuRL());

    // Create checkAccess request.
    TgCheckAccessRequest params = new TgCheckAccessRequest();
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setOperation(OPERATION_CREATE);
    params.setResource(theResource);

    logStartParameters(meth, theSessionId, theLogParameter, OPERATION_CREATE, theResource);

    // Call TG-auth.
    try {
      // If access is granted or not, the resource is existing!
      tgextra.tgCheckAccess(params).isResult();
      result = true;
    } catch (UnknownResourceFault e) {
      // Return FALSE only if resource is NOT known!
      result = false;
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "tgextra query duration: "
        + CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime));

    return result;
  }

  /**
   *
   */
  @Override
  public void publish(String theSessionId, String theLogParameter, URI theUri)
      throws AuthFault, IoFault {

    String meth = UNIT_NAME + ".publish()";

    // Get the one and only tgextra-crud stub.
    initTgextraCrudService(this.conf.getAAIcRUDsERVICEuRL());

    // Create request.
    PublishRequest params = new PublishRequest();
    params.setSecret(this.conf.getAAIsPECIALsECRET());
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setResource(theUri.toASCIIString());

    logStartParameters(meth, theSessionId, theLogParameter, "", theUri);

    // Publish.
    try {
      if (!tgextraCrud.publish(params).isResult()) {
        throw new IoFault(ERROR_PUBLISH);
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Published: " + theUri);

    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_UNREGISTER);
    }
  }

  /**
   *
   */
  @Override
  public void nearlyPublish(String theSessionId, String theLogParameter, URI theUri)
      throws AuthFault, IoFault {

    String meth = UNIT_NAME + ".nearlyPublish()";

    // Get the one and only tgextra_crud stub.
    initTgextraCrudService(this.conf.getAAIcRUDsERVICEuRL());

    // Create NearlyPublish request.
    NearlyPublishRequest params = new NearlyPublishRequest();
    params.setSecret(this.conf.getAAIsPECIALsECRET());
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);
    params.setResource(theUri.toASCIIString());

    logStartParameters(meth, theSessionId, theLogParameter, "", theUri.toString());

    // Do (nearly) publish.
    try {
      // FIXME Why can be and when gets this result FALSE???
      if (!tgextraCrud.nearlyPublish(params).isResult()) {
        throw new IoFault(ERROR_PUBLISH);
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "(Nearly) published: " + theUri);

    } catch (info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_UNREGISTER);
    } catch (info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault e) {
      throw CrudServiceExceptions.authFault(e, ERROR_UNREGISTER);
    }
  }

  /**
   *
   */
  @Override
  public void checkRight(String theSessionId, String theLogParameter,
      String theProjectId, String theRight) throws AuthFault, IoFault {

    String meth = UNIT_NAME + ".checkRight()";

    // Get the one and only tgextra stub.
    initTgextraService(this.conf.getAAIsERVICEuRL());

    // Create request.
    TgAssignedRolesRequest params = new TgAssignedRolesRequest();
    params.setAuth(theSessionId);
    params.setLog(theLogParameter);

    logStartParameters(meth, theSessionId, theLogParameter, "", "");

    // Get response and loop.
    RolesetResponse response = tgextra.tgAssignedRoles(params);
    String message = "";
    if (response.getRole() != null) {
      for (String roleString : response.getRole()) {

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "getRole(): " + roleString);

        // Assemble single role array.
        String roleArray[] = roleString.split(",");
        String role = roleArray[0];
        String project = roleArray[1];
        // String baseRole = roleArray[2];

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
            "Role: " + role + ", project: " + project);

        // RIGHT_GETURI: Check if the user assigned to the session ID has the role "Editor" (for the
        // time being, later we can check some specific rights to get URIs, if needed).
        if (theRight.equals(RIGHT_GETURI)) {
          if (role.equals(ROLE_EDITOR)) {
            return;
          } else {
            message =
                "User is NOT allowed to " + RIGHT_GETURI + " for project " + theProjectId + "!";
            CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, message);
          }
        }

        // RIGHT_PUBLISH: Check if the user has the role PROJECT MANAGER in a specific project.
        else if (theRight.equals(RIGHT_PUBLISH)) {
          if (role.equals(ROLE_PROJECTMANAGER) && project.equals(theProjectId)) {
            return;
          } else {
            message =
                "User is NOT allowed to " + RIGHT_PUBLISH + " in project " + theProjectId + "!";
            CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, message);
          }
        }

        // NO_RIGHT: Just go to error mode.
        else {
          message = "User is NOT allowed to " + RIGHT_PUBLISH + " in project " + theProjectId + "!";
          CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, message);
        }
      }
    }

    // RolesetResponse is null (or empty?), means no roles are assigned.
    throw CrudServiceExceptions.authFault(ERROR_ACCESSDENIED + ": " + message);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Initialises a regular TG-extra stub.
   * </p>
   * 
   * @param theEndpoint
   */
  private static void initTgextraService(URL theEndpoint) {

    String meth = UNIT_NAME + ".initTgextraService()";

    if (tgextra == null) {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "TG-extra endpoint: " + getCompleteEndpointUrl(theEndpoint));

      // Create TG-extra service stubs.
      tgextra = TGAuthClientUtilities.getTgextra(theEndpoint);

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "TG-auth stub created");
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Using existing TG-auth stub");
    }
  }

  /**
   * <p>
   * Initialises the TG-extra CRUD stub.
   * </p>
   * 
   * @param theEndpoint
   */
  private static void initTgextraCrudService(URL theEndpoint) {

    String meth = UNIT_NAME + ".initTgextraCrudService()";

    if (tgextraCrud == null) {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "TG-extra CRUD endpoint: " + getCompleteEndpointUrl(theEndpoint));

      // Create TG-extra service stubs.
      tgextraCrud = TGAuthClientCrudUtilities.getTgextraCrud(theEndpoint);

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "TG-auth CRUD stub created");
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Using existing TG-auth CRUD stub");
    }
  }

  /**
   * @param theEndpoint
   * @return The TG-extra endpoint URL.
   */
  private static String getCompleteEndpointUrl(URL theEndpoint) {
    return theEndpoint.getProtocol() + "://" + theEndpoint.getHost()
        + (theEndpoint.getPort() == -1 ? "" : ":" + theEndpoint.getPort()) + theEndpoint.getFile();
  }

  /**
   * @param theSessionId
   * @param theLogParameter
   * @param theOperation
   * @param theResource
   */
  private static void logStartParameters(String theMethod, String theSessionId,
      String theLogParameter, String theOperation, String theResource) {
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod,
        "Session ID: " + TGCrudServiceUtilities.purgeSid(theSessionId));
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod,
        "Log parameter: " + theLogParameter);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod, "Operation: " + theOperation);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod, "Resource: " + theResource);
  }

  /**
   * @param theMethod
   * @param theSessionId
   * @param theLogParameter
   * @param theResource
   * @param theUri
   */
  private static void logStartParameters(String theMethod, String theSessionId,
      String theLogParameter, String theResource, URI theUri) {
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod,
        "Session ID: " + TGCrudServiceUtilities.purgeSid(theSessionId));
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod,
        "Log parameter: " + theLogParameter);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod, "URI: " + theUri.toString());
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, theMethod, "Resource: " + theResource);
  }

}
