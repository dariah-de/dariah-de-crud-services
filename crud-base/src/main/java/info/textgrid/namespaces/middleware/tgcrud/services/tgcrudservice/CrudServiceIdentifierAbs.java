/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.util.HashMap;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2014-12-22 - Funk - Added setKeyValuePairs() method.
 * 
 * 2010-09-04 - Funk - Adapted to the new metadata schema.
 * 
 * 2010-06-28 - Funk - Added missing interface methods.
 * 
 * 2010-06-24 - Funk - First version.
 */

/**
 * <p>
 * Abstract class for the CRUD services Identifier implementation.
 * </p>
 * 
 * <p>
 * <b>NOTE</b> If you really want to use this implementation, please <b>DO REMOVE ANY SPECIAL
 * CHARACTERS</b> first!
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2021-04-08
 * @since 2010-06-24
 */

public abstract class CrudServiceIdentifierAbs implements CrudServiceIdentifier {

  // **
  // STATIC FINALS
  // **

  protected static final String NOT_IMPLEMENTED = "Not implemented";

  // **
  // INSTANCE VARIABLES
  // **

  protected CrudServiceConfigurator conf = null;
  protected HashMap<String, String> keyValueMap = new HashMap<String, String>();

  // **
  // ** GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public void setKeyValuePairs(HashMap<String, String> theKeyValueMap) throws IoFault {
    this.keyValueMap = theKeyValueMap;
  }

  /**
   *
   */
  @Override
  public CrudServiceConfigurator getConfiguration() {
    return this.conf;
  }

  /**
   *
   */
  @Override
  public void setConfiguration(CrudServiceConfigurator theConfiguration) {
    this.conf = theConfiguration;
  }

}
