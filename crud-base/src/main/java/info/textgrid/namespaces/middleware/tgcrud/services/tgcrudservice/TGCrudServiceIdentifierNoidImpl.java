/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.xml.bind.DatatypeConverter;
import org.apache.http.HttpStatus;

/**
 * TODOLOG
 * 
 * TODO Implement key/value pair binding using the method setKeyValuePairs()!
 * 
 **
 * CHANGELOG
 * 
 * 
 * 2023-09-08 - Funk - Remove TGHttpClients.
 * 
 * 2020-11-04 - Funk - Add synchronized blocks in locking methods.
 * 
 * 2016-06-29 - Funk - Added some finals. Fixed bug if only gotten 1 URI from the NOID and
 * (possibly) no "/n" in the result.
 * 
 * 2015-02-17 - Funk - Added some logging for external locking and unlocking.
 * 
 * 2013-10-15 - Funk - Added public methods for binding keys to values.
 * 
 * 2013-10-11 - Funk - Improved falsely implemented internal locking method. Now it should work just
 * fine!
 * 
 * 2012-02-15 - Funk - Locking'n'unlocking completed.
 * 
 * 2012-02-06 - Funk - Minor changes and added static HTTP client.
 * 
 * 2012-01-26 - Funk - Adapted HTTP clients.
 * 
 * 2011-05-11 - Funk - Fixed bug TG-(anything). URIs are stripped now before locking and unlocking.
 * 
 * 2011-03-28 - Funk - Using Ubbo's TGHttpClient now.
 * 
 * 2010-09-03 - Funk - First version.
 */

/**
 * <p>
 * This URI implementation creates NOID URIs from a NOID HTTP service.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-09-08
 * @since 2010-09-03
 */
public class TGCrudServiceIdentifierNoidImpl extends CrudServiceIdentifierAbs {

  // **
  // FINALS
  // **

  protected static final String UNIT_NAME = "noid";
  protected static final String NOID_ERROR = "NOID service status code is not OK";
  protected static final String NOID_FAILURE = "Failed to query NOID service";
  protected static final String NOID_BIND_SET = "set";
  protected static final String ERROR_TEXT = "error";
  protected static final char USER_TIMESTAMP_SEPARATON_CHAR = '@';

  private static final String NOID_BIND_NEW = "new";
  private static final String NOID_BIND_DELETE = "delete";
  private static final String LOCKING_KEY = "lockex";
  private static final String LOCKING_KEY_INTERNAL = "locked";
  private static final String EXISTING_URI_KEY = "Circ";
  private static final String EXISTING_URI_VALUE = "uncirculated";
  private static final String TRUE = "true";
  private static final String DEFAULT_NOID_ENCODING = "UTF-8";
  private static final String NO_VALUE = null;

  // **
  // CLASS VARIABLES
  // **

  protected static volatile Client httpClient;

  // **
  // IMPLEMENTED METHODS
  // **

  /**
  *
  */
  @Override
  public List<URI> getUris(final int theAmount) throws IoFault {

    String meth = UNIT_NAME + ".getUris()";

    List<URI> result = new ArrayList<URI>();

    // Initialise HTTP client.
    initHttpClient();

    // Assemble query string.
    String queryParameter = "mint+" + Integer.toString(theAmount);

    // Execute and check status code.
    WebTarget target =
        httpClient.target(this.conf.getIDsERVICEuRL().toString()).queryParam(queryParameter);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Query  URI: " + target.getUri().toString());

    try (Response response = target.request().headers(getAuthHeaders()).get()) {
      int statusCode = response.getStatus();

      if (statusCode == HttpStatus.SC_OK) {
        String resultBody = response.readEntity(String.class);

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "NOID result: " + resultBody);

        // Remove all the "id: "'s.
        resultBody = resultBody.replaceAll("id: ", "");

        // Check for amount. If only one URI needed, do not split!
        if (theAmount == 1) {
          result.add(URI.create(resultBody.trim()));
        }
        // More than one URI needed: Do split!
        else if (theAmount > 1) {
          String resultArray[] = resultBody.split("\n");
          for (int i = 0; i < resultArray.length; i++) {
            result.add(URI.create(resultArray[i].trim()));
          }
        }
        // This should never happen :-) Please call me if so!
        else {
          throw new IOException(NOID_FAILURE + " [" + response + "]: " + statusCode);
        }

      } else {
        throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
      }
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, NOID_FAILURE);
    }

    return result;
  }

  /**
   * <p>
   * Checks if the given URI is already existing.
   * </p>
   * 
   * <p>
   * http://textgrid-java2.sub.uni-goettingen.de/nd/noidu_textgrid?fetch+ textgrid:2sd5+.0
   * </p>
   * 
   * <p>
   * Returns FALSE, if one result line starts with ":" and contains "uncirculated", as the NOID
   * delivers (via fetch only) the following with unminted IDs: "Circ: uncirculated".
   * 
   * Returns TRUE otherwise. NOID delivers something like "Circ: i|20100604114249|@134.76.20.84
   * www-data/www-data|26163" with already minted IDs.
   * </p>
   */
  @Override
  public boolean uriExists(final URI theUri) throws IoFault {

    Map<String, String> resultMap = fetchNoidData(theUri);

    if (resultMap.containsKey(EXISTING_URI_KEY)
        && resultMap.get(EXISTING_URI_KEY).equals(EXISTING_URI_VALUE)) {
      return false;
    }

    return true;
  }

  /**
   * <p>
   * Locks URIs via NOID bind+new or NOID bind+set, depending on the user ID and automagic unlocking
   * time. If an URI is not yet locked, any user is able to lock this URI. Only a user, who has
   * locked the object in the first place, can re-lock, OR any user if the unlocking time has
   * passed. To keep an object locked, the user has to re-lock before the unlocking time has
   * elapsed.
   * </p>
   * 
   * <p>
   * [host]/nd/noidu_textgrid?bind+new+textgrid:2sd5+[LOCKING_KEY].0+[user].[timestamp]
   * </p>
   */
  @Override
  public boolean lock(final URI theUri, final String theUser) throws IoFault {

    String meth = UNIT_NAME + ".lock()";

    // Get revision key.
    String revisionKey = getRevisionKey(theUri);

    // First get existing URI values using a NOID fetch and look if key is
    // contained.
    Map<String, String> resultMap = fetchNoidData(theUri);
    boolean keyContained = resultMap != null && resultMap.containsKey(revisionKey);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Result map contains key: "
        + revisionKey + " > " + (keyContained ? resultMap.get(revisionKey) : "NO"));

    Response response = null;
    try {
      // If the key is not contained (means the URI is not yet locked), just do lock using bind+new!
      if (!keyContained) {
        response = noidBind(theUri, NOID_BIND_NEW, theUser, revisionKey);
      }

      // If key is contained, check locking time and user.
      else {
        synchronized (this) {
          // Compute locking duration (in millis), parse locking value first.
          Long lockingValue = Long.parseLong(resultMap.get(revisionKey).substring(
              resultMap.get(revisionKey).lastIndexOf(USER_TIMESTAMP_SEPARATON_CHAR) + 1));
          Long lockingDuration = System.currentTimeMillis() - lockingValue;

          // Some locking logging :-D
          String lockingDurationString = CrudServiceUtilities.getDuration(lockingDuration);
          long stillLocked = this.conf.getIDaUTOMAGICuNLOCKINGtIME() - lockingDuration;
          String stillLockedString = CrudServiceUtilities.getDuration(stillLocked);

          // Do log.
          String message = "Locked since " + lockingDurationString + " by " + theUser;
          if (stillLocked > 0) {
            message += ", automagically unlocked in " + stillLockedString;
          } else {
            message += ", automagically unlocking NOW!";
          }
          message += " [" + lockingDuration + ">" + this.conf.getIDaUTOMAGICuNLOCKINGtIME() + "]";
          CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, message);

          // Check if user is the one who locked this URI in the first place or URI has been locked
          // longer than permitted without re-locking. If so, re-lock with bind+set.
          if (resultMap.get(revisionKey).startsWith(theUser)
              || lockingDuration > this.conf.getIDaUTOMAGICuNLOCKINGtIME()) {
            response = noidBind(theUri, NOID_BIND_SET, theUser, revisionKey);
          }

          // Otherwise no locking is allowed!
          else {
            return false;
          }
        }
      }

      // Check status code.
      int statusCode = response.getStatus();
      if (statusCode == HttpStatus.SC_OK) {
        String resultBody = response.readEntity(String.class);

        // Parse result.
        if (resultBody.startsWith(ERROR_TEXT)) {
          return false;
        }
      } else {
        throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
      }

    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, NOID_FAILURE);
    } finally {
      if (response != null) {
        response.close();
      }
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Locking complete: " + theUri);

    return true;
  }

  /**
   * <p>
   * Returns the user ID of the user who locked this URI, if locked, or an empty string if URI is
   * not locked (and if the automagic unlocking time has not exceeded already) .
   * </p>
   */
  @Override
  public String isLockedBy(final URI theUri) throws IoFault {

    String meth = UNIT_NAME + ".isLockedBy()";

    String result = "";

    // Get revision key.
    String revisionKey = getRevisionKey(theUri);

    // First get existing URI values using a NOID fetch and look if key is contained.
    Map<String, String> resultMap = fetchNoidData(theUri);
    boolean keyContained = resultMap != null && resultMap.containsKey(revisionKey);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Result map contains key: "
        + revisionKey + " > " + (keyContained ? resultMap.get(revisionKey) : "NO"));

    // If key is contained, check locking time and user.
    if (keyContained) {
      // Compute locking duration (in millis), parse locking value first.
      Long lockingValue = Long.parseLong(resultMap.get(revisionKey)
          .substring(resultMap.get(revisionKey).lastIndexOf(USER_TIMESTAMP_SEPARATON_CHAR) + 1));
      Long lockingDuration = System.currentTimeMillis() - lockingValue;

      // Some locking logging :-D
      String lockingDurationString = CrudServiceUtilities.getDuration(lockingDuration);
      long stillLocked = this.conf.getIDaUTOMAGICuNLOCKINGtIME() - lockingDuration;
      String stillLockedString = CrudServiceUtilities.getDuration(stillLocked);

      // Do log.
      String message = "Locked since " + lockingDurationString;
      if (stillLocked > 0) {
        message += ", automagically unlocked in " + stillLockedString;
      } else {
        message += ", no external lock is existing anymore";
      }

      // Get the user ID who has a lock on this URI AND check if the URI has NOT BEEN locked longer
      // than permitted without re-locking. If so, the URI is still locked!
      String value = resultMap.get(revisionKey);
      if (lockingDuration < this.conf.getIDaUTOMAGICuNLOCKINGtIME()) {
        result = value.substring(0, value.lastIndexOf(USER_TIMESTAMP_SEPARATON_CHAR)).trim();

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
            "Locked by user with ID: " + result);

        message += " [" + lockingDuration + "<" + this.conf.getIDaUTOMAGICuNLOCKINGtIME() + "]";
      } else {
        message += " [" + lockingDuration + ">" + this.conf.getIDaUTOMAGICuNLOCKINGtIME() + "]";
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, message);
    }

    return result;
  }

  /**
   * <p>
   * Just lock internally. No further functions are included.
   * </p>
   * 
   * <p>
   * [host]/nd/noidu_textgrid?bind+new+textgrid:2sd5+[LOCKING_KEY_INTERNAL].0+[TRUE]
   * </p>
   */
  @Override
  public boolean lockInternal(final URI theUri) throws IoFault {

    String meth = UNIT_NAME + ".lockInternal()";

    // Get revision key.
    String revisionKey = getRevisionKeyInternal(theUri);

    // First get existing URI values using a NOID fetch and look if key is contained.
    Map<String, String> resultMap = fetchNoidData(theUri);
    boolean keyContained = resultMap != null && resultMap.containsKey(revisionKey);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Result map contains key: "
        + revisionKey + " > " + (keyContained ? resultMap.get(revisionKey) : "NO"));

    // If the key is contained (means the URI is locked), return false.
    if (keyContained) {
      return false;
    }
    // If the key is not contained (means the URI is not yet locked), just do lock using bind+new!
    else {
      synchronized (this) {
        try (Response response = noidBindInternal(theUri, NOID_BIND_NEW, revisionKey, TRUE)) {

          // Check status code.
          int statusCode = response.getStatus();
          if (statusCode == HttpStatus.SC_OK) {
            String resultBody = response.readEntity(String.class);

            // Parse result.
            if (resultBody.startsWith(ERROR_TEXT)) {
              return false;
            }
          } else {
            throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
          }
        } catch (IOException e) {
          throw CrudServiceExceptions.ioFault(e, NOID_FAILURE);
        }
      }
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Locking complete: " + theUri);

    return true;
  }

  /**
   * <p>
   * Unlocks an URI via NOID delete. Unlocking is permitted for the user who has locked the object,
   * and for any user, if the automagic unlocking time has elapsed.
   * </p>
   * 
   * <p>
   * [host]/nd/noidu_textgrid?bind+delete+textgrid:2sd1+[LOCKING_KEY].0
   * </p>
   */
  @Override
  public boolean unlock(final URI theUri, final String theUser)
      throws IoFault {

    String meth = UNIT_NAME + ".unlock()";

    // Get revision key.
    String revisionKey = getRevisionKey(theUri);

    // First get existing URI values using a NOID fetch and look if key is contained.
    Map<String, String> resultMap = fetchNoidData(theUri);
    boolean keyContained = resultMap != null && resultMap.containsKey(revisionKey);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Result map contains key: "
        + revisionKey + " > " + (keyContained ? resultMap.get(revisionKey) : "NO"));

    // If the key is not contained (means the URI is not yet locked), nothing is to unlock.
    if (!keyContained) {
      return true;
    }

    // If key is contained, check locking time and user.
    else {
      // Compute locking duration (in millis), parse locking value first.
      Long lockingValue = Long.parseLong(resultMap.get(revisionKey)
          .substring(resultMap.get(revisionKey).lastIndexOf(USER_TIMESTAMP_SEPARATON_CHAR) + 1));
      Long lockingDuration = System.currentTimeMillis() - lockingValue;

      // Some locking logging :-D
      String lockingDurationString = CrudServiceUtilities.getDuration(lockingDuration);
      long stillLocked = this.conf.getIDaUTOMAGICuNLOCKINGtIME() - lockingDuration;
      String stillLockedString = CrudServiceUtilities.getDuration(stillLocked);

      // Do log.
      String message = "Locked since " + lockingDurationString + " by " + theUser;
      if (stillLocked > 0) {
        message += ", automagically unlocked in " + stillLockedString + " [" + lockingDuration + "<"
            + this.conf.getIDaUTOMAGICuNLOCKINGtIME() + "]";
      } else {
        message += ", automagically unlocking NOW! [" + lockingDuration + ">"
            + this.conf.getIDaUTOMAGICuNLOCKINGtIME() + "]";
      }
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, message);

      // Check if user is the one who locked this URI in the first place or URI has been locked
      // longer than permitted without re-locking. If so, do unlock.
      if (resultMap.get(revisionKey).startsWith(theUser)
          || lockingDuration > this.conf.getIDaUTOMAGICuNLOCKINGtIME()) {
        try (Response response = noidBind(theUri, NOID_BIND_DELETE, theUser, revisionKey)) {

          // Check status code.
          int statusCode = response.getStatus();
          if (statusCode != HttpStatus.SC_OK) {
            throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
          }

          CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
              "Unlocking complete: " + theUri);

        } catch (IOException e) {
          throw CrudServiceExceptions.ioFault(e, NOID_FAILURE);
        }

        return true;
      }

      // Otherwise no unlocking is allowed!
      else {
        return false;
      }
    }
  }

  /**
   * <p>
   * Unlocks an URI via NOID delete, only for TG-crud internal locks.
   * </p>
   * 
   * <p>
   * [host]/nd/noidu_textgrid?bind+delete+textgrid:2sd1+locked.0
   * </p>
   */
  @Override
  public boolean unlockInternal(final URI theUri) throws IoFault {

    String meth = UNIT_NAME + ".unlockInternal()";

    // Get revision key.
    String revisionKey = getRevisionKeyInternal(theUri);

    // First get existing URI values using a NOID fetch and look if key is contained.
    Map<String, String> resultMap = fetchNoidData(theUri);
    boolean keyContained = resultMap != null && resultMap.containsKey(revisionKey);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Result map contains key: "
        + revisionKey + " > " + (keyContained ? resultMap.get(revisionKey) : "NO"));

    // If the key is not contained (means the URI is not yet locked), nothing is to unlock.
    if (!keyContained) {
      return true;
    }

    // If key is contained, just do unlock.
    else {
      try (Response response = noidBindInternal(theUri, NOID_BIND_DELETE, revisionKey, NO_VALUE)) {
        // Check status code.
        int statusCode = response.getStatus();
        if (statusCode != HttpStatus.SC_OK) {
          throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
        }

        CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Unlocking complete: " + theUri);

      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, NOID_FAILURE);
      }

      return true;
    }

    // TODO We always return TRUE here! Why is that? --> Remove boolean return value?
  }

  /**
   *
   */
  @Override
  public void setKeyValuePairs(final HashMap<String, String> theKeyValueMap) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  // **
  // OTHER PUBLIC METHODS
  // **

  /**
   * <p>
   * Binds a key/value pair to the NOIDs given identifier.
   * </p>
   * 
   * @param theUri
   * @param theKey
   * @param theValue
   * @throws IoFault
   * @throws IOException
   */
  public void bindSet(final URI theUri, final String theKey, final String theValue)
      throws IoFault, IOException {

    // Call bind new.
    Response response = noidBindInternal(theUri, NOID_BIND_NEW, theKey, theValue);

    // Check status code.
    int statusCode = response.getStatus();
    if (statusCode != HttpStatus.SC_OK) {
      throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
    }
  }

  /**
   * <p>
   * Gets a value for an existing key.
   * </p>
   * 
   * @param theUri
   * @param theKey
   * @return The value for a given key.
   * @throws IoFault
   */
  public String bindGet(final URI theUri, final String theKey) throws IoFault {
    return fetchNoidData(theUri).get(theKey);
  }

  /**
   * <p>
   * Deletes an existing key.
   * </p>
   * 
   * @param theUri
   * @param theKey
   * @throws IOException
   * @throws IoFault
   */
  public void bindDelete(final URI theUri, final String theKey) throws IoFault, IOException {

    // Call bind delete.
    Response response = noidBindInternal(theUri, NOID_BIND_DELETE, theKey, NO_VALUE);

    // Check status code.
    int statusCode = response.getStatus();
    if (statusCode != HttpStatus.SC_OK) {
      throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
    }

  }

  /**
   *
   */
  @Override
  public void updatePidMetadata(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String getHandleMetadata(URI theUri, String theType) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Initialise the one and only HTTP client.
   * </p>
   * 
   * @throws IoFault
   */
  private static void initHttpClient() throws IoFault {

    String meth = UNIT_NAME + ".initHttpClient()";

    if (httpClient == null) {
      httpClient = ClientBuilder.newClient().property("thread.safe.client", "true");

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "New HTTP client created: ");
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Using existing HTTP client");
    }
  }

  /**
   * <p>
   * Does a NOID fetch via HTTP.
   * </p>
   * 
   * @param theUri
   * @return A Map of strings containing the data from a NOID fetch.
   * @throws IoFault
   */
  protected Map<String, String> fetchNoidData(URI theUri) throws IoFault {

    String meth = UNIT_NAME + ".fetchNoidData()";

    // Initialise HTTP client.
    initHttpClient();

    // Assemble query string depending on theUri.
    String queryParameter = "fetch+";
    if (TGCrudServiceUtilities.isBaseUri(theUri)) {
      queryParameter += theUri.toASCIIString();
    } else {
      queryParameter += TGCrudServiceUtilities.stripRevision(theUri);
    }

    WebTarget target =
        httpClient.target(this.conf.getIDsERVICEuRL().toString()).queryParam(queryParameter);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Query  URI: " + target.getUri().toString());

    // Execute and check status code.
    try (Response response = target.request().headers(getAuthHeaders()).get()) {
      int statusCode = response.getStatus();

      if (statusCode == HttpStatus.SC_OK) {
        String resultBody = response.readEntity(String.class);

        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
            "Result body: '" + resultBody.trim() + "'");

        // Parse result.
        return parseNoidResult(resultBody);

      } else {
        throw CrudServiceExceptions.ioFault(NOID_ERROR + ": " + statusCode);
      }
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, NOID_FAILURE);
    }
  }

  /**
   * <p>
   * Calls a NOID bind with configurable method (e.g. "set" or "new").
   * </p>
   * 
   * @param theUri
   * @param theMethod
   * @param theUser
   * @param theKey
   * @return A HTTP response of a NOID bind.
   * @throws IoFault
   * @throws IOException
   */
  private Response noidBind(URI theUri, String theMethod, String theUser, String theKey)
      throws IoFault, IOException {

    Response result;

    String meth = UNIT_NAME + ".noidBind()";

    // Initialise HTTP client.
    initHttpClient();

    // Check if we have a base URI.
    if (TGCrudServiceUtilities.isBaseUri(theUri)) {
      throw CrudServiceExceptions.ioFault("Base URIs cannot be locked!");
    }

    // Get revision.
    URI strippedUri = TGCrudServiceUtilities.stripRevision(theUri);

    // Assemble query string.
    String queryParameter = "bind+" + theMethod + "+" + strippedUri + "+" + theKey;
    if (!theMethod.equals(NOID_BIND_DELETE)) {
      queryParameter += "+" + theUser + USER_TIMESTAMP_SEPARATON_CHAR + System.currentTimeMillis();
    }

    WebTarget target =
        httpClient.target(this.conf.getIDsERVICEuRL().toString()).queryParam(queryParameter);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Query  URI: " + target.getUri().toString());

    result = target.request().headers(getAuthHeaders()).get();

    return result;
  }

  /**
   * <p>
   * Calls a NOID bind with configurable method (e.g. "set" or "new"), but no user ID.
   * </p>
   * 
   * @param theUri
   * @param theMethod
   * @param theKey
   * @param theValue
   * @return A HTTP response of a NOID bind.
   * @throws IoFault
   * @throws IOException
   */
  private Response noidBindInternal(URI theUri, String theMethod, String theKey,
      String theValue) throws IoFault, IOException {

    Response result;

    String meth = UNIT_NAME + ".noidBindInternal()";

    // Initialise HTTP client.
    initHttpClient();

    // Check if we have a base URI.
    if (TGCrudServiceUtilities.isBaseUri(theUri)) {
      throw CrudServiceExceptions.ioFault("Base URIs cannot be locked!");
    }

    // Get revision.
    URI strippedUri = TGCrudServiceUtilities.stripRevision(theUri);

    // Assemble query string.
    String queryParameter = "bind+" + theMethod + "+" + strippedUri + "+" + theKey;
    if (!theMethod.equals(NOID_BIND_DELETE)) {
      queryParameter += "+" + theValue;
    }

    WebTarget target =
        httpClient.target(this.conf.getIDsERVICEuRL().toString()).queryParam(queryParameter);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Query  URI: " + target.getUri().toString());

    result = target.request().headers(getAuthHeaders()).get();

    return result;
  }

  /**
   * <p>
   * Parses a NOID result including the following: "Circ: i|20100604114249|@134.76.20.84
   * www-data/www-data|26163" and returns a Map containing the results in key/value pairs.
   * </p>
   * 
   * @param theBody
   * @return A Map of strings as result of a NOID command.
   * @throws UnsupportedEncodingException
   */
  private static Map<String, String> parseNoidResult(String theBody)
      throws UnsupportedEncodingException {

    String meth = UNIT_NAME + ".parseNoidResult()";

    Map<String, String> result = new HashMap<String, String>();

    String resultArray[] = theBody.split("\n");
    for (int i = 0; i < resultArray.length; i++) {
      String keyValue[] = resultArray[i].split(":\\s+");

      // Decode from URL encoded HTTP request (resolving TG-1876).
      keyValue[0] = URLDecoder.decode(keyValue[0], DEFAULT_NOID_ENCODING).trim();
      keyValue[1] = URLDecoder.decode(keyValue[1], DEFAULT_NOID_ENCODING).trim();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "NOID key/values: '" + keyValue[0] + "' > '" + keyValue[1] + "'");

      result.put(keyValue[0], keyValue[1]);
    }

    return result;
  }

  /**
   * <p>
   * Computes the NOID locking key for the given URI.
   * </p>
   * 
   * @param theUri
   * @return The revision key for a given TextGrid URI.
   */
  protected static String getRevisionKey(final URI theUri) {
    return LOCKING_KEY + REVISION_SEPARATION_CHAR + TGCrudServiceUtilities.getRevision(theUri);
  }

  /**
   * <p>
   * Computes the NOID locking key for the given URI for internal locks.
   * </p>
   * 
   * @param theUri
   * @return The internal revision key for a given TextGrid URI.
   */
  protected static String getRevisionKeyInternal(final URI theUri) {
    return LOCKING_KEY_INTERNAL + REVISION_SEPARATION_CHAR
        + TGCrudServiceUtilities.getRevision(theUri);
  }

  /**
   * @return
   * @throws IoFault
   */
  private MultivaluedMap<String, Object> getAuthHeaders() throws IoFault {

    MultivaluedMap<String, Object> result = new MultivaluedHashMap<String, Object>();

    String user = this.conf.getIDsERVICEuSER();
    String pass = this.conf.getIDsERVICEpASS();

    try {
      String token = user + ":" + pass;
      String auth = "BASIC " + DatatypeConverter.printBase64Binary(token.getBytes("UTF-8"));

      result.add("Authorization", auth);

    } catch (UnsupportedEncodingException e) {
      throw new IoFault(e.getMessage());
    }

    return result;
  }

}
