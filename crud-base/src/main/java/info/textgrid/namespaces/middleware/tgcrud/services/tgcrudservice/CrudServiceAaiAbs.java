/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 *
 * 2011-01-19 - Funk - Changed the Tgextra stub to be static and get it as a Singleton.
 * 
 * 2010-09-07 - Funk - First version.
 * 
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * Abstract class for the TG-crud security implementation.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2018-06-13
 * @since 2010-09-07
 ******************************************************************************/

public abstract class CrudServiceAaiAbs implements CrudServiceAai {

  // **
  // STATIC FINALS
  // **

  public static final String NOT_IMPLEMENTED = "Not implemented";

  // **
  // INSTANCE VARIABLES
  // **

  protected CrudServiceConfigurator conf = null;

  // **
  // GETTERS AND SETTERS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * TGCrudServiceAai#getConfiguration()
   */
  @Override
  public CrudServiceConfigurator getConfiguration() {
    return this.conf;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. TGCrudServiceAai
   * #setConfiguration(info.textgrid.namespaces.middleware.tgcrud
   * .services.tgcrudservice.TGCrudServiceConfigurator)
   */
  @Override
  public void setConfiguration(CrudServiceConfigurator theConfiguration) {
    this.conf = theConfiguration;
  }

}
