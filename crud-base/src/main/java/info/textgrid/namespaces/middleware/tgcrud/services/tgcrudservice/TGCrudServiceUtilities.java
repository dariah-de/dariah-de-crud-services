/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.input.CountingInputStream;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RiotException;
import org.codehaus.jettison.json.JSONException;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.CollectionType;
import info.textgrid.namespaces.metadata.core._2010.EditionType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Project;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.TextgridUri;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.ItemType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.metadata.core._2010.WorkType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.consistency.api.Checksum;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.consistency.api.Consistency;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.consistency.api.Filesize;
import jakarta.activation.DataHandler;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.ValidationEvent;
import jakarta.xml.bind.util.ValidationEventCollector;

/**
 * TODOLOG
 * 
 * TODO Generalise the class loading of the storage classes with the DHCrudServiceUtilities class!
 * 
 **
 * CHANGELOG
 * 
 * 2024-05-22 - Funk - Check if RDF data is invalid if generating from TextGrid metadata (#340).
 * 
 * 2023-04-24 - Funk - add TextGrid URI to metadata RDF part if resources are anonymous (issue#313)
 * 
 * 2022-02-10 - Funk - Refactor some logging methods.
 * 
 * 2021-11-23 - Funk - Remove deprecated closeQuietly().
 * 
 * 2021-06-21 - Funk - Add method for getting filename from URI and format.
 * 
 * 2015-03-09 - Funk - Slightly changed start method log and version log.
 */

/**
 * <p>
 * The TGCrudServiceUtilities class provides some utilities, needed by all the TGCrudService
 * classes.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-06-24
 * @since 2010-06-24
 */

public class TGCrudServiceUtilities extends CrudServiceUtilities {

  // **
  // STATIC FINALS
  // **

  protected static final String TEXTGRID_METADATA_SCHEMA_LOCATION =
      "https://textgridlab.org/schema/textgrid-metadata_2010.xsd";

  protected static final String RDF_NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  protected static final String RDF_OWL_SAMEAS = "http://www.w3.org/2002/07/owl#sameAs";
  protected static final String RDF_BOOLEAN = "http://www.w3.org/2001/XMLSchema#boolean";
  protected static final String RDF_AGGREGATES = "http://www.openarchives.org/ore/terms/aggregates";
  protected static final String RDF_DC_FORMAT = "http://purl.org/dc/elements/1.1/format";

  protected static final String REL_INPROJECT = "#inProject";
  protected static final String REL_ISDELETED = "#isDeleted";
  protected static final String REL_HASADAPTOR = "#hasAdaptor";
  protected static final String REL_HASSCHEMA = "#hasSchema";
  protected static final String REL_ISDERIVEDFROM = "#isDerivedFrom";
  protected static final String REL_ISALTERNATIVEFORMATOF = "#isAlternativeFormatOf";
  protected static final String REL_ISBASEURIOF = "#isBaseUriOf";
  protected static final String REL_ISNEARLYUBLISHED = "#isNearlyPublished";
  protected static final String REL_HASPID = "#hasPid";
  protected static final String REL_ISEDITIONOF = "#isEditionOf";
  protected static final String REL_OPEN = "<";
  protected static final String REL_CLOSE = ">";
  protected static final String REL_CLOSEOPEN = "> <";
  protected static final String REL_CLOSEEND = "> .";

  protected static final String RDF_TAG = "RDF";
  protected static final String MODEL_RDF = "RDF/XML";
  protected static final String MODEL_NTRIPLE = "N-TRIPLE";

  private static final String ERROR_PARSING_METADATA = "Error parsing metadata! ";
  private static final String VALIDATION_ISSUE = "metadata validation issue: ";

  public static final String GET_ALL_HANDLE_METADATA = "";
  public static final String HDL_HSALIAS = "HS_ALIAS";
  public static final String HDL_FILESIZE = "FILESIZE";
  public static final String HDL_CHECKSUM = "CHECKSUM";

  private static final String NOT_IN_METADATA = "NOT_IN_METADATA";
  private static final String CHECKSUM_EMPTY_OR_NOT_FOUND = "NOT_FOUND";
  private static final int FILESIZE_EMPTY_OR_NOT_FOUND = -99;

  protected static final String STATUS_OK = "OK";
  protected static final String STATUS_ERROR = "ERROR";

  // **
  // STATIC
  // **

  private static Unmarshaller tgUnmarshaller = null;

  // **
  // METADATA UTILITIES
  // **

  /**
   * <p>
   * Creates a GeneratedType and sets the given values.
   * </p>
   * 
   * TODO Can we use some JAXB factory to create the type easier?
   * 
   * @param theCreatedDate
   * @param theLastModifiedDate
   * @param theUri
   * @param theRevision
   * @param theProjectId
   * @param theSize
   * @param theOwnerId
   * @param theProjectName
   * @return A fresh generated metadata type.
   * @throws IoFault
   */
  public static GeneratedType createGereratedType(long theCreatedDate, long theLastModifiedDate,
      URI theUri, int theRevision, String theProjectId, long theSize, URI theOwnerId,
      String theProjectName) throws IoFault {

    // Set dates, URI, project, and size.
    XMLGregorianCalendar cDate = getXMLGregorianCalendar(theCreatedDate);
    XMLGregorianCalendar lmDate = getXMLGregorianCalendar(theLastModifiedDate);

    TextgridUri tUri = new TextgridUri();
    tUri.setValue(theUri);

    Project project = new Project();
    project.setId(theProjectId);
    project.setValue(theProjectName);

    BigInteger size = new BigInteger(Long.toString(theSize));

    return createGereratedType(cDate, lmDate, tUri, theRevision, project, size, theOwnerId);
  }

  /**
   * <p>
   * Set generated things without type mapping.
   * </p>
   * 
   * @param theCreatedDate
   * @param theLastModifiedDate
   * @param theTextgridUri
   * @param theRevision
   * @param theProject
   * @param theSize
   * @param theOwnerId
   * @return A fresh generated metadata type.
   * @throws IoFault
   */
  public static GeneratedType createGereratedType(XMLGregorianCalendar theCreatedDate,
      XMLGregorianCalendar theLastModifiedDate, TextgridUri theTextgridUri, int theRevision,
      Project theProject, BigInteger theSize, URI theOwnerId) throws IoFault {

    GeneratedType result = new GeneratedType();

    // Set dates, URI, project, size, and owner.
    result.setCreated(theCreatedDate);
    result.setLastModified(theLastModifiedDate);
    result.setTextgridUri(theTextgridUri);
    result.setRevision(theRevision);
    result.setProject(theProject);
    result.setExtent(theSize);
    result.setDataContributor(theOwnerId);

    return result;
  }

  /**
   * <p>
   * Checks for an empty metadata element, and title or an empty format in the metadata object.
   * </p>
   * 
   * @param theMetadata
   * @return
   * @throws MetadataParseFault
   */
  public static List<Warning> checkMetadata(final MetadataContainerType theMetadata)
      throws MetadataParseFault {

    String meth = "checkMetadata()";

    serviceLog(CrudService.DEBUG, meth, "Preliminary metadata check");

    // Check for empty element.
    if (theMetadata == null) {
      throw CrudServiceExceptions.metadataParseFault(ERROR_PARSING_METADATA + "No metadata!");
    }
    // Check for empty <object> element.
    if (theMetadata.getObject() == null) {
      throw CrudServiceExceptions.metadataParseFault(ERROR_PARSING_METADATA
          + "No <object> tag found or root differs from <tgObjectMetadata>!");
    }
    // Check for GenericType.
    if (theMetadata.getObject().getGeneric() == null) {
      throw CrudServiceExceptions
          .metadataParseFault(ERROR_PARSING_METADATA + "No <generic> tag found!");
    }
    // Check for ProvidedType.
    if (theMetadata.getObject().getGeneric().getProvided() == null) {
      throw CrudServiceExceptions
          .metadataParseFault(ERROR_PARSING_METADATA + "No <provided> tag found!");
    }
    // Check for title.
    List<String> titles = theMetadata.getObject().getGeneric().getProvided().getTitle();
    if (titles == null || titles.isEmpty() || titles.get(0).equals("")) {
      throw CrudServiceExceptions
          .metadataParseFault(ERROR_PARSING_METADATA + "Please provide title!");
    }
    // Check for format.
    String format = theMetadata.getObject().getGeneric().getProvided().getFormat();
    if (format == null || format.equals("")) {
      throw CrudServiceExceptions
          .metadataParseFault(ERROR_PARSING_METADATA + "Please provide format!");
    }

    // If preliminary metadata check did not throw an exception, do validate the metadata and get
    // warnings for the metadata to return.
    return validateMetadata(theMetadata);
  }

  /**
   * <p>
   * Transfer a TextGrid metadata string into a TextGrid metadata container.
   * </p>
   * 
   * @param theMetadataString
   * @return A metadata container type from a metadata string.
   */
  public static MetadataContainerType createMetadataContainer(String theMetadataString) {
    return JAXB.unmarshal(new StringReader(theMetadataString), MetadataContainerType.class);
  }

  /**
   * <p>
   * Transfer a TextGrid metadata input stream into a TextGrid metadata container.
   * </p>
   * 
   * @param theMetadataStream
   * @return A metadata container type from a metadata input stream.
   */
  public static MetadataContainerType createMetadataContainer(InputStream theMetadataStream) {
    return JAXB.unmarshal(theMetadataStream, MetadataContainerType.class);
  }

  /**
   * <p>
   * Takes a TextGrid Metadata object and gives back an array of bytes.
   * </p>
   * 
   * @param theMetadata
   * @param theConfiguration
   * @return A byte array from a metadata container type.
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws UnsupportedEncodingException
   */
  public static byte[] getByteArray(MetadataContainerType theMetadata,
      CrudServiceConfigurator theConfiguration) throws IoFault, MetadataParseFault {

    try (StringWriter metadataWriter = new StringWriter()) {
      JAXB.marshal(theMetadata, metadataWriter);
      return metadataWriter.toString().getBytes(theConfiguration.getDEFAULTeNCODING());
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, CrudServiceExceptions.CONFIGURATION_ERROR_MESSAGE);
    }
  }

  /**
   * @param theMetadata
   * @param theConfigurator
   * @return A string of a TextGrid metadata type.
   * @throws MetadataParseFault
   * @throws IoFault
   */
  public static String getString(MetadataContainerType theMetadata,
      CrudServiceConfigurator theConfigurator) throws IoFault, MetadataParseFault {
    return new String(getByteArray(theMetadata, theConfigurator));
  }

  /**
   * <p>
   * Adds a warning message to the metadata.
   * </p>
   * 
   * @param theMetadata
   * @param theMessage
   */
  public static void addWarning(MetadataContainerType theMetadata, String theMessage) {
    Warning warning = new Warning();
    warning.setValue(theMessage);
    theMetadata.getObject().getGeneric().getGenerated().getWarning().add(warning);
  }

  // **
  // RELATION UTILITIES
  // **

  /**
   * <p>
   * Returns a string containing the nearlyPublished relation.
   * </p>
   * 
   * @throws IoFault
   */
  public static String assembleNearlyPublished(URI theUri,
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theConfiguration.getRDFDBnAMESPACE()
        + REL_ISNEARLYUBLISHED + REL_CLOSE + " \"true\"^^<" + RDF_BOOLEAN + REL_CLOSEEND;
  }

  /**
   * <p>
   * Returns the URI <deleted> TRUE relation.
   * </p>
   * 
   * @param theUri
   * @param theConfiguration
   * @return The deleted relation string (in an array list).
   * @throws IoFault
   */
  public static List<String> assembleDeletedRelation(URI theUri,
      CrudServiceConfigurator theConfiguration) throws IoFault {

    ArrayList<String> result = new ArrayList<String>();

    // Assemble isDeleted relation.
    result.add(
        REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theConfiguration.getRDFDBnAMESPACE()
            + REL_ISDELETED + REL_CLOSE + "\"true\"^^<" + RDF_BOOLEAN + REL_CLOSEEND);

    return result;
  }

  /**
   * <p>
   * Assembles all the relations from the relational metadata object, and more.
   * </p>
   * 
   * @param theUri
   * @param theTextgridRdfDbNamespace
   * @param theUriPrefix
   * @param theMetadata
   * @return All relations needed for #CREATE in a list of strings.
   * @throws IoFault
   */
  public static List<String> assembleRelationsForCreation(URI theUri,
      String theTextgridRdfDbNamespace, String theUriPrefix, MetadataContainerType theMetadata)
      throws IoFault {

    ArrayList<String> result = new ArrayList<String>();

    String meth = UNIT_NAME + ".assembleRelationsForCreation()";

    // **
    // Handle relations from metadata, if not empty.
    // **
    RelationType relation = theMetadata.getObject().getRelations();
    if (relation != null) {

      // **
      // Process generic RDF relation tag (only if valid RDF!).
      // **
      if (relation.getRDF() != null) {

        serviceLog(CrudService.INFO, meth, "Processing RDF from relation metadata");

        StringWriter rdfWriter = new StringWriter();
        JAXBElement<RdfType> rdfType = new JAXBElement<RdfType>(new QName(RDF_NAMESPACE, RDF_TAG),
            RdfType.class, relation.getRDF());

        // Only read the rdf:RDF tag.
        JAXB.marshal(rdfType, rdfWriter);

        // Create model to get N-TRIPLES: First put RDF/XML in, use TextGrid URI as base for all
        // triples, that do not have a base already (in RDF: about)!
        Model model = ModelFactory.createDefaultModel();
        try {
          model.read(new StringReader(rdfWriter.toString()), theUri.toASCIIString(), MODEL_RDF);

          // FIXME (i) Do we do that? Where? I the line above?
          serviceLog(CrudService.INFO, meth,
              "Add TextGrid URI as base for all base-less (about=\"\") triples!");

          // Now get every triple for the current TextGrid URI (subject) and add it to the RDFDB.
          // Sort out all the relations which have set another as the current URI as subject, so
          // that users can not add relations for other subjects via RDF metadata!
          Resource r = model.getResource(theUri.toASCIIString());
          Selector selector = new SimpleSelector(r, null, (String) null);
          List<Statement> urisWeWant = model.listStatements(selector).toList();
          model = ModelFactory.createDefaultModel();
          model.add(urisWeWant);

          // FIXME (ii) Write model back to relations RDF metadata? To do (i)?
          // theMetadata.getObject().setRelations(model.write());

          serviceLog(CrudService.INFO, meth, "Selected relations with subject: " + theUri);

          // Then get as N-TRIPLES and add to result.
          StringWriter n3Writer = new StringWriter();
          model.write(n3Writer, MODEL_NTRIPLE);
          result.add(n3Writer.toString());

        } catch (RiotException r) {
          String message = "Error processing relations RDF due to a " + r.getClass().getName()
              + ": " + r.getMessage() + ". RDF data is: " + rdfWriter.toString();
          serviceLog(CrudService.ERROR, meth, message);
          throw new IoFault(message);
        }
      }

      // Add hasAdaptor relation.
      URI hasAdaptor = relation.getHasAdaptor();
      if (hasAdaptor != null) {
        result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
            + REL_HASADAPTOR + REL_CLOSEOPEN + hasAdaptor + REL_CLOSEEND);
      }

      // Add hasSchema relation.
      URI hasSchema = relation.getHasSchema();
      if (hasSchema != null) {
        result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
            + REL_HASSCHEMA + REL_CLOSEOPEN + hasSchema + REL_CLOSEEND);
      }

      // Add isDerived relation.
      URI isDerived = relation.getIsDerivedFrom();
      if (isDerived != null) {
        result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
            + REL_ISDERIVEDFROM + REL_CLOSEOPEN + isDerived + REL_CLOSEEND);
      }

      // Add isAlternativeFormat relation.
      URI isAlternativeFormat = relation.getIsAlternativeFormatOf();
      if (isAlternativeFormat != null) {
        result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
            + REL_ISALTERNATIVEFORMATOF + REL_CLOSEOPEN + isAlternativeFormat + REL_CLOSEEND);
      }
    }

    // Add base URI.
    result.add(
        REL_OPEN + stripRevision(theUri).toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
            + REL_ISBASEURIOF + REL_CLOSEOPEN + theUri.toASCIIString() + REL_CLOSEEND);

    // Add project ID (URI and base URI).
    String projectId = theMetadata.getObject().getGeneric().getGenerated().getProject().getId();
    result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
        + REL_INPROJECT + REL_CLOSEOPEN + theUriPrefix + ":project:" + projectId + REL_CLOSEEND);
    result.add(REL_OPEN + stripRevision(theUri).toASCIIString() + REL_CLOSEOPEN
        + theTextgridRdfDbNamespace + REL_INPROJECT + REL_CLOSEOPEN + theUriPrefix + ":project:"
        + projectId + REL_CLOSEEND);

    // Add PID.
    result.addAll(TGCrudServiceUtilities.assembleRelationForPid(theUri, theMetadata,
        theTextgridRdfDbNamespace));

    // Add format.
    result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + RDF_DC_FORMAT + REL_CLOSEOPEN
        + theUriPrefix + ":mime:" + theMetadata.getObject().getGeneric().getProvided().getFormat()
        + REL_CLOSEEND);

    // Add work.
    EditionType edition = theMetadata.getObject().getEdition();
    if (edition != null && edition.getIsEditionOf() != null) {
      result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespace
          + REL_ISEDITIONOF + REL_CLOSEOPEN + edition.getIsEditionOf() + REL_CLOSEEND);
    }

    return result;
  }

  /**
   * <p>
   * Creates relations for every pid in the PID list, if existing.
   * </p>
   * 
   * @param theUri
   * @param theMetadata
   * @param theTextgridRdfDbNamespcae
   * @return
   * @throws IoFault
   */
  public static List<String> assembleRelationForPid(URI theUri, MetadataContainerType theMetadata,
      String theTextgridRdfDbNamespcae) throws IoFault {

    ArrayList<String> result = new ArrayList<String>();

    // Get all the PIDs from the metadata.
    List<Pid> pids = theMetadata.getObject().getGeneric().getGenerated().getPid();

    // Add every PID to a sameAs relation.
    for (Pid p : pids) {
      result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + RDF_OWL_SAMEAS + REL_CLOSEOPEN
          + p.getValue() + REL_CLOSE + ".");
      result.add(REL_OPEN + theUri.toASCIIString() + REL_CLOSEOPEN + theTextgridRdfDbNamespcae
          + REL_HASPID + REL_CLOSEOPEN + p.getValue() + REL_CLOSEEND);
    }

    return result;
  }

  // **
  // TGEXTRA UTILITIES
  // **

  /**
   * <p>
   * Returns a single String from the permissions String array.
   * </p>
   * 
   * @param thePermissions
   * @return A permission string out of a permission list.
   */
  public static String getPermissionString(List<String> thePermissions) {

    StringBuffer resultBuffer = new StringBuffer();

    // Just a security check, tgextra should always deliver a valid permissions array if access is
    // granted.
    if (thePermissions == null) {
      return "";
    }

    for (String p : thePermissions) {
      resultBuffer.append(p);
      resultBuffer.append(" ");
    }

    return resultBuffer.toString().trim();
  }

  // **
  // CLASS LOADING
  // **

  /**
   * <p>
   * Instantiates the GRID storage to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The data storage implementation class instance.
   * @throws IoFault
   */
  public static CrudStorageAbs<MetadataContainerType> getDataStorageImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return getStorageImplementation(theConfiguration,
        theConfiguration.getDATAsTORAGEiMPLEMENTATION());
  }

  /**
   * <p>
   * Instantiates the Index storage to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The index storage implementation class instance.
   * @throws IoFault
   */
  public static CrudStorageAbs<MetadataContainerType> getIndexStorageImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return getStorageImplementation(theConfiguration,
        theConfiguration.getIDXDBsTORAGEiMPLEMENTATION());
  }

  /**
   * <p>
   * Instantiates the RDF storage to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The RDF storage implementation class instance.
   * @throws IoFault
   */
  public static CrudStorageAbs<MetadataContainerType> getRdfStorageImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return getStorageImplementation(theConfiguration,
        theConfiguration.getRDFDBsTORAGEiMPLEMENTATION());
  }

  /**
   * <p>
   * Strips the revision from the given URI, if existing.
   * </p>
   * 
   * @param theUri
   * @return The URI stripped from revision numbers.
   * @throws IoFault
   */
  public static URI stripRevision(URI theUri) throws IoFault {

    if (isBaseUri(theUri)) {
      return theUri;
    }

    return URI.create(theUri.toASCIIString().substring(0, theUri.toASCIIString().indexOf(".")));
  }

  /**
   * <p>
   * Checks if the given URI is a base URI.
   * </p>
   * 
   * @param theUri
   * @return Is it a base URI or is it not a base URI?
   * @throws IoFault
   */
  public static boolean isBaseUri(URI theUri) throws IoFault {

    if (theUri == null) {
      throw CrudServiceExceptions.ioFault("URI must not be null!");
    }

    return !theUri.toASCIIString().contains(CrudServiceIdentifier.REVISION_SEPARATION_CHAR);
  }

  /**
   * <p>
   * Removes the URI prefix and the ":", it's not nice to have ":"s in the ES database.
   * </p>
   * 
   * @param theUri
   * @param theConfiguration
   * @return The URI stripped from its prefix.
   * @throws IoFault
   */
  public static String stripUriPrefix(URI theUri, CrudServiceConfigurator theConfiguration)
      throws IoFault {
    return theUri.toASCIIString().replaceFirst(theConfiguration.getURIpREFIX() + ":", "");
  }

  /**
   * <p>
   * Returns the URI's revision number as an int.
   * </p>
   * 
   * @param theUri
   * @return The revision of an URI as an int.
   */
  public static int getRevision(URI theUri) {
    return Integer.parseInt(theUri.toASCIIString().substring(
        theUri.toASCIIString().lastIndexOf(CrudServiceIdentifier.REVISION_SEPARATION_CHAR) + 1));
  }

  /**
   * <p>
   * Purges the SID to a few chars from the beginning and the ending.
   * </p>
   * 
   * @param theSessionId
   * @return The purged session ID if SID != null and length > 10, an empty string otherwise.
   */
  public static String purgeSid(String theSessionId) {
    return ((theSessionId != null && theSessionId.length() > 10) ? theSessionId.substring(0, 3)
        + CrudService.URI_SEPARATOR + theSessionId.substring(theSessionId.length() - 3) : "");
  }



  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Instantiates the storage implementation class to use.
   * </p>
   * 
   * @param theConfiguration
   * @param theClassName
   * @return The storage implementation.
   * @throws IoFault
   */
  @SuppressWarnings("unchecked")
  private static CrudStorageAbs<MetadataContainerType> getStorageImplementation(
      CrudServiceConfigurator theConfiguration, String theClassName) throws IoFault {

    String meth = UNIT_NAME + ".getStorageImplementation()";

    Class<?> storageImplementation;
    try {
      storageImplementation = TGCrudServiceUtilities.class.getClassLoader().loadClass(theClassName);
      CrudStorageAbs<MetadataContainerType> result =
          (CrudStorageAbs<MetadataContainerType>) storageImplementation.getDeclaredConstructor()
              .newInstance();
      result.setConfiguration(theConfiguration);

      serviceLog(CrudService.DEBUG, meth, "Instantiated storage implementation: " + theClassName);

      return result;

    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException e) {
      throw CrudServiceExceptions.ioFault(e, "TG-Crud service storage implementation class ["
          + theConfiguration.getDATAsTORAGEiMPLEMENTATION() + "] can  not be instanciated due to a "
          + e.getClass().getName() + ": " + e.getMessage());
    }
  }

  /**
   * <p>
   * Build a fine filename from metadata:
   * 
   * <ul>
   * <li>edition: [title].[baseURI].[revision].edition<br/>
   * Reise_nach_dem_Mittelpunkt_der_Erde.wr7j.0.edition</li>
   * <li>collection: [title].[baseURI].[revision].collection<br/>
   * example: Reise_nach_dem_Mittelpunkt_der_Erde.wr7j.0.collection</li>
   * <li>aggregation: [title].[baseURI].[revision].aggregation<br/>
   * example: Reise_nach_dem_Mittelpunkt_der_Erde.wr7j.0.aggregation</li>
   * <li>work: [title].[baseURI].[revision].work<br/>
   * example: Reise_nach_dem_Mittelpunkt_der_Erde.wr7g.0.work</li>
   * <li>data: [title].[baseUri].[revision].[fileExtension]<br/>
   * example: Graphic_Recording_1.24g6b.0.jpg</li>
   * </ul>
   * 
   * Metadata filename is always data filename plus ".meta"! Has to be appended from calling method!
   * </p>
   * 
   * <p>
   * TODO Put this method in common, it's used in TGCrudClientUtils, too!
   * </p>
   * 
   * 
   * @param theURI
   * @param theMimetype
   * @return The filename for the given URI.
   */
  public static String getDataFilenameFromMetadata(MetadataContainerType theMetadata) {

    String result;

    // Get suffix from format or file extension, if applicable.
    String mimetype = theMetadata.getObject().getGeneric().getProvided().getFormat();
    String formatSuffix;
    switch (mimetype) {
      case TextGridMimetypes.EDITION:
        formatSuffix = ".edition";
        break;
      case TextGridMimetypes.COLLECTION:
        formatSuffix = ".collection";
        break;
      case TextGridMimetypes.AGGREGATION:
        formatSuffix = ".aggregation";
        break;
      case TextGridMimetypes.WORK:
        formatSuffix = ".work";
        break;
      default:
        formatSuffix = LTPUtils.getFileExtension(mimetype);
    }

    // Get more information.
    String title = theMetadata.getObject().getGeneric().getProvided().getTitle().get(0);
    String uri = theMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue()
        .toASCIIString();

    // Gather everything.
    result = title.replaceAll("\\W+", "_") + "." + uri.replace("textgrid:", "") + formatSuffix;

    return result.trim();
  }

  /**
   * @param theInfo
   * @param theUri
   */
  protected static void logStartParameters(CrudServiceMethodInfo theInfo, URI theUri) {
    logStartParameters(theInfo, null, null, theUri, null, "", -1);
  }


  /**
   * @param theInfo
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   */
  protected static void logStartParameters(CrudServiceMethodInfo theInfo, String theSessionId,
      String theLogParameter, URI theUri) {
    logStartParameters(theInfo, theSessionId, theLogParameter, theUri, null, "", -1);
  }

  /**
   * <p>
   * Logs the start parameters.
   * </p>
   *
   * @param theInfo
   * @param theSessionId
   * @param theLogParameter
   * @param howMany
   */
  protected static void logStartParameters(CrudServiceMethodInfo theInfo, String theSessionId,
      String theLogParameter, int howMany) {
    logStartParameters(theInfo, theSessionId, theLogParameter, null, null, "", howMany);
  }

  /**
   * <p>
   * Logs the TG-crud method start parameters.
   * </p>
   *
   * @param theInfo
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @param newRevision
   * @param theProjectId
   * @param howMany
   */
  protected static void logStartParameters(CrudServiceMethodInfo theInfo, String theSessionId,
      String theLogParameter, URI theUri, Boolean newRevision, String theProjectId,
      Integer howMany) {

    CrudService.log(CrudService.INFO, theInfo, "TG-crud process ID [" + theInfo.getUuid() + "]");
    CrudService.log(CrudService.DEBUG, theInfo,
        "Session ID [" + TGCrudServiceUtilities.purgeSid(theSessionId) + "]");
    CrudService.log(CrudService.DEBUG, theInfo,
        "Log parameter [" + (theLogParameter != null ? theLogParameter : "") + "]");
    CrudService.log(CrudService.INFO, theInfo,
        "URI [" + (theUri != null ? theUri.toASCIIString() : "") + "]");
    CrudService.log(CrudService.DEBUG, theInfo,
        "How Many URIs [" + (howMany != null && howMany != -1 ? howMany : "") + "]");
    CrudService.log(CrudService.DEBUG, theInfo,
        "Project ID [" + (theProjectId != null ? theProjectId : "") + "]");
    CrudService.log(CrudService.DEBUG, theInfo,
        "New revision [" + (newRevision != null ? newRevision : "") + "]");
  }

  /**
   * <p>
   * Logs to the rollback logger.
   * </p>
   *
   * @param theLogLevel
   * @param theInfo
   * @param theLogMessage
   * @param theUri
   * @param theComponent
   */
  protected static void rbLog(int theLogLevel, CrudServiceMethodInfo theInfo, String theLogMessage,
      URI theUri, String theComponent) {

    String uri = (theUri == null ? "" : "\t" + theUri.toASCIIString());

    rollbackLog(theLogLevel, theInfo.getUuid() + "\t" + theInfo.getName() + "\t" + theLogMessage
        + "\t" + theComponent + uri);
  }

  /**
   * <p>
   * Logs to the rollback logger using no URI.
   * </p>
   *
   * @param theLogLevel
   * @param theInfo
   * @param theLogMessage
   * @param theComponent
   */
  protected static void rblog(int theLogLevel, CrudServiceMethodInfo theInfo, String theLogMessage,
      String theComponent) {
    rbLog(theLogLevel, theInfo, theLogMessage, null, theComponent);
  }

  /**
   * <p>
   * Logs to the TG-crud rollback logger.
   * </p>
   * 
   * @param theLogLevel
   * @param theLogMessage
   */
  protected static void rollbackLog(int theLogLevel, String theLogMessage) {

    // Log, if a rollback logger is existing.
    if (crudRollbackLogger != null) {
      switch (theLogLevel) {
        case CrudService.DEBUG:
          crudRollbackLogger.debug(theLogMessage);
          break;
        case CrudService.INFO:
          crudRollbackLogger.info(theLogMessage);
          break;
        case CrudService.WARN:
          crudRollbackLogger.warn(theLogMessage);
          break;
        case CrudService.ERROR:
          crudRollbackLogger.error(theLogMessage);
          break;
        default:
          // Nothing to log here.
      }
    }
  }

  /**
   * <p>
   * Gets a list from a string. Divider is: whitespace.
   * </p>
   * 
   * @return
   */
  public static List<String> getListFromConfigString(final String theVal) {

    List<String> result = new ArrayList<String>();

    if (theVal != null) {
      String splitted[] = ((String) theVal).split("\\s");
      if (splitted != null) {
        result = Arrays.asList(splitted);
      }
    }

    return result;
  }

  /**
   * <p>
   * Validate TextGrid metadata object.
   * </p>
   *
   * @param theMetadata
   * @return
   * @throws MetadataParseFault
   */
  public static List<Warning> validateMetadata(final MetadataContainerType theMetadata)
      throws MetadataParseFault {

    List<Warning> result = new ArrayList<Warning>();

    String meth = "validateMetadata()";

    serviceLog(CrudService.DEBUG, meth, "Validating incoming object metadata");

    // Clone object to validate first!
    ObjectType objectMetadata = cloneObjectTypeWithoutGenerated(theMetadata.getObject());
    try {
      // First create XML metadata string.
      String objectMetadataString = "";
      try (Writer metadataWriter = new StringWriter()) {
        JAXB.marshal(objectMetadata, metadataWriter);
        objectMetadataString = metadataWriter.toString();
        metadataWriter.close();
      }

      // Check for empty metadata string.
      if (objectMetadataString.isEmpty()) {
        throw CrudServiceExceptions
            .metadataParseFault(ERROR_PARSING_METADATA + "No <object> metadata!");
      }

      // Try unmarshalling, and validate on the go.
      initTextGridObjectTypeUnmarshaller();

      // Check error events, that do NOT deliver an exception.
      ValidationEventCollector validationCollector = new ValidationEventCollector();
      tgUnmarshaller.setEventHandler(validationCollector);

      // Finally unmarshal.
      tgUnmarshaller.unmarshal(new StringReader(objectMetadataString));

      // Get validation messages and throw exception if any.
      if (validationCollector.hasEvents()) {
        for (ValidationEvent event : validationCollector.getEvents()) {
          String message = event.getMessage();
          Warning w = new Warning();
          w.setValue(VALIDATION_ISSUE + message);
          result.add(w);
        }
      }
    } catch (JAXBException | SAXException | IOException e) {
      String message = VALIDATION_ISSUE + e.getClass().getName() + ": " + e.getCause().getMessage();
      Warning w = new Warning();
      w.setValue(message);
      result.add(w);
    }

    serviceLog(CrudService.WARN, meth, "Validation warnings: " + result.toString());

    return result;
  }

  /**
   * <p>
   * Checks consistency of file size and checksum in storage, metadata, and Handle metadata. Public
   * objects only!
   * </p>
   * 
   * @param theUri The TextGrid URI to check
   * @return An empty string if everything is OK,a string that describes the consistency otherwise.
   * @param theConfiguration The tgcrud config
   * @throws IoFault
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  protected static Consistency checkConsistency(final URI theUri,
      CrudServiceConfigurator theConfiguration)
      throws IoFault, IOException, NoSuchAlgorithmException {

    Consistency result = new Consistency();
    result.uri = theUri.toASCIIString();

    String meth = "checkConsistency()";

    // Some defaults used in case of an error.
    String hdl = NOT_IN_METADATA;
    String mimetype = NOT_IN_METADATA;
    String created = NOT_IN_METADATA;
    long filesizeFromMetadata = FILESIZE_EMPTY_OR_NOT_FOUND;
    long filesizeFromHandle = FILESIZE_EMPTY_OR_NOT_FOUND;
    long filesizeFromStorage = FILESIZE_EMPTY_OR_NOT_FOUND;
    String checksumType = "md5";
    String checksumFromMetadata = CHECKSUM_EMPTY_OR_NOT_FOUND;
    String checksumFromHandle = CHECKSUM_EMPTY_OR_NOT_FOUND;
    String checksumFromStorage = CHECKSUM_EMPTY_OR_NOT_FOUND;

    //
    // Get (public!) TextGrid object by URI.
    //

    // TODO Could be implemented for non-public and public crud differently, if needed.
    CrudObject<MetadataContainerType> o =
        CrudService.dataStorageImplementation.retrievePublic(theUri);

    //
    // Get filesize and checksum from Handle metadata, if existing in metadata.
    //
    GeneratedType g = o.getMetadata().getObject().getGeneric().getGenerated();
    List<Pid> pidList = g.getPid();
    // If Handle is existing.
    if (pidList != null && !pidList.isEmpty() &&
        pidList.get(0).getValue() != null && !pidList.get(0).getValue().isEmpty()) {

      Pid pid = pidList.get(0);
      hdl = pid.getValue();

      serviceLog(CrudService.INFO, meth,
          "PID from metadata: " + hdl + " (" + pid.getPidType() + ")");

      TGCrudServiceIdentifierPidImpl hdlImplementation = new TGCrudServiceIdentifierPidImpl();
      hdlImplementation.setConfiguration(theConfiguration);

      // Get Handle metadata (all of it).
      try {
        String allHandleMetadata =
            hdlImplementation.getHandleMetadata(URI.create(hdl), GET_ALL_HANDLE_METADATA);

        // Override Handle PID from metadata (in case of a deprecated Handle prefix).
        String newPidWithCorrectPrefix =
            CrudServiceUtilities.getCurrentHandlePID(allHandleMetadata);
        if (!newPidWithCorrectPrefix.isEmpty()) {
          hdl = newPidWithCorrectPrefix;
        }

        // Get filesize and checksum (if existing in Handle metadata).
        String tmpFs = getHandleMetadata(allHandleMetadata, HDL_FILESIZE);
        if (!tmpFs.isEmpty()) {
          filesizeFromHandle = Long.valueOf(tmpFs);
        }
        String tmpCs = getHandleMetadata(allHandleMetadata, HDL_CHECKSUM);
        if (!tmpCs.isEmpty()) {
          checksumFromHandle = tmpCs;
        }

      } catch (NumberFormatException | MetadataParseFault | JSONException e) {
        throw new IoFault(e.getMessage());
      }

      serviceLog(CrudService.DEBUG, meth, "Handle metadata: hdl=" + hdl + ", size="
          + filesizeFromHandle + ", checksum=" + checksumFromHandle);
    }

    // No Handle found in TextGrid metadata.
    else {
      serviceLog(CrudService.WARN, meth, "Handle PID not found in TextGrid metadata!");
    }

    //
    // Get file size and checksum (and more) from metadata.
    //

    // Filesize from metadata can not be null.
    filesizeFromMetadata = o.getFilesize();
    if (o.getChecksumType() != null) {
      checksumType = o.getChecksumType();
    }
    if (o.getChecksum() != null) {
      checksumFromMetadata = checksumType + ":" + o.getChecksum();
    }
    if (o.getMimetype() != null) {
      mimetype = o.getMimetype();
    }
    if (g.getCreated() != null) {
      created = g.getCreated().toString();
    }

    serviceLog(CrudService.DEBUG, meth, "TextGrid metadata: size=" + filesizeFromMetadata
        + ", checksum=" + checksumFromMetadata + ", mimetype=" + mimetype + ", created=" + created);

    //
    // Get filesize and checksum from object stream.
    //

    // Create a counting stream around a file input stream.
    MessageDigest md = MessageDigest.getInstance(checksumType);
    try (CountingInputStream count = new CountingInputStream(o.getData().getInputStream())) {
      // Create a message digest stream around the counting stream.
      try (DigestInputStream digest = new DigestInputStream(count, md)) {
        // Read from the storage stream using the digest stream using the counting stream :-)
        digest.readAllBytes();
      }

      // Do count, get the message digest, and set things to get it into the metadata!
      filesizeFromStorage = count.getByteCount();
      checksumFromStorage = checksumType + ":" + new String(Hex.encodeHex(md.digest()));
    }

    serviceLog(CrudService.DEBUG, meth,
        "Storage metadata: size=" + filesizeFromStorage + ", checksum=" + checksumFromStorage);

    //
    // Return OK, or build return Consistency object..
    //

    // Check file sizes.
    if (filesizeFromMetadata != FILESIZE_EMPTY_OR_NOT_FOUND &&
        filesizeFromHandle != FILESIZE_EMPTY_OR_NOT_FOUND &&
        filesizeFromStorage != FILESIZE_EMPTY_OR_NOT_FOUND &&
        filesizeFromMetadata == filesizeFromHandle &&
        filesizeFromMetadata == filesizeFromStorage) {
      result.filesizeStatus = STATUS_OK;
    } else {
      result.filesizeStatus = STATUS_ERROR;
    }
    result.filesize = new Filesize(filesizeFromMetadata, filesizeFromStorage, filesizeFromHandle);
    serviceLog(CrudService.INFO, meth,
        "Filesize consistency for URI " + theUri + ": " + result.filesizeStatus);

    // Check checksums.
    if (!checksumFromMetadata.equals(STATUS_ERROR) &&
        !checksumFromHandle.equals(STATUS_ERROR) &&
        !checksumFromStorage.equals(STATUS_ERROR) &&
        checksumFromMetadata.equals(checksumFromHandle) &&
        checksumFromMetadata.equals(checksumFromStorage)) {
      result.checksumStatus = STATUS_OK;
    } else {
      result.checksumStatus = STATUS_ERROR;
    }
    result.checksum = new Checksum(checksumFromMetadata, checksumFromStorage, checksumFromHandle);
    serviceLog(CrudService.INFO, meth,
        "Checksum consistency for URI " + theUri + ": " + result.checksumStatus);

    // Set more general values.
    result.hdl = hdl;
    result.mimetype = mimetype;
    result.created = created;

    return result;
  }

  /**
   * <p>
   * Check for <item>, <edition>, <collection>, and <work> tags. If not existing, just create the
   * empty tags for valid metadata objects. A fine workaround for the TG-lab import, that is not
   * writing those tags at the moment.
   * </p>
   * 
   * @param theMetadata
   */
  public static void checkAndCorrectMissingTags(MetadataContainerType theMetadata) {

    if (theMetadata != null
        && theMetadata.getObject() != null
        && theMetadata.getObject().getGeneric() != null
        && theMetadata.getObject().getGeneric().getProvided() != null
        && theMetadata.getObject().getGeneric().getProvided().getFormat() != null) {

      String format = theMetadata.getObject().getGeneric().getProvided().getFormat();

      // Look for <collection>, <edition>, and <work> objects first.
      if (format.equals(TextGridMimetypes.COLLECTION)
          && theMetadata.getObject().getCollection() == null) {
        theMetadata.getObject().setCollection(new CollectionType());
      } else if (format.equals(TextGridMimetypes.EDITION)
          && theMetadata.getObject().getEdition() == null) {
        theMetadata.getObject().setEdition(new EditionType());
      } else if (format.equals(TextGridMimetypes.WORK)
          && theMetadata.getObject().getWork() == null) {
        theMetadata.getObject().setWork(new WorkType());
      } else if (!format.equals(TextGridMimetypes.COLLECTION)
          && !format.equals(TextGridMimetypes.EDITION)
          && !format.equals(TextGridMimetypes.WORK)) {
        // Look for <item> object otherwise.
        if (theMetadata.getObject().getItem() == null) {
          theMetadata.getObject().setItem(new ItemType());
        }
      }
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Singleton for creating the TextGrid Schema Unmarshaller for validation.
   * </p>
   * 
   * @throws MalformedURLException
   * @throws SAXException
   * @throws JAXBException
   */
  private static void initTextGridObjectTypeUnmarshaller()
      throws MalformedURLException, SAXException, JAXBException {

    // NOTE We are validating ObjectType only, for <tgObjectMetadata> and <metadataContainerType>
    // are seemingly complicated to parse/create/validate? I really don't know!

    if (tgUnmarshaller == null) {
      // Set schema and context.
      SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
      Schema tgSchema = sf.newSchema(new URL(TEXTGRID_METADATA_SCHEMA_LOCATION));
      JAXBContext tgContext = JAXBContext.newInstance(ObjectType.class);

      // Create Unmarshaller and set TextGrid schema.
      tgUnmarshaller = tgContext.createUnmarshaller();
      tgUnmarshaller.setSchema(tgSchema);
    }
  }

  /**
   * <p>
   * Clone a JAXB ObjectType via marshal/unmarshal.
   * </p>
   * 
   * @param theObject
   * @return
   */
  private static ObjectType cloneObjectTypeWithoutGenerated(final ObjectType theObject) {

    // TODO Use faster object cloning here...?

    ObjectType result = null;

    // Marshal.
    StringWriter w = new StringWriter();
    JAXB.marshal(theObject, w);
    String o = w.toString();

    // Unmarshal.
    StringReader r = new StringReader(o);
    result = JAXB.unmarshal(r, ObjectType.class);

    // Remove <generated> from object to validate (do NOT remove <generated> element from actual
    // metadata object, use the clone for it!)
    result.getGeneric().setGenerated(null);

    return result;
  }

  /**
   * <p>
   * Re-adapt object for #UPDATEMETADATA to re-set some metadata in case adaptor has changed or some
   * RDF metadata is invalid (only EXIF metadata creation then).
   * </p>
   * 
   * @param theURI
   * @param theSessionID
   * @param theLogParameter
   * @param theMethodInfo
   * @param theMetadata
   * @return
   * @throws IoFault
   */
  public static List<String> reAdapt(URI theURI, String theSessionID, String theLogParameter,
      CrudServiceMethodInfo theMethodInfo, MetadataContainerType theMetadata,
      CrudServiceConfigurator theConf) throws IoFault {

    List<String> result = new ArrayList<String>();

    serviceLog(CrudService.DEBUG, theMethodInfo.getName(),
        "Adaptor has changed, data file will be re-adapted");

    /**
     * RETRIEVE OBJECT DATA FROM THE DATA STORAGE
     */

    DataHandler tgObjectData = null;
    try {
      tgObjectData = CrudService.dataStorageImplementation.retrieve(theURI).getData();

      serviceLog(CrudService.DEBUG, theMethodInfo.getName(),
          "Loaded TextGrid data file FROM THE DATA STORAGE for re-adaptation");

    } catch (IoFault e) {
      // Add a warning directly to the metadata object, if the adaptor file can not be read FROM
      // THE DATA STORAGE.
      String message = "Adaptor file could not be read FROM THE DATA STORAGE: " + theURI
          + "! Re-adaptation failed: " + e.getMessage();
      TGCrudServiceUtilities.addWarning(theMetadata, message);
      serviceLog(CrudService.WARN, theMethodInfo.getName(), message);
    }

    // Only call the Adaptor Manager if data file is not empty.
    if (tgObjectData != null) {
      // Gather relations, any warnings are added directly to the metadata object.
      result = TGCrudServiceAdaptorManager.go(theSessionID, theLogParameter, theURI,
          new TGCrudTextGridObject(theMetadata, tgObjectData), CrudService.rdfdbImplementation,
          CrudService.dataStorageImplementation, CrudService.idxdbImplementation,
          CrudService.aaiImplementation, theConf, CrudOperations.UPDATEMETADATA);
    }

    return result;
  }

  /**
   * <p>
   * Checks the RDF relations part and validates for valid RDF (including namespace issues!).
   * </p>
   * 
   * @param theMetadata
   * @return
   */
  public static List<Warning> checkRelationRDFMetadata(MetadataContainerType theMetadata) {

    List<Warning> result = new ArrayList<Warning>();

    String meth = UNIT_NAME + ".checkRelationRDFMetadata()";

    RelationType relation = theMetadata.getObject().getRelations();
    if (relation != null && relation.getRDF() != null) {

      serviceLog(CrudService.INFO, meth, "Validating RDF from relations metadata");

      String rdfString = "";
      // Get RDF string from RDF type.
      try (StringWriter rdfWriter = new StringWriter()) {
        JAXBElement<RdfType> rdfType = new JAXBElement<RdfType>(new QName(RDF_NAMESPACE, RDF_TAG),
            RdfType.class, relation.getRDF());
        JAXB.marshal(rdfType, rdfWriter);
        rdfString = rdfWriter.toString();
        rdfWriter.close();

        // Create model to check RDF validity!
        Model model = ModelFactory.createDefaultModel();
        model.read(new StringReader(rdfString), MODEL_RDF);

      } catch (RiotException r) {
        String message = "Error validating relations RDF due to a " + r.getClass().getName()
            + " (EXIF information will be rewritten!): " + r.getMessage() + " in " + rdfString;
        serviceLog(CrudService.WARN, meth, message);
        Warning w = new Warning();
        w.setValue(message);
        result.add(w);
      } catch (IOException e) {
        // Catch what here?
      }
    }

    return result;
  }

}
