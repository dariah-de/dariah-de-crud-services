/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;

/**
 * <p>
 * Crud Service Message for using the messaging service.
 * </p>
 */
public class CrudServiceMessage {

  public URI objectId; // tguri for tg, pid for dh
  public int operation; // create, update, delete
  public String format; // mimetype
  public String rootId; // collectionId for dh, projectId for tg
  public boolean isPublic; // public or not public

  /**
   * Default constructor required for Jackson serializer.
   */
  public CrudServiceMessage() {}

  /**
   *
   */
  @Override
  public String toString() {
    return "CrudMessage{objectId='%s', operation='%d', format='%s', rootId='%s', public='%b'}"
        .formatted(this.objectId.toString(), this.operation, this.format, this.rootId,
            this.isPublic);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return
   */
  public URI getObjectId() {
    return this.objectId;
  }

  /**
   * @param objectId
   * @return
   */
  public CrudServiceMessage setObjectId(URI objectId) {
    this.objectId = objectId;
    return this;
  }

  /**
   * @return
   */
  public int getOperation() {
    return this.operation;
  }

  /**
   * @param operation
   * @return
   */
  public CrudServiceMessage setOperation(int operation) {
    this.operation = operation;
    return this;
  }

  /**
   * @return
   */
  public String getFormat() {
    return this.format;
  }

  /**
   * @param format
   * @return
   */
  public CrudServiceMessage setFormat(String format) {
    this.format = format;
    return this;
  }

  /**
   * @return
   */
  public String getRootId() {
    return this.rootId;
  }

  /**
   * @param rootId
   * @return
   */
  public CrudServiceMessage setRootId(String rootId) {
    this.rootId = rootId;
    return this;
  }

  /**
   * @return
   */
  public boolean isPublic() {
    return this.isPublic;
  }

  /**
   * @param isPublic
   * @return
   */
  public CrudServiceMessage setPublic(boolean isPublic) {
    this.isPublic = isPublic;
    return this;
  }

}
