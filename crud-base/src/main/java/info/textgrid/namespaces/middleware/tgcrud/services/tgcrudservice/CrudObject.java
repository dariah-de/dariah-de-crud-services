/**
 * This software is copyright (c) 2020 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import jakarta.activation.DataHandler;

/**
 * TODOLOGS
 * 
 * TODO Some of the attributes must still be generalised with the TextGrid object, such as
 * createdDate and LastModifiedDate. We want all this to be handled by the admMD object of
 * GeneratedType.
 * 
 **
 * CHANGELOG
 * 
 * 2020-06-16 - Funk - Add portal config file.
 * 
 * 2017-09-26 - Funk - Add abstract method isCollection.
 * 
 * 2017-09-07 - Funk - Add wrapper checksum getter and setter.
 * 
 * 2015-10-23 - Funk - Add technical and provenance metadata.
 * 
 * 2015-10-20 - Funk - Add mimetype.
 * 
 * 2015-08-07 - Funk - Refactor constructors to always need an URI.
 * 
 * 2015-01-30 - Funk- Generalise from TGCrudTextGridObject.
 */

/**
 * <p>
 * This generic object class just binds together a metadata object and a DataHandler for the storage
 * interfaces.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2020-06-19
 * @since 2015-01-30
 ******************************************************************************/

public abstract class CrudObject<T> {

  // **
  // STATIC FINALS
  // **

  protected static final int BASELINE_DATA = 1;
  protected static final int AGGREGATION_DATA = 2;
  protected static final int ORIGINAL_DATA = 3;
  protected static final int BINARY_DATA = 4;
  protected static final int RDF_DATA = 5;
  protected static final int PROJECTFILE_DATA = 6;
  protected static final int PORTALCONFIG_DATA = 7;

  // **
  // INSTANCE VARIABLES
  // **

  private URI uri = null;
  private DataHandler data = null;
  private List<String> relations = null;
  private int kindOfData = BINARY_DATA;
  private T metadata = null;
  private T admMD = null;
  private String techMD = null;
  private String provMD = null;
  private URL location = null;

  // **
  // CONSTRUCTORS
  // **

  /**
   * @param theUri
   */
  public CrudObject(URI theUri) {
    this.uri = theUri;
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return The URI
   */
  public URI getUri() {
    return this.uri;
  }

  /**
   * @param theUri
   */
  public void setUri(URI theUri) {
    this.uri = theUri;
  }

  /**
   * @return The metadata object
   */
  public T getMetadata() {
    return this.metadata;
  }

  /**
   * @param theMetadata
   */
  public void setMetadata(T theMetadata) {
    this.metadata = theMetadata;
  }

  /**
   * @return The administrative metadata object.
   */
  public T getAdmMD() {
    return this.admMD;
  }

  /**
   * @param theAdmMd
   */
  public void setAdmMD(T theAdmMd) {
    this.admMD = theAdmMd;
  }

  /**
   * @return The technical metadata object.
   */
  public String getTechMD() {
    return this.techMD;
  }

  /**
   * @param theTechMd
   */
  public void setTechMD(String theTechMd) {
    this.techMD = theTechMd;
  }

  /**
   * @return The provenance metadata object.
   */
  public String getProvMD() {
    return this.provMD;
  }

  /**
   * @param theProvMd
   */
  public void setProvMD(String theProvMd) {
    this.provMD = theProvMd;
  }

  /**
   * @return The data handler.
   */
  public DataHandler getData() {
    return this.data;
  }

  /**
   * @param theData
   */
  public void setData(DataHandler theData) {
    this.data = theData;
  }

  /**
   * @return The relations of the object as a list of strings.
   */
  public List<String> getRelations() {
    return this.relations;
  }

  /**
   * @param theRelations
   */
  public void setRelations(List<String> theRelations) {
    this.relations = theRelations;
  }

  /**
   * @return The kind of data.
   */
  public int getKindOfData() {
    return this.kindOfData;
  }

  /**
   * @param theKindOfData
   */
  public void setKindOfData(int theKindOfData) {
    this.kindOfData = theKindOfData;
  }

  /**
   * @return The location of the resource.
   */
  public URL getLocation() {
    return this.location;
  }

  /**
   * @param theLocation
   */
  public void setLocation(URL theLocation) {
    this.location = theLocation;
  }

  // **
  // ABSTRACTS
  // **

  /**
   * @return The file size
   */
  public abstract long getFilesize();

  /**
   * @param theFilesize
   */
  public abstract void setFilesize(long theFilesize);

  /**
   * @return The file size of the container holding data and metadata files (SIP, BAG, etc.).
   */
  public abstract long getWrapperSize();

  /**
   * @param theWrapperSize
   */
  public abstract void setWrapperSize(long theWrapperSize);

  /**
   * @return The checksum
   */
  public abstract String getChecksum();

  /**
   * @param theChecksum
   */
  public abstract void setChecksum(String theChecksum);

  /**
   * @return The checksum type
   */
  public abstract String getChecksumType();

  /**
   * @param theChecksumType
   */
  public abstract void setChecksumType(String theChecksumType);

  /**
   * @return The checksum origin
   */
  public abstract String getChecksumOrigin();

  /**
   * @param theChecksumOrigin
   */
  public abstract void setChecksumOrigin(String theChecksumOrigin);

  /**
   * @return The checksum
   */
  public abstract String getWrapperChecksum();

  /**
   * @param theChecksum
   */
  public abstract void setWrapperChecksum(String theChecksum);

  /**
   * @return The checksum type
   */
  public abstract String getWrapperChecksumType();

  /**
   * @param theChecksumType
   */
  public abstract void setWrapperChecksumType(String theChecksumType);

  /**
   * @return The checksum origin
   */
  public abstract String getWrapperChecksumOrigin();

  /**
   * @param theChecksumOrigin
   */
  public abstract void setWrapperChecksumOrigin(String theChecksumOrigin);

  /**
   * @return The creation date of the object.
   * @throws ParseException
   */
  public abstract Date getCreationDate() throws ParseException;

  /**
   * @param theDate
   * @throws IoFault
   */
  public abstract void setCreationDate(Date theDate) throws IoFault;

  /**
   * @return The lastModified date of the object.
   * @throws ParseException
   */
  public abstract Date getLastModifiedDate() throws ParseException;

  /**
   * @param theDate
   * @throws IoFault
   */
  public abstract void setLastModifiedDate(Date theDate) throws IoFault;

  /**
   * @return The mimetype of the object.
   */
  public abstract String getMimetype();

  /**
   * @param theMimetype
   * @throws IoFault
   */
  public abstract void setMimetype(String theMimetype) throws IoFault;

  /**
   * @return Is object a collection? Or maybe not?
   */
  public abstract boolean isCollection();

}
