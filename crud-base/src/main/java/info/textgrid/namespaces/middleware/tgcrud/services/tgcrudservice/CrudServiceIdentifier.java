/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public (http://www.gnu.org/licenses/lgpl-3.0.txt) License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2021-04-016 - Funk - Add new method getHandleMetadata().
 * 
 * 2015-02-10 - Funk - Add update method.
 * 
 * 2014-12-22 - Funk - Add setKeyValuePairs() method.
 * 
 * 2013-12-11 - Funk - Remove public's for they are not needed anymore in interface method
 * declarations.
 * 
 * 2012-02-06 - Funk - Add user to lock and unlock parameters.
 * 
 * 2010-09-03 - Funk - Adapt to the new metadata schema. Added getUri() method.
 * 
 * 2010-06-28 - Funk - Add method getUris().
 * 
 * 2010-06-24 - Funk - First version.
 */

/**
 * <p>
 * This Identifier interface provides access to TextGrid and DARIAH-DE IDs, such as TextGrid URIs or
 * Handle PIDs. The implementations must provide thread-safety itself!
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2021-04-08
 * @since 2010-06-24
 */

public interface CrudServiceIdentifier {

  // **
  // STATIC FINAL VARIABLES
  // **

  static final String REVISION_SEPARATION_CHAR = ".";
  static final int INITIAL_REVISION = 0;
  static final ObjectType NO_METADATA = null;

  /**
   * <p>
   * Creates a bunch of URIs, metadata parameter is needed.
   * </p>
   * 
   * @param theAmount
   * @return A list of URIs.
   * @throws IoFault
   */
  List<URI> getUris(int theAmount) throws IoFault;

  /**
   * <p>
   * Updates identifier's metadata using the key/value pair map.
   * </p>
   * 
   * @param theUri
   * @throws IoFault
   */
  void updatePidMetadata(URI theUri) throws IoFault;

  /**
   * <p>
   * Checks if a URI already is registered.
   * </p>
   * 
   * @param theUri
   * @return TRUE if the URI does exist, FALSE if not.
   * @throws IoFault
   */
  boolean uriExists(URI theUri) throws IoFault;

  /**
   * <p>
   * Locks the URI for e.g. #UPDATE, #UPDATEMETADATA, and #DELETE calls.
   * </p>
   * 
   * @param theUri
   * @param theUser
   * @return TRUE if the given URI could be locked, FALSE otherwise.
   * @throws IoFault
   */
  boolean lock(URI theUri, String theUser) throws IoFault;

  /**
   * <p>
   * Only checks if the given URI is locked by the user (and if the automagic unlocking time has
   * exceeded already).
   * </p>
   * 
   * @param theUri
   * @return A string by whom the object is locked.
   * @throws IoFault
   */
  String isLockedBy(URI theUri) throws IoFault;

  /**
   * <p>
   * Unlocks the URI again.
   * </p>
   * 
   * @param theUri
   * @param theUser
   * @return TRUE if the given URI could be unlocked, FALSE otherwise.
   * @throws IoFault
   */
  boolean unlock(URI theUri, String theUser) throws IoFault;

  /**
   * <p>
   * Locks the URI for e.g. #UPDATE, #UPDATEMETADATA, and #DELETE calls. Internal locking only, no
   * user is involved.
   * </p>
   * 
   * @param theUri
   * @return TRUE if the given URI could be locked, FALSE otherwise.
   * @throws IoFault
   */
  boolean lockInternal(URI theUri) throws IoFault;

  /**
   * <p>
   * Unlocks the internal URI again.
   * </p>
   * 
   * @param theUri
   * @return TRUE if the given URI could be unlocked, FALSE otherwise.
   * @throws IoFault
   */
  boolean unlockInternal(URI theUri) throws IoFault;

  /**
   * <p>
   * Example, if existing (CREATED):
   * {"responseCode":1,"handle":"21.T11991/0000-001B-4222-2","values":[{"index":17,"type":"PUBLISHED","data":{"format":"string","value":"true"},"ttl":86400,"timestamp":"2021-04-19T12:21:08Z"}]}
   * </p>
   * <p>
   * Example, if not existing (DELETED):
   * {"responseCode":200,"values":[],"handle":"21.T11991/0000-001B-48B5-6"}
   * </p>
   * 
   * @param theUri The PID of the resource.
   * @param theType Any Handle metadata type (such as BAG, DELETED, PUBLISHED, or CREATOR)
   * @return The JSON string value of the given type metadata, if existing.
   * @throws IoFault If something went wrong.
   * @throws MetadataParseFault If Handle metadata of given type is NOT existing.
   */
  abstract String getHandleMetadata(final URI theUri, final String theType)
      throws IoFault, MetadataParseFault;

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * <p>
   * Set the key/value pairs to bind to the identifier. Please use *before* creating the Identifier!
   * </p>
   * 
   * @param theKeyValueMap
   * @throws IoFault
   */
  void setKeyValuePairs(HashMap<String, String> theKeyValueMap)
      throws IoFault;

  /**
   * @param theConfiguration
   */
  void setConfiguration(CrudServiceConfigurator theConfiguration);

  /**
   * @return The service configurator.
   */
  CrudServiceConfigurator getConfiguration();

}
