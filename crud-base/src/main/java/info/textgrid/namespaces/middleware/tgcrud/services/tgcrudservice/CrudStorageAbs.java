/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2015-02-12 - Funk - Generalised and now implementing the generic class.
 * 
 * 2015-02-10 - Funk - Added MESSAGE_DIGEST_TYPE.
 * 
 * 2011-02-01 - Funk - Fixed Bug TG-1091.
 * 
 * 2011-01-15 - Funk - * Delete now deletes aggregation data, too.
 * 
 * 2010-09-09 - Funk - First version.
 **/

/**
 * <p>
 * Abstract class for TG-crud and DH-crud storage implementations.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2023-09-08
 * @since 2010-09-09
 */

public abstract class CrudStorageAbs<T> implements CrudStorage<T> {

  // **
  // STATIC FINALS
  // **

  protected static final String NOT_IMPLEMENTED = "Not implemented";

  // **
  // INSTANCE VARIABLES
  // **

  protected CrudServiceConfigurator conf = null;

  // **
  // GETTERS AND SETTERS
  // **

  /**
   *
   */
  @Override
  public CrudServiceConfigurator getConfiguration() {
    return this.conf;
  }

  /**
   *
   */
  @Override
  public void setConfiguration(CrudServiceConfigurator theConfiguration) {
    this.conf = theConfiguration;
  }

}
