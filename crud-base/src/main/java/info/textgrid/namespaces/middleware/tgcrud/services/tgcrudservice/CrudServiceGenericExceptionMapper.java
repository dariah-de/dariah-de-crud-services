/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Thorsten Vitt
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Map;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.Response.StatusType;
import jakarta.ws.rs.ext.ExceptionMapper;
import org.apache.cxf.helpers.IOUtils;
import org.springframework.web.util.HtmlUtils;
import com.google.common.collect.ImmutableMap;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2024-04-02 - Funk - Add fine grained error messages to HTML response.
 * 
 * 2018-10-18 - Funk - Adapted to new DH SGT.
 * 
 * 2018-08-31 - Funk - Generalised: TGCrud and DHCrudServiceGenericExceptionMapper.
 * 
 * 2018-03-13 - Funk - Added some new Exceptions to STATUS_MAP to be able to handle them!
 * 
 * 2017-09-01 - Funk - Responding with XHTML now! Added API documentation URL.
 * 
 * 2017-08-22 - Funk - Added DARIAH-DE logo, using DARIAH-DE HTML error template now!
 * 
 * 2014-09-12 - Funk - Copied from TGCrudServiceGenericExceptionMapper.
 * 
 */

/**
 * <p>
 * Writes exception information in HTML bodies.
 * </p>
 * 
 * @author Thorsten Vitt
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-04-02
 * @since 2013-03-07
 */

public abstract class CrudServiceGenericExceptionMapper implements ExceptionMapper<Exception> {

  public static final Map<Class<? extends Exception>, Response.Status> STATUS_MAP =
      ImmutableMap.<Class<? extends Exception>, Response.Status>builder()
          .put(AuthFault.class, Status.UNAUTHORIZED)
          .put(IoFault.class, Status.INTERNAL_SERVER_ERROR)
          .put(MetadataParseFault.class, Status.BAD_REQUEST)
          .put(ObjectNotFoundFault.class, Status.NOT_FOUND)
          .put(ParseException.class, Status.BAD_REQUEST)
          .put(ProtocolNotImplementedFault.class, Status.BAD_REQUEST)
          .put(UnsupportedOperationException.class, Status.NOT_IMPLEMENTED)
          .put(FileNotFoundException.class, Status.NOT_FOUND)
          .put(UpdateConflictFault.class, Status.BAD_REQUEST)
          .build();
  protected static final String UNIT_NAME = "exception";

  protected String htmlTemplateFile;

  // Abstract class for specifying the service version.
  public abstract String getServiceVersion();

  /**
   *
   */
  @Override
  public Response toResponse(final Exception exception) {

    exception.printStackTrace();

    String meth = UNIT_NAME + ".toResponse()";

    Status status = STATUS_MAP.get(exception.getClass());
    String name = exception.getClass().getSimpleName();
    String message = exception.getLocalizedMessage();

    if (exception.getCause() != null) {
      status = STATUS_MAP.get(exception.getCause().getClass());
      name = exception.getCause().getClass().getSimpleName();
      message = exception.getCause().getLocalizedMessage();
    }
    if (status == null) {
      status = Status.INTERNAL_SERVER_ERROR;
    }

    CrudServiceUtilities.serviceLog(CrudService.ERROR, meth, MessageFormat.format(
        "{0} {1} - {2}: {3}", status.getStatusCode(), status.getReasonPhrase(), name, message));

    // Do return response.
    String htmlMessage = "";
    if (CrudService.getConfiguration() != null) {
      htmlMessage = this.prepareXHTMLMessage(status, name, message, getServiceVersion(),
          CrudService.getConfiguration());
    } else {
      htmlMessage = this.prepareXHTMLMessage(status, name, message, getServiceVersion());
    }

    return Response.status(status).type(MediaType.TEXT_HTML).entity(htmlMessage).build();
  }

  /**
   * @param status see toResponse.
   * @param message see toResponse.
   * @param detail see toResponse.
   * @param version
   * @return the prepared XHTML message.
   * @throws IOException
   * @throws FileNotFoundException
   */
  public String prepareXHTMLMessage(final StatusType status, final String message,
      final String detail, String version) {

    String meth = UNIT_NAME + ".prepareXHTMLMessage()";

    String htmlMessage = "";
    try (InputStream htmlTemplateStream =
        this.getClass().getClassLoader().getResourceAsStream(this.htmlTemplateFile)) {
      htmlMessage = IOUtils.toString(htmlTemplateStream);
      htmlTemplateStream.close();
    } catch (IOException e) {
      CrudServiceUtilities.serviceLog(CrudService.ERROR, meth,
          "Could not read HTML error template file");
    }

    return MessageFormat.format(htmlMessage, status.getStatusCode(), status.getReasonPhrase(),
        HtmlUtils.htmlEscapeHex(message), HtmlUtils.htmlEscape(detail), version);
  }

  /**
   * @param theStatus
   * @param theMessage
   * @param theDetail
   * @param theVersion
   * @param theConf
   * @return
   */
  public String prepareXHTMLMessage(final StatusType theStatus, final String theMessage,
      final String theDetail, final String theVersion, final CrudServiceConfigurator theConf) {

    String meth = UNIT_NAME + ".prepareXHTMLMessage()";

    String htmlMessage = "";
    try (InputStream htmlTemplateStream =
        this.getClass().getClassLoader().getResourceAsStream(this.htmlTemplateFile)) {
      htmlMessage = IOUtils.toString(htmlTemplateStream);
      htmlTemplateStream.close();
    } catch (IOException e) {
      CrudServiceUtilities.serviceLog(CrudService.ERROR, meth,
          "Could not read HTML error template file");
    }

    // Get things out of the DHCRUD configuration.
    String menuHeaderColorString = "";
    try {
      if (!theConf.getMENUhEADERcOLOR().equals("none")) {
        menuHeaderColorString =
            "    <style>.header{background:" + theConf.getMENUhEADERcOLOR() + "!important}</style>";
      }
    } catch (IoFault e) {
      // Do nothing and take the empty strings!
    }
    String badgeText = "";
    try {
      if (!theConf.getBADGEtEXT().equals("none")) {
        badgeText = " [" + theConf.getBADGEtEXT() + "]";
      }
    } catch (IoFault e) {
      // Do nothing and take the empty strings!
    }

    return MessageFormat.format(htmlMessage, theStatus.getStatusCode(), theStatus.getReasonPhrase(),
        HtmlUtils.htmlEscapeHex(theMessage), HtmlUtils.htmlEscape(theDetail), theVersion,
        menuHeaderColorString, badgeText);
  }

  // **
  // GETTERS and SETTERS
  // **

  /**
   * @param htmlTemplateFile
   */
  public void setHtmlTemplateFile(String htmlTemplateFile) {
    this.htmlTemplateFile = htmlTemplateFile;
  }

}
