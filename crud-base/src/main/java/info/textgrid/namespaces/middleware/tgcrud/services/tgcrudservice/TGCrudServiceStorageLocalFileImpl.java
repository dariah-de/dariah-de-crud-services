/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.CountingOutputStream;
import info.textgrid.middleware.common.JPairtree;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.xml.bind.JAXB;

/**
 * CHANGELOG
 *
 * 2023-09-15 - Funk - Add try-with-resource for streaming.
 *
 * 2022-05-24 - Funk - Fix some SpotBugs issues.
 *
 * 2022-05-23 - Funk - Checking if files do exist before deletion to avoid not-deleted warning in
 * logs, fixes <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/282>
 * 
 * 2021-03-29 - Funk - Remove some deprecation warnings in new CXF version. Add check for existing
 * folders in pairtree path creation to avoid not needed warning.
 *
 * 2018-11-27 - Funk - Creating and delivering TECHMD for public objects, that do not have a TECHMD
 * yet.
 * 
 * 2018-01-12 - Funk - Added TECHMD creation to public tgcrud instance.
 * 
 * 2018-01-12 - Funk - Added deletion of TECHMD files to delete() method.
 * 
 * 2018-01-07 - Funk - Creating and writing TECHMD file TO THE STORAGE!
 * 
 * 2017-08-30 - Funk - Removed all the GAT's and JavaGAT's from class text :-)
 * 
 * 2017-02-24- Funk - Copied from GatImpl.
 * 
 */

/**
 * <p>
 * This storage implementation grants CRUD operations to local file systems.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-09-15
 * @since 2017-02-24
 */

public class TGCrudServiceStorageLocalFileImpl extends CrudStorageAbs<MetadataContainerType> {

  // **
  // STATIC FINALS
  // **

  private static final String UNIT_NAME = "localFile";
  private static final String MESSAGE_DIGEST_TYPE = "MD5";
  private static final String FAILURE_STORING = "Failure storing TO THE STORAGE";
  private static final String FAILURE_READING = "Failure reading FROM THE STORAGE";
  private static final String FAILURE_DELETING = "Failure deleting FROM THE STORAGE";
  private static final String FAILURE_MOVING = "Failure moving TO PUBLIC STORAGE";
  private static final String FAILURE_CREATING_TECH_MD = "Failure creating technical metadata";
  private static final String DELETE_COMPLETE = "Deletion complete";
  private static final String UPDATEMETADATA_COMPLETE = "Metadata update complete";
  private static final boolean IS_PUBLIC = true;
  private static final boolean NOT_PUBLIC = false;

  // **
  // STATICS
  // **

  protected static ExecutorService replicaExecutor = Executors.newCachedThreadPool();

  // **
  // IMPLEMENTED METHODS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage #
   * create(info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject)
   */
  @Override
  public void create(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".create()";

    // Get URI and title.
    URI uri = theObject.getUri();

    try {
      // Get destination path.
      URI destPath = createPairtreeFolders(uri, this.conf.getDEFAULTdATAsTORAGEuRI());

      // Store data TO THE STORAGE.
      storeDataTOTHESTORAGE(uri, theObject, destPath);

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Size of file: " + theObject.getFilesize() + " bytes");
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Message digest of file: " + theObject.getChecksumType() + ":" + theObject.getChecksum());

      // Store metadata TO THE STORAGE.
      // NOTE Filesize and fixity information was written already to the metadata using the abstract
      // CrudObject methods.
      storeMetadataTOTHESTORAGE(uri, (MetadataContainerType) theObject.getMetadata(), destPath);

      // Create technical metadata from destination file (if configured so AND if we have got a
      // public CRUD!).
      if (this.conf.getDIRECTLYpUBLISHwITHcREATE() && this.conf.getEXTRACTtECHMD()) {
        createTECHMD(destPath);
      }

    } catch (UnsupportedEncodingException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    } catch (MetadataParseFault e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    } catch (URISyntaxException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Creation complete");
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #createMetadata(info.textgrid.namespaces.middleware.tgcrud.services. tgcrudservice.CrudObject)
   */
  @Override
  public void createMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage #
   * delete(info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject)
   */
  @Override
  public void delete(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".delete()";

    try {
      // Delete the given TextGrid object by URI.
      deleteTextgridObjectFROMTHESTORAGE(theObject.getUri());
    } catch (UnsupportedEncodingException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_DELETING);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, DELETE_COMPLETE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#deletePublic(info.textgrid.namespaces.middleware.tgcrud.
   * services.tgcrudservice.CrudObject)
   */
  @Override
  public void deletePublic(CrudObject<MetadataContainerType> theObject)
      throws IoFault, RelationsExistFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#deleteMetadataPublic(info.textgrid.namespaces.middleware.
   * tgcrud.services.tgcrudservice.CrudObject)
   */
  @Override
  public void deleteMetadataPublic(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #deleteMetadata(info.textgrid.namespaces.middleware.tgcrud.services. tgcrudservice.CrudObject)
   */
  @Override
  public void deleteMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #getLatestRevision(java.net.URI, boolean)
   */
  @Override
  public int getLatestRevision(URI theUri, boolean unfiltered) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #getLatestRevisionPublic(java.net.URI, boolean)
   */
  @Override
  public int getLatestRevisionPublic(URI theUri, boolean unfiltered)
      throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorageGeneric#retrieve(java.net.URI)
   */
  @Override
  public CrudObject<MetadataContainerType> retrieve(URI theUri) throws IoFault {
    return retrieve(theUri, NOT_PUBLIC);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorageGeneric#retrievePublic(java.net.URI)
   */
  @Override
  public CrudObject<MetadataContainerType> retrievePublic(URI theUri) throws IoFault {
    return retrieve(theUri, IS_PUBLIC);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorageGeneric#retrieveMetadata(java.net.URI)
   */
  @Override
  public MetadataContainerType retrieveMetadata(URI theUri) throws IoFault {
    return retrieveMetadata(theUri, NOT_PUBLIC);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorageGeneric#retrieveMetadataPublic(java.net.URI)
   */
  @Override
  public MetadataContainerType retrieveMetadataPublic(URI theUri) throws IoFault {
    return retrieveMetadata(theUri, IS_PUBLIC);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveAdmMD(java.net.URI)
   */
  @Override
  public MetadataContainerType retrieveAdmMD(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveAdmMDPublic(java.net.URI)
   */
  @Override
  public MetadataContainerType retrieveAdmMDPublic(URI thePid) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveTechMD(java.net.URI)
   */
  @Override
  public String retrieveTechMD(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveTechMDPublic(java.net.URI)
   */
  @Override
  public String retrieveTechMDPublic(URI theUri) throws IoFault {

    String result = "";

    String meth = UNIT_NAME + ".retrieveTechMDPublic()";

    try {
      // Get data and techmd path.
      URI dataPath = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC());
      URI techmdPath = URI.create(dataPath.toString() + LTPUtils.TECHMD_SUFFIX);

      // Read TECHMD FROM THE STORAGE.
      try {
        result = IOUtils.toString(readFROMTHESTORAGE(techmdPath), this.conf.getDEFAULTeNCODING());
      } catch (FileNotFoundException e) {
        // If not existing yet, just create TECHMD on the fly!
        // TODO Maybe remove this again, if TECHMD has been created for every "old" TextGrid object
        // storage wide (still a TODO)!
        if (this.conf.getEXTRACTtECHMD()) {
          createTECHMD(dataPath);
          result = IOUtils.toString(readFROMTHESTORAGE(techmdPath), this.conf.getDEFAULTeNCODING());
        }
      }

      CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
          "Local TECHMD file retrieval complete");

      return result;

    } catch (URISyntaxException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_READING);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_READING);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage #
   * update(info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject)
   */
  @Override
  public void update(CrudObject<MetadataContainerType> theObject) throws IoFault {
    // Just do a create here, and overwrite if existing.
    create(theObject);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #updateMetadata(info.textgrid.namespaces.middleware.tgcrud.services. tgcrudservice.CrudObject)
   */
  @Override
  public void updateMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".updateMetadata()";

    // Get URI and title.
    URI uri = theObject.getUri();

    // Store metadata TO THE STORAGE.
    try {
      // Get destination path.
      URI destPath = getPairtreeObjectUri(uri, this.conf.getDEFAULTdATAsTORAGEuRI());

      storeMetadataTOTHESTORAGE(uri, theObject.getMetadata(), destPath);

    } catch (UnsupportedEncodingException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    } catch (MetadataParseFault e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    } catch (URISyntaxException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_STORING);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, UPDATEMETADATA_COMPLETE);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #move(java.net.URI)
   */
  @Override
  public void move(URI theUri) throws IoFault, ObjectNotFoundFault {

    try {
      // Assemble source and destination URIs.
      URI srcData = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRI());
      URI srcMeta = new URI(srcData.toASCIIString() + this.conf.getMETADATAfILEsUFFIX());
      URI destData = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC());
      URI destMeta = new URI(destData.toASCIIString() + this.conf.getMETADATAfILEsUFFIX());

      // Move metadata and data.
      moveWITHINTHESTORAGE(srcMeta, destMeta);
      moveWITHINTHESTORAGE(srcData, destData);

      // Create technical metadata from destination file (if configured so).
      if (this.conf.getEXTRACTtECHMD()) {
        createTECHMD(destData);
      }

    } catch (UnsupportedEncodingException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_MOVING);
    } catch (URISyntaxException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_MOVING);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_CREATING_TECH_MD);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #copy(java.net.URI)
   */
  @Override
  public void copy(URI theUri) throws IoFault, ObjectNotFoundFault {

    try {
      // Assemble source and destination URIs.
      URI srcData = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRI());
      URI srcMeta = new URI(srcData.toASCIIString() + this.conf.getMETADATAfILEsUFFIX());
      URI destData = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC());
      URI destMeta = new URI(destData.toASCIIString() + this.conf.getMETADATAfILEsUFFIX());

      // Move metadata and data.
      copyONTHESTORAGE(srcMeta, destMeta);
      copyONTHESTORAGE(srcData, destData);

    } catch (UnsupportedEncodingException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_MOVING);
    } catch (URISyntaxException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_MOVING);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #addKeyValuePair(java.net.URI, java.lang.String, java.lang.Object)
   */
  @Override
  public void addKeyValuePair(URI theUri, String theKey, Object theValue) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#resolve(java.net.URI)
   */
  @Override
  public URI resolve(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#create(info.textgrid.namespaces.middleware.tgcrud.services.
   * tgcrudservice.CrudObject, java.lang.String)
   */
  @Override
  public void create(CrudObject<MetadataContainerType> theObject, String theStorageToken)
      throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   * <p>
   * and the storage base path, such as
   * /storage/pairtree_root/te/xt/gr/id/+1/23/45/,0/textgrid+12345,0.
   * </p>
   * 
   * @param theTextgridUri
   * @param theStorageUri
   * @return The storage URI of the pairtree path including TextGrid URI.
   * @throws UnsupportedEncodingException
   * @throws IoFault
   * @throws URISyntaxException
   */
  public static URI getPairtreeObjectUri(URI theTextgridUri, URI theStorageUriBase)
      throws UnsupportedEncodingException, IoFault, URISyntaxException {

    String meth = UNIT_NAME + ".getPairtreeObjectUri()";

    URI result;

    // Get filename from Pairtree encoded ID.
    String pairtreeFilename = new JPairtree(theTextgridUri.toASCIIString()).getPtreeEncodedId();

    // Get the path using JPairtree.
    String pairtreePath =
        getPairtreePathFolderURI(theTextgridUri, theStorageUriBase).toASCIIString();

    result = new URI(pairtreePath + pairtreeFilename);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Object URI: " + result);

    return result;
  }

  /**
   * @param theUri
   * @param theStorageUri
   * @return The URI of the pairtree path.
   * @throws IoFault
   */
  public static URI getPairtreePathFolderURI(URI theUri, URI theStorageUri) throws IoFault {

    URI result;

    try {
      JPairtree pairtree = new JPairtree(theUri.toASCIIString());
      String path = theStorageUri.toASCIIString() + pairtree.getPtreePathStringAsUriFragment();

      result = URI.create(path);

      return result;

    } catch (UnsupportedEncodingException e) {
      throw CrudServiceExceptions.ioFault(e, "Failed to create Pairtree Path");
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Retrieves a TextGrid object FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @param isPublic
   * @return The TextGrid object.
   * @throws IoFault
   */
  private CrudObject<MetadataContainerType> retrieve(URI theUri, boolean isPublic) throws IoFault {

    String meth = UNIT_NAME + ".retrieve()";

    MetadataContainerType metadata = null;
    DataHandler data = null;
    try {
      // Get data and metadata source path.
      URI dataPath = null;
      if (isPublic) {
        dataPath = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC());
      } else {
        dataPath = getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRI());
      }
      URI metadataPath = new URI(dataPath.toASCIIString() + this.conf.getMETADATAfILEsUFFIX());

      // Read metadata FROM THE STORAGE.
      metadata = TGCrudServiceUtilities.createMetadataContainer(readFROMTHESTORAGE(metadataPath));

      // Read data FROM THE STORAGE.
      data = new DataHandler(new FileDataSource(readFileFROMTHESTORAGE(dataPath)));

    } catch (UnsupportedEncodingException | URISyntaxException | FileNotFoundException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_READING);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Local file retrieval complete");

    return new TGCrudTextGridObject(metadata, data);
  }

  /**
   * <p>
   * Retrieves metadata FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @param isPublic
   * @return The TextGrid metadata object.
   * @throws IoFault
   */
  private MetadataContainerType retrieveMetadata(URI theUri, boolean isPublic) throws IoFault {

    String meth = UNIT_NAME + ".retrieveMetadata()";

    MetadataContainerType metadata = null;
    try {
      // Get source path and add metadata suffix.
      URI metadataPath = null;
      if (isPublic) {
        metadataPath =
            new URI(getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC())
                + this.conf.getMETADATAfILEsUFFIX());
      } else {
        metadataPath = new URI(getPairtreeObjectUri(theUri, this.conf.getDEFAULTdATAsTORAGEuRI())
            + this.conf.getMETADATAfILEsUFFIX());
      }

      // Read metadata FROM THE STORAGE.
      metadata = TGCrudServiceUtilities.createMetadataContainer(readFROMTHESTORAGE(metadataPath));

    } catch (UnsupportedEncodingException | URISyntaxException | FileNotFoundException e) {
      throw CrudServiceExceptions.ioFault(e, FAILURE_READING);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Retrieval complete: " + theUri);

    return metadata;
  }

  /**
   * <p>
   * Stores the data file TO THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @param theObject
   * @param thePath
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws UnsupportedEncodingException
   * @throws URISyntaxException
   */
  private static void storeDataTOTHESTORAGE(URI theUri, CrudObject<MetadataContainerType> theObject,
      URI thePath)
      throws IoFault, MetadataParseFault, UnsupportedEncodingException, URISyntaxException {

    String meth = UNIT_NAME + ".storeDataTOTHESTORAGE()";

    // Store data TO THE STORAGE.
    storeDataStreamTOTHESTORAGE(theUri, theObject, thePath);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Data file stored: " + thePath);
  }

  /**
   * <p>
   * Stores the metadata file TO THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @param theMetadata
   * @param thePath
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws URISyntaxException
   * @throws UnsupportedEncodingException
   */
  private void storeMetadataTOTHESTORAGE(URI theUri, MetadataContainerType theMetadata, URI thePath)
      throws IoFault, MetadataParseFault, URISyntaxException, UnsupportedEncodingException {

    String meth = UNIT_NAME + ".storeMetadataTOTHESTORAGE()";

    // Get destination path and add metadata suffix.
    URI metadataPath = new URI(thePath + this.conf.getMETADATAfILEsUFFIX());

    // Store metadata TO THE STORAGE.
    storeXmlStreamTOTHESTORAGE(theUri, theMetadata, metadataPath);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Metadata file stored: " + metadataPath);
  }

  /**
   * <p>
   * Stores a given InputStream TO THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @param theObject
   * @param theStorageLocation
   * @throws IoFault
   */
  private synchronized static void storeDataStreamTOTHESTORAGE(URI theUri,
      CrudObject<MetadataContainerType> theObject, URI theStorageLocation) throws IoFault {

    TGCrudServiceVersion tgcrudServiceVersion = new TGCrudServiceVersion();

    // return if <data> is empty.
    if (theObject.getData() == null) {
      theObject.setFilesize(0);
      return;
    }

    // Store and compute file size.
    // Create a counting stream around a file output stream.
    try (CountingOutputStream count =
        new CountingOutputStream(new FileOutputStream(new File(theStorageLocation)))) {
      // Create a message digest stream around the counting stream.
      MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST_TYPE);
      try (DigestOutputStream digest = new DigestOutputStream(count, md)) {
        // Write TO THE STORAGE using the digest stream using the counting stream :-)
        theObject.getData().writeTo(digest);
      }

      // Do count, get the message digest, and set things to get it into the metadata!
      theObject.setFilesize(count.getByteCount());
      theObject.setChecksum(new String(Hex.encodeHex(md.digest())));
      theObject.setChecksumOrigin(
          tgcrudServiceVersion.getSERVICENAME() + " " + tgcrudServiceVersion.getVERSION());
      theObject.setChecksumType(MESSAGE_DIGEST_TYPE.toLowerCase());

    } catch (IOException | NoSuchAlgorithmException e) {
      throw CrudServiceExceptions.ioFault(e,
          "Failed writing to OutputStream: " + theStorageLocation);
    }
  }

  /**
   * <p>
   * Stores a TextGrid metadata object TO THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @param theMetadata
   * @param theStorageLocation
   * @throws IoFault
   * @throws MetadataParseFault
   */
  private synchronized static void storeXmlStreamTOTHESTORAGE(URI theUri,
      MetadataContainerType theMetadata, URI theStorageLocation)
      throws IoFault, MetadataParseFault {

    // Serialise the metadata TO THE STORAGE.
    try (FileOutputStream fos = new FileOutputStream(new File(theStorageLocation))) {
      JAXB.marshal(theMetadata, fos);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e,
          "Failed writing to OutputStream: " + theStorageLocation);
    }
  }

  /**
   * <p>
   * Reads a file FROM THE STORAGE using the given URI.
   * </p>
   * 
   * @param theUri
   * @return The file returned FROM THE STORAGE.
   */
  private static File readFileFROMTHESTORAGE(URI theUri) {

    String meth = UNIT_NAME + ".readFileFROMTHESTORAGE()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Reading: " + theUri);

    return new File(theUri);
  }

  /**
   * <p>
   * Reads a file FROM THE STORAGE using the given URI.
   * </p>
   * 
   * @param theUri
   * @return The file input stream FROM THE STORAGE.
   * @throws FileNotFoundException
   * @throws IoFault
   */
  private static FileInputStream readFROMTHESTORAGE(URI theUri) throws FileNotFoundException {

    String meth = UNIT_NAME + ".readFROMTHESTORAGE()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Reading: " + theUri);

    return new FileInputStream(new File(theUri));
  }

  /**
   * <p>
   * Moves a file FROM THE STORAGE location TO ANOTHER STORAGE location.
   * </p>
   * 
   * @param theSourceUri
   * @param theDestinationUri
   * @throws IoFault
   */
  private void moveWITHINTHESTORAGE(URI theSourceUri, URI theDestinationUri) throws IoFault {

    String meth = UNIT_NAME + ".moveWITHINTHESTORAGE()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Moving " + theSourceUri + " to " + theDestinationUri);

    // Create source and destination file.
    File src = new File(theSourceUri);
    File dest = new File(theDestinationUri);

    // Only try to move, if source file is existing.
    if (src.exists()) {
      // Create all needed destination folders, and move.
      synchronized (this) {
        dest.getParentFile().mkdirs();
      }

      boolean moved = src.renameTo(dest);

      // Check if file was moved.
      if (!moved) {
        throw CrudServiceExceptions.ioFault("Source file " + src.getAbsolutePath()
            + " was NOT moved to " + dest.getAbsolutePath() + "!");
      }

    } else {
      throw CrudServiceExceptions.ioFault("Source file does not exist: " + theSourceUri);
    }
  }

  /**
   * <p>
   * Copies a file FROM THE STORAGE location TO ANOTHER STORAGE location.
   * </p>
   * 
   * @param theSourceUri
   * @param theDestinationUri
   * @throws IoFault
   */
  private void copyONTHESTORAGE(URI theSourceUri, URI theDestinationUri) throws IoFault {

    String meth = UNIT_NAME + ".copyONTHESTORAGE()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Copying '" + theSourceUri + "' to '" + theDestinationUri + "'");

    // Create source and destination file.
    File src = new File(theSourceUri);
    File dest = new File(theDestinationUri);

    // Only try to copy, if source file is existing.
    if (src.exists()) {
      // Create all needed destination folders, and copy.
      synchronized (this) {
        dest.getParentFile().mkdirs();
      }

      try {
        FileUtils.copyFile(src, dest);
      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault("Source file " + src.getAbsolutePath()
            + " was NOT copied to " + dest.getAbsolutePath() + "! [" + e.getMessage() + "]");
      }

    } else {
      throw CrudServiceExceptions.ioFault("Source file does not exist: " + theSourceUri);
    }
  }

  /**
   * <p>
   * Deletes a TextGrid object FROM THE STORAGE, including metadata, data, and techmd file.
   * </p>
   * 
   * @param theUri
   * @throws IoFault
   * @throws UnsupportedEncodingException
   */
  private void deleteTextgridObjectFROMTHESTORAGE(URI theUri)
      throws IoFault, UnsupportedEncodingException {

    try {
      // Create URIs for JPairtree files, and delete them.
      JPairtree pairtree = new JPairtree(theUri.toASCIIString());
      URI dataURI = new URI(this.conf.getDEFAULTdATAsTORAGEuRI().toASCIIString()
          + pairtree.getPtreePathStringAsUriFragment() + pairtree.getPtreeEncodedId());
      URI metadataURI = new URI(dataURI.toString() + this.conf.getMETADATAfILEsUFFIX());
      URI techmdURI = new URI(dataURI.toString() + LTPUtils.TECHMD_SUFFIX);

      // Then delete.
      deleteFROMTHESTORAGE(dataURI);
      deleteFROMTHESTORAGE(metadataURI);
      deleteFROMTHESTORAGE(techmdURI);

    } catch (URISyntaxException e) {
      throw CrudServiceExceptions.ioFault(e,
          "Unable to create URI from given filename! " + e.getMessage());
    }
  }

  /**
   * <p>
   * Deletes a file FROM THE STORGAE, if existing.
   * </p>
   * 
   * @param theUri
   * @return Tells if deletion succeeded or not.
   * @throws IoFault
   */
  private boolean deleteFROMTHESTORAGE(URI theUri) throws IoFault {

    boolean result = false;

    String meth = UNIT_NAME + ".deleteFROMTHESTORAGE()";

    // Create file.
    File file = new File(theUri);

    // Delete existing file in synchronised mode.
    synchronized (this) {
      if (file.exists()) {
        result = file.delete();
        // Check if file was deleted.
        if (result) {
          CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "File deleted: " + theUri);
        } else {
          CrudServiceUtilities.serviceLog(CrudService.WARN, meth,
              "File NOT deleted: " + theUri.toString());
        }
      } else {
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
            "File does not exist: " + theUri.toString());
      }
    }

    return result;
  }

  /**
   * <p>
   * Creates all folders contained in a given URI based on a created pairtree path.
   * </p>
   * 
   * @param theUri
   * @param theStorageUri
   * @return The URI of the pairtree path.
   * @throws IoFault
   */
  private URI createPairtreePathFolders(URI theUri, URI theStorageUri) throws IoFault {

    String meth = UNIT_NAME + ".createPairtreePathFolders()";

    URI uri = getPairtreePathFolderURI(theUri, theStorageUri);

    File currentFile = new File(uri);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Creating folders: " + uri.toASCIIString());

    // Create all the needed JPairtree folders.
    long startTime = System.currentTimeMillis();

    // Only create folders if not already existing.
    if (!currentFile.exists()) {
      synchronized (this) {
        // Also creates the "pairtree_root" folder if not yet existing.
        boolean result = currentFile.mkdirs();
        // Check if folders were created.
        if (result) {
          CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
              "Folders successfully created: " + currentFile.getAbsolutePath());
        } else {
          CrudServiceUtilities.serviceLog(CrudService.WARN, meth,
              "Not every folder for URI " + uri.toASCIIString() + " was created!");
        }
      }
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Using existing folder " + currentFile.getName());
    }

    String duration =
        CrudServiceUtilities.getDuration(+(System.currentTimeMillis() - startTime));
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Folder creation processed in " + duration);

    return uri;
  }

  /**
   * <p>
   * Creates all folders from a Pairtree path using a TextGrid URI and the storage base path, such
   * as /storage/pairtree_root/te/xt/gr/id/+1/23/45/,0/. All folders will be created!
   * </p>
   * 
   * @param theUri
   * @param theStorageUri
   * @return The Storage URI of the pairtree path.
   * @throws UnsupportedEncodingException
   * @throws IoFault
   * @throws URISyntaxException
   */
  private URI createPairtreeFolders(URI theUri, URI theStorageUri)
      throws UnsupportedEncodingException, IoFault, URISyntaxException {

    String meth = UNIT_NAME + ".createPairtreeFolders()";

    URI result;

    // Get filename from Pairtree encoded ID.
    String filename = new JPairtree(theUri.toASCIIString()).getPtreeEncodedId();

    // Get the path using JPairtree.
    String path = createPairtreePathFolders(theUri, theStorageUri).toASCIIString();

    result = new URI(path + filename);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Folder URI: " + result);

    return result;
  }

  /**
   * @param theData
   * @throws IoFault
   * @throws URISyntaxException
   * @throws IOException
   */
  private void createTECHMD(URI theData) throws IoFault, URISyntaxException, IOException {

    String meth = UNIT_NAME + "createTECHMD()";

    URI destTechmd = new URI(theData.toASCIIString() + LTPUtils.TECHMD_SUFFIX);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Creating TECHMD from data file: "
        + theData.toString() + " (" + this.conf.getFITSlOCATION().toString() + ")");

    String techMD = LTPUtils.extractTechMD(this.conf.getFITSlOCATION().toString(),
        this.conf.getFITScLIENTsTUBtIMEOUT(), new File(theData));

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "TECHMD extraction complete");

    // Write TECHMD file TO THE STORAGE.
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Writing TECHMD file to public storage: " + destTechmd);

    try (FileOutputStream fos = new FileOutputStream(new File(destTechmd))) {
      IOUtils.write(techMD, fos, this.conf.getDEFAULTeNCODING());
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "TECHMD file creation complete");
  }

}
