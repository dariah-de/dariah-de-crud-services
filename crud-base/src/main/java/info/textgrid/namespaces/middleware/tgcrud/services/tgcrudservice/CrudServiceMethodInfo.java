/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.util.UUID;
import info.textgrid.middleware.common.CrudOperations;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2014-10-23 - Funk - Using TGCrudServiceMethodNames now.
 * 
 * 2014-07-14 - Funk - Removed TextGrid logging.
 * 
 * 2011-12-14 - Funk - Added config file location. Hmpf. Removed again!
 * 
 * 2010-03-31 - Funk - First version.
 *
 */

/**
 * <p>
 * Information object to create instance information for every method call. Used for singleton use
 * of the TG-crud implementation class, so no instance variables must be used.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-02-11
 * @since 2011-03-31
 */

public class CrudServiceMethodInfo {

  /**
   * <p>
   * An unique ID created for every instance of this class used for logging.
   * </p>
   */
  private UUID uuid;
  private int name;

  /**
   * <p>
   * Constructor.
   * </p>
   * 
   * @param theName
   */
  public CrudServiceMethodInfo(int theName) {
    this.uuid = UUID.randomUUID();
    this.name = theName;
  }

  // **
  // GETTERS and SETTERS
  // **

  /**
   * @param theName
   */
  public void setName(int theName) {
    this.name = theName;
  }

  /**
   * @return The name of the crud method.
   */
  public String getName() {
    return CrudOperations.getName(this.name);
  }

  /**
   * @return The UUID of the crud method's instance.
   */
  public UUID getUuid() {
    return this.uuid;
  }

  /**
   * @param theUuid
   */
  public void setUuid(UUID theUuid) {
    this.uuid = theUuid;
  }

}
