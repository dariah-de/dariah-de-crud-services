/**
 * This software is copyright (c) 2024 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vertx.amqp.AmqpClient;
import io.vertx.amqp.AmqpClientOptions;
import io.vertx.amqp.AmqpMessage;
import io.vertx.amqp.AmqpSender;
import io.vertx.core.Future;

/**
 * <p>
 * See <https://www.rabbitmq.com/tutorials/amqp-concepts.html>
 * </p>
 */
public class CrudServiceMessageProducer {

  // **
  // FINALS
  // **

  static final ObjectMapper mapper = new ObjectMapper();

  // **
  // CLASS
  // **

  private AmqpClient client;
  private AmqpSender sender;

  /**
   * @param amqpHost
   * @throws IoFault
   */
  public CrudServiceMessageProducer(CrudServiceConfigurator theConf) throws IoFault {
    setupMessaging(theConf);
  }

  /**
   * @throws IoFault
   */
  private void setupMessaging(CrudServiceConfigurator theConf) throws IoFault {

    String meth = "setupMessaging()";

    String host = theConf.getMESSAGINGsERVICEuRL().getHost();
    int port = theConf.getMESSAGINGsERVICEuRL().getPort();
    String eventName = theConf.getMESSAGINGeVENTnAME();
    String user = theConf.getMESSAGINGuSER();
    String pass = theConf.getMESSAGINGpASS();

    /*
     * Host, port, username and password can also be configured from system properties or
     * environment variables:
     * 
     * Host: system property: amqp-client-host, environment variable: AMQP_CLIENT_HOST` (mandatory)
     * Port: system property: amqp-client-port, environment variable: AMQP_CLIENT_PORT` (defaults to
     * 5672) Username: system property: amqp-client-username, environment variable:
     * AMQP_CLIENT_USERNAME` Password: system property: amqp-client-password, environment variable:
     * AMQP_CLIENT_PASSWORD`
     * 
     * See <https://vertx.io/docs/vertx-amqp-client/java/#_creating_an_amqp_client>
     */

    // TODO When to close the client, or connection, or sender?
    // See <https://vertx.io/docs/vertx-amqp-client/java/#_closing_the_client>
    AmqpClientOptions options = new AmqpClientOptions()
        .setHost(host)
        .setPort(port)
        .setUsername(user)
        .setPassword(pass);

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
        "Setup messaging: host=" + options.getHost() + ", port=" + options.getPort()
            + ", event name=" + eventName + ", user=" + options.getUsername() + ", pass="
            + (options.getPassword() != null && !options.getPassword().isEmpty() ? "***"
                : "NULL or EMPTY"));

    // Create a client using its own internal Vert.x instance.
    this.client = AmqpClient.create(options);

    // You can also create a sender directly from the client: In this case, a connection is
    // established automatically. You can retrieve it using connection.
    Future<AmqpSender> future = this.client.createSender(eventName)
        .onComplete(done -> {
          if (done.failed()) {
            CrudServiceUtilities.serviceLog(CrudService.ERROR, meth,
                "Unable to create message sender for " + eventName);
          } else {
            CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
                "Creating message sender for " + eventName + " complete");
            this.sender = done.result();
          }
        });

    while (!future.isComplete()) {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Connecting...");
      try {
        Thread.sleep(300);
      } catch (InterruptedException e) {
        CrudServiceUtilities.serviceLog(CrudService.WARN, meth,
            "Something has been interrupted here! I have no idea! " + e.getClass().getName() + " :"
                + e.getMessage());
      }
    }
  }

  /**
   * @param theConf
   */
  public void sendMessage(CrudServiceConfigurator theConf, CrudServiceMessage theMessage) {

    String meth = "sendMessage()";

    // Sender may be null, if AMPQ host was not reachable on startup or connection may not be alive,
    // e.g. after Artemis restart.
    if (this.sender == null || this.sender.connection().isDisconnected()) {
      // TODO If there is already an sender existing, we should at least close the old connection,
      // no?
      // See <https://vertx.io/docs/vertx-amqp-client/java/#_closing_the_client>
      try {
        setupMessaging(theConf);
      } catch (IoFault e) {
        CrudServiceUtilities.serviceLog(CrudService.WARN, meth,
            "Message sender could not be re-initiated!");
      }
    }

    String msgString;
    try {
      msgString = mapper.writeValueAsString(theMessage);

      // Send without ack.
      // sender.send(AmqpMessage.create().withBody(msgString).build());

      // Check for sender existence.
      if (this.sender == null) {
        CrudServiceUtilities.serviceLog(CrudService.ERROR, meth,
            "No sender found! Message can not be send for " + theMessage.getRootId()
                + "! Please check your message service!");
      } else {
        // Send with ack.
        this.sender.sendWithAck(AmqpMessage.create().withBody(msgString).build())
            .onComplete(acked -> {
              if (acked.succeeded()) {
                CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
                    "Message accepted for " + theMessage.getObjectId());
              } else {
                CrudServiceUtilities.serviceLog(CrudService.WARN, meth,
                    "Message REJECTED for " + theMessage.getObjectId());
              }
            });

        CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Message sent: " + theMessage);
      }

    } catch (JsonProcessingException e) {
      CrudServiceUtilities.serviceLog(CrudService.WARN, meth,
          "Error assembling JSON message due to a " + e.getClass().getName() + ": "
              + e.getMessage());
    }
  }

}
