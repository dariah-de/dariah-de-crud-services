/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2023-02-15 - Funk - Refactor date methods.
 *
 * 2022-02-11 - Funk - Add process ID to log.
 *
 * 2022-02-07 - Funk - Update logger library.
 * 
 * 2018-06-13 - Funk - Generalized with TGCrudServiceUtilities.
 */

/**
 * <p>
 * The CrudServiceUtilities class provides some utilities, needed by all the CrudService classes.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */

public class CrudServiceUtilities {

  // **
  // STATIC FINALS
  // **

  protected static final String UNIT_NAME = "util";
  protected static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
  public static final DateTimeFormatter CET_FORMATTER =
      DateTimeFormatter.ofPattern(DATE_FORMAT).withZone(ZoneId.of("CET"));
  protected static final String START_LOGCHARS = "==========  ";
  protected static final String END_LOGCHARS = "  ==========";

  private static final int SECS_IN_MILLIS = 1000;
  private static final int MINS_IN_SECS = 60;
  private static final int HORS_IN_MINS = 60;
  private static final int DAYS_IN_HORS = 24;

  private static final String HANDLE = "handle";
  private static final String VALUES = "values";
  private static final String VALUE = "value";
  private static final String TYPE = "type";
  private static final String DATA = "data";

  // **
  // STATICS
  // **

  /**
   * <p>
   * The crud logger logs all information with the given log level from the crud.properties file.
   * Please configure the crud.log4j as stated in the crud.properties configuration file.
   * </p>
   */
  protected static volatile Logger crudLogger = null;

  /**
   * <p>
   * The crud internal logger only logs information to retrace already finished tasks and errors to
   * a certain logfile. Please configure the crud.log4j as stated in the crud.propertioes
   * configuration file.
   * </p>
   */
  protected static volatile Logger crudRollbackLogger = null;

  /**
   * <p>
   * The log4j root logger will only be used to log errors to System.out, e.g. Tomcat's
   * catalina.out.
   * </p>
   */
  protected static volatile Logger rootLogger = null;

  // **
  // DATE AND TIME UTILITIES
  // **

  /**
   * <p>
   * Returns the duration of the given milliseconds in seconds, minutes, hours, or days.
   * </p>
   * 
   * @param theMillis
   * @return A string using days, hours, minutes, and seconds to express the given duration in
   *         millis.
   */
  public static String getDuration(long theMillis) {

    int secs = SECS_IN_MILLIS;
    int mins = secs * MINS_IN_SECS;
    int hors = mins * HORS_IN_MINS;
    int days = hors * DAYS_IN_HORS;

    if (theMillis > days) {
      return theMillis / days + " day" + ((theMillis / days) != 1 ? "s" : "");
    } else if (theMillis > hors) {
      return theMillis / hors + " hour" + ((theMillis / hors) != 1 ? "s" : "");
    } else if (theMillis > mins) {
      return theMillis / mins + " minute" + ((theMillis / mins) != 1 ? "s" : "");
    } else if (theMillis > secs) {
      return theMillis / secs + " second" + ((theMillis / secs) != 1 ? "s" : "");
    } else {
      return theMillis + " millisecond" + (theMillis != 1 ? "s" : "");
    }
  }

  /**
   * <p>
   * Creates a formatted date string based on the given calendar and in CET timezone.
   * </p>
   * 
   * @param theCalendar
   * @return A pretty formatted date string from the given date format in CET.
   */
  public static String getCETDateString(XMLGregorianCalendar theCalendar) {
    return CET_FORMATTER.format(theCalendar.toGregorianCalendar().toInstant());
  }

  /**
   * @param theDate
   * @return
   */
  public static Date getDateFromCETString(String theDate) {

    Instant instant;
    try {
      OffsetDateTime odt = OffsetDateTime.parse(theDate);
      instant = Instant.from(odt);
    }

    // NOTE We are missing to set time zones on dcterms date fields in administrative metadata!
    // Workaround for all DARIAH-DE Repository dates so far (until fix) is using LocalDateTime here
    // with ZoneId CET, what has been local time zone until now!
    catch (DateTimeParseException e) {
      LocalDateTime ldt = LocalDateTime.parse(theDate);
      instant = ldt.atZone(ZoneId.of("CET")).toInstant();
    }

    return Date.from(instant);
  }

  /**
   * <p>
   * Creates a XMLKGregorianCalendar based on the given milliseconds-since-unix-was-born time and
   * sets system timezone.
   * </p>
   * 
   * @param theMillis
   * @return A pretty formatted date string from the given format and milliseconds.
   * @throws IoFault
   */
  public static XMLGregorianCalendar getXMLGregorianCalendar(long theMillis) throws IoFault {

    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(theMillis);
    cal.setTimeZone(TimeZone.getDefault());

    try {
      return DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
    } catch (DatatypeConfigurationException e) {
      throw CrudServiceExceptions.ioFault(e,
          "Unable to create XMLGregorianCalendar: " + e.getMessage());
    }
  }

  /**
   * @return The year.
   * @throws DatatypeConfigurationException
   */
  protected static int getYearFromSystemTime() throws DatatypeConfigurationException {

    int result;

    GregorianCalendar cal = new GregorianCalendar();
    cal.setTimeInMillis(System.currentTimeMillis());
    XMLGregorianCalendar now = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
    result = now.getYear();

    return result;
  }

  // **
  // LOG UTILITIES
  // **

  /**
   * @param theInfo
   * @param theVersion
   * @return The version string.
   */
  public static String getVersionMethodLog(CrudServiceMethodInfo theInfo,
      CrudServiceVersion theVersion) {
    return START_LOGCHARS + "#" + theInfo.getName() + " | " + theVersion.getSERVICENAME()
        + " service | version " + theVersion.getVERSION() + "+" + theVersion.getBUILDDATE()
        + " | up " + getDuration(System.currentTimeMillis() - CrudService.getServiceStartTime())
        + END_LOGCHARS;
  }

  /**
   * @param theInfo
   * @param theVersion
   * @return A start method log string.
   */
  public static String startMethodLog(CrudServiceMethodInfo theInfo,
      CrudServiceVersion theVersion) {
    return START_LOGCHARS + "#" + theInfo.getName() + " started"
        + CrudServiceUtilities.END_LOGCHARS + "  " + theVersion.getVERSION() + "."
        + theVersion.getBUILDDATE() + " | up "
        + getDuration(System.currentTimeMillis() - CrudService.getServiceStartTime())
        + END_LOGCHARS;
  }

  /**
   * @param theInfo
   * @return An end method log string.
   */
  public static String endMethodLog(CrudServiceMethodInfo theInfo) {
    return START_LOGCHARS + "#" + theInfo.getName() + " complete" + END_LOGCHARS;
  }

  // **
  // GET RESOURCES
  // **

  /**
   * <p>
   * Load a resource using the log4j class loader.
   * </p>
   * 
   * @param resPart
   * @return The resource.
   * @throws IOException
   */
  public static File getResource(String resPart) throws IOException {

    File res;

    // Create an URL out of the resPart string.
    URL url = ClassLoader.getSystemResource(resPart);
    if (url == null) {
      throw new IOException("Resource not found: " + resPart);
    }

    // Create file if resPart can be transformed into an URL.
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    // Throw exception if file was not found.
    if (!res.exists()) {
      throw new IOException("Resource not found: " + resPart);
    }

    return res;
  }

  // **
  // INTERNAL SERVICE LOGGING
  // **

  /**
   * <p>
   * Assembles log message and logs, method name is given only, for callers without
   * CrudServiceMethodInfo.
   * </p>
   * 
   * @param theLogLevel
   * @param theMethodName
   * @param theLogMessage
   * @return
   */
  protected static void serviceLog(final int theLogLevel, final String theMethodName,
      final String theLogMessage) {

    String combinedLogMessage = "crud." + theMethodName + "\t" + theLogMessage.trim();

    serviceLog(theLogLevel, combinedLogMessage);
  }

  /**
   * <p>
   * Finally logs.
   * </p>
   * 
   * @param theLogLevel
   * @param theLogMessage
   */
  private static void serviceLog(final int theLogLevel, final String theLogMessage) {

    if (crudLogger != null && rootLogger != null) {
      switch (theLogLevel) {
        case CrudService.DEBUG:
          crudLogger.debug(theLogMessage);
          break;
        case CrudService.INFO:
          crudLogger.info(theLogMessage);
          break;
        case CrudService.WARN:
          crudLogger.warn(theLogMessage);
          break;
        case CrudService.ERROR:
          crudLogger.error(theLogMessage);
          break;
        default:
          // Nothing to log here.
      }
    }
  }

  /**
   * <p>
   * Instantiates the AAI implementation class to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The AAI implementation class instance.
   * @throws IoFault
   */
  public static CrudServiceAai getAaiImplementation(CrudServiceConfigurator theConfiguration)
      throws IoFault {

    String meth = UNIT_NAME + ".getAaiImplementation()";

    Class<?> aaiImplementation;
    try {
      aaiImplementation = TGCrudServiceUtilities.class.getClassLoader()
          .loadClass(theConfiguration.getAAIiMPLEMENTATION());
      CrudServiceAai result =
          (CrudServiceAai) aaiImplementation.getDeclaredConstructor().newInstance();
      result.setConfiguration(theConfiguration);

      serviceLog(CrudService.DEBUG, meth,
          "Instantiated AAI implementation: " + theConfiguration.getAAIiMPLEMENTATION());

      return result;

    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException e) {
      throw CrudServiceExceptions.ioFault(e,
          "Crud service AAI implementation class [" + theConfiguration.getAAIiMPLEMENTATION()
              + "] can  not be instanciated due to a " + e.getClass().getName() + ": "
              + e.getMessage());
    }
  }

  /**
   * <p>
   * Instantiates the URI implementation class to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The ID implementation class instance.
   * @throws IoFault
   */
  public static CrudServiceIdentifier getIdImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {

    String meth = UNIT_NAME + ".getIdImplementation()";

    Class<?> uriImplementation;
    try {
      uriImplementation = TGCrudServiceUtilities.class.getClassLoader()
          .loadClass(theConfiguration.getIDiMPLEMENTATION());
      CrudServiceIdentifier result =
          (CrudServiceIdentifier) uriImplementation.getDeclaredConstructor().newInstance();
      result.setConfiguration(theConfiguration);

      serviceLog(CrudService.DEBUG, meth,
          "Instantiated identifier implementation: " + theConfiguration.getIDiMPLEMENTATION());

      return result;

    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException e) {
      throw CrudServiceExceptions.ioFault(e,
          "Crud service identifier implementation class [" + theConfiguration.getIDiMPLEMENTATION()
              + "] can  not be instanciated due to a " + e.getClass().getName() + ": "
              + e.getMessage());
    }
  }

  /**
   * @param theJSONDeletedMetadata
   * @param theType
   * @return
   * @throws JSONException
   * @throws ParseException
   */
  public static boolean checkTypeInHandleMetadata(final String theJSONTypeMetadata,
      final String theType) throws JSONException, ParseException {

    boolean result = false;

    JSONObject jsonTypeMetadata = new JSONObject(theJSONTypeMetadata);
    if (jsonTypeMetadata.has(VALUES)) {
      JSONArray values = jsonTypeMetadata.getJSONArray(VALUES);
      for (int i = 0; i < values.length(); i++) {
        JSONObject o = values.getJSONObject(i);
        if (o.has(TYPE) && o.getString(TYPE).equals(theType)) {
          result = true;
          break;
        }
      }
    }

    return result;
  }

  /**
   * <p>
   * Get Handle metadata String value for a Handle metadata TYPE or complete set.
   * </p>
   * 
   * @param theJSONTypeMetadata The JSON metadata string.
   * @param theType The type to fetch from HDL metadata, use "" for complete metadata set.
   * @return Type string value if type value existing, empty string if not.
   * @throws JSONException
   */
  public static String getHandleMetadata(final String theJSONTypeMetadata,
      final String theType) throws JSONException {

    String result = "";

    JSONObject jsonTypeMetadata = new JSONObject(theJSONTypeMetadata);
    if (jsonTypeMetadata.has(VALUES)) {
      JSONArray values = jsonTypeMetadata.getJSONArray(VALUES);
      for (int i = 0; i < values.length(); i++) {
        JSONObject o = values.getJSONObject(i);
        if (o.has(TYPE) && o.getString(TYPE).equals(theType)) {
          JSONObject o2 = o.getJSONObject(DATA);
          result = o2.getString(VALUE);
          break;
        }
      }
    }

    return result;
  }

  /**
   * <p>
   * Get actual Handle PID from Handle metadata JSON string.
   * </p>
   * 
   * @param theJSONTypeMetadata
   * @return PID string if existing, empty string if not.
   * @throws JSONException
   */
  public static String getCurrentHandlePID(final String theJSONTypeMetadata) throws JSONException {

    String result = "";

    JSONObject jsonTypeMetadata = new JSONObject(theJSONTypeMetadata);
    if (jsonTypeMetadata.has(HANDLE)) {
      result = jsonTypeMetadata.getString(HANDLE);
    }

    return result;
  }

}
