/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * TextGrid Consortium (http://www.textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 * 
 * 2017-04-07 - Funk - Added public delete and deleteMetadata methods.
 * 
 * 2015-08-13 - Funk - Added ADM and TECH metadata methods.
 * 
 * 2015-03-19 - Funk - Added unfiltered flag for getLatestRevision() methods.
 * 
 * 2015-02-12 - Funk - Generalised with TG and DHCrudServiceStorage interface.
 * 
 * 2013-10-14 - Funk - Removed the createPublic() and createMetadataPublic() methods for we do not
 * need it with our TG-crud public instance.
 * 
 * 2012-03-21 - Funk - Added new interface method copy().
 * 
 * 2011-12-19 - Funk - Added new interface method getLatestRevisionPublic().
 * 
 * 2010-09-09 - Funk - Taken from (former) TG-crud version 1.2. Added update() and createMetadata()
 * methods.
 * 
 * 2010-01-27 - Funk - First version.
 * 
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * This storage interface provides access to the backend storage locations, like THE GRID using
 * different APIs, such as JavaGAT and SAGA, or other Document Management Systems, such as Fedora,
 * Sesame, eXist, ElasticSearch, or the DARIAH Storage.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2018-06-14
 * @since 2015-02-12
 * 
 * @param <T>
 ******************************************************************************/

public interface CrudStorage<T> {

  /**
   * <p>
   * Stores an object TO THE STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   */
  void create(CrudObject<T> theObject) throws IoFault;

  /**
   * <p>
   * Stores an object TO THE STORAGE using a storage token such as OAuth.
   * </p>
   * 
   * @param theObject
   * @param theStorageToken
   * @throws IoFault
   */
  void create(CrudObject<T> theObject, String theStorageToken) throws IoFault;

  /**
   * <p>
   * Stores an object's metadata TO THE STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   */
  void createMetadata(CrudObject<T> theObject) throws IoFault;

  /**
   * <p>
   * Updates an object TO THE STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   */
  void update(CrudObject<T> theObject) throws IoFault;

  /**
   * <p>
   * Updates an object's metadata TO THE STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   */
  void updateMetadata(CrudObject<T> theObject) throws IoFault;

  /**
   * <p>
   * Retrieves a complete object FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The non-public object.
   * @throws IoFault
   */
  CrudObject<T> retrieve(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves a complete object FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The public object.
   * @throws IoFault
   */
  CrudObject<T> retrievePublic(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves metadata only FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The non-public service object, containing metadata only.
   * @throws IoFault
   */
  T retrieveMetadata(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves metadata only FROM THE PUBLIC STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The public service object, containing metadata only.
   * @throws IoFault
   */
  T retrieveMetadataPublic(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves administrative metadata only FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The non-public service object, containing metadata only.
   * @throws IoFault
   */
  T retrieveAdmMD(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves administrative metadata only FROM THE PUBLIC STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The public service object, containing metadata only.
   * @throws IoFault
   */
  T retrieveAdmMDPublic(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves technical metadata only FROM THE STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The non-public service object, containing technical metadata only.
   * @throws IoFault
   */
  String retrieveTechMD(URI theUri) throws IoFault;

  /**
   * <p>
   * Retrieves technical metadata only FROM THE PUBLIC STORAGE.
   * </p>
   * 
   * @param theUri
   * @return The public service object, containing technical metadata only.
   * @throws IoFault
   */
  String retrieveTechMDPublic(URI theUri) throws IoFault;

  /**
   * <p>
   * Deletes an object FROM THE STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   * @throws RelationsExistFault
   */
  void delete(CrudObject<T> theObject) throws IoFault, RelationsExistFault;

  /**
   * <p>
   * Deletes an object FROM THE PUBLIC STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   * @throws RelationsExistFault
   */
  void deletePublic(CrudObject<T> theObject)
      throws IoFault, RelationsExistFault;

  /**
   * <p>
   * Adds a key/value pair.
   * </p>
   * 
   * @param theUri
   * @param theKey
   * @param theValue
   * @throws IoFault
   */
  void addKeyValuePair(URI theUri, String theKey, Object theValue)
      throws IoFault;

  /**
   * <p>
   * Deletes an object's metadata FROM THE STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   */
  void deleteMetadata(CrudObject<T> theObject) throws IoFault;

  /**
   * <p>
   * Deletes an object's metadata FROM THE PUBLIC STORAGE.
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   */
  void deleteMetadataPublic(CrudObject<T> theObject) throws IoFault;

  /**
   * <p>
   * Returns the number of the latest revision.
   * </p>
   * 
   * @param theUri
   * @param unfiltered
   * @return The number of the latest revision.
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  int getLatestRevision(URI theUri, boolean unfiltered)
      throws IoFault, ObjectNotFoundFault;

  /**
   * <p>
   * Returns the number of the latest revision from the public repository.
   * </p>
   * 
   * @param theUri
   * @param unfiltered
   * @return The number of the latest public revision.
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  int getLatestRevisionPublic(URI theUri, boolean unfiltered)
      throws IoFault, ObjectNotFoundFault;

  /**
   * <p>
   * Moves an object from the non-public storage location to the public one.
   * </p>
   * 
   * @param theUri
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  void move(URI theUri) throws IoFault, ObjectNotFoundFault;

  /**
   * <p>
   * Copies an object from the non-public storage location to the public one.
   * </p>
   * 
   * @param theUri
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  void copy(URI theUri) throws IoFault, ObjectNotFoundFault;

  /**
   * <p>
   * Resolves URIs to URIs, e.g. Handle URI to TextGrid URI.
   * </p>
   * 
   * @param theUri
   * @return The resolved URI.
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  URI resolve(URI theUri) throws IoFault, ObjectNotFoundFault;

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @return The configuration.
   */
  CrudServiceConfigurator getConfiguration();

  /**
   * @param theConfiguration
   */
  void setConfiguration(CrudServiceConfigurator theConfiguration);

}
