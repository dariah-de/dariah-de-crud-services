/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2010-09-03 - Funk - Updated faults to CXF created types. Added ptotocolNotImplementedFault().
 * 
 * 2010-07-31 - Funk - Added service exception log. 2010-06-24 Funk First version.
 * 
 */

/**
 * <p>
 * The TGCrudServiceUtilities class provides some utilities, needed by all the TGCrudServie classes.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-02-10
 * @since 2010-02-26
 **/

public final class CrudServiceExceptions {

  // **
  // STATIC FINALS
  // **

  // Some error message strings.
  protected static final String CONFIGURATION_ERROR_MESSAGE = "TG-crud configuration error";

  // The fault numbers to return with a TextGridFaultException.
  public static final int FAULT_NO_AUTH = 1;
  public static final int FAULT_NO_IO = 2;
  public static final int FAULT_NO_OBJECTNOTFOUND = 3;
  public static final int FAULT_NO_METADATAPARSE = 4;
  public static final int FAULT_NO_RELATIONSEXIST = 5;
  public static final int FAULT_NO_UPDATECONFLICT = 6;
  public static final int FAULT_NO_PROTOCOLNOTIMPLEMENTED = 7;
  public static final int FAULT_NO_CONFIGURATION = 8;
  public static final int FAULT_NO_INTERNALSERVICE = 9;

  public static final String NO_CAUSE =
      "Sorry, no cause available, we apologise for the inconvenience!";

  // **
  // FAULT EXCEPTION BUILDERS
  // **

  /**
   * <p>
   * Creates a MetadataParseFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return A MetadataParseFault with the fault message set.
   */
  public static MetadataParseFault metadataParseFault(String theMessage) {
    return metadataParseFault(null, theMessage);
  }

  /**
   * <p>
   * Creates a MetadataParseFault and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return A MetadataParseFault with the fault message and cause set.
   */
  public static MetadataParseFault metadataParseFault(Exception theException, String theMessage) {

    MetadataParseFault fault =
        new MetadataParseFault(theMessage, getTextgridFaultType(FAULT_NO_METADATAPARSE, theMessage,
            theException, "MetadataParseFault"));
    fault.printStackTrace();

    return fault;
  }

  /**
   * <p>
   * Creates an AuthFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return An AuthFault with the fault message set.
   */
  public static AuthFault authFault(String theMessage) {
    return authFault(null, theMessage);
  }

  /**
   * <p>
   * Creates an AuthFault and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return An AuthFault with the fault message and cause set.
   */
  public static AuthFault authFault(Exception theException,
      String theMessage) {

    AuthFault fault = new AuthFault(theMessage,
        getTextgridFaultType(FAULT_NO_AUTH, theMessage, theException, "authFault"));
    fault.printStackTrace();

    return fault;
  }

  /**
   * <p>
   * Creates an IoFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return An IoFault with the fault message set.
   */
  public static IoFault ioFault(String theMessage) {
    return ioFault(null, theMessage);
  }

  /**
   * <p>
   * Creates an IoFaultException and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return An IoFaultException with the fault message and cause set.
   */
  public static IoFault ioFault(Exception theException, String theMessage) {

    IoFault iof = new IoFault(theMessage,
        getTextgridFaultType(FAULT_NO_IO, theMessage, theException, "IoFault"));
    iof.printStackTrace();

    return iof;
  }

  /**
   * <p>
   * Creates an UpdateConflictFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return An UpdateConflictFault with the fault message set.
   */
  public static UpdateConflictFault updateConflictFault(String theMessage) {
    return updateConflictFault(null, theMessage);
  }

  /**
   * <p>
   * Creates an UpdateConflictFault and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return An UpdateConflictFault with the fault message and cause set.
   */
  public static UpdateConflictFault updateConflictFault(Exception theException, String theMessage) {

    UpdateConflictFault fault =
        new UpdateConflictFault(theMessage, getTextgridFaultType(FAULT_NO_UPDATECONFLICT,
            theMessage, theException, "UpdateConflictFault"));
    fault.printStackTrace();

    return fault;
  }

  /**
   * <p>
   * Creates an ObjectNotFoundFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return An ObjectNotFoundFault with the fault message set.
   */
  public static ObjectNotFoundFault objectNotFoundFault(String theMessage) {
    return objectNotFoundFault(null, theMessage);
  }

  /**
   * <p>
   * Creates an ObjectNotFoundFault and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return An ObjectNotFoundFault with the fault message and cause set.
   */
  public static ObjectNotFoundFault objectNotFoundFault(Exception theException, String theMessage) {

    ObjectNotFoundFault fault =
        new ObjectNotFoundFault(theMessage, getTextgridFaultType(FAULT_NO_OBJECTNOTFOUND,
            theMessage, theException, "ObjectNotFoundFault"));
    fault.printStackTrace();

    return fault;
  }

  /**
   * <p>
   * Creates a RelationsExistFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return A RelationExistsFault with the fault message set.
   */
  public static RelationsExistFault relationsExistFault(String theMessage) {
    return relationsExistFault(null, theMessage);
  }

  /**
   * <p>
   * Creates a RelationsExistFault and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return A RelationExistsFault with the fault message and cause set.
   */
  public static RelationsExistFault relationsExistFault(Exception theException, String theMessage) {

    RelationsExistFault fault =
        new RelationsExistFault(theMessage, getTextgridFaultType(FAULT_NO_RELATIONSEXIST,
            theMessage, theException, "RelationsExistFault"));
    fault.printStackTrace();

    return fault;
  }

  /**
   * <p>
   * Creates a ProtocolNotImplementedFault and sets the given message.
   * </p>
   * 
   * @param theMessage
   * @return A ProtocolNotImplementedFault with the fault message set.
   */
  public static ProtocolNotImplementedFault protocolNotImplementedFault(
      String theMessage) {
    return protocolNotImplementedFault(null, theMessage);
  }

  /**
   * <p>
   * Creates a ProtocolNotImplementedFault and sets the given cause and message.
   * </p>
   * 
   * @param theException
   * @param theMessage
   * @return A ProtocolNotImplementedFault with the fault message and cause set.
   */
  public static ProtocolNotImplementedFault protocolNotImplementedFault(Exception theException,
      String theMessage) {

    ProtocolNotImplementedFault fault = new ProtocolNotImplementedFault(theMessage,
        getTextgridFaultType(FAULT_NO_PROTOCOLNOTIMPLEMENTED, theMessage, theException,
            "ProtocolNotImplementedFault"));
    fault.printStackTrace();

    return fault;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Creates a TextGridFaultType.
   * </p>
   * 
   * @param theFaultNo
   * @param theFaultMessage
   * @param theCause
   * @param theFaultName
   * @return A TextGrid fault type.
   */
  private static TextGridFaultType getTextgridFaultType(int theFaultNo, String theFaultMessage,
      Exception theCause, String theFaultName) {

    TextGridFaultType result = new TextGridFaultType();
    result.setFaultNo(theFaultNo);
    result.setFaultMessage(theFaultMessage + "!!");
    result.setCause((theCause == null ? NO_CAUSE : theCause.getMessage()));

    String message = CrudServiceUtilities.START_LOGCHARS + "TEXTGRID FAULT #" + result.getFaultNo()
        + " " + theFaultName + ": " + result.getFaultMessage() + " ["
        + (theCause != null ? theCause.getClass().getName() + ": " : "") + result.getCause() + "]";
    CrudServiceUtilities.serviceLog(CrudService.ERROR, theFaultName.toUpperCase(), message);

    return result;
  }

}
