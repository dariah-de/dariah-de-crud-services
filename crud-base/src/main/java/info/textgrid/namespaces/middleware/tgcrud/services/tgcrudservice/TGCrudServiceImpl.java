/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.transform.TransformerException;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Project;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.middleware.adaptormanager.AdaptorManager;
import info.textgrid.namespaces.middleware.tgauth_crud.TgCrudCheckAccessResponse;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.consistency.api.Consistency;
import jakarta.activation.DataHandler;
import jakarta.jws.WebService;
import jakarta.mail.util.ByteArrayDataSource;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;
import jakarta.xml.ws.Holder;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 * 2024-08-20 - Funk - Fix #349. Delete RDF data in case relation data is invalid. Re-adapt for EXIF
 * metadata creation (#updateMetadata).
 *
 * 2024-05-22 - Funk - Fix #340. Check RDF data from metadata before deleting from RDFDB.
 *
 * 2024-04-03 - Funk - Add metadata validation, create metadata warnings if errors occur.
 *
 * 2024-01-17 - Funk - Add new message producer.
 * 
 * 2023-10-05 - Funk - Add JAXRS annotations again.
 *
 * 2023-03-07 - Funk - Remove extent from READ headers... again...
 *
 * 2023-02-21 - Funk - Add extent to READ headers.
 *
 * 2022-05-25 - Funk - Add stream close to tgAggregationStreams in #CREATE and #UPDATE methods.
 * Fixes #277.
 *
 * 2022-05-24 - Funk - Fix some SpotBugs issues.
 *
 * 2022-02-10 - Funk - Refactor some logging methods.
 * 
 * 2021-06-21 - Funk - Add filenames for REST metadata responses.
 *
 * 2020-11-04 - Funk - Add internal locking for new revision creation.
 *
 * 2020-03-04 - Funk - Remove special header (done by Nginx now! :-)
 *
 * 2019-12-20 - Funk - Implemented re-cache method. A bit of reformatting.
 *
 * 2018-08-31 - Funk - Fixed exception mapping for RESTful HTML response messages.
 *
 * 2018-05-22 - Funk - Added abstract initMessageProducer class.
 *
 * 2015-05-13 - Funk - Added method n4n.
 *
 * 2014-09-10 - Funk - Moved all functionality to the CrudServiceGeneric class.
 **/

/**
 * <p>
 * <b>The TGCrudService implementation class</b>
 * </p>
 *
 * <p>
 * This class implements the TextGrid methods of the CrudServiceGeneric class using the REST API
 * interfaces.
 * </p>
 *
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-11-01
 * @since 2007-10-30
 **/

@WebService(portName = "TGCrudPort", serviceName = "TGCrudService",
    targetNamespace = "http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService",
    endpointInterface = "info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService")
public class TGCrudServiceImpl extends CrudService implements TGCrudService, TGCrudServiceREST {

  // **
  // FINALS
  // **

  private static final String METADATA_MIMETYPES = "*/xml";
  private static final String ORIGINAL_DATA_COULD_NOT_BE_STORED =
      "Original data could not be stored to the index database";
  private static final String PROJECTFILE_COULD_NOT_BE_STORED =
      "Projectfile could not be stored to the index database";
  private static final String PORTALCONFIG_COULD_NOT_BE_STORED =
      "Portalconfig could not be stored to the index database";

  // Overall error messages.
  private static final String STORED_AGGREGATION = "STORED AGGREGATION";
  private static final String STORED_ORIGINAL_DATA = "STORED ORIGINAL DATA";
  private static final String STORED_PROJECTFILE_DATA = "STORED PROJECTFILE DATA";
  private static final String STORED_PORTALCONFIG_DATA = "STORED PORTALCONFIG DATA";
  private static final String STORED_METADATA_DATA = "STORED METADATA/DATA";
  private static final String STORED_RELATION_DATA = "STORED RELATION DATA";
  private static final String REGISTERED_RESOURCE = "REGISTERED RESOURCE";
  private static final String NEARLY_PUBLISHED = "NEARLY PUBLISHED";
  private static final String ORIGINAL_DATA_COULD_NOT_BE_UPDATED =
      "Original data could not be updated to the index database";
  private static final String PROJECTFILE_COULD_NOT_BE_UPDATED =
      "Projectfile could not be updated to the index database";
  private static final String PORTALCONFIG_COULD_NOT_BE_UPDATED =
      "Portalconfig could not be updated to the index database";
  private static final String ADDING_SUBJECT_AGGREGATION_FAILED =
      "Adding subject aggregation to the aggregation file failed";
  private static final String HOLDING_DATA_STREAM_FAILED =
      "Holding data stream from TextGrid object data failed";
  private static final String UPDATE_DENIED = "Update denied";
  private static final String MOVATION_DENIED = "Movation denied";
  private static final String DELETION_DENIED = "Deletion denied";
  private static final String NEW_REVISION_CREATION_DENIED = "New revision creation denied";
  private static final String DATA_MISSING = "Data object missing in request";
  private static final String OBJECT_LOCKED = "Object is locked by another user";
  private static final String PROCESS_CONFLICT = "Object is locked by another process";
  private static final String PERMISSIONS_ADDED = "Permissions added to metadata response";
  private static final String STORED_METADATA = "STORED METADATA";
  private static final String DELETED_RELATION_DATA = "DELETED RELATION DATA";
  private static final String DELETED_DATA = "DELETED METADATA/ORIGINAL/AGGREGATION/BASELINE DATA";
  private static final String UNREGISTERED_RESOURCE = "UNREGISTERED RESOURCE";
  private static final String DELETED_METADATA_DATA = "DELETED METADATA/DATA";
  private static final String UPDATED_METADATA = "UPDATED METADATA";
  private static final String UPDATED_AGGREGATION = "UPDATED AGGREGATION";
  private static final String UPDATED_ORIGINAL_DATA = "UPDATED ORIGINAL DATA";
  private static final String UPDATED_PROJECTFILE_DATA = "UPDATED PROJECTFILE DATA";
  private static final String UPDATED_PORTALCONFIG_DATA = "UPDATED PORTALCONFIG DATA";
  private static final String REMOVED_OLD_RELATION_DATA = "REMOVED OLD RELATION DATA";
  private static final String STORED_NEW_RELATION_DATA = "STORED NEW RELATION DATA";
  private static final String UPDATED_METADATA_DATA = "UPDATED METADATA/DATA";
  private static final String CREATED_NEW_RELATION_DATA = "CREATED NEW RELATION DATA";
  private static final String MESSAGE_SENT = "MESSAGE SENT";

  // Rollback logging messages.
  private static final String COMPONENT_IDXDB = "IDX DATABASE";
  private static final String COMPONENT_RDFDB = "RDF DATABASE";
  private static final String COMPONENT_DATA_STORAGE = "DATA STORAGE";
  private static final String COMPONENT_AAI = "AAI";
  private static final String COMPONENT_MESSAGING = "MESSAGING";
  private static final String STARTED = "STARTED";
  private static final String COMPLETE = "COMPLETE";
  private static final String COMPLETE_WITH_ROLLBACK_UNCRITICAL_ERROR = "COMPLETE";
  private static final String NOT_COMPLETE = "NOT COMPLETE";

  // Directly Published Objects.
  private static final String DPO_AVAILIBILITY_PUBLIC = "public";
  private static final String DPO_PERMISSION_READ = "read";

  // Miscellaneous message strings.
  private static final String IDX_NEARLYPUBLISHED_KEY = "nearlyPublished";
  private static final String NOID_CHARACTERS = "0123456789bcdfghjkmnpqrstvwxz";

  // **
  // STATIC
  // **

  private static TGCrudServiceVersion tgcrudServiceVersion = new TGCrudServiceVersion();

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * CXF calls this constructor just once at service start time... we call init() with every method
   * call. The configuration file location is set in the beans.xml configuration file as constructor
   * argument.
   * </p>
   *
   * @param theConfLocation
   * @throws IoFault
   */
  public TGCrudServiceImpl(String theConfLocation) throws IoFault {

    // Set the services' version.
    new TGCrudServiceVersion();

    // TODO Check if we need a static reference for this configuration objects! Could be tested with
    // multiple TG-crud instances! Normally each TG-crud instance (if there were, we have got
    // singleton defined with CXF) should have it's own configuration, I suppose?
    this.configFileLocation = theConfLocation;

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.INIT);

    try {
      init(methodInfo, tgcrudServiceVersion);

      initImplementationClasses();

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
    }

    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
  }

  /**
   * <p>
   * The <b>#CREATE</b> method of the TG-crud service is used to create TextGrid objects and store
   * them <b>TO DATA STORAGE</b>. It does the following in the given order:
   *
   * <ul>
   * <li>Check if <b>publish</b> access is granted to the given RBAC session ID (ONLY if DIRECTLY
   * PUBLISH is set to TRUE!).</li>
   * <li>Check if <b>create</b> access is granted to the project resource using the given RBAC
   * session ID.</li>
   * <li>Compute the revision number to use.</li>
   * <li>Create new TextGrid URI if needed, check URI if given.</li>
   * <li>Generate the generated metadata type.</li>
   * <li>Get some public data out of the metadata (ONLY if DIRECTLY PUBLISH is set to TRUE!).</li>
   * <li>STORE AGREGATION, EDITION, or COLLECTION DATA to the IDX DATABASE.</li>
   * <li>STORE ORIGINAL XML DATA to the IDX DATABASE.</li>
   * <li>STORE PROJECTFILE DATA to the IDX DATABASE.</li>
   * <li>Call the AdaptorManager, process data if needed.</li>
   * <ul>
   * <li>If an aggregation object is ingested: Add the subject's URI to the aggregation ORE
   * file.</li>
   * <li>If an adaptor URI is existing in the metadata: Read the (baseline) adaptor XSLT file and
   * put the baseline encoding into the ES database.</li>
   * <li>Put the namespaces of XSD files into the RDF database.</li>
   * <li>Put the relations extracted with the AdaptorManager into the RDF database.</li>
   * <li>Add warnings to the metadata, if existing.</li>
   * </ul>
   * <li>STORE METADATA and DATA TO DATA STORAGE.</li>
   * <li>STORE RELATIONS to the RDF DATABASE.</li>
   * <li>STORE METADATA to the IDX DATABASE.</li>
   * <li>REGISTER RESOURCE to the TG-AUTH.</li>
   * <li>Set the isPublic flag in TG-AUTH (ONLY if DIRECTLY PUBLISH is set to TRUE!).</li>
   * <li>Add permissions to the metadata.</li>
   * <li>Return the complete metadata element.</li>
   * </ul>
   * </p>
   */
  @Override
  public void create(final String sessionId, final String logParameter, URI baseUri,
      Boolean createRevision, String projectId,
      final Holder<MetadataContainerType> tgObjectMetadata, DataHandler tgObjectData)
      throws MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.CREATE);

    // Create URI.
    URI uri = null;
    TgCrudCheckAccessResponse checkAccessResponse = null;
    boolean internalLockingSucceeded = false;
    try {
      init(methodInfo, tgcrudServiceVersion);

      // Check mandatory parameters in #CREATE: tgObjectMetadata, tgObjectData.
      if (tgObjectData == null) {
        throw CrudServiceExceptions.ioFault(DATA_MISSING);
      }

      MetadataContainerType metadata = tgObjectMetadata.value;
      // Check for <item>, <edition>, <collection>, and <work> tags. If not existing, just create
      // empty tags for valid metadata.
      TGCrudServiceUtilities.checkAndCorrectMissingTags(metadata);

      // Get and check metadata, include validation warnings from JAXB validation.
      // List<Warning> validationWarnings = TGCrudServiceUtilities.checkMetadata(metadata);
      // Get and check metadata, include validation warnings from JAXB and RDF validation.
      List<Warning> validationWarnings = TGCrudServiceUtilities.checkMetadata(metadata);
      List<Warning> rdfValidationWarnings =
          TGCrudServiceUtilities.checkRelationRDFMetadata(metadata);
      validationWarnings.addAll(rdfValidationWarnings);

      // Check revision.
      if (createRevision == null) {
        createRevision = false;
      }

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, baseUri,
          createRevision, projectId, null);
      TGCrudServiceUtilities.rblog(INFO, methodInfo, STARTED, CrudOperations.MAIN_STRING);

      // Check PUBLISH access permissions for the given session ID, if TG-crud is a public instance.
      if (conf.getDIRECTLYpUBLISHwITHcREATE()) {
        aaiImplementation.checkRight(sessionId, logParameter, projectId,
            CrudServiceAai.RIGHT_PUBLISH);
      }

      // Check general CREATE access permission using the session ID and the project ID, get back
      // some additional data from the RBAC.
      checkAccessResponse = (TgCrudCheckAccessResponse) aaiImplementation
          .checkAccessGetInfo(sessionId, logParameter, projectId, CrudServiceAai.OPERATION_CREATE);

      // Set revision to initial state.
      int revision = CrudServiceIdentifier.INITIAL_REVISION;

      // Check if a new revision shall be created.
      if (createRevision) {
        if (baseUri == null || baseUri.toString().equals("")) {
          throw CrudServiceExceptions.ioFault("No URI given");
        }

        // Get current latest revision and increase.
        revision = getLatestRevision(baseUri, methodInfo, LATEST_REVISION_UNFILTERED) + 1;

        // Get project ID from latest (filtered) URI's metadata.
        projectId = getProjectIDOfLatestMetadataFilteredNewRevision(baseUri, methodInfo);
      }

      // Create a new URI if none given.
      if (baseUri == null || baseUri.toASCIIString().equals("")) {
        baseUri = identifierImplementation.getUris(1).get(0);

        log(INFO, methodInfo, "TextGrid base URI created: " + baseUri.toASCIIString() + " ("
            + identifierImplementation.getClass().getSimpleName() + ")");
      }

      // Check and use the given URI.
      else {
        // At first, remove the revision, if existing in given URI.
        baseUri = TGCrudServiceUtilities.stripRevision(baseUri);

        // Check if a given (base) URI already is existing within the URI implementation.
        if (!identifierImplementation.uriExists(baseUri)) {
          throw CrudServiceExceptions
              .ioFault("The given URI '" + baseUri + "' was not created by the URI service");
        }
      }

      // Set URI from revision.
      uri = URI.create(
          baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);

      log(INFO, methodInfo, "Revision URI to be used (" + (createRevision ? "NEW" : "INITIAL")
          + " revision): " + uri.toASCIIString());

      // Do internal locking here if revision is not equal 0 (initial revision)! So the same
      // revision cannot be created simultaneously (see #33975)!
      if (revision != CrudServiceIdentifier.INITIAL_REVISION) {
        synchronized (this) {
          internalLockingSucceeded = identifierImplementation.lockInternal(uri);

          log(INFO, methodInfo, "Internal locking fur URI " + uri + " "
              + (internalLockingSucceeded ? "COMPLETE" : "FAILED"));

          if (!internalLockingSucceeded) {
            UpdateConflictFault fault = new UpdateConflictFault(PROCESS_CONFLICT + ": " + uri);
            throw CrudServiceExceptions.ioFault(fault, NEW_REVISION_CREATION_DENIED);
          }
        }
      }

      // Check if the URI is already existing in the security implementation.
      if (aaiImplementation.resourceExists(sessionId, logParameter, uri.toASCIIString())) {
        throw CrudServiceExceptions
            .ioFault("The URI '" + uri + "' already is in use and can not be used again");
      }

      // Set initial relations list.
      List<String> overallRelations = new ArrayList<String>();

      logRelations(methodInfo, uri);

      // Set time from system time.
      long mTime = System.currentTimeMillis();

      // Create and fill the generated metadata element (set size later).
      URI username = URI.create(checkAccessResponse.getUsername());
      String projectname = checkAccessResponse.getProject().getName();
      GeneratedType generated = TGCrudServiceUtilities.createGereratedType(mTime, mTime, uri,
          revision, projectId, 0l, username, projectname);

      // Add warnings from XML and RDF validation, if any!
      generated.getWarning().addAll(validationWarnings);

      log(DEBUG, methodInfo,
          "Created date from metadata:      "
              + CrudServiceUtilities.getCETDateString(generated.getCreated()) + " ("
              + generated.getCreated().toGregorianCalendar().getTimeInMillis() + ")");
      log(DEBUG, methodInfo,
          "LastModified date from metadata: "
              + CrudServiceUtilities.getCETDateString(generated.getLastModified()) + " ("
              + generated.getLastModified().toGregorianCalendar().getTimeInMillis() + ")");

      // Get all public data out of the metadata, if object shall directly be published, check if
      // everything is fine. Add PIDs, availability, issued date, and nearlyPublic flag. Add
      // permission information later.
      if (conf.getDIRECTLYpUBLISHwITHcREATE()) {

        log(INFO, methodInfo, "Current object is handled as a DPO - Directly Published Object");

        GeneratedType pGenerated = metadata.getObject().getGeneric().getGenerated();

        // Check for empty generated element.
        if (pGenerated != null) {
          // Check for PID, warn if no PID is existing.
          if (pGenerated != null
              && (pGenerated.getPid() == null || pGenerated.getPid().isEmpty())) {
            log(WARN, methodInfo, "DPO shall directly be published and has NO PIDs: " + uri);
          } else {
            // All PIDs from the client submitted generated type are taken over! This must be
            // checked by the client, if needed!
            generated.getPid().addAll(pGenerated.getPid());

            List<String> pidList = new ArrayList<String>();
            for (Pid p : pGenerated.getPid()) {
              pidList.add(p.getPidType() + ":" + p.getValue());
            }

            log(DEBUG, methodInfo, "PID list for DPO: " + pidList);
          }
        }

        // Set issued date from createdDate/lastModified date.
        generated.setIssued(generated.getLastModified());

        log(DEBUG, methodInfo, "Issued date for DPO: " + generated.getIssued());

        // Set availability to public.
        generated.setAvailability(DPO_AVAILIBILITY_PUBLIC);

        log(DEBUG, methodInfo, "Availability for DPO set to: " + generated.getAvailability());

        // Add nearlyPublished flag to relations.
        overallRelations.add(TGCrudServiceUtilities.assembleNearlyPublished(uri, conf));

        log(DEBUG, methodInfo, "Added isNearlyPublished relation to DPO");

        // Add nearlyPublished flag to Index database.
        idxdbImplementation.addKeyValuePair(uri, IDX_NEARLYPUBLISHED_KEY, true);

        log(DEBUG, methodInfo, "Added nearlyPublished entry to Index database");
      }

      // Set generated metadata element.
      metadata.getObject().getGeneric().setGenerated(generated);

      log(INFO, methodInfo, "Generated metadata element added");

      // Get the data's format out of the metadata.
      String format = metadata.getObject().getGeneric().getProvided().getFormat();

      // Hold the temporary file until we release it again, so we can access the data stream over
      // and over again: nice!
      try {
        CrudServiceHoldSlipStream.lock(tgObjectData);

        log(DEBUG, methodInfo, HOLDING_DATA_STREAM);

      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, HOLDING_DATA_STREAM_FAILED);
      }

      /**
       * STORE AGGREGATIONS TO IDX DATABASE
       */

      InputStream tgAggregationStream = null;
      if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
        try {
          URI aboutUri = uri;

          // Do call the AdaptorManager to add the URI to the about attribute of the aggregation
          // file, the PID if TG-crud public is used, and a PID is existing.
          tgAggregationStream =
              AdaptorManager.insertAggregationUri(tgObjectData.getInputStream(), aboutUri);

          log(DEBUG, methodInfo, "Created AdaptorManager aggregation stream");

          // Re-create a DataHandler from the AdaptorManager's input stream. Seems not to interfere
          // with our stream holding...
          tgObjectData = new DataHandler(
              new ByteArrayDataSource(tgAggregationStream, TextGridMimetypes.OCTET_STREAM));

          log(DEBUG, methodInfo,
              "Added the aggregation file's URI to the aggregation's about attribtue: " + aboutUri);

          CrudObject<MetadataContainerType> xmlObject =
              new TGCrudTextGridObject(uri, tgObjectData, CrudObject.AGGREGATION_DATA);

          idxdbImplementation.create(xmlObject);

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_AGGREGATION, uri, COMPONENT_IDXDB);

        } catch (TransformerException | IOException | IoFault e) {
          throw CrudServiceExceptions.ioFault(e, ADDING_SUBJECT_AGGREGATION_FAILED);
        } finally {
          try {
            if (tgAggregationStream != null) {
              tgAggregationStream.close();

              log(DEBUG, methodInfo, "Closed AdaptorManager aggregation stream");
            }
          } catch (IOException e) {
            throw new IoFault(e.getMessage(), e);
          }
        }
      }

      /**
       * STORE ORIGINAL DATA | PROJECTFILE DATA | PORTALCONFIG DATA TO IDX DATABASE
       */

      try {
        if (TextGridMimetypes.ORIGINAL_SET.contains(format)) {
          idxdbImplementation
              .create(new TGCrudTextGridObject(metadata, tgObjectData, CrudObject.ORIGINAL_DATA));

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_ORIGINAL_DATA, uri,
              COMPONENT_IDXDB);
        }

        else if (TextGridMimetypes.PORTALCONFIG.equals(format)) {
          idxdbImplementation.create(
              new TGCrudTextGridObject(metadata, tgObjectData, CrudObject.PORTALCONFIG_DATA));

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_PORTALCONFIG_DATA, uri,
              COMPONENT_IDXDB);
        }

        else if (TextGridMimetypes.PROJECTFILE.equals(format)) {
          idxdbImplementation.create(
              new TGCrudTextGridObject(metadata, tgObjectData, CrudObject.PROJECTFILE_DATA));

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_PROJECTFILE_DATA, uri,
              COMPONENT_IDXDB);
        }

      } catch (IoFault e) {
        // Only do warn if we get a TransformerException, throw the exception otherwise. Identify
        // this by public static final error literal from ES storage class.
        if (e.getMessage()
            .equals(TGCrudServiceStorageElasticSearchImpl.ERROR_STORING_ORIGINAL_DATA)) {
          TGCrudServiceUtilities.addWarning(metadata, e.getMessage());
          log(WARN, methodInfo, ORIGINAL_DATA_COULD_NOT_BE_STORED);
        } else if (e.getMessage()
            .equals(TGCrudServiceStorageElasticSearchImpl.ERROR_STORING_PROJECTFILE_DATA)) {
          TGCrudServiceUtilities.addWarning(metadata, e.getMessage());
          log(WARN, methodInfo, PROJECTFILE_COULD_NOT_BE_STORED);
        } else if (e.getMessage()
            .equals(TGCrudServiceStorageElasticSearchImpl.ERROR_STORING_PORTALCONFIG_DATA)) {
          TGCrudServiceUtilities.addWarning(metadata, e.getMessage());
          log(WARN, methodInfo, PORTALCONFIG_COULD_NOT_BE_STORED);
        } else {
          throw e;
        }
      }

      // Call the AdaptorManager, if object data is not empty. Gather relations, any warnings are
      // added directly to the metadata object.
      List<String> relationsFromAdaptorManager = TGCrudServiceAdaptorManager.go(sessionId,
          logParameter, uri, new TGCrudTextGridObject(metadata, tgObjectData), rdfdbImplementation,
          dataStorageImplementation, idxdbImplementation, aaiImplementation, conf,
          CrudOperations.CREATE);
      overallRelations.addAll(relationsFromAdaptorManager);

      /**
       * STORE METADATA / DATA TO DATA STORAGE
       */

      dataStorageImplementation.create(new TGCrudTextGridObject(metadata, tgObjectData));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_METADATA_DATA, uri,
          COMPONENT_DATA_STORAGE);

      // Add all the other relations from the relational metadata, including RDF type, and some
      // special metadata to the relations gathered by the AdaptorManager.
      overallRelations.addAll(TGCrudServiceUtilities.assembleRelationsForCreation(uri,
          conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadata));

      /**
       * STORE RELATIONS TO RDF DATABASE
       */

      if (!overallRelations.isEmpty()) {
        CrudObject<MetadataContainerType> rdfObject =
            new TGCrudTextGridObject(uri, overallRelations);
        rdfdbImplementation.create(rdfObject);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_RELATION_DATA, uri, COMPONENT_RDFDB);
      }

      logRelations(methodInfo, uri);
      logWarnings(methodInfo, metadata);
      logMetadata(methodInfo, metadata);

      /**
       * STORE METADATA TO IDX DATABASE
       */

      idxdbImplementation.createMetadata(new TGCrudTextGridObject(metadata));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_METADATA, uri, COMPONENT_IDXDB);

      /**
       * REGISTER RESOURCE AT TG-AUTH
       */

      List<String> permissions =
          aaiImplementation.registerResource(sessionId, logParameter, projectId, uri);

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, REGISTERED_RESOURCE, uri, COMPONENT_AAI);

      /**
       * SET ISPUBLIC FLAG (NEARLY PUBLISH ONLY) AT TG-AUTH
       */

      if (conf.getDIRECTLYpUBLISHwITHcREATE()) {
        try {
          aaiImplementation.nearlyPublish(sessionId, logParameter, uri);
        } catch (IoFault e) {
          // TODO Do more than logging here? Look into nearlyPublish() implementation!
          log(ERROR, methodInfo, "Got failure from nearlyPublish: " + e.getMessage());
          throw e;
        }

        // Set permissions to "read" only. We don't want to update the metadata again after nearly
        // publishing!
        permissions.clear();
        permissions.add(DPO_PERMISSION_READ);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, NEARLY_PUBLISHED, uri, COMPONENT_AAI);
      }

      /**
       * SEND #CREATE MESSAGE WITH MESSAGING SYSTEM
       */

      if (conf.getUSEmESSAGING()) {
        CrudServiceMessage message = new CrudServiceMessage()
            .setOperation(CrudOperations.CREATE)
            .setObjectId(uri)
            .setRootId(projectId)
            .setFormat(metadata.getObject().getGeneric().getProvided().getFormat())
            .setPublic(isPublic(metadata.getObject()));
        messageProducer.sendMessage(conf, message);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, MESSAGE_SENT, uri, COMPONENT_MESSAGING);
      }

      // Add permissions to the middleware element.
      addPermissions(metadata, permissions);

      log(INFO, methodInfo, PERMISSIONS_ADDED);
      logMetadata(methodInfo, metadata);
      logRelations(methodInfo, uri);

      // Fill the return holder with the created metadata object.
      tgObjectMetadata.value = metadata;
    }

    // Check error messages for rollback logging, catch non-critical errors and log rollback end
    // log.
    catch (MetadataParseFault | ObjectNotFoundFault | AuthFault e) {
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE_WITH_ROLLBACK_UNCRITICAL_ERROR, uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch IoFaults and log rollback error.
    catch (IoFault e) {
      TGCrudServiceUtilities.rbLog(ERROR, methodInfo,
          NOT_COMPLETE + ": " + e.getClass().getSimpleName() + ": " + e.getMessage(), uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
    // Release data stream hold to delete the cached data file.
    finally {
      synchronized (CrudService.class) {
        // If internal locking succeeded, release internal lock.
        if (internalLockingSucceeded && checkAccessResponse != null) {
          identifierImplementation.unlockInternal(uri);
        }
      }

      CrudServiceHoldSlipStream.releaseAndClose(tgObjectData);

      log(DEBUG, methodInfo, STREAM_RELEASE_COMPLETE);
    }

    TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE, uri, CrudOperations.MAIN_STRING);

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
  }

  /**
   * <p>
   * Do a #CREATE via REST.
   * </p>
   */
  @Override
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/create")
  public Response createREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @QueryParam(PARAM_URI) URI baseUri,
      final @QueryParam(PARAM_CREATE_REVISION) Boolean createRevision,
      final @QueryParam(PARAM_PROJECT_ID) String projectId,
      final @Multipart(value = PARAM_TGOBJECT_METADATA,
          type = METADATA_MIMETYPES) MetadataContainerType tgObjectMetadata,
      final @Multipart(value = PARAM_TGOBJECT_DATA) DataHandler tgObjectData) {

    try {
      // Create metadata holder.
      Holder<MetadataContainerType> metadata = new Holder<MetadataContainerType>(
          tgObjectMetadata);

      // Call TG-crud#CREATE and assemble response.
      this.create(sessionId, logParameter, baseUri, createRevision, projectId, metadata,
          tgObjectData);

      // Get filename from metadata.
      String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata.value)
          + conf.getMETADATAfILEsUFFIX();

      // Build a response.
      ResponseBuilder rBuilder =
          Response.ok(metadata.value).header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML)
              .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + filename + "\"");

      // Set last modified date for client caching reasons, and location.
      Date lastModified = new Date(metadata.value.getObject().getGeneric().getGenerated()
          .getLastModified().toGregorianCalendar().getTimeInMillis());
      rBuilder.lastModified(lastModified);
      URI location =
          metadata.value.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      rBuilder.location(location);

      return rBuilder.build();

    } catch (MetadataParseFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * <b>TG-crud service method #CREATEMETADATA</b>
   * </p>
   *
   * <p>
   * The <b>#CREATEMETADATA</b> allows to create TextGrid objects, that consists of TextGrid
   * metadata and a reference to the object only. TG-crud is able to deliver that object, but only
   * if it is available freely over HTTP. Furthermore, it does the following in the given order:
   *
   * <ul>
   * <li>TODO TO BE DONE!!</li>
   * </ul>
   * </p>
   */
  @Override
  public void createMetadata(final String sessionId, final String logParameter, final URI baseUri,
      final String projectId, final URI externalReference,
      final Holder<MetadataContainerType> tgObjectMetadata)
      throws MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {

    // TODO Think about revision handling! What to do with base URIs here?
    // TODO Can we update objects with external references at all?
    // TODO DO ALL THE REST!

    throw CrudServiceExceptions
        .ioFault("TG-CRUD#" + CrudOperations.CREATEMETADATA_STRING + " NOT IMPLEMENTED YET");
  }

  /**
   * <p>
   * Do a #CREATEMETADATA via REST.
   * </p>
   */
  @Override
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/createMetadata")
  public Response createMetadataREST(final String sessionId, final String logParameter,
      final URI baseUri, final String projectId, final URI externalReference,
      final Holder<MetadataContainerType> tgObjectMetadata) {

    try {
      this.createMetadata(sessionId, logParameter, baseUri, projectId, externalReference,
          tgObjectMetadata);

      // Get filename from metadata.
      String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(tgObjectMetadata.value);

      // Build a response.
      ResponseBuilder rBuilder =
          Response.ok(tgObjectMetadata.value).header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML)
              .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + filename + "\"");

      // Set last modified date for client caching reasons, and location.
      Date lastModified = new Date(tgObjectMetadata.value.getObject().getGeneric().getGenerated()
          .getLastModified().toGregorianCalendar().getTimeInMillis());
      rBuilder.lastModified(lastModified);
      URI location = tgObjectMetadata.value.getObject().getGeneric().getGenerated().getTextgridUri()
          .getValue();
      rBuilder.location(location);

      return rBuilder.build();

    } catch (IoFault | MetadataParseFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * The <b>#DELETE</b> method of the TG-crud service processes the following in the given order:
   *
   * <ul>
   * <li>In non-public state: Add <deleted> relation to the RDF DATABASE.</li>
   * <li>In public state: Really remove all relations from the the RDF DATABASE.</li> *
   * <li>Delete metadata, original, aggregation, and baseline data from the INDEX DATABASE.</li>
   * <li>Unregister from AAI.</li>
   * <li>Delete metadata and data from the DATA STORAGE.</li>
   * </ul>
   * </p>
   */
  @Override
  public void delete(final String sessionId, final String logParameter, final URI baseUri)
      throws ObjectNotFoundFault, RelationsExistFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.DELETE);

    // Create uri from base URI.
    URI uri = baseUri;

    boolean internalLockingSucceeded = false;
    try {
      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, baseUri);

      // Get revision URI if base URI is given.
      if (TGCrudServiceUtilities.isBaseUri(baseUri)) {
        // For creation we need filtered latest revision, because we want to delete the latest
        // existing object.
        int revision = rdfdbImplementation.getLatestRevision(baseUri, LATEST_REVISION_FILTERED);
        uri = URI.create(
            baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);

        log(INFO, methodInfo, "Base URI '" + baseUri + "' resolved to latest revision URI: " + uri);
      }

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, STARTED, uri, CrudOperations.MAIN_STRING);

      // Check DELETE access permission.
      boolean objectNotFound;
      try {
        aaiImplementation.checkAccess(sessionId, logParameter, uri.toASCIIString(),
            CrudServiceAai.OPERATION_DELETE);

        objectNotFound = false;
      }
      // If object is not found, do delete everything except the data and metadata ON THE DATA
      // STORAGE! We do assume that TG-auth MUST know every TextGrid object! So we may delete
      // unknown objects.
      catch (ObjectNotFoundFault e) {

        log(WARN, methodInfo, "Object is not known to the TG-auth! Ignoring ObjectNotFoundFault: "
            + e.getFaultInfo().getCause());

        objectNotFound = true;
      }

      // Lock the given URI internally. Only administrators can do a #DELETE, so we only need
      // internal locking for a clean behaviour.
      synchronized (CrudService.class) {
        internalLockingSucceeded = identifierImplementation.lockInternal(uri);

        log(INFO, methodInfo, "Internal locking fur URI " + uri + " "
            + (internalLockingSucceeded ? "COMPLETE" : "FAILED"));

        if (!internalLockingSucceeded) {
          IoFault fault = new IoFault(PROCESS_CONFLICT + ": " + uri);
          throw CrudServiceExceptions.ioFault(fault, DELETION_DENIED);
        }
      }

      /**
       * DELETE RELATIONS FROM RDF DATABASE
       */

      logRelations(methodInfo, uri);

      try {
        // If TG-crud is in NON-PUBLIC state, just add the "deleted" relation using the delete()
        // method.
        if (!conf.getDIRECTLYpUBLISHwITHcREATE()) {
          // Assemble all relations to be kept, and add the "deleted" relation, then call delete().
          // We only need to keep inProject, isDerivedFrom, and of cause isDeleted!
          CrudObject<MetadataContainerType> rdfObject = new TGCrudTextGridObject(uri,
              TGCrudServiceUtilities.assembleDeletedRelation(uri, conf));
          rdfdbImplementation.delete(rdfObject);
        }

        // If TG-crud is in PUBLIC state, REALLY delete all the relations from the RDF database
        // using the deleteMetadata() relation.
        else {
          // Delete everything for the given URI (the default TGCrudTextGridObject kind of data is
          // not AGGREGATION_DATA, so we have not to set it here).
          rdfdbImplementation.deletePublic(new TGCrudTextGridObject(uri));
        }
      } catch (IoFault e) {
        // Just log errors here for robust deletion.
        log(WARN, methodInfo,
            "Ignoring IoFault while deleting from RDF database: " + e.getFaultInfo().getCause());
      }

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, DELETED_RELATION_DATA, uri, COMPONENT_RDFDB);

      logRelations(methodInfo, uri);

      /**
       * DELETE METADATA/ORIGINAL/AGGREGATION/BASELINE DATA FROM INDEX DATABASE
       */

      try {
        idxdbImplementation.delete(new TGCrudTextGridObject(uri));
      } catch (IoFault e) {
        // Just log errors here for robust deletion.
        log(WARN, methodInfo,
            "Ignoring IoFault while deleting from ES database: " + e.getFaultInfo().getCause());
      }

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, DELETED_DATA, uri, COMPONENT_IDXDB);

      /**
       * UNREGISTER AT TG-AUTH
       */

      // Only do unregister if object was found in the first place! We would get another
      // ObjectNotFoundFault otherwise!
      if (!objectNotFound) {
        aaiImplementation.unregisterResource(sessionId, logParameter, uri);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, UNREGISTERED_RESOURCE, uri, COMPONENT_AAI);
      } else {
        log(WARN, methodInfo,
            "Object will not be unregistered, because we didn't find it in the first place!");
      }

      /**
       * SEND #DELETE MESSAGE WITH MESSAGING SYSTEM
       */

      if (!objectNotFound) {
        // Get metadata from the storage for retrieving projectId, format, and isPublic flag for
        // messaging, if so configured.
        ObjectType storageMetadata = dataStorageImplementation.retrieveMetadata(uri).getObject();

        if (conf.getUSEmESSAGING()) {
          CrudServiceMessage message = new CrudServiceMessage()
              .setOperation(CrudOperations.DELETE)
              .setObjectId(uri)
              .setRootId(storageMetadata.getGeneric().getGenerated().getProject().getId())
              .setFormat(storageMetadata.getGeneric().getProvided().getFormat())
              .setPublic(isPublic(storageMetadata));
          messageProducer.sendMessage(conf, message);

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, MESSAGE_SENT, uri, COMPONENT_MESSAGING);
        }
      }

      /**
       * DELETE METADATA/DATA FROM THE DATA STORAGE
       */

      // Only delete if object was found! We do keep data and metadata otherwise. See above!
      if (!objectNotFound) {
        try {
          dataStorageImplementation.delete(new TGCrudTextGridObject(uri));
        } catch (IoFault e) {
          // Just log errors here for robust deletion.
          log(WARN, methodInfo, "Ignoring IoFault while deleting FROM THE DATA STORAGE: "
              + e.getFaultInfo().getCause());
        }
      } else {
        log(WARN, methodInfo,
            "Object will not be deleted FROM THE DATA STORAGE due to security reasons!");
      }

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, DELETED_METADATA_DATA, uri,
          COMPONENT_DATA_STORAGE);
    }

    // Check error messages for rollback logging, catch non-critical errors and log rollback end
    // log.
    catch (ObjectNotFoundFault | RelationsExistFault | AuthFault e) {
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE_WITH_ROLLBACK_UNCRITICAL_ERROR, uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch IoFaults and log rollback error.
    catch (IoFault e) {
      TGCrudServiceUtilities.rbLog(ERROR, methodInfo,
          NOT_COMPLETE + ": " + e.getClass().getSimpleName() + ": " + e.getMessage(), uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
    // If internal locking succeeded, release internal lock.
    finally {
      if (internalLockingSucceeded) {
        identifierImplementation.unlockInternal(uri);
      }
    }

    TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE, uri, CrudOperations.MAIN_STRING);
    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
  }

  /**
   * <p>
   * The <b>#READ</b> method of the TG-crud service reads TextGrid objects (including metadata)
   * <b>FROM THE DATA STORAGE</b>, and does the following in the given order:
   *
   * <ul>
   * <li>Check if <b>read</b> access is granted to the given URI using the given RBAC session
   * ID.</li>
   * <li>Read the metadata and data files <b>FROM THE DATA STORAGE</b>.</li>
   * <li>Fill <code>dataContributor</code> and <code>permissions</code> tags with the information
   * provided by the checkAccess query.</li>
   * <li>Return the complete data and metadata elements.</li>
   * </ul>
   * </p>
   */
  @Override
  public void read(final String sessionId, final String logParameter, URI baseUri,
      final Holder<MetadataContainerType> tgObjectMetadata, final Holder<DataHandler> tgObjectdata)
      throws ObjectNotFoundFault, MetadataParseFault, IoFault, ProtocolNotImplementedFault,
      AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, baseUri);

      // Check if we can resolve a given PID.
      if (baseUri.toString().startsWith(HANDLE_PREFIX)) {
        baseUri = rdfdbImplementation.resolve(baseUri);
      }

      // Get revision URI if base URI is given.
      URI uri = baseUri;
      if (TGCrudServiceUtilities.isBaseUri(baseUri)) {
        // Look for revisions in public RDF database, if not existing in non-public.
        try {
          // For reading we need filtered latest revision, because we want to read the latest
          // existing object.
          int revision = rdfdbImplementation.getLatestRevision(baseUri, LATEST_REVISION_FILTERED);
          uri = URI.create(
              baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);
        } catch (ObjectNotFoundFault e) {
          // For reading we need filtered latest revision, because we want to read the latest
          // existing object.
          int revision =
              rdfdbImplementation.getLatestRevisionPublic(baseUri, LATEST_REVISION_FILTERED);
          uri = URI.create(baseUri.toASCIIString()
              + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);
        }

        log(INFO, methodInfo, "Base URI '" + baseUri + "' resolved to latest revision URI: " + uri);
      }

      // Check general READ access permission using the session ID and the given URI, get back some
      // additional data from the RBAC.
      TgCrudCheckAccessResponse checkAccessResponse;
      try {
        checkAccessResponse =
            (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId,
                logParameter, uri.toASCIIString(), CrudServiceAai.OPERATION_READ);
      }

      // If access is denied, try reading the latest public revision, but only if we have a base URI
      // as request parameter. Check access again!
      catch (AuthFault e) {
        if (TGCrudServiceUtilities.isBaseUri(baseUri)) {
          // For reading we need filtered latest revision, because we want to read the latest
          // existing object.
          int revision =
              rdfdbImplementation.getLatestRevisionPublic(baseUri, LATEST_REVISION_FILTERED);
          uri = URI.create(baseUri.toASCIIString()
              + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);

          log(INFO, methodInfo,
              "Base URI '" + baseUri + "' resolved to latest public revision URI: " + uri);

          checkAccessResponse =
              (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId,
                  logParameter, uri.toASCIIString(), CrudServiceAai.OPERATION_READ);
        } else {
          throw e;
        }
      }

      log(DEBUG, methodInfo, uri + " is " + (checkAccessResponse.isPublic() ? PUBLIC : NON_PUBLIC));

      /**
       * <p>
       * RETRIEVE METADATA/DATA FROM THE DATA STORAGE
       * </p>
       */

      CrudObject<MetadataContainerType> tgObject = null;
      if (checkAccessResponse.isPublic()) {
        tgObject = dataStorageImplementation.retrievePublic(uri);
      } else {
        tgObject = dataStorageImplementation.retrieve(uri);
      }

      log(INFO, methodInfo, "Successfully retrieved TextGrid object FROM THE DATA STORAGE");

      // Add permissions to the middleware element.
      addPermissions(tgObject.getMetadata(), checkAccessResponse.getOperation());

      log(INFO, methodInfo, PERMISSIONS_ADDED);

      // Set metadata and data.
      tgObjectMetadata.value = tgObject.getMetadata();
      tgObjectdata.value = tgObject.getData();

    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
  }

  /**
   * <p>
   * Do a #DELETED via REST.
   * </p>
   */
  @Override
  @GET
  @Path("/{uri}/delete")
  public void deleteREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI baseUri) {
    try {
      this.delete(sessionId, logParameter, baseUri);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (RelationsExistFault | IoFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * <b>#GETURI</b> returns as many TextGrid URIs as requested by the user, as long the user has
   * <b>create</b> permissions with the given sessionID (the given projectId is deprecated and will
   * be ignored).
   * </p>
   */
  @Override
  public List<URI> getUri(final String sessionId, final String logParameter, final String projectId,
      final int howMany) throws ObjectNotFoundFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.GETURI);

    List<URI> uris = null;
    try {
      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, howMany);

      // Check for positive amount.
      if (howMany < 1) {
        throw CrudServiceExceptions.ioFault("You cannot get less than one ID! Guess why?");
      }

      // Check if the user is allowed to get TextGrid IDs.
      aaiImplementation.checkRight(sessionId, logParameter, "", CrudServiceAai.RIGHT_GETURI);

      // Get the URIs.
      uris = identifierImplementation.getUris(howMany);

      log(INFO, methodInfo,
          "Successfully created " + howMany + " URI"
              + (howMany != 1 ? "s : " + uris.get(0) + URI_SEPARATOR + uris.get(uris.size() - 1)
                  : " : " + uris.get(0)));
    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return uris;
  }

  /**
   * <p>
   * Do a #GETURI via REST.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getUri")
  public Response getUriREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @QueryParam(PARAM_HOW_MANY) int howMany) {

    // Call TG-crud#GETURI and assemble response.
    try {
      List<URI> uriList = this.getUri(sessionId, logParameter, "",
          howMany);

      StringBuffer uriBuffer = new StringBuffer();
      for (URI uri : uriList) {
        uriBuffer.append(uri);
        uriBuffer.append("\n");
      }

      ResponseBuilder rBuilder = Response.ok(uriBuffer.toString());

      return rBuilder.build();

    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * Do a #GETVERSION via REST.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/version")
  public String getVersionREST() {
    return this.getVersion();
  }

  /**
   * <p>
   * Do #GETVERSION.
   * </p>
   */
  @Override
  public String getVersion() {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.GETVERSION);

    log(DEBUG, methodInfo,
        CrudServiceUtilities.getVersionMethodLog(methodInfo, tgcrudServiceVersion));

    return tgcrudServiceVersion.getFULLVERSION();
  }

  /**
   * <p>
   * The <b>#READMETADATA</b> method of the TG-crud service reads the metadata of a TextGrid object
   * (metadata only) <b>FROM THE DATA STORAGE</b>, and does the following in the given order:
   *
   * <ul>
   * <li>Check if <b>read</b> access is granted to the given URI using the given RBAC session
   * ID.</li>
   * <li>Read the metadata file <b>FROM THE DATA STORAGE</b>.</li>
   * <li>Fill <code>dataContributor</code> and <code>permissions</code> tags with the information
   * provided by the checkAccess query.</li>
   * <li>Return the complete metadata element.</li>
   * </ul>
   * </p>
   */
  @Override
  public MetadataContainerType readMetadata(final String sessionId, final String logParameter,
      URI baseUri) throws ObjectNotFoundFault, MetadataParseFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    MetadataContainerType metadata = null;
    try {
      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, baseUri);

      // Check if we can resolve a given PID.
      if (baseUri.toString().startsWith(HANDLE_PREFIX)) {
        baseUri = rdfdbImplementation.resolve(baseUri);
      }

      // Get revision URI if base URI is given.
      URI uri = baseUri;
      if (TGCrudServiceUtilities.isBaseUri(baseUri)) {
        // Look for revisions in public RDF database, if not existing in non-public.
        try {
          // For reading we need filtered latest revision, because we want to read the latest
          // existing object.
          int revision = rdfdbImplementation.getLatestRevision(baseUri, LATEST_REVISION_FILTERED);
          uri = URI.create(
              baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);
        } catch (ObjectNotFoundFault e) {
          // For reading we need filtered latest revision, because we want to read the latest
          // existing object.
          int revision =
              rdfdbImplementation.getLatestRevisionPublic(baseUri, LATEST_REVISION_FILTERED);
          uri = URI.create(
              baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);
        }

        log(INFO, methodInfo, "Base URI '" + baseUri + "' resolved to latest revision URI: " + uri);
      }

      // Check general READ access permission using the session ID and the given URI, get back some
      // additional data from the RBAC.
      TgCrudCheckAccessResponse checkAccessResponse = null;
      try {
        checkAccessResponse =
            (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId,
                logParameter, uri.toASCIIString(), CrudServiceAai.OPERATION_READ);
      }

      // If access is denied, try reading the latest public revision, but only if we have a base URI
      // as request parameter. Check access again!
      catch (AuthFault e) {
        if (TGCrudServiceUtilities.isBaseUri(baseUri)) {
          // TODO Catch ObjectNotFoundFault here, it is catched as RuntimeException further down
          // instead! Then we get two exceptions: The AuthFault AND the ObjectNotFoundFault!

          // For reading we need filtered latest revision, because we want to read the latest
          // existing object.
          int revision =
              rdfdbImplementation.getLatestRevisionPublic(baseUri, LATEST_REVISION_FILTERED);
          uri = URI.create(
              baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);

          log(INFO, methodInfo,
              "Base URI '" + baseUri + "' resolved to latest public revision URI: " + uri);

          checkAccessResponse =
              (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId,
                  logParameter, uri.toASCIIString(), CrudServiceAai.OPERATION_READ);
        } else {
          throw e;
        }
      }

      log(DEBUG, methodInfo, uri + " is " + (checkAccessResponse.isPublic() ? PUBLIC : NON_PUBLIC));

      /**
       * <p>
       * RETRIEVE METADATA FROM THE DATA STORAGE
       * </p>
       */

      if (checkAccessResponse.isPublic()) {
        metadata = dataStorageImplementation.retrieveMetadataPublic(uri);
      } else {
        metadata = dataStorageImplementation.retrieveMetadata(uri);
      }

      log(INFO, methodInfo, "Successfully retrieved TextGrid metadata FROM THE DATA STORAGE");

      // Add permissions to the middleware element.
      addPermissions(metadata, checkAccessResponse.getOperation());

      log(INFO, methodInfo, PERMISSIONS_ADDED);

    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return metadata;
  }

  /**
   * <p>
   * Do a #READ via REST.
   * </p>
   */
  @Override
  @GET
  @Path("/{uri: .+}/data")
  public Response readREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI baseUri) {

    // Create data and metadata holders.
    Holder<MetadataContainerType> metadata = new Holder<MetadataContainerType>();
    Holder<DataHandler> data = new Holder<DataHandler>();

    // Call TG-crud#READ and assemble response.
    // NOTE We need to read the data AND metadata here, because we do need mimetype, lastModified
    // date, and extent from the metadata for correct and practicable binary HTTP delivery.
    try {
      this.read(sessionId, logParameter, baseUri, metadata, data);

      // Get filename from metadata.
      String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata.value);
      // Get mimetype and extent from metadata.
      String mimetype = metadata.value.getObject().getGeneric().getProvided().getFormat();
      // NOTE Maybe use extent for content-length HTTP header usage? Please see
      // <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/306>.
      // String extent =
      // metadata.value.getObject().getGeneric().getGenerated().getExtent().toString();

      // Build a response.
      ResponseBuilder rBuilder = Response.ok(data.value.getInputStream(), mimetype)
          .header(HttpHeaders.CONTENT_TYPE, MediaType.valueOf(mimetype))
          // .header(HttpHeaders.CONTENT_LENGTH, extent)
          .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + filename + "\"");

      // Set last modified date for client caching reasons.
      Date lastModified = new Date(metadata.value.getObject().getGeneric().getGenerated()
          .getLastModified().toGregorianCalendar().getTimeInMillis());
      rBuilder.lastModified(lastModified);

      return rBuilder.build();

    } catch (MetadataParseFault | ProtocolNotImplementedFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (IoFault | IOException e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * Do a #READMETADATA via REST.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri: .+}/metadata")
  public Response readMetadataREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI baseUri) {

    try {
      // Get metadata object.
      Holder<MetadataContainerType> metadata =
          new Holder<MetadataContainerType>(this.readMetadata(sessionId, logParameter, baseUri));

      // Get filename from metadata.
      String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata.value)
          + conf.getMETADATAfILEsUFFIX();

      // Build a response.
      ResponseBuilder rBuilder =
          Response.ok(metadata.value).header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML)
              .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + filename + "\"");

      // Set last modified date for client caching reasons.
      Date lastModified = new Date(metadata.value.getObject().getGeneric().getGenerated()
          .getLastModified().toGregorianCalendar().getTimeInMillis());
      rBuilder.lastModified(lastModified);

      return rBuilder.build();

    } catch (MetadataParseFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * Gets the TECHMD string FROM THE (public!) STORAGE.
   * </p>
   *
   * @param baseUri
   * @return
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  public String readTechmd(URI baseUri) throws IoFault, ObjectNotFoundFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READTECHMD);

    String techmd = "";
    try {
      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, baseUri);

      // Check if we can resolve a given PID.
      if (baseUri.toString().startsWith(HANDLE_PREFIX)) {
        baseUri = rdfdbImplementation.resolve(baseUri);
      }

      // Get revision URI if base URI is given.
      URI uri = baseUri;
      if (TGCrudServiceUtilities.isBaseUri(baseUri)) {
        // Look for revisions in public RDF database, TECHMD only is existing for published objects!
        // For reading we need filtered latest revision, because we want to read the latest existing
        // object.
        int revision =
            rdfdbImplementation.getLatestRevisionPublic(baseUri, LATEST_REVISION_FILTERED);
        uri = URI.create(
            baseUri.toASCIIString() + CrudServiceIdentifier.REVISION_SEPARATION_CHAR + revision);

        log(INFO, methodInfo, "Base URI '" + baseUri + "' resolved to latest revision URI: " + uri);
      }

      // Get TECHMD string from public storage.
      try {
        techmd = CrudService.dataStorageImplementation.retrieveTechMDPublic(uri);
      } catch (IoFault e) {
        throw new ObjectNotFoundFault("TECHMD file not found");
      }

      log(INFO, methodInfo, "Successfully retrieved TECHMD metadata FROM THE DATA STORAGE");
    }

    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return techmd;
  }

  /**
   * <p>
   * Do a #READTECHMD via REST.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri: .+}/tech")
  public Response readTechmdREST(@PathParam(PARAM_URI) URI baseUri) {

    try {
      // Get TECHMD.
      String techmd = readTechmd(baseUri);

      // Build a response.
      ResponseBuilder rBuilder = Response.ok(techmd);

      // FIXME Set last modified date for client caching reasons?

      return rBuilder.build();

    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (IoFault e) {
      if (e.getFaultInfo().getCause().contains("FileNotFound")) {
        throw new WebApplicationException(e, Response.Status.NOT_FOUND);
      }
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * <p>
   * The <b>#UPDATE</b> method of the TG-crud service updates a TextGrid object including metadata
   * and data. Furthermore it does the following in the given order:
   *
   * <ul>
   * <li>RETRIEVE METADATA FROM DATA STORAGE.</li>
   * <li>STORE AGGREGATION / EDITION / COLLECTION TO IDX DATABASE.</li>
   * <li>STORE ORIGINAL XML TO IDX DATABASE.</li>
   * <li>STORE PROJECTFILE DATA TO IDX DATABASE.</li>
   * <li>DELETE RELATIONS FOR URI FROM RDF DATABASE.</li>
   * <li>Call the Adaptor Manager.</li>
   * <li>STORE RELATIONS TO RDF DATABASE.</li>
   * <li>STORE METADATA / DATA TO DATA STORAGE.</li>
   * <li>STORE METADATA TO IDX DATABASE.</li>
   * <li>Return the updated metadata element.</li>
   * </ul>
   * </p>
   */
  @Override
  public void update(final String sessionId, final String logParameter,
      final Holder<MetadataContainerType> tgObjectMetadata, DataHandler tgObjectData)
      throws ObjectNotFoundFault, MetadataParseFault, UpdateConflictFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.UPDATE);

    // NOTE We do not check for PUBLISH DIRECTLY flag anymore, because we want to be able to update
    // public resources in some cases (use internal LDAP role changes to enforce write access to
    // public data!).

    URI uri = null;
    boolean userLockingSucceeded = false;
    boolean internalLockingSucceeded = false;
    boolean lockedBySameUser = false;
    TgCrudCheckAccessResponse checkAccessResponse = null;
    try {

      MetadataContainerType metadata = tgObjectMetadata.value;

      // Check mandatory parameters in #UPDATE: tgObjectMetadata, tgObjectData.
      if (tgObjectData == null) {
        throw CrudServiceExceptions.ioFault(DATA_MISSING);
      }

      // Get and check metadata, include validation warnings from JAXB and RDF validation.
      List<Warning> validationWarnings = TGCrudServiceUtilities.checkMetadata(metadata);
      List<Warning> rdfValidationWarnings =
          TGCrudServiceUtilities.checkRelationRDFMetadata(metadata);
      validationWarnings.addAll(rdfValidationWarnings);

      // Check if a lastModified date is given in the metadata object. If not, reject the update.
      GeneratedType generated = metadata.getObject().getGeneric().getGenerated();
      if (generated == null || generated.getLastModified() == null) {
        MetadataParseFault e = new MetadataParseFault(
            "Generated element or lastModifiedDate is missing in the given TextGrid metadata");
        throw CrudServiceExceptions.metadataParseFault(e, UPDATE_DENIED);
      }

      // Get the URI.
      uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, uri);
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, STARTED, uri, CrudOperations.MAIN_STRING);

      // Check general WRITE access permission using the session ID and the URI from the metadata,
      // get back some additional data from the RBAC.
      checkAccessResponse =
          (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId, logParameter,
              uri.toASCIIString(), CrudServiceAai.OPERATION_WRITE);

      // Lock the given URI.
      // NOTE We (a) are checking the user lock and (b) are locking internally, so that the same
      // user can not #UPDATE at the same time.
      synchronized (CrudService.class) {

        // Get current user ID.
        String currentUserId = checkAccessResponse.getUsername();

        // Check if the user already has the lock. If so, we are not allowed to unlock the user lock
        // after the #UPDATE, furthermore we do not need to lock.
        String lockedByUser = identifierImplementation.isLockedBy(uri);

        log(DEBUG, methodInfo, "Object is locked by user: " + "[" + lockedByUser + "]");

        if (lockedByUser.equals(currentUserId)) {
          lockedBySameUser = true;
        }

        log(DEBUG, methodInfo, "Object is locked by same user: " + lockedBySameUser);

        // Now do lock the user, if allowed and necessary.
        // TODO Do we need to lock here anyway??
        if (!lockedBySameUser) {
          userLockingSucceeded =
              identifierImplementation.lock(uri, checkAccessResponse.getUsername());
          if (!userLockingSucceeded) {
            UpdateConflictFault fault =
                new UpdateConflictFault(OBJECT_LOCKED + ": " + lockedByUser);
            throw CrudServiceExceptions.ioFault(fault, UPDATE_DENIED);
          }
        }

        // Only if user locking succeeded or already in place, we do lock internally, too.
        internalLockingSucceeded = identifierImplementation.lockInternal(uri);

        log(INFO, methodInfo, "Internal locking fur URI " + uri + " "
            + (internalLockingSucceeded ? "COMPLETE" : "FAILED"));

        if (!internalLockingSucceeded) {
          UpdateConflictFault fault = new UpdateConflictFault(PROCESS_CONFLICT + ": " + uri);
          throw CrudServiceExceptions.updateConflictFault(fault, UPDATE_DENIED);
        }
      }

      /**
       * RETRIEVE METADATA FROM THE DATA STORAGE
       */

      // Get lastModified date from the metadata.
      XMLGregorianCalendar mLastModified = generated.getLastModified();
      ObjectType sMetadata = dataStorageImplementation.retrieveMetadata(uri).getObject();

      // Get creation date and lastModified date.
      XMLGregorianCalendar sLastModified = sMetadata.getGeneric().getGenerated().getLastModified();
      String mLastModifiedString = CrudServiceUtilities.getCETDateString(mLastModified);
      String sLastModifiedString = CrudServiceUtilities.getCETDateString(sLastModified);

      log(DEBUG, methodInfo, "LastModified date from metadata: " + mLastModifiedString + " ("
          + mLastModified.toGregorianCalendar().getTimeInMillis() + ")");
      log(DEBUG, methodInfo, "LastModified date FROM THE DATA STORAGE: " + sLastModifiedString
          + " (" + sLastModified.toGregorianCalendar().getTimeInMillis() + ")");

      // Compare the given lastModified date to the one IN THE DATA STORAGE. If the dates differ,
      // reject the update.
      if (sLastModified.toGregorianCalendar().compareTo(mLastModified.toGregorianCalendar()) != 0) {
        UpdateConflictFault e = new UpdateConflictFault("Given lastModified date ("
            + mLastModifiedString + ") differs from the one stored IN THE DATA STORAGE ("
            + sLastModifiedString + ")");
        throw CrudServiceExceptions.updateConflictFault(e, UPDATE_DENIED);
      }

      log(INFO, methodInfo, "LastModified dates compared and equal");

      // Update the last modified date.
      // TODO Check at method start with the metadata check!?
      generated.setLastModified(
          CrudServiceUtilities.getXMLGregorianCalendar(System.currentTimeMillis()));
      metadata.getObject().getGeneric().setGenerated(generated);

      log(INFO, methodInfo, "LastModified date updated");

      // Delete any warnings already existing in the metadata, add new warnings.
      generated.getWarning().clear();
      generated.getWarning().addAll(validationWarnings);

      log(DEBUG, methodInfo,
          "Deleted incoming warnings from metadata, add new warnings if existing");

      // Get the data's format out of the metadata.
      String format = metadata.getObject().getGeneric().getProvided().getFormat();

      // Hold the temp file until we release it again, so we can access the data stream over and
      // over again: nice!
      try {
        CrudServiceHoldSlipStream.lock(tgObjectData);

        log(DEBUG, methodInfo, HOLDING_DATA_STREAM);

      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, HOLDING_DATA_STREAM_FAILED);
      }

      /**
       * STORE AGGREGATIONS TO IDX DATABASE
       */

      InputStream tgAggregationStream = null;
      if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
        try {
          // Do call the AdaptorManager to add the URI to the about attribute of the aggregation
          // file.
          tgAggregationStream =
              AdaptorManager.insertAggregationUri(tgObjectData.getInputStream(), uri);

          log(DEBUG, methodInfo, "Created AdaptorManager aggregation stream");

          // Re-create a DataHandler from the AdaptorManager's input stream. Seems not to interfere
          // with our stream holding...
          tgObjectData = new DataHandler(
              new ByteArrayDataSource(tgAggregationStream, TextGridMimetypes.OCTET_STREAM));

          log(DEBUG, methodInfo, "Added the aggregation file's URI '" + uri
              + "' to the aggregation's about attribtue");

        } catch (TransformerException | IOException e) {
          throw CrudServiceExceptions.ioFault(e, ADDING_SUBJECT_AGGREGATION_FAILED);
        } finally {
          try {
            if (tgAggregationStream != null) {
              tgAggregationStream.close();

              log(DEBUG, methodInfo, "Closed AdaptorManager aggregation stream");
            }
          } catch (IOException e) {
            throw new IoFault(e.getMessage(), e);
          }
        }

        // Store aggregation data to the IDX database.
        try {
          CrudObject<MetadataContainerType> xmlObject =
              new TGCrudTextGridObject(uri, tgObjectData, CrudObject.AGGREGATION_DATA);

          idxdbImplementation.update(xmlObject);

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_AGGREGATION, uri, COMPONENT_IDXDB);

        } catch (IoFault e) {
          throw CrudServiceExceptions.ioFault(e, ADDING_SUBJECT_AGGREGATION_FAILED);
        }
      }

      /**
       * STORE ORIGINAL DATA | PROJECTFILE DATA | PORTALCONFIG DATA TO IDX DATABASE
       */

      try {
        if (TextGridMimetypes.ORIGINAL_SET.contains(format)) {
          idxdbImplementation
              .update(new TGCrudTextGridObject(metadata, tgObjectData, CrudObject.ORIGINAL_DATA));

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_ORIGINAL_DATA, uri,
              COMPONENT_IDXDB);
        }

        else if (TextGridMimetypes.PORTALCONFIG.equals(format)) {
          idxdbImplementation.update(
              new TGCrudTextGridObject(metadata, tgObjectData, CrudObject.PORTALCONFIG_DATA));

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_PORTALCONFIG_DATA, uri,
              COMPONENT_IDXDB);
        }

        else if (TextGridMimetypes.PROJECTFILE.equals(format)) {
          idxdbImplementation.update(
              new TGCrudTextGridObject(metadata, tgObjectData, CrudObject.PROJECTFILE_DATA));

          TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_PROJECTFILE_DATA, uri,
              COMPONENT_IDXDB);
        }

      } catch (IoFault e) {
        // Only do warn if we get a TransformerException, throw the exception otherwise. Identify
        // this by public static final error literal from ES storage class.
        if (e.getMessage()
            .equals(TGCrudServiceStorageElasticSearchImpl.ERROR_STORING_ORIGINAL_DATA)) {
          TGCrudServiceUtilities.addWarning(metadata, e.getMessage());
          log(WARN, methodInfo, ORIGINAL_DATA_COULD_NOT_BE_UPDATED);
        } else if (e.getMessage()
            .equals(TGCrudServiceStorageElasticSearchImpl.ERROR_STORING_PROJECTFILE_DATA)) {
          TGCrudServiceUtilities.addWarning(metadata, e.getMessage());
          log(WARN, methodInfo, PROJECTFILE_COULD_NOT_BE_UPDATED);
        } else if (e.getMessage()
            .equals(TGCrudServiceStorageElasticSearchImpl.ERROR_STORING_PORTALCONFIG_DATA)) {
          TGCrudServiceUtilities.addWarning(metadata, e.getMessage());
          log(WARN, methodInfo, PORTALCONFIG_COULD_NOT_BE_UPDATED);
        } else {
          throw e;
        }
      }

      // Get all the necessary data for the middleware element and re-set them accordingly.
      long newLastModified = System.currentTimeMillis();
      metadata.getObject().getGeneric().getGenerated()
          .setLastModified(CrudServiceUtilities.getXMLGregorianCalendar(newLastModified));

      log(INFO, methodInfo, "Updated lastModifiedDate");

      /**
       * CALL ADAPTOR MANAGER
       */

      // Only call the Adaptor Manager if data file is not empty.
      List<String> overallRelations = new ArrayList<String>();
      // Gather relations, any warnings are added directly to the metadata object.
      overallRelations = TGCrudServiceAdaptorManager.go(sessionId, logParameter, uri,
          new TGCrudTextGridObject(metadata, tgObjectData), rdfdbImplementation,
          dataStorageImplementation, idxdbImplementation, aaiImplementation, conf,
          CrudOperations.UPDATE);

      /**
       * ASSEMBLE MORE RELATIONS FOR URI
       */

      // Assemble relations from the relational metadata, including RDF type, and some special
      // metadata to the relations gathered by the AdaptorManager.
      overallRelations.addAll(TGCrudServiceUtilities.assembleRelationsForCreation(uri,
          conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadata));

      /**
       * DELETE RELATIONS FOR URI FROM RDF DATABASE
       */

      logRelations(methodInfo, uri);

      // Delete relations including aggregation relations! AdaptorManager has already re-set the
      // aggregation relations, metadata relations and TG-crud relations have been be re-set either.
      rdfdbImplementation.deleteMetadata(new TGCrudTextGridObject(uri));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, REMOVED_OLD_RELATION_DATA, uri,
          COMPONENT_RDFDB);
      logRelations(methodInfo, uri);

      /**
       * STORE RELATIONS TO RDF DATABASE
       */

      if (!overallRelations.isEmpty()) {
        TGCrudTextGridObject rdfObject = new TGCrudTextGridObject(uri, overallRelations);
        rdfdbImplementation.create(rdfObject);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_NEW_RELATION_DATA, uri,
            COMPONENT_RDFDB);
      }

      logWarnings(methodInfo, metadata);
      logRelations(methodInfo, uri);
      logMetadata(methodInfo, metadata);

      /**
       * STORE METADATA/DATA TO DATA STORAGE
       */

      dataStorageImplementation.update(new TGCrudTextGridObject(metadata, tgObjectData));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_METADATA_DATA, uri,
          COMPONENT_DATA_STORAGE);

      /**
       * STORE METADATA TO IDX DATABASE
       */

      idxdbImplementation.updateMetadata(new TGCrudTextGridObject(metadata));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, STORED_METADATA, uri, COMPONENT_IDXDB);

      /**
       * SEND #UPDATE MESSAGE WITH MESSAGING SYSTEM
       */

      if (conf.getUSEmESSAGING()) {
        CrudServiceMessage message = new CrudServiceMessage()
            .setOperation(CrudOperations.UPDATE)
            .setObjectId(uri)
            .setRootId(metadata.getObject().getGeneric().getGenerated().getProject().getId())
            .setFormat(metadata.getObject().getGeneric().getProvided().getFormat())
            .setPublic(isPublic(metadata.getObject()));
        messageProducer.sendMessage(conf, message);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, MESSAGE_SENT, uri, COMPONENT_MESSAGING);
      }

      // Add permissions to the middleware element.
      addPermissions(metadata, checkAccessResponse.getOperation());

      log(INFO, methodInfo, PERMISSIONS_ADDED);
      logMetadata(methodInfo, metadata);
      logRelations(methodInfo, uri);

      // Fill the return holder with the created metadata object.
      tgObjectMetadata.value = metadata;
    }

    // Check error messages for rollback logging, catch non-critical errors and log rollback end
    // log.
    catch (ObjectNotFoundFault | MetadataParseFault | UpdateConflictFault | AuthFault e) {
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE_WITH_ROLLBACK_UNCRITICAL_ERROR, uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch IoFaults and log rollback error.
    catch (IoFault e) {
      TGCrudServiceUtilities.rbLog(ERROR, methodInfo,
          NOT_COMPLETE + ": " + e.getClass().getSimpleName() + ": " + e.getMessage(), uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
    // Do some final tasks!
    finally {
      // Release locks.
      synchronized (CrudService.class) {
        // If internal locking succeeded, release internal lock.
        if (internalLockingSucceeded && checkAccessResponse != null) {
          identifierImplementation.unlockInternal(uri);
        }

        // Only if user lock was not in place before, and user locking succeeded, release user lock.
        if (!lockedBySameUser && userLockingSucceeded && checkAccessResponse != null) {
          identifierImplementation.unlock(uri, checkAccessResponse.getUsername());
        }
      }

      // Release data stream hold to delete the cached data file.
      CrudServiceHoldSlipStream.releaseAndClose(tgObjectData);

      log(DEBUG, methodInfo, STREAM_RELEASE_COMPLETE);
    }

    TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE, uri, CrudOperations.MAIN_STRING);
    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
  }

  /**
   * <p>
   * Do a #UPDATE via REST.
   * </p>
   */
  @Override
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri}/update")
  public Response updateREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI uri,
      final @Multipart(value = PARAM_TGOBJECT_METADATA,
          type = METADATA_MIMETYPES) MetadataContainerType tgObjectMetadata,
      final @Multipart(value = PARAM_TGOBJECT_DATA,
          type = MediaType.APPLICATION_OCTET_STREAM) DataHandler tgObjectData) {

    // Create metadata holder.
    Holder<MetadataContainerType> metadata = new Holder<MetadataContainerType>(tgObjectMetadata);

    // Take the URI from the path parameter!
    metadata.value.getObject().getGeneric().getGenerated().getTextgridUri().setValue(uri);

    // Call TG-crud#UPDATE and assemble response.
    try {
      this.update(sessionId, logParameter, metadata, tgObjectData);

      // Get filename from metadata.
      String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata.value)
          + conf.getMETADATAfILEsUFFIX();

      // Build a response.
      ResponseBuilder rBuilder =
          Response.ok(metadata.value).header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML)
              .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + filename + "\"");

      // Set last modified date for client caching reasons, and location.
      Date lastModified = new Date(metadata.value.getObject().getGeneric().getGenerated()
          .getLastModified().toGregorianCalendar().getTimeInMillis());
      rBuilder.lastModified(lastModified);
      URI location =
          metadata.value.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      rBuilder.location(location);

      return rBuilder.build();

    } catch (MetadataParseFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    } catch (UpdateConflictFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    }
  }

  /**
   * <p>
   * The <b>#UPDATEMETADATA</b> method of the TG-crud service updates the metadata of a TextGrid
   * object. Furthermore it does the following in the given order:
   *
   * <ul>
   * <li>RETRIEVE METADATA FROM DATA STORAGE.</li>
   * <li>RETRIEVE ADAPTOR DATA FROM DATA STORAGE.</li>
   * <li>DELETE RELATIONS FOR URI FROM RDF DATABASE.</li>
   * <li>STORE RELATIONS TO RDF DATABASE.</li>
   * <li>STORE METADATA TO DATA STORAGE.</li>
   * <li>STORE METADATA TO IDX DATABASE.</li>
   * <li>Return the updated metadata element.</li>
   * </ul>
   * </p>
   */
  @Override
  public void updateMetadata(final String sessionId, final String logParameter,
      final Holder<MetadataContainerType> tgObjectMetadata)
      throws ObjectNotFoundFault, MetadataParseFault, UpdateConflictFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.UPDATEMETADATA);

    // NOTE We do not check for PUBLISH DIRECTLY flag anymore, because we want to be able to update
    // public metadata resources in some cases (use internal LDAP role changes to enforce write
    // access to public metadata!).

    URI uri = null;
    boolean userLockingSucceeded = false;
    boolean internalLockingSucceeded = false;
    boolean lockedBySameUser = false;
    TgCrudCheckAccessResponse checkAccessResponse = null;
    try {
      MetadataContainerType metadata = tgObjectMetadata.value;

      // Get and check metadata, include validation warnings from JAXB and RDF validation.
      List<Warning> validationWarnings = TGCrudServiceUtilities.checkMetadata(metadata);
      List<Warning> rdfValidationWarnings =
          TGCrudServiceUtilities.checkRelationRDFMetadata(metadata);
      validationWarnings.addAll(rdfValidationWarnings);

      // Check if a lastModified date is given in the metadata object. If not, reject the update.
      GeneratedType generated = metadata.getObject().getGeneric().getGenerated();
      if (generated == null || generated.getLastModified() == null) {
        MetadataParseFault e = new MetadataParseFault(
            "Generated element or lastModifiedDate is missing in the given TextGrid metadata");
        throw CrudServiceExceptions.metadataParseFault(e, UPDATE_DENIED);
      }

      // Get the URI.
      uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, uri);
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, STARTED, uri, CrudOperations.MAIN_STRING);

      // Check general WRITE access permission using the session ID and the URI from the metadata,
      // get back some additional data from the RBAC.
      checkAccessResponse =
          (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId, logParameter,
              uri.toASCIIString(), CrudServiceAai.OPERATION_WRITE);

      // Lock the given URI.
      // NOTE We (a) are checking the user lock and (b) are locking internally, so that the same
      // user can not #UPDATEMETADATA at the same time.
      synchronized (CrudService.class) {

        // Get current user ID.
        String currentUserId = checkAccessResponse.getUsername();

        // Check if the user already has the lock. If so, we are not allowed to unlock the user lock
        // after the #UPDATE, furthermore we do not need to lock.
        String lockedByUser = identifierImplementation.isLockedBy(uri);
        if (lockedByUser.equals(currentUserId)) {
          lockedBySameUser = true;
        }

        // Now do lock the user, if allowed and necessary.
        // TODO Do we need to lock here anyway??
        if (!lockedBySameUser) {
          userLockingSucceeded =
              identifierImplementation.lock(uri, checkAccessResponse.getUsername());
          if (!userLockingSucceeded) {
            UpdateConflictFault fault =
                new UpdateConflictFault(OBJECT_LOCKED + ": " + lockedByUser);
            throw CrudServiceExceptions.ioFault(fault, UPDATE_DENIED);
          }
        }

        // Only if user locking succeeded or already in place, we do lock internally, too.
        internalLockingSucceeded = identifierImplementation.lockInternal(uri);

        log(INFO, methodInfo, "Internal locking fur URI " + uri + " "
            + (internalLockingSucceeded ? "COMPLETE" : "FAILED"));

        if (!internalLockingSucceeded) {
          UpdateConflictFault fault = new UpdateConflictFault(PROCESS_CONFLICT + ": " + uri);
          throw CrudServiceExceptions.updateConflictFault(fault, UPDATE_DENIED);
        }
      }

      /**
       * RETRIEVE METADATA FROM DATA STORAGE
       */

      // Get lastModified date from the metadata.
      XMLGregorianCalendar mLastModified = generated.getLastModified();
      ObjectType sMetadata = dataStorageImplementation.retrieveMetadata(uri).getObject();

      // Get creation date and lastModified date FROM THE DATA STORAGE.
      XMLGregorianCalendar sLastModified = sMetadata.getGeneric().getGenerated().getLastModified();
      String mLastModifiedString = CrudServiceUtilities.getCETDateString(mLastModified);
      String sLastModifiedString = CrudServiceUtilities.getCETDateString(sLastModified);

      log(DEBUG, methodInfo, "LastModified date from metadata: " + mLastModifiedString + " ("
          + mLastModified.toGregorianCalendar().getTimeInMillis() + ")");
      log(DEBUG, methodInfo, "LastModified date FROM THE DATA STORAGE: " + sLastModifiedString
          + " (" + sLastModified.toGregorianCalendar().getTimeInMillis() + ")");

      // Compare the given lastModified date to the one IN THE DATA STORAGE. If the dates differ,
      // reject the update.
      if (sLastModified.toGregorianCalendar().compareTo(mLastModified.toGregorianCalendar()) != 0) {
        UpdateConflictFault e = new UpdateConflictFault("Given lastModified date ("
            + mLastModifiedString + ") differs from the one stored IN THE DATA STORAGE ("
            + sLastModifiedString + ")");
        throw CrudServiceExceptions.updateConflictFault(e, UPDATE_DENIED);
      }

      log(INFO, methodInfo, "LastModified dates compared and equal");

      // Update the last modified date.
      // TODO Check at method start with the metadata check!?
      generated.setLastModified(
          CrudServiceUtilities.getXMLGregorianCalendar(System.currentTimeMillis()));
      metadata.getObject().getGeneric().setGenerated(generated);

      log(INFO, methodInfo, "LastModified date updated");

      // Get adaptor from given metadata and database metadata.
      URI mAdaptor = URI.create("null");
      if (metadata.getObject().getRelations() != null
          && metadata.getObject().getRelations().getHasAdaptor() != null) {
        mAdaptor = metadata.getObject().getRelations().getHasAdaptor();
      }
      URI dbAdaptor = URI.create("null");
      if (sMetadata.getRelations() != null && sMetadata.getRelations().getHasAdaptor() != null) {
        dbAdaptor = sMetadata.getRelations().getHasAdaptor();
      }

      log(DEBUG, methodInfo, "Adaptor URI from metadata: " + mAdaptor);
      log(DEBUG, methodInfo, "Adaptor URI from database: " + dbAdaptor);

      // Compare both adaptor URIs. If the adaptor given in the metadata has changed, the object
      // must be adapted again by using the data file FROM THE DATA STORAGE.
      // Also do re-adapt if RDF validation warnings do exist (means the relations RDF is invalid,
      // and we have to re-write it, at least for images for the time being).
      // TODO If we have the same adaptor URI, adaptor file could have been changed itself, just
      // keep that in mind!
      List<String> overallRelations = new ArrayList<String>();
      if (mAdaptor.equals(dbAdaptor) && rdfValidationWarnings.isEmpty()) {

        log(INFO, methodInfo, "Adaptor URIs compared and equal, no re-adaptation needed");

      } else {
        // For images the relation RDF is being omitted and exif metadata is being re-created from
        // image data via AdaptorManager.
        // TODO Check if a re-adaption also for other formats is working and needed! We have some
        // eltec namespace metadata in the work relation RDF, for example.
        if (TextGridMimetypes.IMAGE_SET
            .contains(metadata.getObject().getGeneric().getProvided().getFormat())) {
          TGCrudServiceUtilities.reAdapt(uri, sessionId, logParameter, methodInfo, metadata, conf);
        }
      }

      // Add warnings from XML and RDF validation, if any!
      generated.getWarning().addAll(validationWarnings);

      // Get all the necessary data for the middleware element and re-set them accordingly.
      long newLastModified = System.currentTimeMillis();
      metadata.getObject().getGeneric().getGenerated()
          .setLastModified(CrudServiceUtilities.getXMLGregorianCalendar(newLastModified));

      log(INFO, methodInfo, "Updated lastModifiedDate");

      /**
       * ASSEMBLE NEW RELATIONS FOR URI
       */

      // Assemble relations from the relational metadata, including RDF type, and some special
      // metadata to the relations gathered by the AdaptorManager.
      overallRelations.addAll(TGCrudServiceUtilities.assembleRelationsForCreation(uri,
          conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadata));

      /**
       * DELETE RELATIONS FOR URI FROM RDF DATABASE
       */

      logRelations(methodInfo, uri);

      // Delete all relations except aggregation relations! Metadata relations and TG-crud internal
      // relations will be re-set.
      TGCrudTextGridObject deleteRdf = new TGCrudTextGridObject(uri);

      // Set kind of data to aggregation if so (to avoid deleting all the aggregations). Fixes bug
      // TG-1522.
      String format = metadata.getObject().getGeneric().getProvided().getFormat();
      if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
        deleteRdf.setKindOfData(CrudObject.AGGREGATION_DATA);
      }
      rdfdbImplementation.deleteMetadata(deleteRdf);

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, REMOVED_OLD_RELATION_DATA, uri,
          COMPONENT_RDFDB);
      logRelations(methodInfo, uri);

      /**
       * STORE NEW RELATIONS TO RDF DATABASE
       */

      if (!overallRelations.isEmpty()) {
        TGCrudTextGridObject rdfObject = new TGCrudTextGridObject(uri, overallRelations);
        rdfdbImplementation.create(rdfObject);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, CREATED_NEW_RELATION_DATA, uri,
            COMPONENT_RDFDB);
      }

      logWarnings(methodInfo, metadata);
      logRelations(methodInfo, uri);
      logMetadata(methodInfo, metadata);

      /**
       * STORE METADATA TO DATA STORAGE
       */

      dataStorageImplementation.updateMetadata(new TGCrudTextGridObject(metadata));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_METADATA, uri, COMPONENT_DATA_STORAGE);

      /**
       * STORE METADATA TO IDX DATABASE
       */

      idxdbImplementation.updateMetadata(new TGCrudTextGridObject(metadata));

      TGCrudServiceUtilities.rbLog(INFO, methodInfo, UPDATED_METADATA, uri, COMPONENT_IDXDB);

      /**
       * SEND #UPDATEMETADATA MESSAGE WITH MESSAGING SYSTEM
       */

      if (conf.getUSEmESSAGING()) {
        CrudServiceMessage message = new CrudServiceMessage()
            .setOperation(CrudOperations.UPDATEMETADATA)
            .setObjectId(uri)
            .setRootId(metadata.getObject().getGeneric().getGenerated().getProject().getId())
            .setFormat(metadata.getObject().getGeneric().getProvided().getFormat())
            .setPublic(isPublic(metadata.getObject()));
        messageProducer.sendMessage(conf, message);

        TGCrudServiceUtilities.rbLog(INFO, methodInfo, MESSAGE_SENT, uri, COMPONENT_MESSAGING);
      }

      // Add permissions to the middleware element.
      addPermissions(metadata, checkAccessResponse.getOperation());

      log(INFO, methodInfo, PERMISSIONS_ADDED);
      logMetadata(methodInfo, metadata);
      logRelations(methodInfo, uri);

      // Fill the return holder with the created metadata object.
      tgObjectMetadata.value = metadata;
    }

    // Check error messages for rollback logging, catch non-critical errors and log rollback end
    // log.
    catch (ObjectNotFoundFault | MetadataParseFault | UpdateConflictFault | AuthFault e) {
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE_WITH_ROLLBACK_UNCRITICAL_ERROR, uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch IoFaults and log rollback error.
    catch (IoFault e) {
      TGCrudServiceUtilities.rbLog(ERROR, methodInfo,
          NOT_COMPLETE + ": " + e.getClass().getSimpleName() + ": " + e.getMessage(), uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
    // Release locks.
    finally {
      synchronized (CrudService.class) {
        // If internal locking succeeded, release internal lock.
        if (internalLockingSucceeded && checkAccessResponse != null) {
          identifierImplementation.unlockInternal(uri);
        }

        // Only if user lock was not in place before, and user locking succeeded, release user lock.
        if (!lockedBySameUser && userLockingSucceeded && checkAccessResponse != null) {
          identifierImplementation.unlock(uri, checkAccessResponse.getUsername());
        }
      }
    }

    TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE, uri, CrudOperations.MAIN_STRING);

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
  }

  /**
   * <p>
   * Do a #UPDATEMETADATA via REST.
   * </p>
   */
  @Override
  @POST
  @Produces(MediaType.TEXT_XML)
  @Path("/{uri}/updateMetadata")
  public Response updateMetadataREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI uri, final @Multipart(value = PARAM_TGOBJECT_METADATA,
          type = METADATA_MIMETYPES) MetadataContainerType tgObjectMetadata) {

    // Create metadata holder.
    Holder<MetadataContainerType> metadata = new Holder<MetadataContainerType>(tgObjectMetadata);

    // Take the URI from the path parameter!
    metadata.value.getObject().getGeneric().getGenerated().getTextgridUri().setValue(uri);

    // Call TG-crud#UPDATEMETADATA and assemble response.
    try {
      this.updateMetadata(sessionId, logParameter, metadata);

      // Get filename from metadata.
      String filename = TGCrudServiceUtilities.getDataFilenameFromMetadata(metadata.value)
          + conf.getMETADATAfILEsUFFIX();

      // Build a response.
      ResponseBuilder rBuilder =
          Response.ok(metadata.value).header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_XML)
              .header(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=\"" + filename + "\"");

      // Set last modified date for client caching reasons, and location.
      Date lastModified = new Date(metadata.value.getObject().getGeneric().getGenerated()
          .getLastModified().toGregorianCalendar().getTimeInMillis());
      rBuilder.lastModified(lastModified);
      URI location =
          metadata.value.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      rBuilder.location(location);

      return rBuilder.build();

    } catch (MetadataParseFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    } catch (UpdateConflictFault e) {
      throw new WebApplicationException(e, Response.Status.BAD_REQUEST);
    }
  }

  /**
   * <p>
   * Do #LOCK.
   * </p>
   */
  @Override
  public boolean lock(final String sessionId, final String logParameter, final URI uri)
      throws ObjectNotFoundFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.LOCK);

    init(methodInfo, tgcrudServiceVersion);

    TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, uri);

    // Check general WRITE access permission using the session ID and the URI from the metadata, get
    // back some additional data from the RBAC.
    TgCrudCheckAccessResponse checkAccessResponse =
        (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId, logParameter,
            uri.toASCIIString(), CrudServiceAai.OPERATION_WRITE);

    // If access is granted, just do lock the URI using the internal locking method, get username
    // first.
    boolean result = identifierImplementation.lock(uri, checkAccessResponse.getUsername());

    // If the result is not TRUE, then resource is locked by another user. Throw exception and get
    // the locking user ID before.
    if (!result) {
      // TODO Get locking user with lock() method already, so we had not to access the URI
      // implementation twice!
      String lockedByUser = identifierImplementation.isLockedBy(uri);

      throw CrudServiceExceptions.ioFault(
          "Locking failed! Object " + uri + " is currently locked by user " + lockedByUser);
    }

    // End method log.
    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return result;
  }

  /**
   * <p>
   * Do #LOCK via REST.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{uri}/lock")
  public boolean lockREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI uri) {
    try {
      return this.lock(sessionId, logParameter, uri);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * Do #UNLOCK.
   * </p>
   */
  @Override
  public boolean unlock(final String sessionId, final String logParameter, final URI uri)
      throws ObjectNotFoundFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.UNLOCK);

    init(methodInfo, tgcrudServiceVersion);

    TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, uri);

    // Check general WRITE access permission using the session ID and the URI from the metadata, get
    // back some additional data from the RBAC.
    TgCrudCheckAccessResponse checkAccessResponse =
        (TgCrudCheckAccessResponse) aaiImplementation.checkAccessGetInfo(sessionId, logParameter,
            uri.toASCIIString(), CrudServiceAai.OPERATION_WRITE);

    // If access is granted, just do unlock the URI using the internal locking method, get username
    // first.
    boolean result = identifierImplementation.unlock(uri, checkAccessResponse.getUsername());

    // If the result is not TRUE, then resource is locked by another user. Throw exception and put
    // user ID in there.
    if (!result) {
      // TODO Get locking user with unlock() method already, so we had not to access the URI
      // implementation twice!
      String lockedByUser = identifierImplementation.isLockedBy(uri);

      throw CrudServiceExceptions.ioFault(
          "Unlocking failed! Object " + uri + " is currently locked by user " + lockedByUser);
    }

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return result;
  }

  /**
   * <p>
   * Do a #UNLOCK via REST.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{uri}/unlock")
  public boolean unlockREST(final @QueryParam(PARAM_SESSION_ID) String sessionId,
      final @QueryParam(PARAM_LOG_PARAMETER) String logParameter,
      final @PathParam(PARAM_URI) URI uri) {
    try {
      return this.unlock(sessionId, logParameter, uri);
    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    } catch (ObjectNotFoundFault e) {
      throw new WebApplicationException(e, Response.Status.NOT_FOUND);
    } catch (AuthFault e) {
      throw new WebApplicationException(e, Response.Status.UNAUTHORIZED);
    }
  }

  /**
   * <p>
   * Compute the decimal number for a given NOID.
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{uri}/n4n")
  public String number4noidREST(final @PathParam(PARAM_URI) URI uri) {

    String result = "";

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.N4N);

    try {
      init(methodInfo, tgcrudServiceVersion);

      String noid = TGCrudServiceUtilities
          .stripRevision(URI.create(TGCrudServiceUtilities.stripUriPrefix(uri, conf))).toString();
      result = noid + " = ";
      int bas = NOID_CHARACTERS.length();
      long res = 0;
      for (int i = noid.length(); i > 0; i--) {
        int exp = i - 1;
        int zif = noid.length() - (i - 1);
        String cha = noid.substring(zif - 1, zif);
        int pos = NOID_CHARACTERS.indexOf(cha);
        res += pos * Math.pow(bas, exp);
        result += pos + "*" + bas + "^" + exp;
        if (i > 1) {
          result += " + ";
        }
      }
      result += " = " + String.valueOf(res).trim();
    } catch (IoFault e) {
      throw new RuntimeException(e);
    }

    log(INFO, methodInfo, "number4noid() called: " + result);

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return result;
  }

  /**
   * <p>
   * Re-cache the given URI: Sends an #UPDATE message for the given URI to updated by any message
   * clients.
   * </p>
   */
  @Override
  @GET
  @Path("/{uri}/recache")
  public Response recacheREST(@PathParam(PARAM_URI) URI uri) {

    // TODO Add internal secret for all the maintenance methods?

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.RECACHE);

    // Send UPDATE message via message producer, if configured.
    try {
      init(methodInfo, tgcrudServiceVersion);

      if (conf.getUSEmESSAGING()) {

        // Get metadata from crud: non-public first, public after IoFault.
        MetadataContainerType metadata;
        try {
          metadata = dataStorageImplementation.retrieveMetadata(uri);
        } catch (IoFault e) {
          metadata = dataStorageImplementation.retrieveMetadataPublic(uri);
        }

        log(INFO, methodInfo, "Calling recache() for URI " + uri);

        // Produce UPDATE message :-)
        CrudServiceMessage message = new CrudServiceMessage()
            .setOperation(CrudOperations.UPDATE)
            .setObjectId(uri)
            .setRootId(metadata.getObject().getGeneric().getGenerated().getProject().getId())
            .setFormat(metadata.getObject().getGeneric().getProvided().getFormat())
            .setPublic(isPublic(metadata.getObject()));
        messageProducer.sendMessage(conf, message);
      }

      ResponseBuilder rBuilder = Response.ok();

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (IoFault e) {
      throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * <p>
   * The RESTful method API for the TG-crud#CONSISTENCY method (REST only).
   * </p>
   */
  @Override
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{uri}/consistency")
  public Response checkConsistencyREST(@PathParam(PARAM_URI) URI uri) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.CONSISTENCY);

    // Gather filesize and checksum metadata and compare, then send response.
    ResponseBuilder rBuilder;
    try {
      init(methodInfo, tgcrudServiceVersion);

      Consistency c = TGCrudServiceUtilities.checkConsistency(uri, conf);

      if (c.checksumStatus == TGCrudServiceUtilities.STATUS_OK
          && c.filesizeStatus == TGCrudServiceUtilities.STATUS_OK) {
        // 204 if all metadata do match! Deliver no HTTP body.
        rBuilder = Response.noContent();
      } else {
        // 200 if something does not match! Deliver JSON response.
        rBuilder = Response.ok(c).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
      }

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    } catch (IoFault | NoSuchAlgorithmException | IOException e) {
      // 500 if errors do occur.
      rBuilder = Response.serverError();
    }

    return rBuilder.build();
  }

  /**
   * <p>
   * The RESTful method API for the TG-crud#REINDEXMETADATA method (REST only).
   * 
   * NOTE: Public TG-crud will re-index public resources, non-public TG-crud will re-index
   * non-public resources!
   * </p>
   */
  @Override
  @GET
  @Path("/{uri}/reindexMetadata")
  public Response reindexMetadataREST(@QueryParam(PARAM_REINDEX_SECRET) String auth,
      @PathParam(PARAM_URI) URI uri) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.REINDEXMETADATA);
    boolean internalLockingSucceeded = false;

    try {
      init(methodInfo, tgcrudServiceVersion);

      /**
       * CHECK SPECIAL AUTH (NO SID!)
       */

      if (auth == null || auth.isEmpty() || !auth.equals(conf.getREINDEXsECRET())) {
        return Response.status(Status.UNAUTHORIZED.getStatusCode()).build();
      }

      /**
       * RETRIEVE METADATA FROM DATA STORAGE
       */

      MetadataContainerType metadata = dataStorageImplementation.retrieveMetadata(uri);

      // Get and check metadata, include validation warnings from JAXB and RDF validation.
      List<Warning> validationWarnings = TGCrudServiceUtilities.checkMetadata(metadata);
      List<Warning> rdfValidationWarnings =
          TGCrudServiceUtilities.checkRelationRDFMetadata(metadata);
      validationWarnings.addAll(rdfValidationWarnings);

      TGCrudServiceUtilities.logStartParameters(methodInfo, "ADMIN", "", uri);

      /**
       * LOCK INTERNALLY
       */

      // Lock the given URI (only lock internally, we do not have a user here, only admin use
      // allowed)!
      synchronized (CrudService.class) {
        // Only if user locking succeeded or already in place, we do lock internally, too.
        internalLockingSucceeded = identifierImplementation.lockInternal(uri);

        log(INFO, methodInfo, "Internal locking fur URI " + uri + " "
            + (internalLockingSucceeded ? "COMPLETE" : "FAILED"));

        if (!internalLockingSucceeded) {
          UpdateConflictFault fault = new UpdateConflictFault(PROCESS_CONFLICT + ": " + uri);
          throw CrudServiceExceptions.updateConflictFault(fault, UPDATE_DENIED);
        }
      }

      /**
       * RE-ADAPT
       */

      // NOTE Only public data can be re-adapted here for we do not have SID at all!)
      // For images the relation RDF is being omitted and EXIF metadata is being re-created from
      // image data via AdaptorManager.
      // TODO Check if a re-adaption also for other formats is working and needed! We have some
      // eltec namespace metadata in the work relation RDF, for example.
      if (TextGridMimetypes.IMAGE_SET
          .contains(metadata.getObject().getGeneric().getProvided().getFormat())) {
        TGCrudServiceUtilities.reAdapt(uri, "ADMIN", "", methodInfo, metadata, conf);
      }

      // Add warnings from XML and RDF validation, if any!
      List<String> overallRelations = new ArrayList<String>();
      metadata.getObject().getGeneric().getGenerated().getWarning().addAll(validationWarnings);

      /**
       * ASSEMBLE NEW RELATIONS FOR URI
       */

      // Assemble relations from the relational metadata, including RDF type, and some special
      // metadata to the relations gathered by the AdaptorManager.
      overallRelations.addAll(TGCrudServiceUtilities.assembleRelationsForCreation(uri,
          conf.getRDFDBnAMESPACE(), conf.getURIpREFIX(), metadata));

      /**
       * DELETE RELATIONS FOR URI FROM RDF DATABASE
       */

      logRelations(methodInfo, uri);

      // Delete all relations except aggregation relations! Metadata relations and TG-crud internal
      // relations will be re-set.
      TGCrudTextGridObject deleteRdf = new TGCrudTextGridObject(uri);

      // Set kind of data to aggregation if so (to avoid deleting all the aggregations). Fixes bug
      // TG-1522.
      String format = metadata.getObject().getGeneric().getProvided().getFormat();
      if (TextGridMimetypes.AGGREGATION_SET.contains(format)) {
        deleteRdf.setKindOfData(CrudObject.AGGREGATION_DATA);
      }
      rdfdbImplementation.deleteMetadata(deleteRdf);
      logRelations(methodInfo, uri);

      /**
       * STORE NEW RELATIONS TO RDF DATABASE
       */

      if (!overallRelations.isEmpty()) {
        TGCrudTextGridObject rdfObject = new TGCrudTextGridObject(uri, overallRelations);
        rdfdbImplementation.create(rdfObject);
      }

      logWarnings(methodInfo, metadata);
      logRelations(methodInfo, uri);
      logMetadata(methodInfo, metadata);

      /**
       * STORE METADATA TO DATA STORAGE
       */

      dataStorageImplementation.updateMetadata(new TGCrudTextGridObject(metadata));

      /**
       * STORE METADATA TO IDX DATABASE
       */

      idxdbImplementation.updateMetadata(new TGCrudTextGridObject(metadata));

      /**
       * SEND #UPDATEMETADATA MESSAGE WITH MESSAGING SYSTEM
       */

      if (conf.getUSEmESSAGING()) {
        CrudServiceMessage message = new CrudServiceMessage()
            .setOperation(CrudOperations.UPDATEMETADATA)
            .setObjectId(uri)
            .setRootId(metadata.getObject().getGeneric().getGenerated().getProject().getId())
            .setFormat(metadata.getObject().getGeneric().getProvided().getFormat())
            .setPublic(isPublic(metadata.getObject()));
        messageProducer.sendMessage(conf, message);
      }
    }

    // Check error messages for rollback logging, catch non-critical errors and log rollback end
    // log.
    catch (MetadataParseFault | UpdateConflictFault | IoFault e) {
      String message = e.getClass().getName() + ": " + e.getMessage();
      return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode(), message).build();
    }

    // Release locks.
    finally {
      synchronized (CrudService.class) {
        // If internal locking succeeded, release internal lock.
        if (internalLockingSucceeded) {
          try {
            identifierImplementation.unlockInternal(uri);
          } catch (IoFault e) {
            String message = "Unable to unlock " + uri + " due to a " + e.getClass().getName()
                + ": " + e.getMessage();
            return Response.status(Status.INTERNAL_SERVER_ERROR.getStatusCode(), message).build();
          }
        }
      }
    }

    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

    return Response.ok().build();
  }

  /**
   * <p>
   * The <b>#MOVEPUBLIC</b> method of the TG-crud service moves a TextGrid object from one DATA
   * storage to another DATA storage.
   * </p>
   */
  @Override
  public void movePublic(final String sessionId, final String logParameter, final URI uri)
      throws ObjectNotFoundFault, IoFault, AuthFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.MOVEPUBLIC);

    // Check for PUBLISH DIRECTLY flag.
    if (conf.getDIRECTLYpUBLISHwITHcREATE()) {
      throw CrudServiceExceptions.ioFault(
          "Operation " + methodInfo.getName() + " not permitted with directly publish flag set");
    }

    boolean internalLockingSucceeded = false;
    try {
      init(methodInfo, tgcrudServiceVersion);

      TGCrudServiceUtilities.logStartParameters(methodInfo, sessionId, logParameter, uri);
      TGCrudServiceUtilities.rblog(INFO, methodInfo, STARTED, CrudOperations.MAIN_STRING);

      // Authenticate.
      if (sessionId == null || sessionId.isEmpty() || !sessionId.equals(conf.getPUBLISHsECRET())) {
        AuthFault e = new AuthFault("DON'T MOVE IT");
        throw CrudServiceExceptions.authFault(e, "Access denied");
      }

      // Lock the given URI internally. Only project leaders can do a #MOVEPUBLIC using TG-publish,
      // so we only need internal locking.
      synchronized (CrudService.class) {
        internalLockingSucceeded = identifierImplementation.lockInternal(uri);

        log(INFO, methodInfo, "Internal locking fur URI " + uri + " "
            + (internalLockingSucceeded ? "COMPLETE" : "FAILED"));

        if (!internalLockingSucceeded) {
          IoFault fault = new IoFault(PROCESS_CONFLICT + ": " + uri);
          throw CrudServiceExceptions.ioFault(fault, MOVATION_DENIED);
        }
      }

      /**
       * MOVE DATA/METADATA TO PUBLIC DATA STORAGE
       */

      dataStorageImplementation.move(uri);
    }

    // Check error messages for rollback logging, catch non-critical errors for rollback and log
    // rollback end log.
    catch (ObjectNotFoundFault | AuthFault e) {
      TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE_WITH_ROLLBACK_UNCRITICAL_ERROR, uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch IoFaults and log rollback error.
    catch (IoFault e) {
      TGCrudServiceUtilities.rbLog(ERROR, methodInfo,
          NOT_COMPLETE + ": " + e.getClass().getSimpleName() + ": " + e.getMessage(), uri,
          CrudOperations.MAIN_STRING);
      throw e;
    }
    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
    // If internal locking succeeded, release internal lock.
    finally {
      if (internalLockingSucceeded) {
        identifierImplementation.unlockInternal(uri);
      }
    }

    TGCrudServiceUtilities.rbLog(INFO, methodInfo, COMPLETE, uri, CrudOperations.MAIN_STRING);
    log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
  }

  // **
  // PROTECTED
  // **

  /**
   * <p>
   * Initialise all the implementing classes given in the crud config file.
   * </p>
   */
  @Override
  protected void initImplementationClasses() throws IoFault {
    aaiImplementation = CrudServiceUtilities.getAaiImplementation(conf);
    identifierImplementation = CrudServiceUtilities.getIdImplementation(conf);
    dataStorageImplementation = TGCrudServiceUtilities.getDataStorageImplementation(conf);
    idxdbImplementation = TGCrudServiceUtilities.getIndexStorageImplementation(conf);
    rdfdbImplementation = TGCrudServiceUtilities.getRdfStorageImplementation(conf);
  }

  // **
  // PRIVATE
  // **

  /**
   * <p>
   * Sets the given permissions to the metadata object.
   * </p>
   *
   * @param theMetadata
   * @param thePermissions
   * @throws IoFault
   */
  private static void addPermissions(MetadataContainerType theMetadata, List<String> thePermissions)
      throws IoFault {

    if (theMetadata == null) {
      throw CrudServiceExceptions.ioFault("Metadata must not be empty!");
    }
    theMetadata.getObject().getGeneric().getGenerated()
        .setPermissions(TGCrudServiceUtilities.getPermissionString(thePermissions));
  }

  /**
   * <p>
   * Gets the latest existing latest revision number.
   * </p>
   *
   * @param theUri
   * @param theInfo
   * @param filteredOreUnfiltered
   * @return
   * @throws IoFault
   */
  private static int getLatestRevision(URI theUri, CrudServiceMethodInfo theInfo,
      boolean filteredOreUnfiltered) throws IoFault {

    int result;

    int latestRevisionUnfilteredNONPUBLIC;
    int latestRevisionUnfilteredPUBLIC;
    // Try getting latest revision from non-public RDFDB.
    try {
      latestRevisionUnfilteredNONPUBLIC =
          rdfdbImplementation.getLatestRevision(theUri, filteredOreUnfiltered);
    } catch (ObjectNotFoundFault f) {
      // Not found in case of object is only publicly available!
      latestRevisionUnfilteredNONPUBLIC = -1;
    }
    // Try getting latest revision from public RDFDB (if not existing in non-public or version on
    // public is higher).
    try {
      latestRevisionUnfilteredPUBLIC =
          rdfdbImplementation.getLatestRevisionPublic(theUri, filteredOreUnfiltered);
    } catch (ObjectNotFoundFault f) {
      // Not found on case of not publicly available!
      latestRevisionUnfilteredPUBLIC = -1;
    }

    log(DEBUG, theInfo, NON_PUBLIC + ":" + latestRevisionUnfilteredNONPUBLIC + ", " + PUBLIC + ":"
        + latestRevisionUnfilteredPUBLIC + " [filtered:" + filteredOreUnfiltered + "]");

    if (latestRevisionUnfilteredNONPUBLIC > latestRevisionUnfilteredPUBLIC) {
      result = latestRevisionUnfilteredNONPUBLIC;
    } else {
      result = latestRevisionUnfilteredPUBLIC;
    }

    return result;
  }

  /**
   * <p>
   * Gets the projectID out of the object's metadata from non-public or public latest revision
   * (filtered, means: existing object with highest revision number!).
   * </p>
   *
   * @param theUri
   * @param theInfo
   * @return
   * @throws IoFault
   * @throws ObjectNotFoundFault
   */
  private static String getProjectIDOfLatestMetadataFilteredNewRevision(URI theUri,
      CrudServiceMethodInfo theInfo) throws IoFault, ObjectNotFoundFault {

    String result = "";

    // For metadata reading we need filtered latest revision, because objects we read metadata from
    // must exist!
    int revision = getLatestRevision(theUri, theInfo, LATEST_REVISION_FILTERED);

    // Use filtered URI for retrieving metadata (fixes bug #17289) to get existing object.
    URI latestUriFiltered =
        URI.create(TGCrudServiceUtilities.stripRevision(theUri) + "." + revision);

    // Retrieve metadata.
    MetadataContainerType metadata;
    String publicOrNonPublic;
    try {
      metadata = dataStorageImplementation.retrieveMetadata(latestUriFiltered);
      publicOrNonPublic = NON_PUBLIC;
    } catch (IoFault f) {
      metadata = dataStorageImplementation.retrieveMetadataPublic(latestUriFiltered);
      publicOrNonPublic = PUBLIC;
    }

    log(DEBUG, theInfo, "Got latestMetadata for URI " + latestUriFiltered + " from "
        + publicOrNonPublic + " storage");

    // Get project ID from latest (filtered)
    Project latestProject = metadata.getObject().getGeneric().getGenerated().getProject();
    if (latestProject != null) {
      String latestProjectID = latestProject.getId();
      // (Re)-set project ID.
      result = latestProjectID;

      log(INFO, theInfo,
          "Project ID (re)-set to the one from latest revision (" + revision + "): " + result);
    } else {
      CrudServiceExceptions.ioFault("BELGARATH! No project ID given in metadata!");
    }

    return result;
  }

  /**
   * <p>
   * Simply logs the relations.
   * </p>
   *
   * @param theInfo
   * @param theUri
   * @throws IoFault
   */
  private static void logRelations(CrudServiceMethodInfo theInfo, URI theUri) throws IoFault {
    log(DEBUG, theInfo, "Relations for URI: " + theUri + "\n"
        + rdfdbImplementation.retrieve(theUri).getRelations());
  }

  /**
   * <p>
   * Simply logs the metadata.
   * </p>
   *
   * @param theInfo
   * @param theMetadata
   * @throws IoFault
   * @throws MetadataParseFault
   */
  private static void logMetadata(CrudServiceMethodInfo theInfo, MetadataContainerType theMetadata)
      throws IoFault, MetadataParseFault {
    log(DEBUG, theInfo,
        "Metadata for URI: "
            + theMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue() + "\n"
            + TGCrudServiceUtilities.getString(theMetadata, conf));
  }

  /**
   * <p>
   * Simply logs the warnings.
   * </p>
   *
   * @param theInfo
   * @param theMetadata
   */
  private static void logWarnings(CrudServiceMethodInfo theInfo,
      MetadataContainerType theMetadata) {

    int warninglistSize = theMetadata.getObject().getGeneric().getGenerated().getWarning().size();

    log(DEBUG, theInfo, "Added " + (warninglistSize == 0 ? "no" : warninglistSize) + " warning"
        + (warninglistSize == 1 ? "" : "s") + " to the metadata");
  }

  /**
   * <p>
   * Get public status from TextGrid metadata. If availability = public then result is true, false
   * otherwise.
   * </p>
   * 
   * @param theMetadata
   * @return
   */
  private static boolean isPublic(ObjectType theMetadata) {

    boolean result = false;

    if (theMetadata.getGeneric().getGenerated().getAvailability() != null && theMetadata
        .getGeneric().getGenerated().getAvailability().equals(DPO_AVAILIBILITY_PUBLIC)) {
      result = true;
    }

    return result;
  }

}
