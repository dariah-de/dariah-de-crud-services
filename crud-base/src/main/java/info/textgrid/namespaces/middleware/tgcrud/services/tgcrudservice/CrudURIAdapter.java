/*******************************************************************************
 * This software is copyright (c) 2015 by
 * 
 *  - TextGrid Consortium (http://www.textgrid.de)
 *  - DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the
 * terms described in the GNU Lesser General Public License v3 of which you
 * should have received a copy. Otherwise you can download it from
 *
 *   http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license http://www.gnu.org/licenses/lgpl-3.0.txt GNU Lesser General Public
 * License v3
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 ******************************************************************************/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;

/*******************************************************************************
 * TODOLOG
 * 
 *******************************************************************************
 * CHANGELOG
 *
 *	2015-08-14	Funk	Renamed for DH-crud use.
 *	2014-10-23	Funk	Again corrected URL to jaxb.java.net guide :-)
 *	2013-03-22	Funk	Corrected URL to jaxb.java.net guide.
 *	2010-09-16	Funk	First version.
 *
 ******************************************************************************/

/*******************************************************************************
 * <p>
 * TGCrudURIAdaptor adapts the URIs from the WSDL file (xsd:anyURI) to
 * java.net.URI, instead of java.lang.String, what is not nice to have for URI
 * types. Is used by the JAXB marshaller and unmarshaller to map to/from
 * URI/String.
 * </p>
 * 
 * <p>
 * Please see <a
 * href="https://jaxb.java.net/guide/Using_different_datatypes.html"
 * >https://jaxb.java.net/guide/Using_different_datatypes.html</a>.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2014-10-23
 * @since 2010-09-16
 *******************************************************************************/

public class CrudURIAdapter {

	/**
	 * <p>
	 * Creates an URI from a given string.
	 * </p>
	 * 
	 * @param theString
	 * @return The URI from the given string
	 */
	public static URI parseURI(String theString) {
		return URI.create(theString);
	}

	/**
	 * <p>
	 * Creates a string from a given URI.
	 * </p>
	 * 
	 * @param theUri
	 * @return The string for the given URI
	 */
	public static String printURI(URI theUri) {
		return (theUri == null ? "" : theUri.toASCIIString());
	}

}
