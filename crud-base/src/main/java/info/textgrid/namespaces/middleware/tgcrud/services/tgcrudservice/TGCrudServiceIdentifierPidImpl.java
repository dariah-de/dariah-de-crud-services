/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import org.apache.cxf.helpers.IOUtils;
import org.codehaus.jettison.json.JSONException;
import info.textgrid.middleware.common.LTPUtils;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2024-06-28 - Funk - First version.
 */

/**
 * <p>
 * This Handle identifier implementation for tgcrud. Only gets handle metadata from the GWDG Handle
 * Service (EPIC2).
 * </p>
 */

public class TGCrudServiceIdentifierPidImpl extends CrudServiceIdentifierAbs {

  // **
  // FINALS
  // **

  private static final String UNIT_NAME = "handle";

  // **
  // CLASS VARIABLES
  // **

  protected static Client hdlClient = null;

  // **
  // IMPLEMENTED METHODS
  // **

  /**
   *
   */
  @Override
  public List<URI> getUris(int theAmount) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void updatePidMetadata(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public boolean uriExists(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public boolean lock(URI theUri, String theUser) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String isLockedBy(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public boolean lockInternal(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public boolean unlock(URI theUri, String theUser) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public boolean unlockInternal(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   * <p>
   * Get some handle metadata, such as filesize and mimetype. Use null or empty for complete
   * metadata set.
   * </p>
   */
  @Override
  public String getHandleMetadata(URI theUri, String theType) throws IoFault, MetadataParseFault {

    String result;

    String meth = UNIT_NAME + ".getHandleMetadata()";

    // Initialise the HDL service client.
    init();

    String resolverURL = this.conf.getHANDLErESOLVERuRL().toString();
    String handle = fixHandlePrefix(theUri);

    // Create and call HDL service.
    // https://hdl.handle.net/api/handles/21.T11991/0000-001B-41BD-5?type=[type]&auth
    // NOTE We use authoritative answers here, to always get the latest values, and not proxied
    // ones! Hopefully we do not create too much traffic with this queries!
    WebTarget w = hdlClient
        .target(resolverURL)
        .path("api/handles/" + handle)
        .queryParam("auth");

    // Set type if not null or empty.
    if (theType != null && !theType.equals("")) {
      w.queryParam("type", theType);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Fetching authoritative HDL metadata of " + handle + " for type="
            + ((theType == null || theType.equals("")) ? "*" : theType) + " metadata from "
            + w.getUri());

    Response r = w
        .request()
        .get();

    try {
      result = IOUtils.readStringFromStream(r.readEntity(InputStream.class));

      // Look for type metadata, if not null or empty, throw error if not existing.
      if (theType != null && !theType.equals("")) {
        result = CrudServiceUtilities.getHandleMetadata(result, theType);
      }

    } catch (IOException | JSONException e) {
      String message =
          "Error checking HDL metadata type=" + (theType == null ? "NULL" : theType) + " for "
              + theUri.toString() + " due to a " + e.getClass().getName() + ": " + e.getMessage();
      CrudServiceUtilities.serviceLog(CrudService.ERROR, meth, message);
      throw new IoFault(message);
    }

    return result;
  }

  // **
  // INTERNAL METHODS
  // **

  /**
   * 
   */
  private static void init() {

    String meth = UNIT_NAME + ".init()";

    if (hdlClient == null) {
      hdlClient = ClientBuilder.newClient().property("thread.safe.client", "true");

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Creating new HDL client");

    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Using existing HDL client");
    }
  }

  /**
   * @param theUri Takes a Handle URI.
   * @return The corrected Handle (without Handle prefix!).
   */
  public static String fixHandlePrefix(URI theUri) {

    String result = LTPUtils.omitHdlPrefix(theUri.toASCIIString());

    String meth = "fixHandlePrefix()";

    // Use NEW Handle Prefix only, change old prefixes from "11858" and "11378" to "21.11113" to
    // have REAL Handles to work with later on!
    if (result.startsWith("11858") || result.startsWith("11378")) {
      result = result.replaceFirst("11858|11378", "21.11113");

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Replacing deprecated Handle alias: " + result);
    }

    return result;
  }

}
