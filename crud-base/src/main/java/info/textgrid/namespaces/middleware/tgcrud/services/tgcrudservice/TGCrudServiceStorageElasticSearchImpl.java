/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Fatih Berber
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpHost;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.common.elasticsearch.ESJsonBuilder;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.xml.bind.JAXB;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2024-11-01 - Funk - Implement method retrieveMetadata().
 * 
 * 2023-09-06 - Funk - Remove es types.
 * 
 * 2020-06-24 - Funk - Fix
 * <https://gitlab.gwdg.de/dariah-de/textgridlab/textgrid-laboratory/-/issues/2154>
 * 
 * 2020-06-15 - Funk - Handle MARKDOWN as PLAINTEXT to put in ES.
 * 
 * 2019-11-07 - Funk - Add HTTP ports to logging, reinserted index name and type extraction.
 * 
 * 2019-10-29 - Funk - Removed deprecated clustername and correct elasticsearch port.
 * 
 * 2016-01-29 - Funk - Commented out projectfile handling, esutils is not supporting that anymore!
 * 
 * 2015-08-13 - Funk - Added ADM and TECH metadata methods.
 * 
 * 2014-05-23 - Funk - Adapted to new ESUtils version 0.6.0.
 * 
 * 2014-05-08 - Funk - Added JSON debugging logging. Removed ElasticSearchException due to version
 * change to 1.0.1.
 * 
 * 2013-12-11 - Funk - Removed some Exceptions due to Sonar hints. Mostly RuntimeExceptions...
 * 
 * 2013-10-23 - Funk - Added public method for adding key/value pairs (mainly for setting the
 * nearlyPublish flag in ElasticSearch).
 * 
 * 2013-10-08 - Funk - Adapted to new ESUtils class, some minor class refactored.
 * 
 * 2013-09-24 - Funk - Added more logging and error handling.
 * 
 * 2013-09-23 - Funk - Preparing for the new ElasticSearch code.
 * 
 * 2013-06-19 - Funk - Added some test code for testing the ElasticSearch utilities.
 * 
 * 2013-05-22 - Funk - Copied from TGCrudServiceStorageExistImpl.
 */

/**
 * <p>
 * This storage implementation grants CRUD operations to the TextGrid ElasticSearch database.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @author Fatih Berber, GWDG
 * @version 2023-09-06
 * @since 2010-09-09
 */

public class TGCrudServiceStorageElasticSearchImpl extends CrudStorageAbs<MetadataContainerType> {

  // **
  // STATIC FINALS
  // **

  private static final String UNIT_NAME = "esdb";

  private static final String ERROR_STORING_DATA = "Failure storing data to ElasticSearch database";
  protected static final String ERROR_STORING_ORIGINAL_DATA =
      "Failure storing original data to ElasticSearch database";
  protected static final String ERROR_STORING_PROJECTFILE_DATA =
      "Failure storing projectfile to ElasticSearch database";
  protected static final String ERROR_STORING_PORTALCONFIG_DATA =
      "Failure storing portalconfig file to ElasticSearch database";
  private static final String ERROR_STORING_METADATA =
      "Failure storing metadata to ElasticSearch database";
  private static final String ERROR_RETRIEVING_METADATA =
      "Failure retrieving metadata from the ElasticSearch database";
  private static final String ERROR_DELETING = "Failure deleting from ElasticSearch database";
  private static final String FILE_EMPTY = "File is empty, no data needs to be stored";
  private static final String WRONG_KIND_OF_DATA =
      "Kind of data must be BASELINE, PROJECTFILE, ORIGINAL, or AGGREGATION";
  private static final String NULL_URI = "The URI must not be null";
  private static final String DATA_CREATION_STARTED = "ElasticSearch data creation started";
  private static final String DATA_CREATION_COMPLETE = "ElasticSearch data creation complete";
  private static final String METADATA_CREATION_STARTED = "ElasticSearch metadata creation started";
  private static final String METADATA_CREATION_COMPLETE =
      "ElasticSearch metadata creation complete";
  private static final String METADATA_RETRIEVAL_STARTED = "ElasticSearch data retrieval started";
  private static final String METADATA_RETRIEVAL_COMPLETE = "ElasticSearch data retrieval complete";
  private static final String DELETION_STARTED = "ElasticSearch data and metadata deletion started";
  private static final String DELETION_COMPLETE =
      "ElasticSearch data and metadata deletion complete";
  private static final String BASELINE_DATA_POSTPONED =
      "Baseline data handling not implemented right now";
  private static final String ORIGINAL_DATA_STORED =
      "Stored original data to ElasticSearch database";
  private static final String PROJECTFILE_DATA_STORED =
      "Stored projectfile data to ElasticSearch database";
  private static final String AGGREGATION_DATA_STORED =
      "Stored aggregation data to ElasticSearch database";
  private static final String PORTALCONFIG_DATA_STORED =
      "Stored portalconfig data to ElasticSearch database";
  private static final String DURATION_LITERAL = "Duration";

  // **
  // PRIVATES
  // **

  private RestHighLevelClient client = null;
  private ESJsonBuilder esJson = null;
  private String indexName = null;

  // **
  // IMPLEMENTED METHODS
  // **

  /**
   *
   */
  @Override
  public void create(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".create()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DATA_CREATION_STARTED);

    try {
      init();
      putData(theObject);
    } catch (IOException | SAXException | ParseException | XmlPullParserException
        | ParserConfigurationException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_DATA);
    } catch (TransformerException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_ORIGINAL_DATA);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_STORING_DATA);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, DATA_CREATION_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);
  }

  /**
   *
   */
  @Override
  public void createMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".createMetadata()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, METADATA_CREATION_STARTED);

    try {
      if (theObject.getUri() == null) {
        throw CrudServiceExceptions.ioFault(NULL_URI);
      }
      URI uri = theObject.getUri();

      init();
      checkAndCreateDocument(uri,
          this.esJson.buildTextgridMetadataObject((MetadataContainerType) theObject.getMetadata())
              .toJSONString());

    } catch (IOException | TransformerException | ParseException | XmlPullParserException
        | SAXException | ParserConfigurationException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_METADATA);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_STORING_METADATA);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, METADATA_CREATION_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);
  }

  /**
   *
   */
  @Override
  public void delete(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".delete()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DELETION_STARTED);

    try {
      init();
      // TODO Only delete, if NO baseline data is given (see AdaptorManager!).
      if (theObject.getKindOfData() != CrudObject.BASELINE_DATA) {
        deleteObject(theObject.getUri());
      }
    } catch (TransformerException | SAXException | ParserConfigurationException | IOException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_DELETING);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_DELETING);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, DELETION_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);
  }

  /**
   *
   */
  @Override
  public void deleteMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public int getLatestRevision(URI theUri, boolean unfiltered) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public int getLatestRevisionPublic(URI theUri, boolean unfiltered)
      throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public CrudObject<MetadataContainerType> retrieve(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public CrudObject<MetadataContainerType> retrievePublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveMetadata(URI theUri) throws IoFault {

    // NOTE public or non-public index is taken from crud configuration, means public crud reads
    // from public index, and non-public crud reads from non-public index! So retrievePublic() is
    // sort of unneeded (At lease at the moment)!

    MetadataContainerType result;

    String meth = UNIT_NAME + ".retrieveMetadata()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, METADATA_RETRIEVAL_STARTED);

    try {
      init();
      result = retrieveObject(theUri);
    } catch (IOException | SAXException | ParserConfigurationException | TransformerException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_RETRIEVING_METADATA);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_RETRIEVING_METADATA);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, METADATA_RETRIEVAL_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);

    return result;
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveMetadataPublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveAdmMD(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveAdmMDPublic(URI thePid) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String retrieveTechMD(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String retrieveTechMDPublic(URI thePid) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void update(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".update()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DATA_CREATION_STARTED);

    try {
      init();
      putData(theObject);
    } catch (IOException | SAXException | ParseException | XmlPullParserException
        | ParserConfigurationException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_DATA);
    } catch (TransformerException e) {
      if (theObject.getKindOfData() == CrudObject.ORIGINAL_DATA) {
        throw CrudServiceExceptions.ioFault(e, ERROR_STORING_ORIGINAL_DATA);
      } else if (theObject.getKindOfData() == CrudObject.PROJECTFILE_DATA) {
        throw CrudServiceExceptions.ioFault(e, ERROR_STORING_PROJECTFILE_DATA);
      } else {
        throw CrudServiceExceptions.ioFault(e, ERROR_STORING_DATA);
      }
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_STORING_DATA);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, DATA_CREATION_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);
  }

  /**
   *
   */
  @Override
  public void updateMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".updateMetadata()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, METADATA_CREATION_STARTED);

    try {
      if (theObject.getUri() == null) {
        throw CrudServiceExceptions.ioFault(NULL_URI);
      }
      URI uri = theObject.getUri();

      init();
      updateObject(uri,
          this.esJson.buildTextgridMetadataObject((MetadataContainerType) theObject.getMetadata())
              .toJSONString());

    } catch (IOException | TransformerException | ParseException | SAXException
        | XmlPullParserException | ParserConfigurationException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_METADATA);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_STORING_METADATA);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, METADATA_CREATION_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);
  }

  /**
   *
   */
  @Override
  public void move(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void copy(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void addKeyValuePair(URI theUri, String theKey, Object theValue) throws IoFault {

    try {
      init();
      checkAndCreateDocument(theUri,
          ESJsonBuilder.buildKeyValueObject(theKey, theValue).toJSONString());
    } catch (TransformerConfigurationException | IoFault | SAXException
        | ParserConfigurationException | IOException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_METADATA);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_STORING_METADATA);
    }
  }

  /**
   *
   */
  @Override
  public URI resolve(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void create(CrudObject<MetadataContainerType> theObject, String theStorageToken)
      throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deletePublic(CrudObject<MetadataContainerType> theObject)
      throws IoFault, RelationsExistFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deleteMetadataPublic(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Stores an input stream into the ElasticSearch database (data only).
   * </p>
   * 
   * @param theObject
   * @throws IoFault
   * @throws IOException
   * @throws TransformerException
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void putData(CrudObject<MetadataContainerType> theObject)
      throws IoFault, IOException, TransformerException, ParseException, XmlPullParserException,
      SAXException, ParserConfigurationException {

    String meth = UNIT_NAME + ".putData()";

    // Return if data stream is empty.
    if (theObject.getData().getInputStream() == null) {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, FILE_EMPTY);
      return;
    }

    // Get URI.
    if (theObject.getUri() == null) {
      throw CrudServiceExceptions.ioFault(NULL_URI);
    }
    URI uri = theObject.getUri();

    // Put URI and the stream into the ElasticSearch database, depending on the incoming document
    // type.
    switch (theObject.getKindOfData()) {

      // TO BE INDEXED...

      // TODO Handle baseline later!
      case CrudObject.BASELINE_DATA:
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, BASELINE_DATA_POSTPONED);
        break;
      // Documents for the fulltext index here!
      case CrudObject.ORIGINAL_DATA:
        putDocumentIndexed(uri, theObject.getData().getInputStream(),
            ((MetadataContainerType) theObject.getMetadata()).getObject().getGeneric().getProvided()
                .getFormat());
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, ORIGINAL_DATA_STORED);
        break;
      // Documents for project files here!
      // TODO Why shall it be indexed in the fulltext index??
      case CrudObject.PROJECTFILE_DATA:
        putDocumentIndexed(uri, theObject.getData().getInputStream(),
            ((MetadataContainerType) theObject.getMetadata()).getObject().getGeneric().getProvided()
                .getFormat());
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, PROJECTFILE_DATA_STORED);
        break;

      // ...AND NOT TO BE INDEXED...

      // Aggregation data shall NOT be put into the fulltext index!
      case CrudObject.AGGREGATION_DATA:
        putDocumentNonIndexed(uri, theObject.getData().getInputStream());
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, AGGREGATION_DATA_STORED);
        break;
      // Portal config file data shall NOT be put into the fulltext index!
      case CrudObject.PORTALCONFIG_DATA:
        putDocumentNonIndexed(uri, theObject.getData().getInputStream());
        CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, PORTALCONFIG_DATA_STORED);
        break;
      // Default --> error :-)
      default:
        throw CrudServiceExceptions.ioFault(WRONG_KIND_OF_DATA);
    }
  }

  // **
  // INTERNAL CLASSES FOR ES COMMUNICATION.
  // **

  /**
   * <p>
   * Adds documents into the fulltext index.
   * </p>
   * 
   * @param theUri
   * @param theData
   * @param theType
   * @throws IOException
   * @throws TransformerException
   * @throws IoFault
   * @throws ParseException
   * @throws XmlPullParserException
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void putDocumentIndexed(URI theUri, InputStream theData, String theType)
      throws IOException, TransformerException, IoFault, ParseException, XmlPullParserException,
      SAXException, ParserConfigurationException {

    // If we have a plain text or markdown object, we just put in the plain text.
    if (theType.equals(TextGridMimetypes.PLAINTEXT) || theType.equals(TextGridMimetypes.MARKDOWN)) {
      checkAndCreateDocument(theUri,
          this.esJson.buildJsonPlaintextField(IOUtils.toString(theData)).toJSONString());
    }

    // Any other XML object.
    else {
      checkAndCreateDocument(theUri,
          this.esJson.buildJsonPlaintextFieldFromXML(theData).toJSONString());
    }
  }

  /**
   * <p>
   * Puts documents into the index, but without fulltext indexing (e.g. for aggregations).
   * </p>
   * 
   * @param theUri
   * @param theData
   * @throws IOException
   * @throws IoFault
   */
  private void putDocumentNonIndexed(URI theUri, InputStream theData) throws IOException, IoFault {
    checkAndCreateDocument(theUri, this.esJson.buildJsonAggregationField(theData).toJSONString());
  }

  /**
   * <p>
   * Updates an object if it is already existing, creates a new one if not.
   * </p>
   * 
   * @param theUri
   * @param theJson
   * @throws IoFault
   * @throws IOException
   */
  private void checkAndCreateDocument(URI theUri, String theJson) throws IoFault, IOException {
    if (idExists(theUri)) {
      updateObject(theUri, theJson);
    } else {
      createObject(theUri, theJson);
    }
  }

  // **
  // DEEP ES METHODS (USING THE ES CLIENT)
  // **

  private MetadataContainerType retrieveObject(URI theUri) throws IoFault, IOException {

    MetadataContainerType result = new MetadataContainerType();

    String meth = UNIT_NAME + ".retrieveObject()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Retrieving ElasticSearch object: " + theUri.toASCIIString());

    // Get the complete metadata JSON from ElasticSearch.
    String uriWithout = TGCrudServiceUtilities.stripUriPrefix(theUri, this.conf);
    GetRequest getRequest = new GetRequest().index(this.indexName).id(uriWithout);
    GetResponse response = this.client.get(getRequest, RequestOptions.DEFAULT);
    String jsonMetadataString = response.getSourceAsString();

    // Get original_metadata from JSON and decode from base64.
    try {
      JSONObject jsonMetadata = new JSONObject(jsonMetadataString);
      byte decodedMetadata[] =
          Base64.getDecoder().decode(jsonMetadata.getString("original_metadata"));

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "ElasticSearch index/id: " + response.getIndex() + "/" + response.getId());

      result =
          JAXB.unmarshal(new ByteArrayInputStream(decodedMetadata), MetadataContainerType.class);

    } catch (JSONException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_METADATA);
    }

    return result;
  }

  /**
   * <p>
   * Creates an object in the ES database.
   * </p>
   * 
   * @param theUri
   * @param theJson
   * @throws IoFault
   * @throws IOException
   */
  private void createObject(URI theUri, String theJson) throws IoFault, IOException {

    String meth = UNIT_NAME + ".createObject()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Creating ElasticSearch object: " + theUri.toASCIIString());

    String uriWithout = TGCrudServiceUtilities.stripUriPrefix(theUri, this.conf);
    IndexRequest indexRequest =
        new IndexRequest().index(this.indexName).id(uriWithout).source(theJson, XContentType.JSON);
    IndexResponse response = this.client.index(indexRequest, RequestOptions.DEFAULT);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "ElasticSearch index/id: "
        + response.getIndex() + "/" + response.getId());
  }

  /**
   * <p>
   * Updates an object in the ES database.
   * </p>
   * 
   * @param theUri
   * @param theJson
   * @throws IoFault
   * @throws IOException
   */
  private void updateObject(URI theUri, String theJson) throws IoFault, IOException {

    String meth = UNIT_NAME + ".updateObject()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Updating ElasticSearch object: " + theUri.toASCIIString());

    String uriWithout = TGCrudServiceUtilities.stripUriPrefix(theUri, this.conf);
    UpdateRequest updateRequest =
        new UpdateRequest().index(this.indexName).id(uriWithout).doc(theJson, XContentType.JSON);
    UpdateResponse response = this.client.update(updateRequest, RequestOptions.DEFAULT);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "ElasticSearch index/id: " + response.getIndex() + "/" + response.getId());
  }

  /**
   * <p>
   * Deletes an object in the ES database.
   * </p>
   * 
   * @param theUri
   * @throws IoFault
   * @throws IOException
   */
  private void deleteObject(URI theUri) throws IoFault, IOException {

    String meth = UNIT_NAME + ".deleteObject()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Deleting ElasticSearch object: " + theUri.toASCIIString());

    String uriWithout = TGCrudServiceUtilities.stripUriPrefix(theUri, this.conf);
    DeleteRequest deleteRequest = new DeleteRequest().index(this.indexName).id(uriWithout);
    DeleteResponse response = this.client.delete(deleteRequest, RequestOptions.DEFAULT);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "ElasticSearch index/id: "
        + response.getIndex() + "/" + response.getId());
  }

  // **
  // UTILITY METHODS
  // **

  /**
   * <p>
   * Checks if ID already exists.
   * </p>
   * 
   * @param theUri
   * @return Is the ID already existing?
   * @throws IoFault
   * @throws IOException
   */
  private boolean idExists(URI theUri) throws IoFault, IOException {

    String uriWithout = TGCrudServiceUtilities.stripUriPrefix(theUri, this.conf);
    GetRequest getRequest = new GetRequest().index(this.indexName).id(uriWithout);
    boolean res = this.client.exists(getRequest, RequestOptions.DEFAULT);

    return res;
  }

  /**
   * <p>
   * Initialise the ES client and the ES JSON objects once.
   * </p>
   * 
   * TODO Put all init() classes in a TextGrid utility module!? (as already done in TG-publish
   * service!)
   * 
   * @throws TransformerConfigurationException
   * @throws IoFault
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void init() throws TransformerConfigurationException, IoFault, SAXException,
      ParserConfigurationException {

    String meth = UNIT_NAME + ".init()";

    // Get host, port, and path config.
    String host = this.conf.getIDXDBsERVICEuRL().getHost();
    this.indexName = getIndexName(this.conf.getIDXDBsERVICEuRL());
    List<String> esPorts = this.conf.getIDXDBsERVICEpORTlIST();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Extracted index name [" + this.indexName + "]");

    // Get client as singleton.
    if (this.client == null) {

      // Extract ports, create host list.
      List<HttpHost> hosts = new ArrayList<HttpHost>();
      for (String p : esPorts) {
        hosts.add(new HttpHost(host, Integer.valueOf(p), "http"));
      }

      // Create ElasticSearch client.
      this.client =
          new RestHighLevelClient(RestClient.builder(hosts.toArray(new HttpHost[hosts.size()])));

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Created new ElasticSearch client [" + this.client.hashCode() + "]: " + host + " "
              + esPorts.toString());
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Using existing ElasticSearch client [" + this.client.hashCode() + "]: " + " "
              + esPorts.toString());
    }

    // Get JSON builder as singleton.
    if (this.esJson == null) {

      this.esJson = new ESJsonBuilder();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Created new ElasticSearch builder [" + this.esJson.hashCode() + "]");

    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Using existing ElasticSearch builder [" + this.esJson.hashCode() + "]");
    }
  }

  /**
   * @param theIndexName
   * @return
   */
  public static String getIndexName(final URL theCompleteServiceURL) {

    String result = theCompleteServiceURL.getPath();

    // Index name must NOT have any leading "/"s!
    if (result.startsWith("/")) {
      result = result.substring(1, result.length());
    }
    // Index name must NOT have any trailing "/"s!
    if (result.endsWith("/")) {
      result = result.substring(0, result.length() - 1);
    }
    // Root path only will be taken as index name!
    if (result.contains("/")) {
      result = result.substring(0, result.indexOf("/"));
    }

    return result;
  }

}
