/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @copyright DAASI International GmbH (http://www.daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.math.BigInteger;
import java.net.URI;
import java.util.Date;
import java.util.List;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Fixity;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import jakarta.activation.DataHandler;

/**
 * TODO
 *
 **
 * CHANGELOG
 *
 * 2017-09-26 - Funk - Added method isCollection.
 * 
 * 2017-09-07 - Funk - Added wrapper checksum methods.
 * 
 * 2015-10-20 - Funk - Added mimetype.
 * 
 * 2015-02-12 - Funk - Generalised with CrudObjectGeneric.
 * 
 * 2013-10-08 - Funk - Added new constructor with data, metadata, and kinfOfData.
 * 
 * 2010-09-21 - Funk - Added kindOfData field.
 * 
 * 2010-09-08 - Funk - Taken from (former) TG-crud version 1.2.
 * 
 * 2010-01-28 - Funk - First version.
 */

/**
 * <p>
 * This TGCrudTextGridObject just binds together a metadata ObjectType and a DataHandler for the
 * storage interface.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2017-09-26
 * @since 2010-01-28
 */

public class TGCrudTextGridObject extends CrudObject<MetadataContainerType> {

  // **
  // CONSTRUCTORS
  // **

  /**
   * @param theUri
   */
  public TGCrudTextGridObject(URI theUri) {
    super(theUri);
  }

  /**
   * @param theUri
   * @param theRelations
   */
  public TGCrudTextGridObject(URI theUri, List<String> theRelations) {
    super(theUri);
    super.setRelations(theRelations);
  }

  /**
   * @param theMetadata
   * @param theData
   * @throws IoFault
   */
  public TGCrudTextGridObject(MetadataContainerType theMetadata,
      DataHandler theData) throws IoFault {
    super(uriFromMetadata(theMetadata));
    super.setMetadata(theMetadata);
    super.setData(theData);
  }

  /**
   * @param theUri
   * @param theData
   * @param theKind
   * @throws IoFault
   */
  public TGCrudTextGridObject(URI theUri, DataHandler theData, int theKind)
      throws IoFault {
    super(theUri);
    super.setData(theData);
    super.setKindOfData(theKind);
  }

  /**
   * @param theMetadata
   * @param theData
   * @param theKind
   * @throws IoFault
   */
  public TGCrudTextGridObject(MetadataContainerType theMetadata,
      DataHandler theData, int theKind) throws IoFault {
    super(uriFromMetadata(theMetadata));
    super.setMetadata(theMetadata);
    super.setData(theData);
    super.setKindOfData(theKind);
  }

  /**
   * @param theMetadata
   * @throws IoFault
   */
  public TGCrudTextGridObject(MetadataContainerType theMetadata)
      throws IoFault {
    super(uriFromMetadata(theMetadata));
    super.setMetadata(theMetadata);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theMetadata
   * @return The URI from the metadata
   */
  private static URI uriFromMetadata(MetadataContainerType theMetadata) {
    return theMetadata.getObject().getGeneric().getGenerated()
        .getTextgridUri().getValue();
  }

  // **
  // IMPLEMENTED ABSTRACTS
  // **

  /**
   *
   */
  @Override
  public long getFilesize() {
    return this.getMetadata().getObject().getGeneric().getGenerated()
        .getExtent().longValue();
  }

  /**
   *
   */
  @Override
  public void setFilesize(long theFilesize) {
    this.getMetadata().getObject().getGeneric().getGenerated()
        .setExtent(BigInteger.valueOf(theFilesize));
  }

  /**
   *
   */
  @Override
  public long getWrapperSize() {
    // TODO
    return 0;
  }

  /**
   *
   */
  @Override
  public void setWrapperSize(long theWrapperSize) {
    // TODO
  }

  /**
   *
   */
  @Override
  public String getChecksum() {
    List<Fixity> f = this.getMetadata().getObject().getGeneric().getGenerated().getFixity();
    if (f == null || f.isEmpty()) {
      return null;
    }
    return f.get(0).getMessageDigest();
  }

  /**
   *
   */
  @Override
  public void setChecksum(String theChecksum) {
    // Get fixity list from metadata, set checksum and add one and only fixity element to metadata.
    Fixity first = getFixity();
    first.setMessageDigest(theChecksum);
    this.getMetadata().getObject().getGeneric().getGenerated().getFixity().add(first);
  }

  /**
   *
   */
  @Override
  public String getChecksumType() {
    List<Fixity> f = this.getMetadata().getObject().getGeneric().getGenerated().getFixity();
    if (f == null || f.isEmpty()) {
      return null;
    }
    return f.get(0).getMessageDigestAlgorithm();
  }

  /**
   *
   */
  @Override
  public void setChecksumType(String theType) {
    // Get fixity list from metadata, set fixity type and add one and only
    // fixity element to metadata.
    Fixity first = getFixity();
    first.setMessageDigestAlgorithm(theType);
    this.getMetadata().getObject().getGeneric().getGenerated().getFixity()
        .add(first);
  }

  /**
   *
   */
  @Override
  public String getChecksumOrigin() {
    return this.getMetadata().getObject().getGeneric().getGenerated().getFixity().get(0)
        .getMessageDigestOriginator();
  }

  /**
   *
   */
  @Override
  public void setChecksumOrigin(String theOrigin) {
    // Get fixity list from metadata, set fixity originator and add one and only fixity element to
    // metadata.
    Fixity first = getFixity();
    first.setMessageDigestOriginator(theOrigin);
    this.getMetadata().getObject().getGeneric().getGenerated().getFixity().add(first);
  }

  /**
   *
   */
  @Override
  public String getWrapperChecksum() {
    // TODO
    return null;
  }

  /**
   *
   */
  @Override
  public void setWrapperChecksum(String theChecksum) {
    // TODO
  }

  /**
   *
   */
  @Override
  public String getWrapperChecksumType() {
    // TODO
    return null;
  }

  /**
   *
   */
  @Override
  public void setWrapperChecksumType(String theChecksumType) {
    // TODO
  }

  /**
   *
   */
  @Override
  public String getWrapperChecksumOrigin() {
    // TODO
    return null;
  }

  /**
   *
   */
  @Override
  public void setWrapperChecksumOrigin(String theChecksumOrigin) {
    // TODO
  }

  /**
   *
   */
  @Override
  public void setCreationDate(Date theDate) throws IoFault {
    this.getMetadata().getObject().getGeneric().getGenerated()
        .setCreated(CrudServiceUtilities.getXMLGregorianCalendar(theDate.getTime()));
  }

  /**
   *
   */
  @Override
  public Date getCreationDate() {
    return this.getMetadata().getObject().getGeneric().getGenerated().getCreated()
        .toGregorianCalendar().getTime();
  }

  /**
   *
   */
  @Override
  public void setLastModifiedDate(Date theDate) throws IoFault {
    this.getMetadata().getObject().getGeneric().getGenerated()
        .setLastModified(CrudServiceUtilities.getXMLGregorianCalendar(theDate.getTime()));
  }

  /**
   *
   */
  @Override
  public Date getLastModifiedDate() {
    return this.getMetadata().getObject().getGeneric().getGenerated().getLastModified()
        .toGregorianCalendar().getTime();
  }

  /**
   *
   */
  @Override
  public String getMimetype() {
    return this.getMetadata().getObject().getGeneric().getProvided().getFormat();
  }

  /**
   *
   */
  @Override
  public void setMimetype(String theMimetype) {
    this.getMetadata().getObject().getGeneric().getProvided()
        .setFormat(theMimetype);
  }

  /**
   *
   */
  @Override
  public boolean isCollection() {
    return TextGridMimetypes.AGGREGATION_SET.contains(this.getMimetype());
  }

  // **
  // INTERNAL
  // **

  /**
   * <p>
   * NOTE: We do use the first fixity object only, if existing, and overwrite its data! All
   * following objects are deleted, because we want all old checksums to vanish! So we can be sure
   * to always have the correct and one and only checksum!
   * </p>
   * 
   * @return The fixity object stored in the metadata or a new object, if not existing.
   */
  private Fixity getFixity() {

    Fixity result = new Fixity();

    List<Fixity> f = this.getMetadata().getObject().getGeneric().getGenerated().getFixity();

    // Get first fixity, if existing, and clear list.
    if (f != null && !f.isEmpty()) {
      result = f.get(0);
      this.getMetadata().getObject().getGeneric().getGenerated().getFixity().clear();
    }

    return result;
  }

}
