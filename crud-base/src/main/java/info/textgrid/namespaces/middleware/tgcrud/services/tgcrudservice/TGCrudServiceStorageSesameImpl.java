/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpStatus;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.utils.sesameclient.SesameClient;
import info.textgrid.utils.sesameclient.TGSparqlUtils;

/**
 * CHANGELOG
 * 
 * 2022-10-13 - Funk - Fix #27
 * <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/merge_requests/27>
 * 
 * 2018-11-27 - Funk - Just reformatted and added TODO concerning resolving PIDs.
 * 
 * 2017-04-07 - Funk - Fixing bug #21556. Using public and non-public delete and deleteMetadata
 * methods now!
 * 
 * 2016-04-01 - Funk - Improved logging in getLatestRevision methods due to bug #17289.
 * 
 * 2015-08-13 - Funk - Added ADM and TECH metadata methods.
 * 
 * 2014-07-16 - Funk - Added query() and resolve() methods.
 * 
 * 2012-08-01 - Funk - Added FileNotFoundException from TGHttpClient in getLatestRevision methods.
 * 
 * 2012-02-15 - Funk - Using only one SesameClient now.
 * 
 * 2012-01-26 - Funk - Adapted HTTP clients.
 * 
 * 2011-12-19 - Funk - Added new interface method getLatestRevisionPublic().
 * 
 * 2011-07-27 - Funk - Uploading the N3 triple in delete() now vie uploadN3() instead of create().
 * 
 * 2011-03-28 - Funk - Using Ubbo's TGHttpClient now.
 * 
 * 2010-09-09 - Funk - First version.
 * 
 */

/**
 * <p>
 * This storage implementation grants CRUD operations to the TextGrid Sesame RDF database.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-10-13
 * @since 2010-09-09
 */

public class TGCrudServiceStorageSesameImpl extends CrudStorageAbs<MetadataContainerType> {

  // **
  // STATIC FINALS
  // **

  private static final String UNIT_NAME = "rdfdb";
  private static final String HANDLE_PREFIX = "hdl:";
  private static final String SUBJECT_PARAM = "?S";
  private static final String OH_OH_OH =
      "URGL!! ARGL!! AUA!! YOU DO NOT WANT TO DELETE ALL THE RELATION DATA!!";
  private static final boolean NO_N3_VALIDATION = false;
  private static final boolean N3_VALIDATION = true;

  // **
  // STATICS
  // **

  private static SesameClient sesameClient;
  private static SesameClient sesameClientPublic;

  // **
  // IMPLEMENTED METHODS
  // **

  /**
   *
   */
  @Override
  public void create(CrudObject<MetadataContainerType> theObject)
      throws IoFault {

    String meth = UNIT_NAME + ".create()";

    try {
      // Store RDF data stream to the Sesame RDF database, if existing.
      if (theObject.getKindOfData() == CrudObject.RDF_DATA) {
        uploadRdf(theObject.getData().getInputStream());
      } else {
        // Store relation data to the RDF database.
        uploadN3(theObject.getRelations(), N3_VALIDATION);
      }
    } catch (IOException | ParseException e) {
      throw CrudServiceExceptions.ioFault(e, "Failure storing to the RDF database");
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "Sesame RDF data creation complete");
  }

  /**
   *
   */
  @Override
  public void createMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deleteMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {

    /**
     * <p>
     * Deletes everything from the Sesame RDF database for one TextGrid URI subject! Keeps
     * aggregation data if needed (just set kind of data to AGGREGATION_DATA)!
     * </p>
     */

    String meth = UNIT_NAME + ".deleteMetadata()";

    // Get a TextGrid Sesame HTTP client.
    SesameClient client = getSesameClient();

    // Store aggregations, if kind of data is aggregation.
    String aggregationTriples = "";
    if (theObject.getKindOfData() == CrudObject.AGGREGATION_DATA) {
      String query = "<" + theObject.getUri().toASCIIString() + "> <"
          + TGCrudServiceUtilities.RDF_AGGREGATES + "> ?O";

      try {
        aggregationTriples = IOUtils.readStringFromStream(querySesame(query, query));
      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, "Unable to get aggregates relations");
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Storing aggregates relations: " + aggregationTriples);
    }

    // Delete everything belonging to that URI.
    String subject = theObject.getUri().toASCIIString();
    String predicate = "";
    String object = "";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Deleting relations: s=" + subject + ", p=" + predicate + ", o=" + object);

    // Execute HTTP POST and evaluate status code.
    if (subject == null || subject.equals("")) {
      throw CrudServiceExceptions.ioFault(OH_OH_OH);
    }

    int statusCode = 0;
    try {
      // Delete revision URI.
      statusCode = client.delete(subject, predicate, object);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Malformed URI for HTTP client detected");
    }

    if (statusCode != HttpStatus.SC_NO_CONTENT) {
      throw CrudServiceExceptions.ioFault("HTTP request failed with status code: " + statusCode);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "HTTP status code: " + statusCode);

    // Put in aggregates relations again, if kind of data is aggregation.
    if (theObject.getKindOfData() == CrudObject.AGGREGATION_DATA) {
      try {
        uploadN3(aggregationTriples);
      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, "Unable to upload aggregations again");
      }
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
        "RDF database metadata deletion complete");
  }

  /**
   *
   */
  @Override
  public void delete(CrudObject<MetadataContainerType> theObject) throws IoFault {

    /**
     * <p>
     * Deletes from the Sesame RDF database. In our case we just add a "deleted" relation, and we
     * keep isDerived, inProject, and aggregation data.
     * </p>
     */

    String meth = UNIT_NAME + ".delete()";

    // Store aggregations to be kept after deletion.
    String inProjectQuery = "<" + theObject.getUri().toASCIIString() + "> <"
        + this.conf.getRDFDBnAMESPACE() + TGCrudServiceUtilities.REL_INPROJECT + "> ?O";
    String isDerivedQuery = "<" + theObject.getUri().toASCIIString() + "> <"
        + this.conf.getRDFDBnAMESPACE() + TGCrudServiceUtilities.REL_ISDERIVEDFROM + "> ?O";

    try {
      theObject.getRelations()
          .add(IOUtils.readStringFromStream(querySesame(inProjectQuery, inProjectQuery)));
      theObject.getRelations()
          .add(IOUtils.readStringFromStream(querySesame(isDerivedQuery, isDerivedQuery)));
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Unable to get inProject or isDerived relations");
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Storing aggregations to keep: " + theObject.getRelations().toString());

    // Delete everything belonging to that URI.
    String s = theObject.getUri().toASCIIString();
    String p = "";
    String o = "";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Deleting relations: s=" + s + ", p=" + p + ", o=" + o);

    // Execute HTTP POST and evaluate status code.
    if (s == null || s.equals("")) {
      throw CrudServiceExceptions.ioFault(OH_OH_OH);
    }

    int statusCode = 0;
    try {
      statusCode = sesameClient.delete(s, p, o);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Malformed URI for HTTP client detected");
    }

    if (statusCode != HttpStatus.SC_NO_CONTENT) {
      throw CrudServiceExceptions.ioFault("HTTP request failed with status code: " + statusCode);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "HTTP status code: " + statusCode);

    // Put in all relations to be kept.
    try {
      // No validation here!
      uploadN3(theObject.getRelations(), NO_N3_VALIDATION);
    } catch (IOException | ParseException e) {
      throw CrudServiceExceptions.ioFault(e, "Failure adding deleted relation to the RDF database");
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "RDF deletion complete");
  }

  /**
   *
   */
  @Override
  public int getLatestRevision(final URI theUri, final boolean unfiltered)
      throws ObjectNotFoundFault, IoFault {

    String meth = UNIT_NAME + ".getLatestRevision()";
    URI u = TGCrudServiceUtilities.stripRevision(theUri);

    try {
      // Get the TextGrid Sesame database HTTP client.
      SesameClient client = getSesameClient();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "RDF database server URL: " + this.conf.getRDFDBsERVICEuRL());

      int lr = 0;
      if (unfiltered) {
        lr = TGSparqlUtils.getLatestUnfilteredRevision(client, u.toASCIIString());
      } else {
        lr = TGSparqlUtils.getLatestRevision(client, u.toASCIIString());
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Latest revison from non-public RDF DB: " + lr + (unfiltered ? " (unfiltered)" : ""));

      return lr;

    } catch (IOException e) {
      throw CrudServiceExceptions.objectNotFoundFault(e,
          "URI could not be found in non-public RDF DB: " + u);
    }
  }

  /**
   *
   */
  @Override
  public int getLatestRevisionPublic(final URI theUri, final boolean unfiltered)
      throws IoFault, ObjectNotFoundFault {

    String meth = UNIT_NAME + ".getLatestRevisionPublic()";
    URI u = TGCrudServiceUtilities.stripRevision(theUri);

    try {
      // Get the TextGrid Sesame database HTTP client.
      SesameClient client = getSesameClientPublic();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "RDF database server URL: " + this.conf.getRDFDBsERVICEuRLpUBLIC());

      int lr = 0;
      if (unfiltered) {
        lr = TGSparqlUtils.getLatestUnfilteredRevision(client, u.toASCIIString());
      } else {
        lr = TGSparqlUtils.getLatestRevision(client, u.toASCIIString());
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Latest revison from public RDF DB: " + lr + (unfiltered ? " (unfiltered)" : ""));

      return lr;

    } catch (IOException e) {
      throw CrudServiceExceptions.objectNotFoundFault(e,
          "URI could not be found in public RDF DB: " + u);
    }
  }

  /**
   *
   */
  @Override
  public CrudObject<MetadataContainerType> retrieve(URI theUri) throws IoFault {

    // Assemble retrieval request.
    List<String> relationQuery = new ArrayList<String>();
    try {
      relationQuery
          .add(IOUtils.readStringFromStream(querySesame("<" + theUri.toASCIIString() + "> ?P ?O",
              "<" + theUri.toASCIIString() + "> ?P ?O")));
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Failure retrieving from the RDF database");
    }

    return new TGCrudTextGridObject(theUri, relationQuery);
  }

  /**
   *
   */
  @Override
  public CrudObject<MetadataContainerType> retrievePublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveMetadata(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveMetadataPublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveAdmMD(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public MetadataContainerType retrieveAdmMDPublic(URI thePid) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String retrieveTechMD(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String retrieveTechMDPublic(URI thePid) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void update(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void updateMetadata(CrudObject<MetadataContainerType> theObject) throws IoFault {

    String meth = UNIT_NAME + ".updateMetadata()";

    // Only store relation data to the RDF database.
    try {
      uploadN3(theObject.getRelations(), N3_VALIDATION);
    } catch (IOException | ParseException e) {
      throw CrudServiceExceptions.ioFault(e, "Failure storing to the RDF database");
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "RDF metadata update complete");
  }

  /**
   *
   */
  @Override
  public void move(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void copy(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void addKeyValuePair(URI theUri, String theKey, Object theValue)
      throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public URI resolve(URI theUri) throws IoFault, ObjectNotFoundFault {

    URI result = null;

    String meth = UNIT_NAME + ".resolve()";

    // TODO We can not get TextGrid URIs from Sesame for PIDs with the old PID prefixes (11858 and
    // 11378) using the new PID prefix (21.11113). So crud can NOT get the TextGrid URI for those
    // PIDs and can NOT deliver the object.

    if (theUri.toString().startsWith(HANDLE_PREFIX)) {
      String query = "?S " + TGCrudServiceUtilities.REL_OPEN
          + this.conf.getRDFDBnAMESPACE()
          + TGCrudServiceUtilities.REL_HASPID
          + TGCrudServiceUtilities.REL_CLOSEOPEN
          + theUri.toASCIIString()
          + TGCrudServiceUtilities.REL_CLOSE;

      // Get list from Sesame database.
      List<String> resultList =
          TGSparqlUtils.urisFromSparqlResponse(querySesamePublic(query, SUBJECT_PARAM));
      if (resultList.isEmpty()) {
        throw CrudServiceExceptions.objectNotFoundFault("PID " + theUri + " is not existing!");
      }
      result = URI.create(resultList.get(0));

      CrudServiceUtilities.serviceLog(CrudService.INFO, meth,
          "PID resolved to TextGrid URI: " + result);
    }

    return result;
  }

  /**
   *
   */
  @Override
  public void create(CrudObject<MetadataContainerType> theObject, String theStorageToken)
      throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deletePublic(CrudObject<MetadataContainerType> theObject)
      throws IoFault, RelationsExistFault {

    /**
     * <p>
     * Deletes everything from the Sesame RDF database for one TextGrid URI subject! Keeps
     * aggregation data if needed (just set kind of data to AGGREGATION_DATA)!
     * 
     * ALSO DELETES BASE URI RELATIONS! USE ONLY FOR PUBLIC DELETES!
     * </p>
     */

    String meth = UNIT_NAME + ".deleteMetadata()";

    // Get a TextGrid Sesame HTTP client.
    SesameClient client = getSesameClient();

    // Store aggregations, if kind of data is aggregation.
    String aggregationTriples = "";
    if (theObject.getKindOfData() == CrudObject.AGGREGATION_DATA) {
      String query = "<" + theObject.getUri().toASCIIString() + "> <"
          + TGCrudServiceUtilities.RDF_AGGREGATES + "> ?O";

      try {
        aggregationTriples = IOUtils.readStringFromStream(querySesame(query, query));
      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, "Unable to get aggregates relations");
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Storing aggregates relations: " + aggregationTriples);
    }

    // Delete everything belonging to that URI.
    String subject = theObject.getUri().toASCIIString();
    String subjectBaseUri =
        TGCrudServiceUtilities.stripRevision(theObject.getUri()).toASCIIString();
    String predicate = "";
    String object = "";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Deleting relations: s=" + subject + ", p=" + predicate + ", o=" + object);

    // Execute HTTP POST and evaluate status code.
    if (subject == null || subject.equals("")) {
      throw CrudServiceExceptions.ioFault(OH_OH_OH);
    }

    int statusCode = 0;
    try {
      // Delete revision URI.
      statusCode = client.delete(subject, predicate, object);
      statusCode = client.delete(subjectBaseUri, predicate, object);
    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Malformed URI for HTTP client detected");
    }

    if (statusCode != HttpStatus.SC_NO_CONTENT) {
      throw CrudServiceExceptions.ioFault("HTTP request failed with status code: " + statusCode);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "HTTP status code: " + statusCode);

    // Put in aggregates relations again, if kind of data is aggregation.
    if (theObject.getKindOfData() == CrudObject.AGGREGATION_DATA) {
      try {
        uploadN3(aggregationTriples);
      } catch (IOException e) {
        throw CrudServiceExceptions.ioFault(e, "Unable to upload aggregations again");
      }
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, "RDF metadata deletion complete");
  }

  /**
   *
   */
  @Override
  public void deleteMetadataPublic(CrudObject<MetadataContainerType> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Initialise the one and only RDF database HTTP client.
   * </p>
   * 
   * @return The Sesame HTTP client.
   * @throws IoFault
   */
  private SesameClient getSesameClient() throws IoFault {

    String meth = UNIT_NAME + ".getHttpClient()";

    if (sesameClient == null) {
      try {
        sesameClient = new SesameClient(
            this.conf.getRDFDBsERVICEuRL().toString(),
            this.conf.getRDFDBuSER(),
            this.conf.getRDFDBpASS());
      } catch (MalformedURLException e) {
        throw CrudServiceExceptions.ioFault(e, "Malformed URI for TGHTTP client detected");
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "New TGHTTP client created");
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Using existing TGHTTP client");
    }

    return sesameClient;
  }

  /**
   * <p>
   * Initialise the one and only RDF database HTTP public client.
   * </p>
   * 
   * @return The Sesame HTTP client.
   * @throws IoFault
   */
  private SesameClient getSesameClientPublic() throws IoFault {

    String meth = UNIT_NAME + ".getSesameClientPublic()";

    if (sesameClientPublic == null) {
      try {
        sesameClientPublic = new SesameClient(
            this.conf.getRDFDBsERVICEuRLpUBLIC().toString(),
            this.conf.getRDFDBuSERpUBLIC(),
            this.conf.getRDFDBpASSpUBLIC());
      } catch (MalformedURLException e) {
        throw CrudServiceExceptions.ioFault(e, "Malformed URI for TGHTTP client detected");
      }

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "New TGHTTP client created");
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Using existing TGHTTP client");
    }

    return sesameClientPublic;
  }

  /**
   * <p>
   * Uploads a string of triples.
   * </p>
   * 
   * @param theTriples
   * @throws IoFault
   * @throws IOException
   */
  private void uploadN3(String theTriples) throws IoFault, IOException {

    String meth = UNIT_NAME + ".uploadN3()";

    // Get a TextGrid Sesame HTTP client.
    SesameClient client = getSesameClient();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "All triples: " + theTriples);

    // Execute HTTP POST and evaluate status code.
    int statusCode = client
        .uploadN3(new ByteArrayInputStream(theTriples.getBytes(this.conf.getDEFAULTeNCODING())));

    // NOTE NO_CONTENT for Sesame, OK for BlazeGraph!
    if (statusCode != HttpStatus.SC_NO_CONTENT && statusCode != HttpStatus.SC_OK) {
      throw new IOException("HTTP request failed with status code: " + statusCode);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "HTTP status code: " + statusCode);
  }

  /**
   * <p>
   * Uploads a list of triples, triples with empty S, P, or O are taken out!
   * </p>
   * 
   * @param theTriples
   * @param validate
   * @throws IoFault
   * @throws IOException
   * @throws ParseException
   */
  private void uploadN3(List<String> theTriples, final boolean validate)
      throws IoFault, IOException, ParseException {

    String meth = UNIT_NAME + ".uploadN3()";

    // Assemble all triples from the triple list.
    StringBuffer allTriples = new StringBuffer();
    for (String triple : theTriples) {
      // Check triple for empty SPO. Only append valid triples if validation is needed!
      if (validate) {
        if (validateN3(triple)) {
          allTriples.append(triple);
        } else {
          CrudServiceUtilities.serviceLog(CrudService.WARN, meth, "Triple " + triple
              + " contains empty SPO and can not be uploaded to the RDF database!");
        }
      } else {
        allTriples.append(triple);
      }
    }

    // Upload to N3, do not validate (again!).
    uploadN3(allTriples.toString());
  }

  /**
   * <p>
   * Stores an RDF XML file as InputStream to the RDF database.
   * </p>
   * 
   * @param theStream
   * @throws IOException
   * @throws IoFaultException
   */
  private void uploadRdf(InputStream theStream) throws IoFault, IOException {

    // Get a TextGrid Sesame HTTP client.
    SesameClient client = getSesameClient();

    // Upload data.
    client.uploadRDFXML(theStream);
  }

  /**
   * <p>
   * Queries the RDF database using subject, predicate and object strings.
   * </p>
   * 
   * @param theQuery
   * @param theResult
   * @return The input stream of the query.
   * @throws IoFault
   */
  private InputStream querySesame(String theQuery, String theResult) throws IoFault {

    String meth = UNIT_NAME + ".querySesame()";

    try {
      // Get the TextGrid Sesame database HTTP client.
      SesameClient client = getSesameClient();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "RDF database server URL: " + this.conf.getRDFDBsERVICEuRL());

      return queryConstruct(client, theQuery, theResult);

    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Failed to query RDF database");
    }
  }

  /**
   * <p>
   * Queries the RDF public database using subject, predicate and object strings.
   * </p>
   * 
   * @param theQuery
   * @param theResult
   * @return The input stream of the query.
   * @throws IoFault
   */
  private InputStream querySesamePublic(String theQuery, String theResult) throws IoFault {

    String meth = UNIT_NAME + ".querySesamePublic()";

    try {
      // Get the TextGrid Sesame public database HTTP client.
      SesameClient client = getSesameClientPublic();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "RDF dabatase server URL: " + this.conf.getRDFDBsERVICEuRLpUBLIC());

      return querySelect(client, theQuery, theResult);

    } catch (IOException e) {
      throw CrudServiceExceptions.ioFault(e, "Failed to query public RDF database");
    }
  }

  /**
   * <p>
   * Queries public or non-public RDF database.
   * </p>
   * 
   * @param theClient
   * @param theQuery
   * @param theResultParams
   * @return The input stream of the query.
   * @throws IOException
   */
  private static InputStream querySelect(SesameClient theClient, String theQuery,
      String theResultParams) throws IOException {

    String meth = UNIT_NAME + ".querySelect()";

    // Get all relations (only once) were the given parameters do apply.
    String queryString = "SELECT DISTINCT " + theResultParams + " WHERE { " + theQuery + " }";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Query string: " + queryString);

    return theClient.sparql(queryString);
  }

  /**
   * <p>
   * Queries public or non-public RDF database.
   * </p>
   * 
   * @param theClient
   * @param theQuery
   * @param theResultParams
   * @return The input stream of the query.
   * @throws IOException
   */
  private static InputStream queryConstruct(SesameClient theClient, String theQuery,
      String theResultParams) throws IOException {

    String meth = UNIT_NAME + ".queryConstruct()";

    // Get all relations were the given parameters do apply.
    String queryString = "CONSTRUCT { " + theResultParams + " } WHERE { " + theQuery + " }";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Query string: " + queryString);

    return theClient.constructN3(queryString);
  }

  /**
   * <p>
   * Check triples for incorrect or empty S, P, or O.
   * </p>
   * 
   * @param theTriples
   * @return
   * @throws ParseException
   */
  public static boolean validateN3(String theTriples) {

    String meth = UNIT_NAME + ".validateN3()";

    boolean result = true;

    // Check validity by serialising the triple's model.
    try {
      Model m = RDFUtils.readModel(theTriples, RDFConstants.NTRIPLES);
      m.write(new ByteArrayOutputStream());
    } catch (Exception e) {
      result = false;
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "N3 " + theTriples + "is valid? " + result);

    return result;
  }

}
