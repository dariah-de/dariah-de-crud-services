/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.adaptormanager.test;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import org.apache.commons.io.IOUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;
import info.textgrid.namespaces.middleware.adaptormanager.AdaptorManager;

/**
 * TODOLOG
 * 
 * TODO Check the results of all the test methods! At the moment we just test for not throwing
 * exceptions!
 * 
 **
 * CHANGELOG
 * 
 * 2017-09-11 - Funk - Added test for getQnames method.
 * 
 * 2015-02-11 - Funk - Removed SaxParseException.
 * 
 * 2012-02-24 - Funk - Adapted to the streaming issues of TGTransform.
 * 
 * 2010-09-08 - Funk - Updated the aggregation and link editor files.
 * 
 * 2010-09-02 - Funk - Integrated BLValidate into here.
 * 
 * 2010-09-01 - Funk - Added assertTrue() method calls.
 * 
 * 2010-08-31 - Funk - Adapt as JUnit test for tgcrud-adaptormanager.
 * 
 * 2009-07-15 - Funk - Added this header, added JavaDoc.
 */

/**
 * <p>
 * JUnit class for testing the AdaptorManager's static methods.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2023-09-15
 * @since 2009-07-15
 */

public class TestAdaptorManager {

  // **
  // FINAL STATICS
  // **

  private static final String URI_STRING = "textgrid:uri:test";

  // **
  // STATICS
  // **

  private static URI uri;

  /**
   * <p>
   * Preparation of all file URLs for the tests.
   * </p>
   * 
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    uri = new URI(URI_STRING);
  }

  /**
   * <p>
   * Test the getBaseline method.
   * </p>
   * 
   * @throws IOException
   * @throws SAXException
   * @throws TransformerException
   */
  @Test
  @Ignore
  public void testBaselineAdaptionWithValidation()
      throws IOException, TransformerException, SAXException {

    System.out.println("Checking baseline adaptor with werther data/adaptor (validation is ON)");

    // Read the needed files.
    try (InputStream inputxml = AdaptorManager.getResourceAsStream("werther_data.xml");
        InputStream adaptor = AdaptorManager.getResourceAsStream("werther_baseline-adaptor.xsl")) {
      AdaptorManager.getBaseline(inputxml, adaptor, uri, AdaptorManager.DO_VALIDATE);
    }
  }

  /**
   * <p>
   * Test the getBaseline method.
   * </p>
   * 
   * @throws IOException
   * @throws SAXException
   * @throws TransformerException
   */
  @Test
  public void testBaselineAdaptionWithoutValidation()
      throws IOException, TransformerException, SAXException {

    System.out.println("Checking baseline adaptor with werther data/adaptor (validation is OFF)");

    // Read the needed files.
    try (InputStream inputxml = AdaptorManager.getResourceAsStream("werther_data.xml");
        InputStream adaptor = AdaptorManager.getResourceAsStream("werther_baseline-adaptor.xsl")) {
      AdaptorManager.getBaseline(inputxml, adaptor, uri, AdaptorManager.DONT_VALIDATE);
    }
  }

  /**
   * <p>
   * Test the getRelations method.
   * </p>
   * 
   * @throws IOException
   * @throws SAXException
   * @throws TransformerException
   */
  @Test
  public void testGetBaselineRelations() throws IOException, TransformerException, SAXException {

    System.out.println("Relations got out of baseline file:");

    try (InputStream inputxml = AdaptorManager.getResourceAsStream("werther_data.xml");
        InputStream adaptor = AdaptorManager.getResourceAsStream("werther_baseline-adaptor.xsl")) {

      InputStream baseline = null;

      // At first get a baseline stream without validation.
      baseline = AdaptorManager.getBaseline(inputxml, adaptor, uri, AdaptorManager.DONT_VALIDATE);

      // Then get out the relations.
      List<String> out = AdaptorManager.getBaselineRelations(baseline, uri);
      System.out.println(out);
    }
  }

  /**
   * <p>
   * Test the getImageLinkRelations.
   * </p>
   * 
   * @throws IOException
   * @throws TransformerException
   */
  @Test
  public void testGetImageLinkRelations() throws IOException, TransformerException {

    System.out.println("Created image link relations:");

    try (InputStream imagelinkXml =
        AdaptorManager.getResourceAsStream("image-link-editor_linkfile.xml")) {
      List<String> out = AdaptorManager.getImageLinkRelations(imagelinkXml, uri);

      System.out.println(out);
    }
  }

  /**
   * <p>
   * Test the registerXSD method.
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test
  public void testRegisterXsd() throws IOException, XMLStreamException {

    System.out.println("Registered XSDs:");

    try (InputStream source = AdaptorManager.getResourceAsStream("textgrid-metadata_2010.xsd")) {
      String out = AdaptorManager.registerXSD(source, uri);

      System.out.println(out);
    }
  }

  /**
   * <p>
   * Test the registerXSD method.
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test
  public void testGetQNameFromXml() throws IOException, XMLStreamException {

    System.out.println("XML root element:");

    try (InputStream source = AdaptorManager.getResourceAsStream("merkwuerdischeDatei.xml")) {
      QName out = AdaptorManager.getQNameFromXML(source);

      System.out.println(out);
    }
  }

  /**
   * <p>
   * Test the registerXSD method (#16630).
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test
  public void testBug12630No1() throws IOException, XMLStreamException {

    System.out.println("Registered XSDs (bug 16360 No1):");

    try (InputStream source = AdaptorManager.getResourceAsStream("bug16360_schema.xsd")) {
      String out = AdaptorManager.registerXSD(source, uri);

      System.out.println("'" + out + "'");
    }
  }

  /**
   * <p>
   * Test the registerXSD method (#16630).
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test
  public void testBug12630No2() throws IOException, XMLStreamException {

    System.out.println("Registered XSDs (bug 16360 No2):");

    try (InputStream source = AdaptorManager.getResourceAsStream("bug16360_xml.xsd")) {
      String out = AdaptorManager.registerXSD(source, uri);

      System.out.println("'" + out + "'");
    }
  }

  /**
   * <p>
   * Test the registerXSD method with empty XSD file.
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test(expected = XMLStreamException.class)
  public void testRegisterXsdWithEmptyFile()
      throws IOException, XMLStreamException {

    System.out.println("Registered XSDs:");

    try (InputStream source = AdaptorManager.getResourceAsStream("empty_file.xsd")) {
      AdaptorManager.registerXSD(source, uri);
    }
  }

  /**
   * <p>
   * Test the registerXSD method with invalid XSD file.
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   */
  @Test(expected = XMLStreamException.class)
  public void testRegisterXsdWithInvalidFile()
      throws IOException, XMLStreamException {

    System.out.println("Registered XSDs:");

    try (InputStream source = AdaptorManager.getResourceAsStream("invalid_file.xsd")) {
      AdaptorManager.registerXSD(source, uri);
    }
  }

  /**
   * <p>
   * Test the teiHeaderFromTextGridMetadata method.
   * </p>
   * 
   * TODO Must be adapted to the new metadata schema!
   * 
   * @throws IOException
   * @throws TransformerException
   */
  @Ignore
  @Test
  public void testTeiHeaderFromTextGridMetadata() throws IOException, TransformerException {

    System.out.println("Extracted TEI header from TextGrid metadata:");

    try (InputStream inputxml = AdaptorManager.getResourceAsStream("werther_metadata.xml");
        InputStream adaptor = AdaptorManager.getResourceAsStream("rewrite_tei_header.xsl")) {
      InputStream out = AdaptorManager.teiHeaderFromTextGridMetadata(inputxml, adaptor, uri);

      System.out.println(IOUtils.toString(out, AdaptorManager.DEFAULT_ENCODING));
    }
  }

  /**
   * <p>
   * Test the insertAggregationUri method.
   * </p>
   * 
   * @throws IOException
   * @throws TransformerException
   */
  @Test
  public void testInsertAggregationUri() throws IOException, TransformerException {

    System.out.println("Checking TextGrid aggregation file:");

    try (InputStream oreXml = AdaptorManager.getResourceAsStream("textgrid-ore-aggregation.xml")) {
      System.out.println(IOUtils.toString(AdaptorManager.insertAggregationUri(oreXml, uri),
          AdaptorManager.DEFAULT_ENCODING));
    }
  }

  /**
   * <p>
   * Test a 2nd insertAggregationUri method.
   * </p>
   * 
   * @throws IOException
   * @throws TransformerException
   */
  @Test
  public void test2ndInsertAggregationUri() throws IOException, TransformerException {

    System.out.println("Checking TextGrid aggregation file No.2:");

    try (InputStream oreXml =
        AdaptorManager.getResourceAsStream("textgrid-ore-aggregation-no2.xml")) {
      System.out.println(IOUtils.toString(AdaptorManager.insertAggregationUri(oreXml, uri),
          AdaptorManager.DEFAULT_ENCODING));
    }
  }

}
