<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tgmc="http://textgrid.info/namespaces/metadata/core/2008-07-24"
	xmlns:tgmd="http://textgrid.info/namespaces/metadata/dateRange/2008-02-18">

	<xsl:output method="xml" indent="yes" encoding="UTF-8" />

	<xsl:template match="/tgmc:tgObjectMetadata">
		<!--
			<xsl:copy> <xsl:apply-templates select="descriptive/agent" />
			</xsl:copy>
		-->
		<teiHeader>
			<fileDesc>
				<titleStmt>
					<title>
						<xsl:value-of select="tgmc:descriptive/tgmc:title[@type='main']" />
					</title>
					<respStmt>
						<resp>compiled by</resp>
						<name>
							<xsl:value-of select="tgmc:administrative/tgmc:middleware/tgmc:owner" />
						</name>
					</respStmt>
				</titleStmt>
				<sourceDesc>
					<biblStruct>
						<monogr>

							<!-- title / subordinate -->
							<title type="main">
								<xsl:value-of select="tgmc:descriptive/tgmc:title[@type='main']" />
							</title>
							<xsl:for-each
								select="//tgmc:descriptive/tgmc:title[@type='subordinate']">
								<title type="subordinate">
									<xsl:value-of select="." />
								</title>
							</xsl:for-each>

							<!-- agent role = author / editor -->
							<xsl:for-each select="//tgmc:descriptive/tgmc:agent">
								<xsl:if test="@role='author'">
									<author>
										<xsl:value-of select="." />
									</author>
								</xsl:if>
								<xsl:if test="@role='editor'">
									<editor>
										<xsl:value-of select="." />
									</editor>
								</xsl:if>
							</xsl:for-each>

							<!-- agent roles = contributor / illustrator / translator / other -->
							<xsl:if test="//tgmc:descriptive/tgmc:agent[@role='contributor']">
								<respStmt>
									<resp>contributor</resp>
									<xsl:for-each
										select="//tgmc:descriptive/tgmc:agent[@role='contributor']">
										<name>
											<xsl:value-of select="." />
										</name>
									</xsl:for-each>
								</respStmt>
							</xsl:if>
							<xsl:if test="//tgmc:descriptive/tgmc:agent[@role='illustrator']">
								<respStmt>
									<resp>illustrator</resp>
									<xsl:for-each
										select="//tgmc:descriptive/tgmc:agent[@role='illustrator']">
										<name>
											<xsl:value-of select="." />
										</name>
									</xsl:for-each>
								</respStmt>
							</xsl:if>
							<xsl:if test="//tgmc:descriptive/tgmc:agent[@role='translator']">
								<respStmt>
									<resp>translator</resp>
									<xsl:for-each
										select="//tgmc:descriptive/tgmc:agent[@role='translator']">
										<name>
											<xsl:value-of select="." />
										</name>
									</xsl:for-each>
								</respStmt>
							</xsl:if>
							<xsl:if test="//tgmc:descriptive/tgmc:agent[@role='other']">
								<respStmt>
									<resp>other</resp>
									<xsl:for-each select="//tgmc:descriptive/tgmc:agent[@role='other']">
										<name>
											<xsl:value-of select="." />
										</name>
									</xsl:for-each>
								</respStmt>
							</xsl:if>

							<imprint>

								<!-- date -->
								<date>
									<xsl:value-of select="normalize-space(tgmc:descriptive/tgmc:date)" />
								</date>

								<!-- chapter / page / voume / issue / part -->
								<xsl:for-each select="//tgmc:descriptive/tgmc:chapter">
									<biblScope type="chap">
										<xsl:value-of select="." />
									</biblScope>
								</xsl:for-each>
								<xsl:if test="tgmc:descriptive/tgmc:page">
									<biblScope type="vol">
										<xsl:value-of select="tgmc:descriptive/tgmc:page" />
									</biblScope>
								</xsl:if>
								<xsl:if test="tgmc:descriptive/tgmc:volume">
									<biblScope type="vol">
										<xsl:value-of select="tgmc:descriptive/tgmc:volume" />
									</biblScope>
								</xsl:if>
								<xsl:if test="tgmc:descriptive/tgmc:issue">
									<biblScope type="issue">
										<xsl:value-of select="tgmc:descriptive/tgmc:issue" />
									</biblScope>
								</xsl:if>
								<xsl:if test="tgmc:descriptive/tgmc:part">
									<biblScope type="part">
										<xsl:value-of select="tgmc:descriptive/tgmc:part" />
									</biblScope>
								</xsl:if>

								<!-- pubPlace / publisher / idno / availability-->
								<xsl:if test="tgmc:descriptive/tgmc:pubPlace">
									<pubPlace>
										<xsl:value-of select="tgmc:descriptive/tgmc:pubPlace" />
									</pubPlace>
								</xsl:if>
								<xsl:if test="tgmc:descriptive/tgmc:publisher">
									<publisher>
										<xsl:value-of select="tgmc:descriptive/tgmc:publisher" />
									</publisher>
								</xsl:if>
								<!-- agent role = providingInstitution -->
								<xsl:for-each
									select="//tgmc:descriptive/tgmc:agent[@role='providingInstitution']">
									<publisher>
										<xsl:value-of select="." />
									</publisher>
								</xsl:for-each>
								<xsl:for-each select="//tgmc:descriptive/tgmc:idno">
									<idno>
										<xsl:value-of select="." />
									</idno>
								</xsl:for-each>
								<!-- textgrid-uri -->
								<idno>
									<xsl:value-of
										select="normalize-space(tgmc:administrative/tgmc:middleware/tgmc:uri)" />
								</idno>
								<xsl:if test="tgmc:descriptive/tgmc:availability">
									<availability>
										<xsl:value-of select="tgmc:descriptive/tgmc:availability" />
									</availability>
								</xsl:if>


							</imprint>

						</monogr>
					</biblStruct>
				</sourceDesc>


				<!-- extent -->
				<xsl:if test="tgmc:descriptive/tgmc:extent">
					<extent>
						<xsl:value-of select="tgmc:descriptive/tgmc:extent" />
					</extent>
				</xsl:if>

				<!-- note -->
				<xsl:if test="tgmc:descriptive/tgmc:note">
					<noteStmt>
						<note>
							<xsl:value-of select="tgmc:descriptive/tgmc:note" />
						</note>
					</noteStmt>
				</xsl:if>

				<publicationStmt>
					<authority>
						<xsl:value-of select="tgmc:administrative/tgmc:middleware/tgmc:owner" />
					</authority>
					<distributor>TextGrid</distributor>
				</publicationStmt>

			</fileDesc>
			<profileDesc>
				<!-- language -->
				<xsl:if test="//tgmc:descriptive/tgmc:language">
					<langUsage>
						<xsl:for-each select="//tgmc:descriptive/tgmc:language">
							<language>
								<xsl:value-of select="." />
							</language>
						</xsl:for-each>
					</langUsage>
				</xsl:if>
				<!-- keyword / type / -->
				<TextClass>
					<xsl:for-each select="//tgmc:descriptive/tgmc:keyword">
						<keywords>
							<xsl:value-of select="." />
						</keywords>
					</xsl:for-each>
					<xsl:if test="tgmc:descriptive/tgmc:type">
						<catRef target="remains to be specified (type)">
							<xsl:value-of select="tgmc:descriptive/tgmc:type" />
						</catRef>
					</xsl:if>
					<xsl:for-each select="//tgmc:descriptive/tgmc:markupclass">
						<catRef target="remains to be specified (markupclass)">
							<xsl:value-of select="." />
						</catRef>
					</xsl:for-each>

				</TextClass>

			</profileDesc>
		</teiHeader>

	</xsl:template>

</xsl:stylesheet>
