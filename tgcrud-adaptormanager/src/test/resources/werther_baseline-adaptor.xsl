<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0">

	<xsl:preserve-space elements="*" />
	<xsl:output method="xml" indent="no" encoding="UTF-8" />

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>

	<xsl:template match="TEI.2">
		<TEI>
			<xsl:apply-templates />
		</TEI>
	</xsl:template>

	<xsl:template match="div1">
		<div id="{@id}" type="{@type}">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="div2">
		<div id="{@id}" type="{@type}">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="div3">
		<div id="{@id}" type="{@type}">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="div4">
		<div id="{@id}" type="{@type}">
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="milestone" />

	<xsl:template match="pb" />

</xsl:stylesheet>
