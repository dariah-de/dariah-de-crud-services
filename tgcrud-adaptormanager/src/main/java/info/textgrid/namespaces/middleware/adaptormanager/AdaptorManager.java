/**
 * This software is copyright (c) 2023 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Wolfgang Pempe (pempe@saphor.de)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 * @author Ubbo Veentjer (veentjer@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.adaptormanager;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.io.IOUtils;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.LTPUtils;
import jakarta.activation.MimeTypeParseException;

/**
 * TODOLOG
 *
 * TODO Check if we can use getters to get status and exception from the inner threads?
 *
 * TODO Try using Thread Pool for all the Piped things?
 *
 **
 * CHANGELOG
 *
 * 2022-05-25 - Funk - Remove deprecated close methods.
 *
 * 2020-03-09 - Funk - Use new EXIF extraction method.
 *
 * 2018-07-09 - Funk - Using EXIF metadata extraction from crud-common now!
 *
 * 2015-09-22 - Funk Removed default buffer size, Java can do it better!
 *
 * 2015-02-11 - Funk - Gotten rid of all the not needed/doubled dependencies (Axiom, etc). Using
 * Java native stuff now!
 *
 * 2014-12-04 - Funk - Fixed #12348, checking XSD namespace declaration for null or empty string.
 *
 * 2013-10-29 - Funk - Corrected some Sonar issues.
 *
 * 2013-10-10 Veentjer Completed EXIF extraction method.
 *
 * 2013-10-09 - Funk - Added EXIF metadata extraction method for image files.
 *
 * 2013-03-21 - Funk - Added @return documentation.
 *
 * 2012-12-20 - Funk - Added transform method for external usage without setting URIs or something
 * else. Just do transform!
 *
 * 2012-02-24 - Funk - Solved the streaming conversion using piped streams! Yeah!
 *
 * 2011-01-16 - Funk - Moved the OutputStream to InputStream conversion to
 * TGTransform.toInputStream() > TO BE DEALT WITH LATER!
 *
 * 2010-09-08 - Funk - Added " ." to the registerXSD() method.
 *
 * 2010-09-01 - Funk - Added the resource loader.
 *
 * 2010-08-31 - Funk - Adapted to maven.
 *
 * 2009-08-13 - Funk - Changed the access to the XSL files to getter/setter. Didn't work otherwise!
 *
 * 2009-08-05 - Funk - Get the XSL files from within the JAR file now.
 *
 * 2009-07-30 - Funk - Added the getImageLinkRelations method.
 *
 * 2009-07-15 - Funk - Added this header, added JavaDoc.
 *
 * 2009-07-14 - Funk - Changed return parameter of RegisterXSD to String.
 */

/**
 * <p>
 * Class for transforming XML files to baseline encoding and generating Relations for RDF Database.
 * </p>
 *
 * @author Wolfgang Pempe
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Ubbo Veentjer, SUB Göttingen
 * @version 2023-09-15
 * @since 2010-01-13
 */

public final class AdaptorManager {

  // **
  // STATIC FINALS
  // **

  /** Trigger validation */
  public static final boolean DO_VALIDATE = true;
  /** Trigger no validation */
  public static final boolean DONT_VALIDATE = false;
  /** The default encoding */
  public static final String DEFAULT_ENCODING = "UTF-8";
  /** The name of the output-input pipe */
  protected static final String OI_PIPE = "AdaptorManagerOutputInputPipe";
  /** The name of the input-output pipe */
  protected static final String IO_PIPE = "AdaptorManagerInpuaOutputPipe";

  private static final String XSD_NAMESPACE_URI = "http://www.w3.org/2001/XMLSchema";
  private static final String RELATION_NAMESPACE_URI = "http://textgrid.info/relation-ns#";
  private static final String BASELINE_RELATION_ADAPTOR = "baseline-relation-adaptor.xsl";
  private static final String IMAGE_LINK_ADAPTOR = "image-link-editor-adaptor.xsl";
  private static final String INSERT_AGGREGATION_URI = "insert-aggregation-uri-adaptor.xsl";
  private static final String TEXTGRID_BASELINE = "textgrid-baseline_2010.xsd";

  /**
   * <p>
   * Gets the baseline encoding out of an input stream.
   * </p>
   *
   * @param theInputXML Input XML to create baseline format from
   * @param theAdaptor XSLT script for transformation
   * @param theURI TextGrid URI of the object to transform
   * @param validate To validate or not to validate
   * @return Baseline Encoding as InputStream
   * @throws TransformerException
   * @throws IOException
   * @throws SAXException
   */
  public static InputStream getBaseline(InputStream theInputXML, InputStream theAdaptor, URI theURI,
      boolean validate) throws TransformerException, IOException, SAXException {

    InputStream is = transformText(theInputXML, theAdaptor, theURI);

    // Validate if configured.
    if (validate) {
      validateBaseline(is);
    }

    return is;
  }

  /**
   * <p>
   * Gets the relations to other TextGrid objects out of the given XML document.
   * </p>
   *
   * @param theInputXML Input XML to extract baseline relations from other objects
   * @param theURI TextGrid URI of the object to transform
   * @return List of Strings that contains the relations from the baseline encoding
   * @throws TransformerException
   * @throws IOException
   */
  public static List<String> getBaselineRelations(InputStream theInputXML, URI theURI)
      throws TransformerException, IOException {

    InputStream s = getResourceAsStream(BASELINE_RELATION_ADAPTOR);
    InputStream relationsintern = transformRDF(theInputXML, theURI, s);

    return getList(relationsintern);
  }

  /**
   * <p>
   * Gets the image-link-relations to the used image TextGrid objects out of the given XML image
   * file document.
   * </p>
   *
   * @param theInputXML Input XML to extract the image-link-relations
   * @param theURI TextGrid URI of the object to get image link relations from
   * @return List of Strings that contains all the image link relations
   * @throws TransformerException
   * @throws IOException
   */
  public static List<String> getImageLinkRelations(InputStream theInputXML, URI theURI)
      throws TransformerException, IOException {

    InputStream s = getResourceAsStream(IMAGE_LINK_ADAPTOR);
    InputStream relationsintern = transformRDF(theInputXML, theURI, s);

    return getList(relationsintern);
  }

  /**
   * <p>
   * Takes an image input stream and extracts EXIF data.
   * </p>
   *
   * @param theInputImage Input image stream
   * @param theURI TextGrid URI of the object to extract EXIF data from
   * @param theMimetype The mimetype used for extraction
   * @return A list of exif image data containing width and height
   * @throws ImageReadException
   * @throws IOException
   * @throws MimeTypeParseException
   */
  public static List<String> extractExifImageData(InputStream theInputImage, URI theURI,
      String theMimetype) throws ImageReadException, IOException, MimeTypeParseException {
    return LTPUtils.extractExifImageData(theInputImage, theURI, theMimetype);
  }

  /**
   * <p>
   * Extracts the schema of an XML schema document (XSD).
   * </p>
   *
   * @param theInputXSD The Input XSD stream
   * @param theXSDURI The XSD namespace URI
   * @return A string in N3 format, or an empty string if no target namespace is declared.
   * @throws XMLStreamException
   */
  public static String registerXSD(InputStream theInputXSD, URI theXSDURI)
      throws XMLStreamException {

    String result = "";

    // Get target namespace from Document.
    XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    XMLStreamReader reader = inputFactory.createXMLStreamReader(theInputXSD);
    String ns = "";
    while (reader.hasNext()) {
      int eventType = reader.next();
      if (eventType == XMLStreamConstants.START_ELEMENT && reader.getLocalName().equals("schema")) {
        for (int i = 0; i < reader.getAttributeCount(); i++) {
          if (reader.getAttributeLocalName(i).equals("targetNamespace")) {
            ns = reader.getAttributeValue(i);
          }
        }
      }
    }
    reader.close();

    if (ns != null && !ns.equals("")) {
      result = "<" + theXSDURI + "> <" + RELATION_NAMESPACE_URI + "definesSchema> <" + ns + "> .";
    }

    return result;
  }

  /**
   * <p>
   * Extract the namespace and the root element from XML files.
   * </p>
   *
   * @param theInputXML The input XML stream
   * @return The QName of the XMLs root element
   * @throws XMLStreamException
   */
  public static QName getQNameFromXML(InputStream theInputXML) throws XMLStreamException {

    XMLInputFactory factory = XMLInputFactory.newInstance();
    factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
    factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
    XMLStreamReader reader = factory.createXMLStreamReader(theInputXML);
    // Will move to the document element.
    reader.nextTag();
    QName rootElementName = reader.getName();
    reader.close();

    return rootElementName;
  }

  /**
   * <p>
   * Transforms the TextGrid metadata into a header for TEI.
   * </p>
   *
   * @param theInputXML The Input XML stream
   * @param theAdaptor The adaptor stream to adapt with
   * @param theURI The source URI parameter
   * @return Returns the TEI header created out of TextGrid metadata
   * @throws TransformerException
   * @throws IOException
   */
  public static InputStream teiHeaderFromTextGridMetadata(InputStream theInputXML,
      InputStream theAdaptor, URI theURI) throws TransformerException, IOException {
    return transformText(theInputXML, theAdaptor, theURI);
  }

  /**
   * <p>
   * Insert the TG-URI of the collection object into rdf:Description@rdf:about.
   * </p>
   *
   * @param theInputXML The input XML stream
   * @param theURI The source URI parameter
   * @return Returns the AggregationXml containing the TextGrid URI of the collection object
   * @throws TransformerException
   * @throws IOException
   */
  public static InputStream insertAggregationUri(InputStream theInputXML, URI theURI)
      throws TransformerException, IOException {
    return transformText(theInputXML, getResourceAsStream(INSERT_AGGREGATION_URI), theURI);
  }

  /**
   * <p>
   * Loads a resource (even if in JAR files :-).
   * </p>
   *
   * @param theResPart The part of the resource to look for
   * @return Returns the given resource
   * @throws IOException
   */
  public static InputStream getResourceAsStream(String theResPart) throws IOException {
    return AdaptorManager.class.getClassLoader().getResourceAsStream(theResPart);
  }

  /**
   * <p>
   * Just transforms XML with an XSLT adaptor.
   * </p>
   *
   * @param theInputXML The input XML stream
   * @param theAdaptor The adaptor stream to adapt with
   * @return Returns the transformed XML stream
   * @throws TransformerException
   * @throws IOException
   */
  public static InputStream transform(InputStream theInputXML, InputStream theAdaptor)
      throws TransformerException, IOException {
    return transformWithoutUri(theInputXML, theAdaptor);
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Validates the given (baseline) input stream.
   * </p>
   *
   * @param theBaseline The baseline input stream to validate
   * @throws SAXException
   * @throws IOException
   */
  private static void validateBaseline(InputStream theBaseline) throws IOException, SAXException {

    // Lookup a factory for the W3C XML Schema language.
    SchemaFactory factory = SchemaFactory.newInstance(XSD_NAMESPACE_URI);

    // Compile the schema.
    StreamSource sourceSchema = new StreamSource(getResourceAsStream(TEXTGRID_BASELINE));
    Schema schema = factory.newSchema(sourceSchema);

    // Get a validator from the schema.
    Validator validator = schema.newValidator();

    // Parse the document you want to check.
    Source source = new StreamSource(theBaseline);

    // Check the document.
    validator.validate(source);
  }

  /**
   * <p>
   * Gets a list of strings out of an input stream.
   * </p>
   *
   * @param theStream The stream to get strings of
   * @return Returns a list of Strings created out of an input stream
   * @throws IOException
   */
  private static List<String> getList(InputStream theStream) throws IOException {
    return Arrays.asList(IOUtils.toString(theStream, DEFAULT_ENCODING).split("\n"));
  }

  /**
   * <p>
   * A text transformation method.
   * </p>
   *
   * @param theInputXML
   * @param theAdaptor
   * @param theURI
   * @return Returns the transformed XML stream
   * @throws TransformerException
   * @throws IOException
   */
  private static InputStream transformText(final InputStream theInputXML, InputStream theAdaptor,
      URI theURI) throws TransformerException, IOException {

    // Create piped streams.
    PipedInputStream result = new PipedInputStream();
    final PipedOutputStream out = new PipedOutputStream(result);

    // Do prepare transforming.
    TransformerFactory tfactory = TransformerFactory.newInstance();
    final Transformer transformer = tfactory.newTransformer(new StreamSource(theAdaptor));
    transformer.setParameter("source-uri", theURI.toASCIIString());

    // Now do transform, and start a new thread that writes to the Pipe (OutputStream >
    // InputStream)...
    Thread thread = new Thread("AdaptorManagerOutputInputPipe") {
      @Override
      public void run() {
        try {
          transformer.transform(new StreamSource(theInputXML), new StreamResult(out));
        } catch (TransformerException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } finally {
          try {
            out.close();
            theInputXML.close();
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
    };
    thread.start();

    // ...and return the transformed and converted InputStream with this thread.
    return result;
  }

  /**
   * <p>
   * A text transformation method.
   * </p>
   *
   * @param theInputXML The input XML stream
   * @param theAdaptor The adaptor stream to adapt with
   * @return Returns the transformed stream
   * @throws TransformerException
   * @throws IOException
   */
  private static InputStream transformWithoutUri(final InputStream theInputXML,
      InputStream theAdaptor) throws TransformerException, IOException {

    // Create piped streams.
    PipedInputStream result = new PipedInputStream();
    final PipedOutputStream out = new PipedOutputStream(result);

    // Do prepare transforming.
    TransformerFactory tfactory = TransformerFactory.newInstance();
    final Transformer transformer = tfactory.newTransformer(new StreamSource(theAdaptor));

    // Now do transform, and start a new thread that writes to the
    // Pipe (OutputStream > InputStream)...
    Thread thread = new Thread(IO_PIPE) {
      @Override
      public void run() {
        try {
          transformer.transform(new StreamSource(theInputXML), new StreamResult(out));
        } catch (TransformerException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } finally {
          try {
            out.close();
            theInputXML.close();
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
    };
    thread.start();

    // ...and return the transformed and converted InputStream with this thread.
    return result;
  }

  /**
   * <p>
   * An RDF transformation method.
   * </p>
   *
   * @param theSourceDOC The source document stream
   * @param theSourceURI The source URI
   * @param theTransformStream The stream to transform with
   * @return Returns the transformed RDF stream
   * @throws TransformerException
   * @throws IOException
   */
  private static InputStream transformRDF(final InputStream theSourceDOC, final URI theSourceURI,
      InputStream theTransformStream) throws IOException, TransformerException {

    // Create piped streams.
    PipedInputStream result = new PipedInputStream();
    final PipedOutputStream out = new PipedOutputStream(result);

    // Do prepare transforming.
    TransformerFactory tfactory = TransformerFactory.newInstance();
    final Transformer transformer = tfactory.newTransformer(new StreamSource(theTransformStream));
    transformer.setParameter("source-uri", theSourceURI.toASCIIString());

    // Now do transform, and start a new thread that writes to the Pipe (OutputStream >
    // InputStream)...
    Thread thread = new Thread(OI_PIPE) {
      @Override
      public void run() {
        try {
          transformer.transform(new StreamSource(theSourceDOC), new StreamResult(out));
        } catch (TransformerException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } finally {
          try {
            out.close();
            theSourceDOC.close();
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
    };
    thread.start();

    // ...and return the transformed and converted InputStream with this thread.
    return result;
  }

}
