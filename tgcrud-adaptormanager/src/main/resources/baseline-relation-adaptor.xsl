<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0">
	<xsl:preserve-space elements="*" />
	<xsl:param name="source-uri" select="'##please_specify_param_source-uri'" />
	<xsl:output method="text" indent="no" encoding="UTF-8" />
	<xsl:template match="/">
		<xsl:for-each-group
			select="//*[@target[starts-with(self::node(),'textgrid:')]]"
			group-by="if (contains(@target, '#')) then substring-before(@target, '#') else @target">
			<xsl:sort select="current-grouping-key()" />
			<xsl:text>&lt;</xsl:text>
			<xsl:value-of select="$source-uri" />
			<xsl:text>&gt;</xsl:text>
			<xsl:text>&lt;http://textgrid.info/relation-ns#refersTo&gt;&lt;</xsl:text>
			<xsl:value-of select="current-grouping-key()" />
			<xsl:text>&gt; .&#xA;</xsl:text>
		</xsl:for-each-group>
	</xsl:template>
</xsl:stylesheet>
