<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:ore="http://www.openarchives.org/ore/terms/">
	<xsl:param name="source-uri" select="''" />
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="rdf:Description">
		<rdf:Description>
			<xsl:attribute name="rdf:about">
						<xsl:value-of select="$source-uri" />
			        </xsl:attribute>
			<!-- <xsl:choose> <xsl:when test="(@rdf:about) and not(@rdf:about='')"> 
				<xsl:attribute name="rdf:about"> <xsl:value-of select="@rdf:about" /> </xsl:attribute> 
				</xsl:when> <xsl:otherwise> <xsl:attribute name="rdf:about"> <xsl:value-of 
				select="$source-uri" /> </xsl:attribute> </xsl:otherwise> </xsl:choose> -->
			<xsl:apply-templates />
		</rdf:Description>
	</xsl:template>
</xsl:stylesheet>
