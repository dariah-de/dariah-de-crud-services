<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="2.0" xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xlink="http://www.w3.org/1999/xlink"
	xmlns:svg="http://www.w3.org/2000/svg">
	<xsl:preserve-space elements="*" />
	<xsl:param name="source-uri" select="'##please_specify_param_source-uri'" />
	<xsl:output method="text" indent="no" encoding="UTF-8" />
	<xsl:template match="/">
		<xsl:apply-templates select="tei:TEI/tei:facsimile/svg:svg/svg:g" />
	</xsl:template>
	<xsl:template match="tei:facsimile/svg:svg/svg:g">
		<xsl:apply-templates select="svg:image" />
	</xsl:template>
	<xsl:template match="svg:image">
		<xsl:text>&lt;</xsl:text>
		<xsl:value-of select="$source-uri" />
		<xsl:text>&gt;</xsl:text>
		<xsl:text>&lt;http://textgrid.info/relation-ns#refersTo&gt;&lt;</xsl:text>
		<xsl:value-of select="@xlink:href" />
		<xsl:text>&gt; .&#xA;</xsl:text>
	</xsl:template>
</xsl:stylesheet>
