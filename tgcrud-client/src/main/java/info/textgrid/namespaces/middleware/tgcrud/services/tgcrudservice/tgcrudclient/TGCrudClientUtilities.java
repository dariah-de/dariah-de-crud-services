/**
 * This software is copyright (c) 2016 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (http://www.daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService_Service;
import jakarta.xml.ws.BindingProvider;
import jakarta.xml.ws.soap.MTOMFeature;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2016-08-03 - Funk - Added private constructor.
 * 
 * 2013-04-02 - Funk - Using an URL with value null instead of null (due to Java7 usage).
 * 
 * 2013-02-05 - Funk - Added return value documentation.
 * 
 * 2012-09-18 - Funk - Now the service endpoint is really used for instantiating the clients.
 * 
 * 2011-03-21 - Funk - First version.
 */

/**
 * <p>
 * TG-crud client utility class to get appropriate TG-crud clients.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2023-09-15
 * @since 2012-03-21
 */

public final class TGCrudClientUtilities {

  /**
   * PRIVATE CONSRUCTOR FOR STATIC CLASS
   */
  private TGCrudClientUtilities() {
    //
  }

  // **
  // MAIN CLASS
  // **

  /**
   * <p>
   * Return a default stub using MTOM.
   * </p>
   * 
   * @param theEndpoint The TG-crud endpoint to use (NOT the WSDL URL!).
   * @return A valid TG-crud service stub
   * @throws MalformedURLException
   */
  public static TGCrudService getTgcrud(String theEndpoint) throws MalformedURLException {
    return getTgcrud(theEndpoint, true);
  }

  /**
   * <p>
   * Return an MTOM or non-MTOM stub.
   * </p>
   * 
   * @param theEndpoint The TG-crud endpoint to use (NOT the WSDL URL!).
   * @param theMtom
   * @return A valid TG-crud service stub
   * @throws MalformedURLException
   */
  public static TGCrudService getTgcrud(String theEndpoint, boolean theMtom)
      throws MalformedURLException {

    // Get TG-crud service from given endpoint.
    TGCrudService_Service service = new TGCrudService_Service((URL) null);
    TGCrudService tgcrud = service.getPort(TGCrudService.class, new MTOMFeature(theMtom));
    BindingProvider bindingProvider = (BindingProvider) tgcrud;
    Map<String, Object> requestContext = bindingProvider.getRequestContext();
    requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, theEndpoint);

    return tgcrud;
  }

}
