/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.consistency.api;

/**
 * <p>
 * Filesize pojo for consistency JSON responses.
 * </p>
 */
public class Filesize {

  public long metadata;
  public long storage;
  public long handle;

  /**
   * @param metadata
   * @param storage
   * @param handle
   */
  public Filesize(long metadata, long storage, long handle) {
    super();
    this.metadata = metadata;
    this.storage = storage;
    this.handle = handle;
  }

}
