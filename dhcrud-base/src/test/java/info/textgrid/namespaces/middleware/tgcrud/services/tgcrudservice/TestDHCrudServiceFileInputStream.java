/**
 * This software is copyright (c) 2020 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import org.apache.cxf.helpers.IOUtils;
import org.junit.Test;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2020-11-30 - Funk - First version.
 *
 *
 */

/**
 * <p>
 * This class does test our fine little DHCrudServiceFileInputStream for deleting temp files on
 * close().
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-12-01
 * @since 20230-12-01
 **/

public class TestDHCrudServiceFileInputStream {

  private static final String TEST_FILE = "<rdf:RDF\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:j.0=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/11022/0000-0000-9AD2-5\">\n"
      + "    <dc:relation>dariah:Collection:11022/0000-0000-9ACF-A</dc:relation>\n"
      + "    <j.0:identifier rdf:resource=\"http://hdl.handle.net/11022/0000-0000-9AD2-5\"/>\n"
      + "    <dc:title>Dragon copy.gif</dc:title>\n" + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";

  /**
   * @throws IOException
   * 
   */
  @Test
  public void testDeleteOnClose() throws IOException {

    System.out.println("Testing delete on close...");

    // Create temp file from String.
    File f = File.createTempFile("JUNIT_", "_TESTITEST");

    IOUtils.transferTo(new ByteArrayInputStream(TEST_FILE.getBytes("UTF-8")), f);

    // File exists.
    System.out.println("  -->  file created: " + f.getCanonicalPath());
    if (!f.exists()) {
      System.err.println("  -->  file does not exist");
      assertTrue(false);
    } else {
      System.out.println("  -->  file exists after creation");
    }

    // Create special file stream.
    DHCrudServiceFileInputStream specialFis = new DHCrudServiceFileInputStream(f);
    specialFis.close();

    if (f.exists()) {
      System.err.println("  -->  file does still exist");
    } else {
      System.out.println("  -->  file does not exist after close()");
    }
  }

}
