/*******************************************************************************
 * This software is copyright (c) 2018 by
 * 
 * DARIAH-DE Consortium (http://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (http://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 ******************************************************************************/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.junit.Test;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
/**
 * @author fugu
 *
 */
public class TestDHCrudServiceStorageElasticSearchImpl {

  private static final String STARS = "****************************************";

  private static final String DMD_DATAOBJECT_CORRECT_DATE =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E874-6>\n"
          + "        a               dariah:DataObject ;\n" + "        dc:creator      \"GDZ\" ;\n"
          + "        dc:date         \"2016-01-20T16:21:15\" ;\n"
          + "        dc:date         \"2016-01-20\" ;\n"
          + "        dc:description  \"Ein Kofferbild vom Hermes-Koffer\" ;\n"
          + "        dc:format       \"image/tiff\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-0002-E874-6> , <http://hdl.handle.net/21.T11991/0000-0002-E874-6> , <https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-0002-E874-6/index> ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-E873-7> ;\n"
          + "        dc:rights       \"restricted\" ;\n"
          + "        dc:title        \"00000099.tif\" .\n";
  private static final String DMD_DATAOBJECT_FALSE_DATE =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E874-6>\n"
          + "        a               dariah:DataObject ;\n" + "        dc:creator      \"GDZ\" ;\n"
          + "        dc:description  \"Ein Kofferbild vom Hermes-Koffer\" ;\n"
          + "        dc:format       \"image/tiff\" ;\n"
          + "        dc:date         \"URGLI?\" ;\n"
          + "        dc:date         \"2016-01-20T16:21:15\" ;\n"
          + "        dc:date         \"ARGLI?\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-0002-E874-6> , <http://hdl.handle.net/21.T11991/0000-0002-E874-6> , <https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-0002-E874-6/index> ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-E873-7> ;\n"
          + "        dc:rights       \"restricted\" ;\n"
          + "        dc:title        \"00000099.tif\" .\n";
  private static final String DMD_DATAOBJECT_FALSE_DATE_CORRECTED =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E874-6>\n"
          + "        a               dariah:DataObject ;\n" + "        dc:creator      \"GDZ\" ;\n"
          + "        dc:description  \"Ein Kofferbild vom Hermes-Koffer\" ;\n"
          + "        dc:format       \"image/tiff\" ;\n"
          + "        dc:date         \"2016-01-20T16:21:15\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-0002-E874-6> , <http://hdl.handle.net/21.T11991/0000-0002-E874-6> , <https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-0002-E874-6/index> ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-E873-7> ;\n"
          + "        dc:rights       \"restricted\" ;\n"
          + "        dc:title        \"00000099.tif\" .\n";

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  @Test
  public void testRemoveDateIfNotXSDDateTimeConformCORRECTDATEs()
      throws XMLStreamException, IOException, DatatypeConfigurationException, ParseException {

    System.out.println(STARS);
    System.out.println("Test creating dmd for ES with valid dc:date field");
    System.out.println(STARS);

    String namespaceUri = "http://hdl.handle.net/21.T11991/0000-0002-E874-6";
    Model model = RDFUtils.readModel(DMD_DATAOBJECT_CORRECT_DATE, RDFConstants.TURTLE);
    Model dateModel = RDFUtils.removeDateIfNotXSDDateTimeConform(namespaceUri, model);
    if (!model.isIsomorphicWith(dateModel)) {
      assertTrue(false);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IOException
   * @throws DatatypeConfigurationException
   * @throws ParseException
   */
  @Test
  public void testRemoveDateIfNotXSDDateTimeConformWRONGDATEs()
      throws XMLStreamException, IOException, DatatypeConfigurationException, ParseException {

    System.out.println(STARS);
    System.out.println("Test creating dmd for ES with FALSE dc:date field");
    System.out.println(STARS);

    String namespaceUri = "http://hdl.handle.net/21.T11991/0000-0002-E874-6";
    Model model = RDFUtils.readModel(DMD_DATAOBJECT_FALSE_DATE, RDFConstants.TURTLE);
    Model dateModel = RDFUtils.removeDateIfNotXSDDateTimeConform(namespaceUri, model);
    Model expectedmodel =
        RDFUtils.readModel(DMD_DATAOBJECT_FALSE_DATE_CORRECTED, RDFConstants.TURTLE);
    if (!dateModel.isIsomorphicWith(expectedmodel)) {
      assertTrue(false);
    }
  }

}
