package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import org.junit.Test;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestDHCrudDariahObject {

  private static final String ADM_MD = "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
      + "<http://hdl.handle.net/21.T11991/0000-0002-F1BD-9>\n"
      + "        a                            dariah:Collection ;\n"
      + "        dcterms:created              \"2017-09-29T14:31:42.183\" ;\n"
      + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
      + "        dcterms:extent               \"384\" ;\n"
      + "        dcterms:format               \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dcterms:identifier           <http://hdl.handle.net/21.T11991/0000-0002-F1BD-9> , <http://dx.doi.org/10.20375/0000-0002-F1BD-9> ;\n"
      + "        dcterms:modified             \"2017-09-29T14:31:42.183\" ;\n"
      + "        dcterms:source               \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-BC32-6C5E-FC25-0\" ;\n"
      + "        premis:hasMessageDigest      \"583465ddf9a826446d3ad1e3c3e8df74\" ;\n"
      + "        premis:hasMessageDigestOriginator\n"
      + "                \"dhcrud-base-7.17.3-SNAPSHOT.201709281725\" ;\n"
      + "        premis:hasMessageDigestType  \"md5\" .\n" + "";
  private static final String EXPECTED_MIMETYPE = "text/vnd.dariah.dhrep.collection+turtle";
  private static final String URI_WITHOUT_PREFIX = "21.T11991/0000-0002-F1BD-9";
  private static final String EXPECTED_URI = "hdl:21.T11991/0000-0002-F1BD-9";

  /**
   * @throws ParseException
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testDHCrudDariahObject()
      throws ParseException, IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println("Testing DHCrudDariahObject...");

    // Create CRUD object - URI with prefix.
    URI uri = URI.create(EXPECTED_URI);
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    System.out.println("URI: " + object.getUri());

    // Set object's ADM_MD.
    object.setAdmMD(RDFUtils.readModel(ADM_MD, RDFConstants.TURTLE));

    // Check mimetype.
    String mimetype = object.getMimetype();
    if (!mimetype.equals(EXPECTED_MIMETYPE)) {
      assertTrue("Mimetype is not correct: " + mimetype + "≠" + EXPECTED_MIMETYPE, false);
    }

    // Create CRUD object - URI without prefix.
    uri = URI.create(URI_WITHOUT_PREFIX);
    object = new DHCrudDariahObject(uri);

    System.out.println("URI: " + object.getUri());

    // Set object's ADM_MD.
    object.setAdmMD(RDFUtils.readModel(ADM_MD, RDFConstants.TURTLE));

    // Check mimetype.
    mimetype = object.getMimetype();
    if (!mimetype.equals(EXPECTED_MIMETYPE)) {
      assertTrue("Mimetype is not correct: " + mimetype + "≠" + EXPECTED_MIMETYPE, false);
    }

    // TODO More checks could be done here!
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testIsCollectionWithPrefix()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println("Testing isCollection method...");

    // Create CRUD object - URI with prefix.
    URI uri = URI.create(EXPECTED_URI);
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    System.out.println("URI: " + object.getUri());

    // Set object's ADM_MD.
    object.setAdmMD(RDFUtils.readModel(ADM_MD, RDFConstants.TURTLE));

    // Check collection with prefix.
    if (!object.isCollection()) {
      assertTrue(false);
    }
  }

}
