package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.datatype.DatatypeConfigurationException;
import org.apache.jena.rdf.model.Model;
import org.codehaus.jettison.json.JSONException;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestDHCrudServiceUtilities {

  private static final String METADATA = "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
      + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
      + "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
      + "@prefix doi:   <http://dx.doi.org/> .\n"
      + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
      + "<http://hdl.handle.net/21.T11998/0000-0002-17EC-B>\n"
      + "        a              dariah:Collection ;\n" + "        dc:creator     \"fu\" ;\n"
      + "        dc:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dc:identifier  <http://dx.doi.org/10.20375/0000-0002-17EC-B> , <http://box.dariah.local/1.0/dhcrud/21.T11998/0000-0002-17EC-B/index> , <http://hdl.handle.net/21.T11998/0000-0002-17EC-B> ;\n"
      + "        dc:rights      \"free\" ;\n"
      + "        dc:relation    <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-0EA5-5> ;\n"
      + "        dc:title       \"Kollektione!\" .";
  private static final String EXPECTED_ADM_MD_DATAOBJECT =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11998/0000-0002-0EA8-2>\n"
          + "        a                   dariah:DataObject ;\n"
          + "        dcterms:creator     \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:format      \"img/gif\" ;\n"
          + "        dcterms:identifier  <http://dx.doi.org/10.20375/0000-0002-0EA8-2> , <http://hdl.handle.net/21.T11998/0000-0002-0EA8-2> ;\n"
          + "        dcterms:relation    <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-0EA5-5> .";
  private static final String EXPECTED_ADM_MD_COLLECTION =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11998/0000-0002-0EA8-2>\n"
          + "        a                   dariah:Collection ;\n"
          + "        dcterms:creator     \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:identifier  <http://dx.doi.org/10.20375/0000-0002-0EA8-2> , <http://hdl.handle.net/21.T11998/0000-0002-0EA8-2> ;\n"
          + "        dcterms:relation    <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-0EA5-5> .";
  private static final String SOURCE_TTL = "@prefix dcam:  <http://purl.org/dc/dcam/> .\n"
      + "@prefix owl:   <http://www.w3.org/2002/07/owl#> .\n"
      + "@prefix seafile: <https://sftest.de.dariah.eu/files/> .\n"
      + "@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .\n"
      + "@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .\n"
      + "@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n"
      + "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix tgforms: <http://de.dariah.eu/rdf/tgforms/terms#> .\n"
      + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n"
      + "@prefix doi:   <http://dx.doi.org/> .\n" + "\n"
      + "<http://hdl.handle.net/21.T11998/0000-0002-445B-C>\n"
      + "        a                   dariah:Collection ;\n"
      + "        dc:creator          \"fu\" ;\n" + "        dc:description      \"urgli000\" ;\n"
      + "        dc:format           \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dc:identifier       <http://hdl.handle.net/21.T11998/0000-0002-445B-C> , <http://dx.doi.org/10.20375/0000-0002-445B-C> \n"
      + ";\n"
      + "        dc:relation         <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-4454-3> ;\n"
      + "        dc:rights           \"free\" ;\n" + "        dc:title            \"blu\" ;\n"
      + "        dcterms:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
      + "        dcterms:hasPart     dariahstorage:EAEA0-4969-35DA-8251-0 ;\n"
      + "        dcterms:identifier  <http://hdl.handle.net/21.T11998/0000-0002-445B-C> , <http://dx.doi.org/10.20375/0000-0002-445B-C> \n"
      + ";\n"
      + "        dcterms:source      \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-7BB9-CA6B-E86F-0\" .";
  private static final String OK = " ...OK";
  private static final String FAILED = " ...FAILED";
  private static final String DATACITE_API_LOCATION = "https://api.datacite.org/";
  private static final String EXPECTED_DATACITE_XML_METADATA =
      "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
          "<resource xmlns=\"http://datacite.org/schema/kernel-4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://datacite.org/schema/kernel-4 http://schema.datacite.org/meta/kernel-4/metadata.xsd\">\n"
          +
          "    <identifier identifierType=\"DOI\">10.20375/0000-000B-C8EF-7</identifier>\n" +
          "    <creators>\n" +
          "        <creator>\n" +
          "            <creatorName>DARIAH-DE</creatorName>\n" +
          "        </creator>\n" +
          "    </creators>\n" +
          "    <titles>\n" +
          "        <title>DARIAH-DE Repository – Terms of Use</title>\n" +
          "    </titles>\n" +
          "    <publisher>DARIAH-DE</publisher>\n" +
          "    <publicationYear>2017</publicationYear>\n" +
          "    <resourceType resourceTypeGeneral=\"Dataset\">Dataset</resourceType>\n" +
          "    <relatedIdentifiers>\n" +
          "        <relatedIdentifier relatedIdentifierType=\"Handle\" relationType=\"IsIdenticalTo\">21.11113/0000-000B-C8EF-7</relatedIdentifier>\n"
          +
          "    </relatedIdentifiers>\n" +
          "    <rightsList>\n" +
          "        <rights>https://creativecommons.org/choose/zero/</rights>\n" +
          "        <rights>CC0</rights>\n" +
          "    </rightsList>\n" +
          "    <descriptions>\n" +
          "        <description descriptionType=\"Other\">The collection containing the terms of use for the DARIAH-DE Repository.</description>\n"
          +
          "    </descriptions>\n" +
          "</resource>\n";
  private static final String HANDLE_DELETED_JSON =
      "{\"responseCode\":1,\"handle\":\"21.T11991/0000-001B-41BD-5\",\"values\":[{\"index\":99,\"type\":\"DELETED\",\"data\":{\"format\":\"string\",\"value\":\"Fugu Fischinger, Stefan E. Funk (2021). Deleted Dragon Collection. DARIAH-DE. https://doi.org/10.20375/0000-001b-41bd-5\"},\"ttl\":86400,\"timestamp\":\"2021-04-16T14:36:46Z\"}]}";
  private static final String EXPECTED_DATE_STRING = "2021-04-16T14:36:46Z";
  private static final String EXPECTED_DOI_CITATION =
      "Fugu Fischinger, Stefan E. Funk (2021). Deleted Dragon Collection. DARIAH-DE. https://doi.org/10.20375/0000-001b-41bd-5";
  public static final List<String> IDENTIFIER_AND_RELATIONS_LIST =
      new ArrayList<String>(Arrays.asList(
          "http://hdl.handle.net/21.T11998/0000-0002-445B-C",
          "http://dx.doi.org/10.20375/0000-0002-445B-C",
          "https://hdl.handle.net/21.T11998/0000-0002-445B-C",
          "https://dx.doi.org/10.20375/0000-0002-445B-C",
          "https://doi.org/10.20375/0000-0002-445B-C",
          "http://doi.org/10.20375/0000-0002-445B-C",
          "hdl:21.T11998/0000-0002-445B-C",
          "doi:10.20375/0000-0002-445B-C",
          "https://fugu.de/testitest/urglimurgli"));
  public static final List<String> EXPECTED_IDENTIFIER_AND_RELATIONS_LIST =
      new ArrayList<String>(Arrays.asList(
          "<a href=\"http://doi.org/10.20375/0000-0002-445B-C\">doi:10.20375/0000-0002-445B-C</a>",
          "<a href=\"http://dx.doi.org/10.20375/0000-0002-445B-C\">doi:10.20375/0000-0002-445B-C</a>",
          "<a href=\"http://hdl.handle.net/21.T11998/0000-0002-445B-C\">hdl:21.T11998/0000-0002-445B-C</a>",
          "<a href=\"https://doi.org/10.20375/0000-0002-445B-C\">doi:10.20375/0000-0002-445B-C</a>",
          "<a href=\"https://dx.doi.org/10.20375/0000-0002-445B-C\">doi:10.20375/0000-0002-445B-C</a>",
          "<a href=\"https://hdl.handle.net/21.T11998/0000-0002-445B-C\">hdl:21.T11998/0000-0002-445B-C</a>",
          "[external] <a href=\"https://fugu.de/testitest/urglimurgli\">https://fugu.de/testitest/urglimurgli</a>"));

  /**
   * <p>
   * Checks if we get the correct COLREG_ID value from PID response using JSON.
   * </p>
   * 
   * @throws JSONException
   */
  @Test
  public void testExtractPidValueTHEBAG() throws JSONException {

    String json =
        "{\"responseCode\":1,\"handle\":\"11022/0000-0000-9C7D-5\",\"values\":[{\"index\":6,\"type\":\"BAG\",\"data\":{\"format\":\"string\",\"value\":\"http://trep.de.dariah.eu/dhcrud/11022/0000-0000-9C7D-5/bag\"},\"ttl\":86400,\"timestamp\":\"2016-05-13T11:25:32Z\"}]}";
    String toFind = "BAG";

    System.out.print("Getting BAG URL from PID response... ");
    String found = DHCrudServiceUtilities.extractPidValue(json, toFind);

    if (!found.equals("http://trep.de.dariah.eu/dhcrud/11022/0000-0000-9C7D-5/bag")) {
      System.out.println(FAILED);
      assertTrue(false);
    } else {
      System.out.println(found);
      System.out.println(OK);
    }
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testCreatingAdministrativeMetadataDataObject()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println("Creating ADM_MD model for DataObject... ");

    String resource = "http://hdl.handle.net/21.T11998/0000-0002-0EA8-2";
    String pid = "http://hdl.handle.net/21.T11998/0000-0002-0EA8-2";
    String eppn = "StefanFunk@dariah.eu";
    String doi = "http://dx.doi.org/10.20375/0000-0002-0EA8-2";
    String rootCollection =
        "http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-0EA5-5";
    String format = "img/gif";

    Model admMd = DHCrudServiceUtilities.createAdministrativeMetadata(resource, eppn, doi, pid,
        rootCollection, format);
    Model expectedAdmMd = RDFUtils.readModel(EXPECTED_ADM_MD_DATAOBJECT, RDFConstants.TURTLE);

    if (admMd.isIsomorphicWith(expectedAdmMd)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("CREATED\n" + RDFUtils.getTTLFromModel(admMd));
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expectedAdmMd));
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testCreatingAdministrativeMetadataCollection()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println("Creating ADM_MD model for collection... ");

    String resource = "http://hdl.handle.net/21.T11998/0000-0002-0EA8-2";
    String pid = "http://hdl.handle.net/21.T11998/0000-0002-0EA8-2";
    String eppn = "StefanFunk@dariah.eu";
    String doi = "http://dx.doi.org/10.20375/0000-0002-0EA8-2";
    String rootCollection =
        "http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11998/0000-0002-0EA5-5";
    String format = TextGridMimetypes.DARIAH_COLLECTION;

    Model admMd = DHCrudServiceUtilities.createAdministrativeMetadata(resource, eppn, doi, pid,
        rootCollection, format);
    Model expectedAdmMd = RDFUtils.readModel(EXPECTED_ADM_MD_COLLECTION, RDFConstants.TURTLE);

    if (admMd.isIsomorphicWith(expectedAdmMd)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("CREATED\n" + RDFUtils.getTTLFromModel(admMd));
      System.out.println("EXPECTED\n" + RDFUtils.getTTLFromModel(expectedAdmMd));
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testGetSourceFromRdf()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    Model model = RDFUtils.readModel(SOURCE_TTL, RDFConstants.TURTLE);
    String expectedSource = "https://cdstar.de.dariah.eu/test/dariah/EAEA0-7BB9-CA6B-E86F-0";

    String source = DHCrudServiceUtilities.getSourceFromRDF(model);

    if (!source.equals(expectedSource)) {
      System.out.println(expectedSource + " ≠ " + source);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testRemoveUnusedNamespacesFromModel()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    Model model = RDFUtils.readModel(METADATA, RDFConstants.TURTLE);
    int expectedBefore = 14;
    int expectedAfter = 3;

    int numberOfNamespacesBeforePurging = model.getNsPrefixMap().size();

    RDFUtils.removeUnusedNamespacesFromModel(model);

    int numberOfNamespacesAfterPurging = model.getNsPrefixMap().size();

    if (numberOfNamespacesBeforePurging != expectedBefore
        || numberOfNamespacesAfterPurging != expectedAfter) {
      System.out.println("numberOfNamespacesBeforePurging: " + numberOfNamespacesBeforePurging
          + " ≠ " + expectedBefore);
      System.out.println("OR");
      System.out.println("numberOfNamespacesAfterPurging:  " + numberOfNamespacesAfterPurging
          + " ≠ " + expectedAfter);
      assertTrue(false);
    }
  }

  /**
   *
   */
  @Test
  public void testOmitTrailingSlash() {

    String theStringWithTrailingSlash = "https://trep.de.dariah.eu/1.0/";
    String theStringWithoutTrailingSlash = "https://trep.de.dariah.eu/1.0";
    String expected = "https://trep.de.dariah.eu/1.0";

    String theResult1 = DHCrudServiceUtilities.omitTrailingSlash(theStringWithTrailingSlash);
    String theResult2 = DHCrudServiceUtilities.omitTrailingSlash(theStringWithoutTrailingSlash);

    if (!theResult1.equals(expected)) {
      System.out.println(FAILED);
      System.out.println(theResult1 + " != " + expected);
      assertTrue(false);
    }

    if (!theResult2.equals(expected)) {
      System.out.println(FAILED);
      System.out.println(theResult2 + " != " + expected);
      assertTrue(false);
    }
  }

  /**
   * @throws ParseException
   */
  @Test
  public void testCheckPid() {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    String logID = "URGLI";

    String pidPrefix = "21.T11991";
    String doiPrefix = "10.20375";

    URI pid = URI.create("21.T11991/0000-0008-BB04-1");
    URI doi = URI.create("10.20375/0000-0008-BB04-1");
    URI expectedPID = pid;
    try {
      URI pidResult = DHCrudServiceUtilities.checkPid(pid, methodInfo, pidPrefix, doiPrefix, logID);
      URI doiResult = DHCrudServiceUtilities.checkPid(doi, methodInfo, pidPrefix, doiPrefix, logID);

      if (!expectedPID.toASCIIString().equals(pidResult.toString())) {
        System.out.println("01: " + expectedPID + " != " + pidResult);
        assertTrue(false);
      }
      if (!expectedPID.toASCIIString().equals(doiResult.toString())) {
        System.out.println("01: " + expectedPID + " != " + doiResult);
        assertTrue(false);
      }

      System.out.println("01: " + pid + " --> " + pidResult);
      System.out.println("01: " + doi + " --> " + doiResult);

    } catch (ParseException e) {
      System.out.println("01: unexpected ParseException caught: " + e.getMessage());
      assertTrue(false);
    }

    URI pidWithPrefix = URI.create("hdl:21.T11991/0000-0008-BB04-1");
    URI doiWithPrefix = URI.create("doi:10.20375/0000-0008-BB04-1");
    try {
      URI pidResult =
          DHCrudServiceUtilities.checkPid(pidWithPrefix, methodInfo, pidPrefix, doiPrefix, logID);
      URI doiResult =
          DHCrudServiceUtilities.checkPid(doiWithPrefix, methodInfo, pidPrefix, doiPrefix, logID);

      if (!expectedPID.toString().equals(pidResult.toString())) {
        System.out.println("02: " + expectedPID + " != " + pidResult);
        assertTrue(false);
      }
      if (!expectedPID.toString().equals(doiResult.toString())) {
        System.out.println("02: " + expectedPID + " != " + doiResult);
        assertTrue(false);
      }

      System.out.println("02: " + pidWithPrefix + " --> " + pidResult);
      System.out.println("02: " + doiWithPrefix + " --> " + doiResult);

    } catch (ParseException e) {
      assertTrue("02: unexpected ParseException caught: " + e.getMessage(), false);
    }

    URI wrongPID = URI.create("urgl");
    try {
      DHCrudServiceUtilities.checkPid(wrongPID, methodInfo, pidPrefix, doiPrefix, logID);
    } catch (ParseException e) {
      System.out
          .println("03: expected ParseException caught for " + wrongPID + ": " + e.getMessage());
      assertTrue(true);
    }

    URI wrongDOI = URI.create("10.12345/0000-000B-CAC4-4");
    try {
      DHCrudServiceUtilities.checkPid(wrongDOI, methodInfo, pidPrefix, doiPrefix, logID);
    } catch (ParseException e) {
      System.out
          .println("04: expected ParseException caught for " + wrongDOI + ": " + e.getMessage());
      assertTrue(true);
    }

    URI unexpectedPathPID = URI.create("21.T11991/0000-0008-BB04-1/argliwargli");
    try {
      DHCrudServiceUtilities.checkPid(unexpectedPathPID, methodInfo, pidPrefix, doiPrefix, logID);
    } catch (ParseException e) {
      System.out.println(
          "05: expected ParseException caught for " + unexpectedPathPID + ": " + e.getMessage());
      assertTrue(true);
    }

    URI anotherUnexpectedPathPID = URI.create("21.T11991/0000-000E-FA31-1/urgl/argl/aua");
    try {
      DHCrudServiceUtilities.checkPid(anotherUnexpectedPathPID, methodInfo, pidPrefix, doiPrefix,
          logID);
    } catch (ParseException e) {
      System.out.println("06: expected ParseException caught for " + anotherUnexpectedPathPID + ": "
          + e.getMessage());
      assertTrue(true);
    }
  }

  String[] ids = {
      "21.T11991/0000-000E-F1B4-6", "10.5072/0000-000E-F1B4-6",
      "21.11113/0000-000B-CAC4-4", "10.20375/0000-000B-CAC4-4"};


  /**
   *
   */
  @Test
  public void checkCheckPIDSyntax() {

    System.out.println("Expected Handle RegExp is " + DHCrudService.GWDG_HANDLE_PID_REGEXP);

    // Uppercase Handle.
    URI handlePID = URI.create("21.T11991/0000-0008-BB04-1");
    boolean correct = DHCrudServiceUtilities.checkHandlePIDSyntax(handlePID);
    if (!correct) {
      System.out.println(handlePID + " is not a correct DARIAH-DE Handle PID!");
      assertTrue(false);
    }

    // Lowercase Handle.
    handlePID = URI.create("21.t11991/0000-0008-bb04-1");
    correct = DHCrudServiceUtilities.checkHandlePIDSyntax(handlePID);
    if (!correct) {
      System.out.println(handlePID + " is not a correct DARIAH-DE Handle PID!");
      assertTrue(false);
    }

    // Uppercase DOI.
    URI doiPID = URI.create("10.20375/0000-0008-BB04-1");
    correct = DHCrudServiceUtilities.checkHandlePIDSyntax(doiPID);
    if (correct) {
      System.out.println(handlePID
          + " is a correct uppercase DOI, but not a correct (uppercase) DARIAH-DE Handle PID!");
      assertTrue(false);
    }

    // Lowercase DOI.
    doiPID = URI.create("10.20375/0000-0008-bb04-1");
    correct = DHCrudServiceUtilities.checkHandlePIDSyntax(doiPID);
    if (correct) {
      System.out.println(doiPID
          + " is a correct lowercase DOI, but not a correct (lowercase) DARIAH-DE Handle PID!");
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void checkCheckPIDAndDOISyntax() {

    System.out.println("Expected PID RegExp is " + DHCrudService.PID_AND_DOI_REGEXP);

    // Uppercase Handle.
    URI handlePID = URI.create("21.T11991/0000-0008-BB04-1");
    boolean correct = DHCrudServiceUtilities.checkPIDAndDOISyntax(handlePID);
    if (!correct) {
      System.out.println(handlePID + " is not a correct DARIAH-DE Handle PID!");
      assertTrue(false);
    }

    // Lowercase Handle.
    handlePID = URI.create("21.t11991/0000-0008-bb04-1");
    correct = DHCrudServiceUtilities.checkPIDAndDOISyntax(handlePID);
    if (!correct) {
      System.out.println(handlePID + " is not a correct DARIAH-DE Handle PID!");
      assertTrue(false);
    }

    // Uppercase DOI.
    URI doiPID = URI.create("10.20375/0000-0008-BB04-1");
    correct = DHCrudServiceUtilities.checkPIDAndDOISyntax(doiPID);
    if (!correct) {
      System.out.println(handlePID + " is not a correct uppercase DARIAH-DE DOI!");
      assertTrue(false);
    }

    // Lowercase DOI.
    doiPID = URI.create("10.20375/0000-0008-bb04-1");
    correct = DHCrudServiceUtilities.checkPIDAndDOISyntax(doiPID);
    if (!correct) {
      System.out.println(doiPID + " is not a correct lowercase DARIAH-DE DOI!");
      assertTrue(false);
    }

    // Handle with Handle prefix "hdl:".
    handlePID = URI.create("hdl:21.T11991/0000-0008-bb04-1");
    correct = DHCrudServiceUtilities.checkPIDAndDOISyntax(handlePID);
    if (!correct) {
      System.out.println(handlePID + " is not a correct DARIAH-DE Handle PID!");
      assertTrue(false);
    }

    // DOI with DOI prefix "doi:".
    doiPID = URI.create("doi:10.20375/0000-0008-BB04-1");
    correct = DHCrudServiceUtilities.checkPIDAndDOISyntax(doiPID);
    if (!correct) {
      System.out.println(doiPID + " is not a correct uppercase DARIAH-DE DOI!");
      assertTrue(false);
    }
  }

  /**
   *
   */
  @Test
  @Ignore
  public void testGetNameFromORCIDCreatorEntry() {

    String orcidURL = "https://orcid.org/";
    boolean error = false;

    String entry = "orcid:0000-0002-4019-9857";
    String expectedORCHIDEntry = "Michael Czolkoß-Hettwer";
    String orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "https://orcid.org/0000-0002-4019-9857";
    expectedORCHIDEntry = "Michael Czolkoß-Hettwer";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "http://orcid.org/0000-0002-4019-9857";
    expectedORCHIDEntry = "Michael Czolkoß-Hettwer";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "orcid.org/0000-0002-4019-9857";
    expectedORCHIDEntry = "Michael Czolkoß-Hettwer";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "orcid:0000-0003-1259-2288";
    expectedORCHIDEntry = "Stefan E. Funk";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "https://orcid.org/0000-0003-1259-2288";
    expectedORCHIDEntry = "Stefan E. Funk";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "http://orcid.org/0000-0003-1259-2288";
    expectedORCHIDEntry = "Stefan E. Funk";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    entry = "orcid.org/0000-0003-1259-2288";
    expectedORCHIDEntry = "Stefan E. Funk";
    orcid = DHCrudServiceUtilities.getNameFromORCIDCreatorEntry(entry, orcidURL);
    if (!orcid.equals(expectedORCHIDEntry)) {
      System.out.println(orcid + " != " + expectedORCHIDEntry);
      error = true;
    }

    if (error) {
      assertTrue(false);
    }

  }

  /**
   *
   */
  @Test
  public void testGetORCIDFromCreatorEntry() {

    String entry = "orcid:0000-0002-4019-9857";
    String expectedORCID = "0000-0002-4019-9857";
    String orcid = DHCrudServiceUtilities.getORCIDFromCreatorEntry(entry);
    if (!orcid.equals(expectedORCID)) {
      System.out.println(orcid + " != " + expectedORCID);
      assertTrue(false);
    }

    entry = "https://orcid.org/0000-0002-4019-9857";
    expectedORCID = "0000-0002-4019-9857";
    orcid = DHCrudServiceUtilities.getORCIDFromCreatorEntry(entry);
    if (!orcid.equals(expectedORCID)) {
      System.out.println(orcid + " != " + expectedORCID);
      assertTrue(false);
    }

    entry = "http://orcid.org/0000-0002-4019-9857";
    expectedORCID = "0000-0002-4019-9857";
    orcid = DHCrudServiceUtilities.getORCIDFromCreatorEntry(entry);
    if (!orcid.equals(expectedORCID)) {
      System.out.println(orcid + " != " + expectedORCID);
      assertTrue(false);
    }

    entry = "orcid:0000-0003-1259-2288";
    expectedORCID = "0000-0003-1259-2288";
    orcid = DHCrudServiceUtilities.getORCIDFromCreatorEntry(entry);
    if (!orcid.equals(expectedORCID)) {
      System.out.println(orcid + " != " + expectedORCID);
      assertTrue(false);
    }

    entry = "https://orcid.org/0000-0003-1259-2288";
    expectedORCID = "0000-0003-1259-2288";
    orcid = DHCrudServiceUtilities.getORCIDFromCreatorEntry(entry);
    if (!orcid.equals(expectedORCID)) {
      System.out.println(orcid + " != " + expectedORCID);
      assertTrue(false);
    }

    entry = "http://orcid.org/0000-0003-1259-2288";
    expectedORCID = "0000-0003-1259-2288";
    orcid = DHCrudServiceUtilities.getORCIDFromCreatorEntry(entry);
    if (!orcid.equals(expectedORCID)) {
      System.out.println(orcid + " != " + expectedORCID);
      assertTrue(false);
    }
  }

  /**
   * PLEASE NOTE This is an online test!
   * 
   * @throws IoFault
   */
  @Test
  @Ignore
  public void testGetDOIMetadataXML() throws IoFault {

    String doi = "10.20375/0000-000B-C8EF-7";
    String logID = "JUNIT_FUCRUD_" + System.currentTimeMillis();

    String doiMetadataXML =
        DHCrudServiceUtilities.getDOIMetadataXML(doi, logID, DATACITE_API_LOCATION);

    if (!doiMetadataXML.trim().equals(EXPECTED_DATACITE_XML_METADATA.trim())) {
      System.out.println(doiMetadataXML);
      System.out.println(" != ");
      System.out.println(EXPECTED_DATACITE_XML_METADATA);
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testGetDOIfromPID() {

    String doiPrefix = "10.20375";
    String expectedDOI = doiPrefix + "/0000-0001-F6BE-4";

    URI pid1 = URI.create("21.T11998/0000-0001-F6BE-4");
    String doi1 = DHCrudServiceUtilities.getDOIfromPID(pid1, doiPrefix);
    if (!doi1.equals(expectedDOI)) {
      System.out.println(doi1 + " != " + expectedDOI);
      assertTrue(false);
    }

    URI pid2 = URI.create("hdl:21.T11998/0000-0001-F6BE-4");
    String doi2 = DHCrudServiceUtilities.getDOIfromPID(pid2, doiPrefix);
    if (!doi2.equals(expectedDOI)) {
      System.out.println(doi2 + " != " + expectedDOI);
      assertTrue(false);
    }

    URI pid3 = URI.create("https://hdl.handle.net/21.T11998/0000-0001-F6BE-4");
    String doi3 = DHCrudServiceUtilities.getDOIfromPID(pid3, doiPrefix);
    if (!doi3.equals(expectedDOI)) {
      System.out.println(doi3 + " != " + expectedDOI);
      assertTrue(false);
    }
  }

  /**
   * @throws JSONException
   * @throws ParseException
   */
  @Test
  public void testGetTimestampFromHandleMetadata() throws JSONException, ParseException {

    SimpleDateFormat handleMetadataTimestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    Date expectedDate = handleMetadataTimestampFormat.parse(EXPECTED_DATE_STRING);

    Date timestamp = DHCrudServiceUtilities.getTimestampFromHandleMetadata(HANDLE_DELETED_JSON);

    if (expectedDate.equals(timestamp)) {
      System.out.println(timestamp + " == " + expectedDate);
    } else {
      System.out.println(timestamp + " != " + expectedDate);
      assertTrue(false);
    }
  }

  /**
   * @throws JSONException
   * @throws ParseException
   */
  @Test
  public void testGetCitationFromHandleMetadata() throws JSONException, ParseException {

    String citation = DHCrudServiceUtilities.getCitationFromHandleMetadata(HANDLE_DELETED_JSON);

    if (citation.equals(EXPECTED_DOI_CITATION)) {
      System.out.println(citation + " == " + EXPECTED_DOI_CITATION);
    } else {
      System.out.println(citation + " != " + EXPECTED_DOI_CITATION);
      assertTrue(false);
    }
  }

  /**
   * @throws JSONException
   * @throws ParseException
   */
  @Test
  public void testCheckTypeInHandleMetadataNotExisting() throws JSONException, ParseException {

    String emptyJSONString =
        "{\"responseCode\":200,\"values\":[],\"handle\":\"21.T11991/0000-001B-48B5-6\"}";
    String type = "DELETED";

    boolean typeExisting = CrudServiceUtilities.checkTypeInHandleMetadata(emptyJSONString, type);
    if (typeExisting) {
      assertTrue(false);
    }
  }

  /**
   * @throws JSONException
   * @throws ParseException
   */
  @Test
  public void testCheckTypeInHandleMetadataExisting() throws JSONException, ParseException {

    String publishedTypeJSONString =
        "{\"responseCode\":1,\"handle\":\"21.T11991/0000-001B-4222-2\",\"values\":[{\"index\":17,\"type\":\"PUBLISHED\",\"data\":{\"format\":\"string\",\"value\":\"true\"},\"ttl\":86400,\"timestamp\":\"2021-04-19T12:21:08Z\"}]}";
    String type = "PUBLISHED";

    boolean typeExisting =
        CrudServiceUtilities.checkTypeInHandleMetadata(publishedTypeJSONString, type);
    if (!typeExisting) {
      assertTrue(false);
    }
  }

  /**
   * @throws JSONException
   * @throws ObjectNotFoundFault
   */
  @Test
  public void testGetTitleListFromJSON() throws JSONException, ObjectNotFoundFault {

    String json;
    List<String> expectedTitleList;
    List<String> jsonTitleList;

    // Test correct settings.
    json = "{\"descriptiveMetadata\":{\"dc:title\":[\"WESPE!\",\"GiantWasp.gif\"]}}";
    expectedTitleList = new ArrayList<String>();
    expectedTitleList.add("WESPE!");
    expectedTitleList.add("GiantWasp.gif");
    jsonTitleList = DHCrudServiceUtilities.getTitlesFromMetadataJSON(json);
    if (!jsonTitleList.equals(expectedTitleList)) {
      System.out.println(jsonTitleList + " != " + expectedTitleList);
      assertTrue(false);
    }

    json = "{\"descriptiveMetadata\":{\"dc:title\":\"GreyLord.gif\"}}";
    expectedTitleList = new ArrayList<String>();
    expectedTitleList.add("GreyLord.gif");
    jsonTitleList = DHCrudServiceUtilities.getTitlesFromMetadataJSON(json);
    if (!jsonTitleList.equals(expectedTitleList)) {
      System.out.println(jsonTitleList + " != " + expectedTitleList);
      assertTrue(false);
    }

    json =
        "{\"descriptiveMetadata\":{\"dc:title\":[\"WESPE!\",\"GiantWasp.gif\",\"KRAMS\",\"KAKKI\"]}}";
    expectedTitleList = new ArrayList<String>();
    expectedTitleList.add("WESPE!");
    expectedTitleList.add("GiantWasp.gif");
    expectedTitleList.add("KRAMS");
    expectedTitleList.add("KAKKI");
    jsonTitleList = DHCrudServiceUtilities.getTitlesFromMetadataJSON(json);
    if (!jsonTitleList.equals(expectedTitleList)) {
      System.out.println(jsonTitleList + " != " + expectedTitleList);
      assertTrue(false);
    }

    // Test false settings.
    json =
        "{\"descriptiveMetadata\":{\"dc:title\":[\"WESPE!\",\"GiantWasp.gif\",\"KRAMS\",\"KAKKI\"]}}";
    expectedTitleList = new ArrayList<String>();
    expectedTitleList.add("WESPE!");
    expectedTitleList.add("GiantWasp.gif");
    expectedTitleList.add("KRAMS");
    jsonTitleList = DHCrudServiceUtilities.getTitlesFromMetadataJSON(json);
    if (jsonTitleList.equals(expectedTitleList)) {
      System.out.println(jsonTitleList + " == " + expectedTitleList);
      assertTrue(false);
    }
  }

  /**
   * @throws JSONException
   */
  @Test
  public void testMimetypeFromJSON() throws JSONException {

    String json;
    String expectedMimetype;
    String jsonMimetype;

    // Test correct settings.
    json = "{\"administrativeMetadata\":{\"dcterms:format\":\"image/tiff\"}}";
    expectedMimetype = "image/tiff";
    jsonMimetype = DHCrudServiceUtilities.getMimetypeFromMetadataJSON(json);
    if (!jsonMimetype.equals(expectedMimetype)) {
      System.out.println(jsonMimetype + " != " + expectedMimetype);
      assertTrue(false);
    }

    // Test false settings.
    json = "{\"administrativeMetadata\":{\"dcterms:format\":\"image/png\"}}";
    expectedMimetype = "image/tiff";
    jsonMimetype = DHCrudServiceUtilities.getMimetypeFromMetadataJSON(json);
    if (jsonMimetype.equals(expectedMimetype)) {
      System.out.println(jsonMimetype + " == " + expectedMimetype);
      assertTrue(false);
    }
  }

  /**
   * 
   */
  @Test
  public void testHumaniseExtent() {

    String bytes = "345";
    String kByte = "12004";
    String mByte = "42456395";
    String gByte = "893247873094";
    String tByte = "95156489587351";

    String expBytes = "345 bytes";
    String expKByte = "11 KB";
    String expMByte = "40 MB";
    String expGByte = "831 GB";
    String expTByte = "86 TB";

    String humanisedBytes = DHCrudServiceUtilities.humaniseExtent(bytes);
    String humanisedKByte = DHCrudServiceUtilities.humaniseExtent(kByte);
    String humanisedMByte = DHCrudServiceUtilities.humaniseExtent(mByte);
    String humanisedGByte = DHCrudServiceUtilities.humaniseExtent(gByte);
    String humanisedTByte = DHCrudServiceUtilities.humaniseExtent(tByte);

    if (!expBytes.equals(humanisedBytes)) {
      System.out.println(bytes + ": " + humanisedBytes + " != " + expBytes);
    }
    if (!expKByte.equals(humanisedKByte)) {
      System.out.println(kByte + ": " + humanisedKByte + " != " + expKByte);
    }
    if (!expMByte.equals(humanisedMByte)) {
      System.out.println(mByte + ": " + humanisedMByte + " != " + expMByte);
    }
    if (!expGByte.equals(humanisedGByte)) {
      System.out.println(gByte + ": " + humanisedGByte + " != " + expGByte);
    }
    if (!expTByte.equals(humanisedTByte)) {
      System.out.println(gByte + ": " + humanisedTByte + " != " + expTByte);
    }
  }

  /**
   * @throws IoFault
   * @throws IOException
   */
  @Test
  public void testGetIdentifiers() throws IoFault, IOException {

    CrudServiceConfigurator config = CrudServiceConfigurator.getInstance("");
    config.setProperty("PID_PREFIX", "21.T11998");
    config.setProperty("DOI_PREFIX", "10.20375");
    config.setProperty("DOI_RESOLVER", "https://dx.doi.org");
    config.setProperty("PID_RESOLVER", "https://hdl.handle.net");

    Set<String> identifierStrings =
        DHCrudServiceUtilities.getIdentifiers(IDENTIFIER_AND_RELATIONS_LIST, config);

    TreeSet<String> expectedTreeSet = new TreeSet<String>(EXPECTED_IDENTIFIER_AND_RELATIONS_LIST);
    TreeSet<String> actualTreeSet = new TreeSet<String>(identifierStrings);

    String message = expectedTreeSet + " !=\n" + actualTreeSet;
    assertTrue(message, expectedTreeSet.equals(actualTreeSet));
  }

  /**
   * @throws IoFault
   * @throws IOException
   */
  @Test
  public void testGetRelations() throws IoFault, IOException {

    CrudServiceConfigurator config = CrudServiceConfigurator.getInstance("");
    config.setProperty("PID_PREFIX", "21.T11998");
    config.setProperty("DOI_PREFIX", "10.20375");
    config.setProperty("DOI_RESOLVER", "https://dx.doi.org");
    config.setProperty("PID_RESOLVER", "https://hdl.handle.net");

    Set<String> relationsStrings =
        DHCrudServiceUtilities.getRelations(IDENTIFIER_AND_RELATIONS_LIST, config);

    TreeSet<String> expectedTreeSet = new TreeSet<String>(EXPECTED_IDENTIFIER_AND_RELATIONS_LIST);
    TreeSet<String> actualTreeSet = new TreeSet<String>(relationsStrings);

    String message = expectedTreeSet + " !=\n" + actualTreeSet;
    assertTrue(message, expectedTreeSet.equals(actualTreeSet));
  }

  /**
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testGetYearFromSystemTime() throws DatatypeConfigurationException {
    System.out.println(
        "The year is " + CrudServiceUtilities.getYearFromSystemTime() + "! IS THAT CORRECT? :-D");
  }

}
