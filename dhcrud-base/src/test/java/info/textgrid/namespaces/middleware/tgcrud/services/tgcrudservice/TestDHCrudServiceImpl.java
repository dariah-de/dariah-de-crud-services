package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import org.codehaus.jettison.json.JSONException;
import org.junit.Test;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestDHCrudServiceImpl {

  private static final String URGLI = "<rdf:RDF\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:j.0=\"http://purl.org/dc/terms/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/11022/0000-0000-9AD2-5\">\n"
      + "    <dc:relation>dariah:Collection:11022/0000-0000-9ACF-A</dc:relation>\n"
      + "    <j.0:identifier rdf:resource=\"http://hdl.handle.net/11022/0000-0000-9AD2-5\"/>\n"
      + "    <dc:title>Dragon copy.gif</dc:title>\n" + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String URGLI_EXPECTED = "<rdf:RDF\n"
      + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\" > \n"
      + "  <rdf:Description rdf:about=\"http://hdl.handle.net/11022/0000-0000-9AD2-5\">\n"
      + "    <dc:relation>dariah:Collection:11022/0000-0000-9ACF-A</dc:relation>\n"
      + "    <dc:title>Dragon copy.gif</dc:title>\n" + "    <dc:format>image/gif</dc:format>\n"
      + "    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/DataObject\"/>\n"
      + "  </rdf:Description>\n" + "</rdf:RDF>";
  private static final String GETTING_DOI = "<rdf:RDF\n"
      + "	    xmlns:dcterms=\"http://purl.org/dc/terms/\"\n"
      + "	    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
      + "	    xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n"
      + "	    xmlns:dariah=\"http://de.dariah.eu/rdf/dataobjects/terms/\"\n"
      + "	    xmlns:hdl=\"http://hdl.handle.net/\" > \n"
      + "	  <rdf:Description rdf:about=\"https://de.dariah.eu/storage/EAEA0-59F3-2837-4561-0\">\n"
      + "	    <dc:format>text/tg.collection+tg.aggregation+xml</dc:format>\n"
      + "	    <dcterms:format>text/tg.collection+tg.aggregation+xml</dcterms:format>\n"
      + "	    <dc:identifier rdf:resource=\"http://hdl.handle.net/11022/0000-0007-930D-5\"/>\n"
      + "	    <dcterms:identifier rdf:resource=\"http://hdl.handle.net/11022/0000-0007-930D-5\"/>\n"
      + "	    <dc:title>Der Fugu</dc:title>\n"
      + "	    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-F0DE-8CF7-10B6-0\"/>\n"
      + "	    <dcterms:source>https://dariah-cdstar.gwdg.de/dariah/EAEA0-59F3-2837-4561-0</dcterms:source>\n"
      + "	    <dc:identifier rdf:resource=\"http://dx.doi.org/10.3249/0000-0007-930D-5\"/>\n"
      + "	    <dc:rights>free</dc:rights>\n"
      + "	    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-C623-258A-EE0F-0\"/>\n"
      + "	    <dcterms:identifier rdf:resource=\"http://dx.doi.org/10.3249/0000-0007-930D-5\"/>\n"
      + "	    <dc:creator>fu</dc:creator>\n"
      + "	    <dcterms:hasPart rdf:resource=\"https://de.dariah.eu/storage/EAEA0-B19E-E6D8-D512-0\"/>\n"
      + "	    <rdf:type rdf:resource=\"http://de.dariah.eu/rdf/dataobjects/terms/Collection\"/>\n"
      + "	  </rdf:Description>\n" + "	</rdf:RDF>";
  private static final String METADATA_4_HTML_ROOTCOLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
          + "        a               dariah:Collection ;\n"
          + "        dc:creator      \"fugu fischinger\" , \"Stefan E. Funk\" ;\n"
          + "        dc:description  \"Describe your object here please!\" ;\n"
          + "        dc:format       \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-000E-FDCA-2> , <http://box.dariah.local/1.0/dhcrud/21.T11991/0000-000E-FDCA-2/index> , <http://hdl.handle.net/21.T11991/0000-000E-FDCA-2> ;\n"
          + "        dc:rights       \"free\" , \"unfree\" ;\n"
          + "        dc:relation     \"fugu.de\" , \"http://fugu.de\" ;\n"
          + "        dc:title        \"Ein Titel!\" , \"Ein anderer Titel\" .";
  private static final String METADATA_4_HTML_SUBCOLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
          + "        a               dariah:Collection ;\n"
          + "        dc:creator      \"fugu fischinger\" , \"Stefan E. Funk\" ;\n"
          + "        dc:description  \"Dies hier ist eine Beschreibung des Objetcs! Eine Gute!\" ;\n"
          + "        dc:format       \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-000E-FDCA-2> , <http://box.dariah.local/1.0/dhcrud/21.T11991/0000-000E-FDCA-2/index> , <http://hdl.handle.net/21.T11991/0000-000E-FDCA-2> , <doi:10.20375/0000-000B-C8EF-7> ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-61EE-5> ;\n"
          + "        dc:relation     \"http://fugu.de\" ;\n"
          + "        dc:rights       \"free\" , \"restricted :-)\" ;\n"
          + "        dc:title        \"Titel Nummer Eins\" , \"Titel Nummer Zwei\" .";
  private static final String ADM_MD_4_HTML_ROOTCOLLECTION =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
          + "        a                            dariah:Collection ;\n"
          + "        dcterms:created              \"2017-09-08T16:22:20.312\" ;\n"
          + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:extent               \"384\" ;\n"
          + "        dcterms:format               \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:identifier           <http://dx.doi.org/10.20375/0000-000E-FDCA-2> , <http://hdl.handle.net/21.T11991/0000-000E-FDCA-2> ;\n"
          + "        dcterms:modified             \"2017-09-08T16:22:20.312\" ;\n"
          + "        dcterms:source               \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-7BB9-CA6B-E86F-0\" ;\n"
          + "        premis:hasMessageDigest      \"8e4fc6a587b60d4c03286e92637298e8\" ;\n"
          + "        premis:hasMessageDigestOriginator\n"
          + "                \"dhcrud-base 7.15.4-SNAPSHOT.201709081607\" ;\n"
          + "        premis:hasMessageDigestType  \"md5\" .";
  private static final String ADM_MD_4_HTML_SUBCOLLECTION =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
          + "        a                            dariah:Collection ;\n"
          + "        dcterms:created              \"2017-09-08T16:22:20.312\" ;\n"
          + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:extent               \"384\" ;\n"
          + "        dcterms:format               \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:identifier           <http://dx.doi.org/10.20375/0000-000E-FDCA-2> , <http://hdl.handle.net/21.T11991/0000-000E-FDCA-2> ;\n"
          + "        dcterms:modified             \"2017-09-08T16:22:20.312\" ;\n"
          + "        dcterms:relation             <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-61EE-5> ;\n"
          + "        dcterms:source               \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-7BB9-CA6B-E86F-0\" ;\n"
          + "        premis:hasMessageDigest      \"8e4fc6a587b60d4c03286e92637298e8\" ;\n"
          + "        premis:hasMessageDigestOriginator\n"
          + "                \"dhcrud-base 7.15.4-SNAPSHOT.201709081607\" ;\n"
          + "        premis:hasMessageDigestType  \"md5\" .";
  private static final String METADATA_4_HTML_DATAOBJECT =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
          + "        a               dariah:DataObject ;\n" + "        dc:creator      \"fu\" ;\n"
          + "        dc:creator      \"orcid:0000-0003-1259-2288\" ;\n"
          + "        dc:creator      \"https://orcid.org/0000-0002-4019-9857\" ;\n"
          + "        dc:creator      \"gnd:116873574\" ;\n"
          + "        dc:description  \"urgl - argl - aua\" ;\n"
          + "        dc:description  \"murgl - margl - maua\" ;\n"
          + "        dc:format       \"img/jpeg\" ;\n"
          + "        dc:format       \"img/jpeg+irgendetwas\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-000E-FDCA-2> , <http://hdl.handle.net/21.T11991/0000-000E-FDCA-2> ;\n"
          + "        dc:identifier   <https://doi.org/10.12345/blubbidiblabbidi> ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.5072/0000-000E-FDCA-2> ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-61EE-5> ;\n"
          + "        dc:relation     \"https://bla.blubb/hier_ist_noch_eine_relation\" ;\n"
          + "        dc:relation     \"http://fugu.de\" ;\n"
          + "        dc:rights       \"free\" ;\n"
          + "        dc:rights       \"noch ein right\" ;\n"
          + "        dc:title        \"blu\" .";
  // private static final String DATA_ROOTCOLLECTION = "@prefix hdl: <http://hdl.handle.net/> .\n"
  // + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
  // + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
  // + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n" + "\n"
  // + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
  // + " a dariah:Collection ;\n"
  // + " dcterms:hasPart <http://hdl.handle.net/21.T11991/0000-0002-61F6-B> .\n";
  private static final String DATA_SUBCOLLECTION = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n" + "\n"
      + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
      + "        a                dariah:Collection ;\n"
      + "        dcterms:hasPart  <http://hdl.handle.net/21.T11991/0000-0002-61F6-B> .\n";
  private static final String ADM_MD_4_HTML_DATAOBJECT =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-000E-FDCA-2>\n"
          + "        a                            dariah:Collection ;\n"
          + "        dcterms:created              \"2017-09-08T16:22:20.312\" ;\n"
          + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:extent               \"384\" ;\n"
          + "        dcterms:format               \"img/jpeg\" ;\n"
          + "        dcterms:identifier           <http://dx.doi.org/10.20375/0000-000E-FDCA-2> , <http://hdl.handle.net/21.T11991/0000-000E-FDCA-2> ;\n"
          + "        dcterms:modified             \"2017-09-08T16:22:20.312\" ;\n"
          + "        dcterms:relation             <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-61EE-5> ;\n"
          + "        premis:hasMessageDigest      \"8e4fc6a587b60d4c03286e92637298e8\" ;\n"
          + "        premis:hasMessageDigestOriginator\n"
          + "                \"dhcrud-base 7.15.4-SNAPSHOT.201709081607\" ;\n"
          + "        premis:hasMessageDigestType  \"md5\" .";
  private static final String DATA_DATAOBJECT = "blabliblu";
  private static final String CRUD_LOCATION = "https://box/1.0/dhcrud/";
  private static final String DATACITE_LOCATION = "https://commons.datacite.org/doi.org/";
  private static final String DIGILIB_LOCATION = "https://trep.de.dariah.eu/1.0/digilib/rest/IIIF/";
  private static final String SWITCHBOARD_LOCATION =
      "https://switchboard.clarin-dev.eu/switchboard-test/#/dhrep/";
  private static final String MANIFEST_LOCATION = "https://testitest.de/1.0/iiif/manifests/";
  private static final String MIRADOR_LOCATION = "https://projectmirador.org/embed";
  private static final String TIFY_LOCATION = "https://tify.rocks";
  private static final String CRUD_VERSION = "testitest crudifugi v1.005";
  private static final String PID_RESOLVER = "https://hdl.handle.net/";
  private static final String PID_PREFIX = "21.T11991";
  private static final String DOI_RESOLVER = "https://dx.doi.org/";
  private static final String DOI_PREFIX = "10.20375";
  private static final String ORCID_RESOLVER = "https://orcid.org/";
  private static final String GND_RESOLVER = "http://d-nb.info/gnd/";
  private static final String OAI_HOST = "https://box/1.0/oaipmh/";
  private static final String PUBLIKATOR_LOCATION = "https://box/publikator";
  private static final String DOCUMENTATION = "https://documentation";
  private static final String API_DOCUMENTATION = "https://api/documentation";
  private static final String MAIL_OF_CONTACT = "funk@sub.uni-goettingen.de";
  private static final String NAME_OF_CONTACT = "Stefan E. Funk";
  private static final String FAQ_LOCATION = "https://faqs";
  private static final String IMPRINT_URL = "https://de.dariah.eu/impressum";
  private static final String PRIVACY_POLICY_URL = "https://de.dariah.eu/privacy-policy";
  private static final String CONTACT_URL = "https://de.dariah.eu/kontakt";
  private static final String MENU_HEADER_COLOR = "#123456";
  private static final String BADGE_TEXT = "none";
  private static final String OK = " ...OK";
  private static final String FAILED = " ...FAILED";
  private static final String HANDLE_DELETED_JSON =
      "{\"responseCode\":1,\"handle\":\"21.T11991/0000-001B-41BD-5\",\"values\":[{\"index\":99,\"type\":\"DELETED\",\"data\":{\"format\":\"string\",\"value\":\"Fugu Fischinger, Stefan E. Funk (2021). Deleted Dragon Collection. DARIAH-DE. https://doi.org/10.20375/0000-001b-41bd-5\"},\"ttl\":86400,\"timestamp\":\"2021-04-16T14:36:46Z\"}]}";

  /**
   * @throws IOException
   * @throws ParseException
   */
  @Test
  public void testRemoveJDINGSDA() throws IOException, ParseException {

    Model m = RDFUtils.readModel(URGLI);
    Model expectedModel = RDFUtils.readModel(URGLI_EXPECTED);
    String uri = "http://hdl.handle.net/11022/0000-0000-9AD2-5";

    // Get dcterms namespace prefix.
    String dcterms = m.getNsURIPrefix(RDFConstants.DCTERMS_NAMESPACE);

    // Delete dcterms:identifier from collection's and data objects'
    // metadata.
    RDFUtils.removeFromModel(m, uri, "j.0", RDFConstants.ELEM_DC_IDENTIFIER);
    // Remove dcterms namespace from metadata.
    m.removeNsPrefix(dcterms);

    if (!m.isIsomorphicWith(expectedModel)) {
      System.out.println("CREATED:\n" + RDFUtils.getTTLFromModel(m));
      System.out.println("CREATED:\n" + RDFUtils.getTTLFromModel(expectedModel));
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  @Test
  public void testGettingDoi() throws XMLStreamException, IOException, ParseException {

    Model model = RDFUtils.readModel(GETTING_DOI);
    String expectedUri = "http://dx.doi.org/10.3249/0000-0007-930D-5";

    String doiValue = RDFUtils.findFirstObject(model, null, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);

    if (!doiValue.equals(expectedUri)) {
      System.out.println(doiValue + " ≠ " + expectedUri);
      assertTrue(false);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IoFault
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws JSONException
   * @throws TransformerConfigurationException
   * @throws DatatypeConfigurationException
   * @throws ObjectNotFoundFault
   */
  @Test
  public void testCreateLandingHTML4Subcollection()
      throws IOException, IoFault, XMLStreamException, URISyntaxException, ParseException,
      TransformerConfigurationException, JSONException, SAXException, ParserConfigurationException,
      DatatypeConfigurationException, ObjectNotFoundFault {

    System.out.println("Creating landing HTML from some data and metadata (subcollection)... ");

    // Set some things.
    URI pid = URI.create("21.T11991/0000-000E-FDCA-2");
    String logID = "testCreateIndexHTML4Subcollection-" + System.currentTimeMillis();

    // Call HTML creation method.
    HashMap<String, Object> templateMap = DHCrudServiceUtilities.createHTMLPageHashMap(pid,
        RDFUtils.readModel(METADATA_4_HTML_SUBCOLLECTION, RDFConstants.TURTLE),
        RDFUtils.readModel(ADM_MD_4_HTML_SUBCOLLECTION, RDFConstants.TURTLE), DATA_SUBCOLLECTION,
        CRUD_VERSION, getConf(), logID);
    String landingHTML = DHCrudServiceImpl.getLandingHTMLFromTemplate(templateMap);

    String expectedLandingHtmlSubcollection = IOUtils.toString(new FileInputStream(
        new File("./src/test/resources/EXPECTED_LANDING_HTML_SUBCOLLECTION.html"))).trim();

    // Check for equality!
    if (landingHTML.trim().equals(expectedLandingHtmlSubcollection)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("EXPECTED:\n" + expectedLandingHtmlSubcollection);
      System.out.println("CREATED:\n" + landingHTML);
      assertTrue(false);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IoFault
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws JSONException
   * @throws TransformerConfigurationException
   * @throws DatatypeConfigurationException
   * @throws ObjectNotFoundFault
   */
  @Test
  public void testCreateLandingHTML4Rootcollection()
      throws IOException, IoFault, XMLStreamException, URISyntaxException, ParseException,
      TransformerConfigurationException, JSONException, SAXException, ParserConfigurationException,
      DatatypeConfigurationException, ObjectNotFoundFault {

    System.out.println("Creating landing HTML from some data and metadata (root collection)... ");

    // Set some things.
    URI pid = URI.create("21.T11991/0000-000E-FDCA-2");
    String logID = "testCreateLandingHTML4Rootcollection-" + System.currentTimeMillis();

    // Call HTML creation method.
    HashMap<String, Object> templateMap = DHCrudServiceUtilities.createHTMLPageHashMap(pid,
        RDFUtils.readModel(METADATA_4_HTML_ROOTCOLLECTION, RDFConstants.TURTLE),
        RDFUtils.readModel(ADM_MD_4_HTML_ROOTCOLLECTION, RDFConstants.TURTLE), DATA_SUBCOLLECTION,
        CRUD_VERSION, getConf(), logID);
    String landingHTML = DHCrudServiceImpl.getLandingHTMLFromTemplate(templateMap);

    String expectedLandingHtmlRootCollection = IOUtils.toString(new FileInputStream(
        new File("./src/test/resources/EXPECTED_LANDING_HTML_ROOTCOLLECTION.html"))).trim();

    // Check for equality!
    if (landingHTML.trim().equals(expectedLandingHtmlRootCollection)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("EXPECTED:\n" + expectedLandingHtmlRootCollection);
      System.out.println("CREATED:\n" + landingHTML);
      assertTrue(false);
    }
  }

  /**
   * @throws XMLStreamException
   * @throws IoFault
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws JSONException
   * @throws TransformerConfigurationException
   * @throws DatatypeConfigurationException
   * @throws ObjectNotFoundFault
   */
  @Test
  public void testCreateLandingHTML4Dataobject()
      throws IOException, IoFault, XMLStreamException, URISyntaxException, ParseException,
      TransformerConfigurationException, JSONException, SAXException, ParserConfigurationException,
      DatatypeConfigurationException, ObjectNotFoundFault {

    System.out.println("Creating landing HTML from some data and metadata (data object)... ");

    // Set some things.
    URI pid = URI.create("21.T11991/0000-000E-FDCA-2");
    String logID = "testCreateLandingHTML4Dataobject-" + System.currentTimeMillis();

    // Call HTML creation method.
    HashMap<String, Object> templateMap = DHCrudServiceUtilities.createHTMLPageHashMap(pid,
        RDFUtils.readModel(METADATA_4_HTML_DATAOBJECT, RDFConstants.TURTLE),
        RDFUtils.readModel(ADM_MD_4_HTML_DATAOBJECT, RDFConstants.TURTLE), DATA_DATAOBJECT,
        CRUD_VERSION, getConf(), logID);
    String landingHTML = DHCrudServiceImpl.getLandingHTMLFromTemplate(templateMap);

    String expectedLandingHtmlDataObject = IOUtils.toString(
        new FileInputStream(new File("./src/test/resources/EXPECTED_LANDING_HTML_DATAOBJECT.html")))
        .trim();

    // Check for equality!
    if (landingHTML.trim().equals(expectedLandingHtmlDataObject)) {
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("EXPECTED:\n" + expectedLandingHtmlDataObject);
      System.out.println("CREATED:\n" + landingHTML);
      assertTrue(false);
    }
  }

  /**
   * @throws IOException
   * @throws IoFault
   * @throws XMLStreamException
   * @throws URISyntaxException
   * @throws ParseException
   * @throws JSONException
   * @throws java.text.ParseException
   * @throws DatatypeConfigurationException
   */
  @Test
  public void testCreateTombstoneHTMLDeletedObject() throws IOException, IoFault,
      XMLStreamException, URISyntaxException, ParseException, JSONException,
      java.text.ParseException, DatatypeConfigurationException {

    System.out.println("Creating tombstone HTML from PID and DOI metadata...");

    URI pid = URI.create("21.T11991/0000-001B-41BD-5");

    // Create mustache template HashMap.
    HashMap<String, Object> templateMap =
        DHCrudServiceUtilities.createTombstoneHTMLPageHashMap(pid, getConf(), CRUD_VERSION,
            HANDLE_DELETED_JSON);
    String tombstoneHTML = DHCrudServiceImpl.getTombstoneHTMLFromTemplate(templateMap).trim();

    String expectedTombstoneHtmlDataObject = IOUtils
        .toString(
            new FileInputStream(new File("./src/test/resources/EXPECTED_TOMBSTONE_HTML.html")))
        .trim();

    // Check for equality!
    if (tombstoneHTML.equals(expectedTombstoneHtmlDataObject)) {
      // System.out.println("CREATED:\n" + tombstoneHTML);
      System.out.println(OK);
    } else {
      System.out.println(FAILED);
      System.out.println("EXPECTED:\n" + expectedTombstoneHtmlDataObject);
      System.out.println("CREATED:\n" + tombstoneHTML);
      // System.out.println(tombstoneHTML);

      // IOUtils.transferTo(new ByteArrayInputStream(expectedTombstoneHtmlDataObject.getBytes()),
      // new File("./data-expected.html"));
      // IOUtils.transferTo(new ByteArrayInputStream(tombstoneHTML.getBytes()),
      // new File("./tombstone.html"));

      assertTrue(false);
    }
  }

  // **
  // PRIVATES
  // **

  /**
   * @return
   * @throws IOException
   */
  private static CrudServiceConfigurator getConf() throws IOException {

    CrudServiceConfigurator result = CrudServiceConfigurator.getInstance("");
    result.setProperty("CRUD_LOCATION", CRUD_LOCATION);
    result.setProperty("PID_RESOLVER", PID_RESOLVER);
    result.setProperty("PID_PREFIX", PID_PREFIX);
    result.setProperty("DOI_RESOLVER", DOI_RESOLVER);
    result.setProperty("DOI_PREFIX", DOI_PREFIX);
    result.setProperty("DATACITE_LOCATION", DATACITE_LOCATION);
    result.setProperty("ORCID_RESOLVER", ORCID_RESOLVER);
    result.setProperty("GND_RESOLVER", GND_RESOLVER);
    result.setProperty("PUBLIKATOR_LOCATION", PUBLIKATOR_LOCATION);
    result.setProperty("MAIL_OF_CONTACT", MAIL_OF_CONTACT);
    result.setProperty("NAME_OF_CONTACT", NAME_OF_CONTACT);
    result.setProperty("FAQ_LOCATION", FAQ_LOCATION);
    result.setProperty("OAIPMH_LOCATION", OAI_HOST);
    result.setProperty("DOCUMENTATION_LOCATION", DOCUMENTATION);
    result.setProperty("API_DOCUMENTATION_LOCATION", API_DOCUMENTATION);
    result.setProperty("IMPRINT_URL", IMPRINT_URL);
    result.setProperty("PRIVACY_POLICY_URL", PRIVACY_POLICY_URL);
    result.setProperty("CONTACT_URL", CONTACT_URL);
    result.setProperty("DIGILIB_LOCATION", DIGILIB_LOCATION);
    result.setProperty("SWITCHBOARD_LOCATION", SWITCHBOARD_LOCATION);
    result.setProperty("MANIFEST_LOCATION", MANIFEST_LOCATION);
    result.setProperty("MIRADOR_LOCATION", MIRADOR_LOCATION);
    result.setProperty("TIFY_LOCATION", TIFY_LOCATION);
    result.setProperty("MENU_HEADER_COLOR", MENU_HEADER_COLOR);
    result.setProperty("BADGE_TEXT", BADGE_TEXT);

    return result;
  }

}
