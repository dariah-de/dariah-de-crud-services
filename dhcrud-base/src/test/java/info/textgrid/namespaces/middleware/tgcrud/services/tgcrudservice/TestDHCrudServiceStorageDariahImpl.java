/**
 * This software is copyright (c) 2023 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.tika.exception.TikaException;
import org.junit.Ignore;
import org.junit.Test;
import gov.loc.repository.bagit.domain.Bag;
import gov.loc.repository.bagit.exceptions.CorruptChecksumException;
import gov.loc.repository.bagit.exceptions.FileNotInPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.InvalidBagitFileFormatException;
import gov.loc.repository.bagit.exceptions.MaliciousPathException;
import gov.loc.repository.bagit.exceptions.MissingBagitFileException;
import gov.loc.repository.bagit.exceptions.MissingPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.MissingPayloadManifestException;
import gov.loc.repository.bagit.exceptions.UnsupportedAlgorithmException;
import gov.loc.repository.bagit.exceptions.VerificationException;
import gov.loc.repository.bagit.exceptions.conformance.BagitVersionIsNotAcceptableException;
import gov.loc.repository.bagit.exceptions.conformance.FetchFileNotAllowedException;
import gov.loc.repository.bagit.exceptions.conformance.MetatdataValueIsNotAcceptableException;
import gov.loc.repository.bagit.exceptions.conformance.MetatdataValueIsNotRepeatableException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredManifestNotPresentException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredMetadataFieldNotPresentException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredTagFileNotPresentException;
import gov.loc.repository.bagit.verify.BagVerifier;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.mail.util.ByteArrayDataSource;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestDHCrudServiceStorageDariahImpl {

  private static final boolean DELETE = true;
  private static final boolean DONT_EXTRACT_TECHMD = false;
  private static final String NO_LOCATION = "";
  private static final long NO_TIMEOUT = 0;
  private static final String STARS = "****************************************";
  private static final String OK = "...OK";
  private static final String CREATED = "CREATED";
  private static final String EXPECTED = "EXPECTED";
  private static final String DIFFERENCE = "DIFFERENCE";
  private static final String CRUD_SERVICE_VERSION = "dhcrud-base 7.17.3-SNAPSHOT.201709272251";

  private static final String DMD_COLLECTION =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E873-7>\n"
          + "        a              dariah:Collection ;\n" + "        dc:creator     \"GDZ\" ;\n"
          + "        dc:format      \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dc:identifier  <http://dx.doi.org/10.20375/0000-0002-E873-7> , <http://hdl.handle.net/21.T11991/0000-0002-E873-7> , <https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-0002-E873-7/index> ;\n"
          + "        dc:rights      \"LIMITED\" ;\n" + "        dc:title       \"DER KOFFER\" .\n";
  private static final String ADMMD_COLLECTION =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E873-7>\n"
          + "        a                            dariah:Collection ;\n"
          + "        dcterms:created              \"2017-09-28T13:50:33.959\" ;\n"
          + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:extent               \"384\" ;\n"
          + "        dcterms:format               \"text/vnd.dariah.dhrep.collection+turtle\" ;\n"
          + "        dcterms:identifier           <http://hdl.handle.net/21.T11991/0000-0002-E873-7> , <http://dx.doi.org/10.20375/0000-0002-E873-7> ;\n"
          + "        dcterms:modified             \"2017-09-28T13:50:33.959\" ;\n"
          + "        dcterms:source               \"https://cdstar.de.dariah.eu/test/dariah/EAEA0-E382-4AD9-52D4-0\" ;\n"
          + "        premis:hasMessageDigest      \"c2b038d8c3cd98fd49ddcc0aa82055c4\" ;\n"
          + "        premis:hasMessageDigestOriginator\n"
          + "                \"dhcrud-base 7.17.3-SNAPSHOT.201709272251\" ;\n"
          + "        premis:hasMessageDigestType  \"md5\" .";
  private static final String TECHMD_COLLECTION = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      + "<fits xmlns=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd\" version=\"1.1.0\" timestamp=\"28.09.17 13:50\">\n"
      + "  <identification>\n"
      + "    <identity format=\"Plain text\" mimetype=\"text/plain\" toolname=\"FITS\" toolversion=\"1.1.0\">\n"
      + "      <tool toolname=\"Droid\" toolversion=\"6.1.5\" />\n"
      + "      <tool toolname=\"Jhove\" toolversion=\"1.16\" />\n"
      + "      <tool toolname=\"file utility\" toolversion=\"5.14\" />\n"
      + "      <externalIdentifier toolname=\"Droid\" toolversion=\"6.1.5\" type=\"puid\">x-fmt/111</externalIdentifier>\n"
      + "    </identity>\n" + "  </identification>\n" + "  <fileinfo>\n"
      + "    <size toolname=\"Jhove\" toolversion=\"1.16\">384</size>\n"
      + "    <filepath toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">/tmp/tomcat-crud-tmp/dhrep_aa7262cea9144e388f92dc3b6935cfa7/dhrep_21.T11991_0000-0002-E873-7.bagit/data.txt</filepath>\n"
      + "    <filename toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">data.txt</filename>\n"
      + "    <md5checksum toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">c2b038d8c3cd98fd49ddcc0aa82055c4</md5checksum>\n"
      + "    <fslastmodified toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">1506599433000</fslastmodified>\n"
      + "  </fileinfo>\n" + "  <filestatus>\n"
      + "    <well-formed toolname=\"Jhove\" toolversion=\"1.16\" status=\"SINGLE_RESULT\">true</well-formed>\n"
      + "    <valid toolname=\"Jhove\" toolversion=\"1.16\" status=\"SINGLE_RESULT\">true</valid>\n"
      + "  </filestatus>\n" + "  <metadata>\n" + "    <text>\n"
      + "      <linebreak toolname=\"Jhove\" toolversion=\"1.16\" status=\"SINGLE_RESULT\">LF</linebreak>\n"
      + "      <charset toolname=\"Jhove\" toolversion=\"1.16\">US-ASCII</charset>\n"
      + "      <standard>\n" + "        <textMD:textMD xmlns:textMD=\"info:lc/xmlns/textMD-v3\">\n"
      + "          <textMD:character_info>\n"
      + "            <textMD:charset>US-ASCII</textMD:charset>\n"
      + "            <textMD:linebreak>LF</textMD:linebreak>\n"
      + "          </textMD:character_info>\n" + "        </textMD:textMD>\n"
      + "      </standard>\n" + "    </text>\n" + "  </metadata>\n"
      + "  <statistics fitsExecutionTime=\"78\">\n"
      + "    <tool toolname=\"MediaInfo\" toolversion=\"0.7.75\" status=\"did not run\" />\n"
      + "    <tool toolname=\"OIS Audio Information\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"ADL Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"VTT Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"Droid\" toolversion=\"6.1.5\" executionTime=\"26\" />\n"
      + "    <tool toolname=\"Jhove\" toolversion=\"1.16\" executionTime=\"40\" />\n"
      + "    <tool toolname=\"file utility\" toolversion=\"5.14\" executionTime=\"70\" />\n"
      + "    <tool toolname=\"Exiftool\" toolversion=\"10.00\" status=\"did not run\" />\n"
      + "    <tool toolname=\"NLNZ Metadata Extractor\" toolversion=\"3.6GA\" status=\"did not run\" />\n"
      + "    <tool toolname=\"OIS File Information\" toolversion=\"0.2\" executionTime=\"13\" />\n"
      + "    <tool toolname=\"OIS XML Metadata\" toolversion=\"0.2\" status=\"did not run\" />\n"
      + "    <tool toolname=\"ffident\" toolversion=\"0.2\" executionTime=\"14\" />\n"
      + "    <tool toolname=\"Tika\" toolversion=\"1.10\" executionTime=\"21\" />\n"
      + "  </statistics>\n" + "</fits>";
  private static final String DATA_COLLECTION = "@prefix hdl:   <http://hdl.handle.net/> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
      + "@prefix dariahstorage: <https://de.dariah.eu/storage/> .\n" + "\n"
      + "<http://hdl.handle.net/21.T11991/0000-0002-E873-7>\n"
      + "        a                dariah:Collection ;\n"
      + "        dcterms:hasPart  <http://hdl.handle.net/21.T11991/0000-0002-E874-6> .\n";
  private static final String DMD_DATAOBJECT =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E874-6>\n"
          + "        a               dariah:DataObject ;\n" + "        dc:creator      \"GDZ\" ;\n"
          + "        dc:date         \"2016-01-20T16:21:15\" ;\n"
          + "        dc:description  \"Ein Kofferbild vom Hermes-Koffer\" ;\n"
          + "        dc:format       \"image/tiff\" ;\n"
          + "        dc:identifier   <http://dx.doi.org/10.20375/0000-0002-E874-6> , <http://hdl.handle.net/21.T11991/0000-0002-E874-6> , <https://trep.de.dariah.eu/1.0/dhcrud/21.T11991/0000-0002-E874-6/index> ;\n"
          + "        dc:relation     <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-E873-7> ;\n"
          + "        dc:rights       \"restricted\" ;\n"
          + "        dc:title        \"00000099.tif\" .\n" + "";
  private static final String ADMMD_DATAOBJECT =
      "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
          + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0002-E874-6>\n"
          + "        a                            dariah:DataObject ;\n"
          + "        dcterms:created              \"2017-09-28T13:50:39.610\" ;\n"
          + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
          + "        dcterms:extent               \"127476104\" ;\n"
          + "        dcterms:format               \"image/tiff\" ;\n"
          + "        dcterms:identifier           <http://hdl.handle.net/21.T11991/0000-0002-E874-6> , <http://dx.doi.org/10.20375/0000-0002-E874-6> ;\n"
          + "        dcterms:modified             \"2017-09-28T13:50:39.610\" ;\n"
          + "        dcterms:relation             <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0002-E873-7> ;\n"
          + "        premis:hasMessageDigest      \"bc319383ffeee03175c44f5cc650e4d6\" ;\n"
          + "        premis:hasMessageDigestOriginator\n"
          + "                \"dhcrud-base 7.17.3-SNAPSHOT.201709272251\" ;\n"
          + "        premis:hasMessageDigestType  \"md5\" .\n";
  private static final String TECHMD_DATAOBJECT = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      + "<fits xmlns=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd\" version=\"1.1.0\" timestamp=\"28.09.17 13:50\">\n"
      + "  <identification status=\"SINGLE_RESULT\">\n"
      + "    <identity format=\"TIFF EXIF\" mimetype=\"image/tiff\" toolname=\"FITS\" toolversion=\"1.1.0\">\n"
      + "      <tool toolname=\"Exiftool\" toolversion=\"10.00\" />\n" + "    </identity>\n"
      + "  </identification>\n" + "  <fileinfo>\n"
      + "    <size toolname=\"Jhove\" toolversion=\"1.16\">127476104</size>\n"
      + "    <creatingApplicationName toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">Digibook Version :5, 6, 2, 0</creatingApplicationName>\n"
      + "    <lastmodified toolname=\"Exiftool\" toolversion=\"10.00\" status=\"CONFLICT\">2016:01:20 15:21:15</lastmodified>\n"
      + "    <lastmodified toolname=\"Tika\" toolversion=\"1.10\" status=\"CONFLICT\">2016-01-20T15:21:15</lastmodified>\n"
      + "    <filepath toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">/tmp/tomcat-crud-tmp/dhrep_86914b0beb534bbab1e990d6c4c7c4e3/dhrep_21.T11991_0000-0002-E874-6.bagit/data.tiff</filepath>\n"
      + "    <filename toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">data.tiff</filename>\n"
      + "    <md5checksum toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">bc319383ffeee03175c44f5cc650e4d6</md5checksum>\n"
      + "    <fslastmodified toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">1506599447000</fslastmodified>\n"
      + "  </fileinfo>\n" + "  <filestatus />\n" + "  <metadata>\n" + "    <image>\n"
      + "      <compressionScheme toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">Uncompressed</compressionScheme>\n"
      + "      <imageWidth toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">7296</imageWidth>\n"
      + "      <imageHeight toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">5824</imageHeight>\n"
      + "      <colorSpace toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">RGB</colorSpace>\n"
      + "      <orientation toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">normal*</orientation>\n"
      + "      <xSamplingFrequency toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">300</xSamplingFrequency>\n"
      + "      <ySamplingFrequency toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">300</ySamplingFrequency>\n"
      + "      <bitsPerSample toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">8 8 8</bitsPerSample>\n"
      + "      <samplesPerPixel toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">3</samplesPerPixel>\n"
      + "      <scannerManufacturer toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">i2s Digibook Scanner</scannerManufacturer>\n"
      + "      <scannerModelName toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">Suprascan 6002 RGB</scannerModelName>\n"
      + "      <scanningSoftwareName toolname=\"Exiftool\" toolversion=\"10.00\" status=\"SINGLE_RESULT\">Digibook Version :5, 6, 2, 0</scanningSoftwareName>\n"
      + "      <standard>\n" + "        <mix:mix xmlns:mix=\"http://www.loc.gov/mix/v20\">\n"
      + "          <mix:BasicDigitalObjectInformation>\n" + "            <mix:Compression>\n"
      + "              <mix:compressionScheme>Uncompressed</mix:compressionScheme>\n"
      + "            </mix:Compression>\n" + "          </mix:BasicDigitalObjectInformation>\n"
      + "          <mix:BasicImageInformation>\n" + "            <mix:BasicImageCharacteristics>\n"
      + "              <mix:imageWidth>7296</mix:imageWidth>\n"
      + "              <mix:imageHeight>5824</mix:imageHeight>\n"
      + "              <mix:PhotometricInterpretation>\n"
      + "                <mix:colorSpace>RGB</mix:colorSpace>\n"
      + "              </mix:PhotometricInterpretation>\n"
      + "            </mix:BasicImageCharacteristics>\n"
      + "          </mix:BasicImageInformation>\n" + "          <mix:ImageCaptureMetadata>\n"
      + "            <mix:GeneralCaptureInformation />\n" + "            <mix:ScannerCapture>\n"
      + "              <mix:scannerManufacturer>i2s Digibook Scanner</mix:scannerManufacturer>\n"
      + "              <mix:ScannerModel>\n"
      + "                <mix:scannerModelName>Suprascan 6002 RGB</mix:scannerModelName>\n"
      + "              </mix:ScannerModel>\n" + "              <mix:ScanningSystemSoftware>\n"
      + "                <mix:scanningSoftwareName>Digibook Version :5, 6, 2, 0</mix:scanningSoftwareName>\n"
      + "              </mix:ScanningSystemSoftware>\n" + "            </mix:ScannerCapture>\n"
      + "            <mix:orientation>normal*</mix:orientation>\n"
      + "          </mix:ImageCaptureMetadata>\n" + "          <mix:ImageAssessmentMetadata>\n"
      + "            <mix:SpatialMetrics>\n" + "              <mix:xSamplingFrequency>\n"
      + "                <mix:numerator>300</mix:numerator>\n"
      + "                <mix:denominator>1</mix:denominator>\n"
      + "              </mix:xSamplingFrequency>\n" + "              <mix:ySamplingFrequency>\n"
      + "                <mix:numerator>300</mix:numerator>\n"
      + "                <mix:denominator>1</mix:denominator>\n"
      + "              </mix:ySamplingFrequency>\n" + "            </mix:SpatialMetrics>\n"
      + "            <mix:ImageColorEncoding>\n" + "              <mix:BitsPerSample>\n"
      + "                <mix:bitsPerSampleValue>8</mix:bitsPerSampleValue>\n"
      + "                <mix:bitsPerSampleValue>8</mix:bitsPerSampleValue>\n"
      + "                <mix:bitsPerSampleValue>8</mix:bitsPerSampleValue>\n"
      + "                <mix:bitsPerSampleUnit>integer</mix:bitsPerSampleUnit>\n"
      + "              </mix:BitsPerSample>\n"
      + "              <mix:samplesPerPixel>3</mix:samplesPerPixel>\n"
      + "            </mix:ImageColorEncoding>\n" + "          </mix:ImageAssessmentMetadata>\n"
      + "        </mix:mix>\n" + "      </standard>\n" + "    </image>\n" + "  </metadata>\n"
      + "  <statistics fitsExecutionTime=\"2281\">\n"
      + "    <tool toolname=\"MediaInfo\" toolversion=\"0.7.75\" status=\"did not run\" />\n"
      + "    <tool toolname=\"OIS Audio Information\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"ADL Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"VTT Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"Droid\" toolversion=\"6.1.5\" executionTime=\"15\" />\n"
      + "    <tool toolname=\"Jhove\" toolversion=\"1.16\" executionTime=\"89\" />\n"
      + "    <tool toolname=\"file utility\" toolversion=\"5.14\" executionTime=\"83\" />\n"
      + "    <tool toolname=\"Exiftool\" toolversion=\"10.00\" executionTime=\"232\" />\n"
      + "    <tool toolname=\"NLNZ Metadata Extractor\" toolversion=\"3.6GA\" status=\"did not run\" />\n"
      + "    <tool toolname=\"OIS File Information\" toolversion=\"0.2\" executionTime=\"819\" />\n"
      + "    <tool toolname=\"OIS XML Metadata\" toolversion=\"0.2\" status=\"did not run\" />\n"
      + "    <tool toolname=\"ffident\" toolversion=\"0.2\" executionTime=\"8\" />\n"
      + "    <tool toolname=\"Tika\" toolversion=\"1.10\" executionTime=\"2254\" />\n"
      + "  </statistics>\n" + "</fits>";
  private static final String LOGID = "012345ABCDEFG";
  private static final String ADMMD_XLSX = "@prefix premis: <http://www.loc.gov/premis/rdf/v1#> .\n"
      + "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
      + "@prefix dcterms: <http://purl.org/dc/terms/> .\n"
      + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n" + "\n"
      + "<http://hdl.handle.net/21.T11991/0000-0007-A3A2-9>\n"
      + "        a                            dariah:DataObject ;\n"
      + "        dcterms:created              \"2018-01-26T13:05:05.918\" ;\n"
      + "        dcterms:creator              \"StefanFunk@dariah.eu\" ;\n"
      + "        dcterms:extent               \"8260\" ;\n"
      + "        dcterms:format               \"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" ;\n"
      + "        dcterms:identifier           <http://dx.doi.org/10.5072/0000-0007-A3A2-9> , <http://hdl.handle.net/21.T11991/0000-0007-A3A2-9> ;\n"
      + "        dcterms:modified             \"2018-01-26T13:05:05.918\" ;\n"
      + "        dcterms:relation             <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0007-A3A1-A> ;\n"
      + "        premis:hasMessageDigest      \"9edb4e8b337f308b5ed15b559d541f31\" ;\n"
      + "        premis:hasMessageDigestOriginator\n"
      + "                \"dhcrud-base 7.17.3-SNAPSHOT.201709272251\" ;\n"
      + "        premis:hasMessageDigestType  \"md5\" .";
  private static final String DMD_XLSX =
      "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n"
          + "@prefix dariah: <http://de.dariah.eu/rdf/dataobjects/terms/> .\n"
          + "@prefix dc:    <http://purl.org/dc/elements/1.1/> .\n" + "\n"
          + "<http://hdl.handle.net/21.T11991/0000-0007-A3A2-9>\n"
          + "        a              dariah:DataObject ;\n" + "        dc:creator     \"fu\" ;\n"
          + "        dc:format      \"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" ;\n"
          + "        dc:identifier  <http://hdl.handle.net/21.T11991/0000-0007-A3A2-9> , <http://dx.doi.org/10.5072/0000-0007-A3A2-9> ;\n"
          + "        dc:relation    <http://de.dariah.eu/rdf/dataobjects/terms/Collection/21.T11991/0000-0007-A3A1-A> ;\n"
          + "        dc:rights      \"free\" ;\n" + "        dc:title       \"furgli.xlsx\" .";
  private static final String TECHMD_XLSX = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      + "<fits xmlns=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://hul.harvard.edu/ois/xml/ns/fits/fits_output http://hul.harvard.edu/ois/xml/xsd/fits/fits_output.xsd\" version=\"1.1.0\" timestamp=\"26.01.18 13:05\">\n"
      + "  <identification status=\"CONFLICT\">\n"
      + "    <identity format=\"Office Open XML Document\" mimetype=\"application/vnd.openxmlformats-officedocument.wordprocessingml.document\" toolname=\"FITS\" toolversion=\"1.1.0\">\n"
      + "      <tool toolname=\"Droid\" toolversion=\"6.1.5\" />\n"
      + "      <version toolname=\"Droid\" toolversion=\"6.1.5\">2007 onwards</version>\n"
      + "      <externalIdentifier toolname=\"Droid\" toolversion=\"6.1.5\" type=\"puid\">fmt/189</externalIdentifier>\n"
      + "    </identity>\n"
      + "    <identity format=\"Microsoft Excel 2007+\" mimetype=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" toolname=\"FITS\" toolversion=\"1.1.0\">\n"
      + "      <tool toolname=\"file utility\" toolversion=\"5.14\" />\n" + "    </identity>\n"
      + "    <identity format=\"Office Open XML Workbook\" mimetype=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" toolname=\"FITS\" toolversion=\"1.1.0\">\n"
      + "      <tool toolname=\"Tika\" toolversion=\"1.10\" />\n" + "    </identity>\n"
      + "  </identification>\n" + "  <fileinfo>\n"
      + "    <lastmodified toolname=\"Tika\" toolversion=\"1.10\" status=\"SINGLE_RESULT\">2018-01-26T12:00:32Z</lastmodified>\n"
      + "    <creatingApplicationName toolname=\"Tika\" toolversion=\"1.10\" status=\"SINGLE_RESULT\">Microsoft Macintosh Excel</creatingApplicationName>\n"
      + "    <creatingApplicationVersion toolname=\"Tika\" toolversion=\"1.10\" status=\"SINGLE_RESULT\">16.0300</creatingApplicationVersion>\n"
      + "    <filepath toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">/tmp/tomcat-crud-tmp/dhrep_43d27c3bf0b749e8baa79ef9ea115f25/dhrep_21.T11991_0000-0007-A3A2-9.bagit/data</filepath>\n"
      + "    <filename toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">data</filename>\n"
      + "    <size toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">8260</size>\n"
      + "    <md5checksum toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">9edb4e8b337f308b5ed15b559d541f31</md5checksum>\n"
      + "    <fslastmodified toolname=\"OIS File Information\" toolversion=\"0.2\" status=\"SINGLE_RESULT\">1516968305000</fslastmodified>\n"
      + "  </fileinfo>\n" + "  <filestatus />\n" + "  <metadata />\n"
      + "  <statistics fitsExecutionTime=\"2433\">\n"
      + "    <tool toolname=\"MediaInfo\" toolversion=\"0.7.75\" status=\"did not run\" />\n"
      + "    <tool toolname=\"OIS Audio Information\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"ADL Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"VTT Tool\" toolversion=\"0.1\" status=\"did not run\" />\n"
      + "    <tool toolname=\"Droid\" toolversion=\"6.1.5\" executionTime=\"16\" />\n"
      + "    <tool toolname=\"Jhove\" toolversion=\"1.16\" executionTime=\"162\" />\n"
      + "    <tool toolname=\"file utility\" toolversion=\"5.14\" executionTime=\"158\" />\n"
      + "    <tool toolname=\"Exiftool\" toolversion=\"10.00\" executionTime=\"353\" />\n"
      + "    <tool toolname=\"NLNZ Metadata Extractor\" toolversion=\"3.6GA\" status=\"did not run\" />\n"
      + "    <tool toolname=\"OIS File Information\" toolversion=\"0.2\" executionTime=\"5\" />\n"
      + "    <tool toolname=\"OIS XML Metadata\" toolversion=\"0.2\" status=\"did not run\" />\n"
      + "    <tool toolname=\"ffident\" toolversion=\"0.2\" executionTime=\"26\" />\n"
      + "    <tool toolname=\"Tika\" toolversion=\"1.10\" executionTime=\"2398\" />\n"
      + "  </statistics>\n" + "</fits>";
  private final static String DOI_API_LOCATION = "https://api.test.datacite.org/";

  /**
   * @throws IoFault
   * @throws TikaException
   * @throws IOException
   * @throws NoSuchAlgorithmException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws KeyStoreException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testCompleteBagAndZippingCollection()
      throws IOException, TikaException, IoFault, NoSuchAlgorithmException, KeyManagementException,
      KeyStoreException, URISyntaxException, org.json.simple.parser.ParseException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test creating THE BAG with small data");
    System.out.println(STARS);

    URI uri = URI.create("hdl:21.T11991/0000-0002-E873-7");
    DataHandler data = new DataHandler(new ByteArrayDataSource(DATA_COLLECTION.getBytes(), ""));

    createTestBag(uri, data, DMD_COLLECTION, ADMMD_COLLECTION, TECHMD_COLLECTION);
  }

  /**
   * @throws IoFault
   * @throws TikaException
   * @throws IOException
   * @throws NoSuchAlgorithmException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws KeyStoreException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testCompleteBagAndZippingLargeData()
      throws IOException, TikaException, IoFault, NoSuchAlgorithmException, KeyManagementException,
      KeyStoreException, URISyntaxException, org.json.simple.parser.ParseException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test creating THE BAG with large data");
    System.out.println(STARS);

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DataHandler data =
        new DataHandler(new FileDataSource(new File("./src/test/resources/00000099_LARGE.tif")));

    createTestBag(uri, data, DMD_DATAOBJECT, ADMMD_DATAOBJECT, TECHMD_DATAOBJECT);
  }

  /**
   * <p>
   * For XLSC Tika cannot create a file suffix from the file type, so we are testing here what
   * happens if we get no file suffixes from the Tika!
   * </p>
   *
   * @throws IoFault
   * @throws TikaException
   * @throws IOException
   * @throws NoSuchAlgorithmException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws KeyStoreException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testCompleteBagAndZippingXLSX()
      throws IOException, TikaException, IoFault, NoSuchAlgorithmException, KeyManagementException,
      KeyStoreException, URISyntaxException, org.json.simple.parser.ParseException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test creating THE BAG with XLSX file");
    System.out.println(STARS);

    URI uri = URI.create("hdl:21.T11991/0000-0007-A3A2-9");
    DataHandler data =
        new DataHandler(new FileDataSource(new File("./src/test/resources/furgli.xlsx")));

    createTestBag(uri, data, DMD_XLSX, ADMMD_XLSX, TECHMD_XLSX);
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws IoFault
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadIndexFileLargeData() throws FileNotFoundException, IOException,
      ParseException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IoFault,
      URISyntaxException, org.json.simple.parser.ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading index file with large data");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E874-6.bagit_LARGE.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zip));
    System.out.println("Reading metadata...");

    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    zipStream.close();

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    checkMD(object.getMetadata(), RDFUtils.readModel(DMD_DATAOBJECT, RDFConstants.TURTLE), "DMD");

    // Create ZIP stream. Again. Reading ADMMD.
    start = System.currentTimeMillis();
    zipStream = new ZipInputStream(new FileInputStream(zip));
    System.out.println("Reading ADMMD...");

    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    zipStream.close();

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    checkMD(object.getAdmMD(), RDFUtils.readModel(ADMMD_DATAOBJECT, RDFConstants.TURTLE), "ADMMD");

    // Create ZIP stream. Again. Reading TECHMD.
    start = System.currentTimeMillis();
    zipStream = new ZipInputStream(new FileInputStream(zip));
    System.out.println("Reading TECHMD...");

    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    zipStream.close();

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    checkTECHMD(object.getTechMD(), TECHMD_DATAOBJECT);

    // Create ZIP stream. Again. Reading data file.
    start = System.currentTimeMillis();
    System.out.print("Reading data file...");

    addMetadataAndData(zip, object, LOGID);

    int av = 0;
    int count = 0;
    InputStream in = object.getData().getInputStream();
    while ((av = in.available()) != 0) {
      count += av;
      in.skip(av);
    }
    in.close();

    System.out.println(" skipped " + count + " bytes");

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadDMDLargeData() throws FileNotFoundException, IOException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading DMD with large data");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E874-6.bagit_LARGE.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zip));
    System.out.println("Reading metadata...");

    System.out.println("PID: " + uri);

    // Get metadata from public DARIAH storage using the PID.
    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();

    // Check for empty TECH_MD!
    if (dmd == null || dmd.isEmpty()) {
      System.out.println("DMD empty!");
      assertTrue(false);
    }
    if (admMd == null || admMd.isEmpty()) {
      System.out.println("ADM_MD empty!");
      assertTrue(false);
    }
    if (techMd == null || techMd.isEmpty()) {
      System.out.println("TECH_MD empty!");
      assertTrue(false);
    }

    System.out.println("LastModified: " + object.getLastModifiedDate());
    System.out.println("DMD:\n" + RDFUtils.getTTLFromModel(dmd));

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadADMMDLargeData() throws FileNotFoundException, IOException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading ADMMD with large data");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E874-6.bagit_LARGE.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zip));
    System.out.println("Reading metadata...");

    System.out.println("PID: " + uri);

    // Get metadata from public DARIAH storage using the PID.
    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();

    // Check for empty TECH_MD!
    if (dmd == null || dmd.isEmpty()) {
      System.out.println("DMD empty!");
      assertTrue(false);
    }
    if (admMd == null || admMd.isEmpty()) {
      System.out.println("ADM_MD empty!");
      assertTrue(false);
    }
    if (techMd == null || techMd.isEmpty()) {
      System.out.println("TECH_MD empty!");
      assertTrue(false);
    }

    System.out.println("LastModified: " + object.getLastModifiedDate());
    System.out.println("ADMMD:\n" + RDFUtils.getTTLFromModel(admMd));

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadTECHMDLargeData() throws FileNotFoundException, IOException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading TECHMD with large data");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E874-6.bagit_LARGE.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zip));
    System.out.println("Reading metadata...");

    System.out.println("PID: " + uri);

    // Get metadata from public DARIAH storage using the PID.
    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();

    // Check for empty TECH_MD!
    if (dmd == null || dmd.isEmpty()) {
      System.out.println("DMD empty!");
      assertTrue(false);
    }
    if (admMd == null || admMd.isEmpty()) {
      System.out.println("ADM_MD empty!");
      assertTrue(false);
    }
    if (techMd == null || techMd.isEmpty()) {
      System.out.println("TECH_MD empty!");
      assertTrue(false);
    }

    System.out.println("LastModified: " + object.getLastModifiedDate());
    System.out.println("TECHMD:\n" + techMd);

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws IoFault
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadDATALargeData() throws FileNotFoundException, IOException, ParseException,
      KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IoFault,
      URISyntaxException, org.json.simple.parser.ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading DATA with large data");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E874-6.bagit_LARGE.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    System.out.println("Reading everything data and metadata...");

    System.out.println("PID: " + uri);

    // Get all data and metadata from ZIP file.
    addMetadataAndData(zip, object, LOGID);
    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();
    int data = object.getData().getInputStream().available();

    // Check for empty TECH_MD!
    if (dmd == null || dmd.isEmpty()) {
      System.out.println("DMD empty!");
      assertTrue(false);
    }
    if (admMd == null || admMd.isEmpty()) {
      System.out.println("ADM_MD empty!");
      assertTrue(false);
    }
    if (techMd == null || techMd.isEmpty()) {
      System.out.println("TECH_MD empty!");
      assertTrue(false);
    }

    System.out.println("LastModified: " + object.getLastModifiedDate());
    System.out.println("DATA: " + data);

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws URISyntaxException
   * @throws IoFault
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadCOLDATANocolLargeData()
      throws FileNotFoundException, IOException, ParseException, KeyManagementException,
      NoSuchAlgorithmException, KeyStoreException, IoFault, URISyntaxException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading collection DATA with large data (no collection)");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E874-6");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E874-6.bagit_LARGE.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    System.out.println("Reading metadata...");

    System.out.println("PID: " + uri);

    // Get metadata from public DARIAH storage using the PID.
    File tempDataFile = addMetadataAndCollectionData(zip, object, LOGID);

    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();

    if (object.getData() != null) {
      System.out.println("Non collection should not have been read!");
      boolean deleted = tempDataFile.delete();
      if (!deleted) {
        assertTrue(false);
      }
      assertTrue(false);
    }

    // Check for empty TECH_MD!
    if (dmd == null || dmd.isEmpty()) {
      System.out.println("DMD empty!");
      assertTrue(false);
    }
    if (admMd == null || admMd.isEmpty()) {
      System.out.println("ADM_MD empty!");
      assertTrue(false);
    }
    if (techMd == null || techMd.isEmpty()) {
      System.out.println("TECH_MD empty!");
      assertTrue(false);
    }

    System.out.println("LastModified: " + object.getLastModifiedDate());

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws URISyntaxException
   * @throws IoFault
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadCOLDATAColLargeData()
      throws FileNotFoundException, IOException, ParseException, KeyManagementException,
      NoSuchAlgorithmException, KeyStoreException, IoFault, URISyntaxException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading collection DATA with large data (collection)");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-F1BD-9");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip =
        new File("./src/test/resources/COLLECTION_dhrep_21.T11991_0000-0002-F1BD-9.bagit.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    System.out.println("Reading metadata and collection data...");

    System.out.println("PID: " + uri);

    // Get metadata from public DARIAH storage using the PID (including collection data).
    File tempDataFile = addMetadataAndData(zip, object, LOGID);
    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();
    String data = IOUtils.toString(object.getData().getInputStream(), "UTF-8");

    // Check for empty TECH_MD!
    if (dmd == null || dmd.isEmpty()) {
      System.out.println("DMD empty!");
      assertTrue(false);
    }
    if (admMd == null || admMd.isEmpty()) {
      System.out.println("ADM_MD empty!");
      assertTrue(false);
    }
    if (techMd == null || techMd.isEmpty()) {
      System.out.println("TECH_MD empty!");
      assertTrue(false);
    }

    System.out.println("LastModified: " + object.getLastModifiedDate());
    System.out.println("DATA:\n" + data);

    boolean deleted = tempDataFile.delete();
    if (!deleted) {
      assertTrue(false);
    }

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws IoFault
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testReadIndexFileSmallData() throws FileNotFoundException, IOException,
      ParseException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IoFault,
      URISyntaxException, org.json.simple.parser.ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test reading index file with small data");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create("hdl:21.T11991/0000-0002-E873-7");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.T11991_0000-0002-E873-7.bagit_SMALL.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading DMD.
    long start = System.currentTimeMillis();
    System.out.println("Reading metadata...");

    addMetadataOnly(zip, object, LOGID);

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    checkMD(object.getMetadata(), RDFUtils.readModel(DMD_COLLECTION, RDFConstants.TURTLE), "DMD");

    // Create ZIP stream. Again. Reading ADMMD.
    start = System.currentTimeMillis();
    System.out.println("Reading ADMMD...");

    addMetadataOnly(zip, object, LOGID);

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    checkMD(object.getAdmMD(), RDFUtils.readModel(ADMMD_COLLECTION, RDFConstants.TURTLE), "ADMMD");

    // Create ZIP stream. Again. Reading TECHMD.
    start = System.currentTimeMillis();
    System.out.println("Reading TECHMD...");

    addMetadataOnly(zip, object, LOGID);

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    checkTECHMD(object.getTechMD(), TECHMD_COLLECTION);

    // Create ZIP stream. Again. Reading data file.
    start = System.currentTimeMillis();
    System.out.print("Reading data file...");

    File tempDataFile = addMetadataAndData(zip, object, LOGID);

    int av = 0;
    int count = 0;
    InputStream in = object.getData().getInputStream();
    while ((av = in.available()) != 0) {
      count += av;
      in.skip(av);
    }
    in.close();

    System.out.println(" skipped " + count + " bytes");

    boolean deleted = tempDataFile.delete();
    if (!deleted) {
      assertTrue(false);
    }

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");

    // FIXME Check things!
  }

  /**
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  public void testRdfFormatOutput()
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Testing RDF outputs");
    System.out.println(STARS);

    Model dmd = RDFUtils.readModel(DMD_COLLECTION, RDFConstants.TURTLE);
    RDFUtils.getStringFromModel(dmd, RDFConstants.TURTLE);
    RDFUtils.getStringFromModel(dmd, RDFConstants.RDF_XML);
    RDFUtils.getStringFromModel(dmd, RDFConstants.RDF_JSON);
    RDFUtils.getStringFromModel(dmd, RDFConstants.NTRIPLES);
    RDFUtils.getStringFromModel(dmd, RDFConstants.JSON_LD);

    Model admmd = RDFUtils.readModel(ADMMD_COLLECTION, RDFConstants.TURTLE);
    RDFUtils.getStringFromModel(admmd, RDFConstants.TURTLE);
    RDFUtils.getStringFromModel(admmd, RDFConstants.RDF_XML);
    RDFUtils.getStringFromModel(admmd, RDFConstants.RDF_JSON);
    RDFUtils.getStringFromModel(admmd, RDFConstants.NTRIPLES);
    RDFUtils.getStringFromModel(admmd, RDFConstants.JSON_LD);

    System.out.println("...complete");
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theObject
   * @param theZippedBag
   * @throws IOException
   */
  private static void checkEntriesOrder(CrudObject<Model> theObject, File theZippedBag)
      throws IOException {

    System.out.println("Checking zipped entries' order...");

    // Get ZIP stream.
    ZipInputStream bagStream = new ZipInputStream(new FileInputStream(theZippedBag));

    // Go through every ZIP entry: Read metadata first!
    ZipEntry entry;
    int count = 0;
    while ((entry = bagStream.getNextEntry()) != null) {
      count++;
      String e = entry.getName().substring(entry.getName().lastIndexOf(File.separatorChar) + 1);

      // Look for DMD.
      if (e.contains(DHCrudServiceImpl.DMD_PREFIX)) {
        System.out.println("    --> entry " + count + ": " + entry.getName());
        if (count != 5) {
          System.out.println("DMD ZIP entry not at position 5, it's on " + count);
          assertTrue(false);
        }
      }

      // Look for ADMMD.
      else if (e.contains(DHCrudServiceImpl.ADMMD_PREFIX)) {
        System.out.println("    --> entry " + count + ": " + entry.getName());
        if (count != 6) {
          System.out.println("ADMMD ZIP entry not at position 6, it's on " + count);
          assertTrue(false);
        }
      }

      // Look for TECHMD.
      else if (e.contains(DHCrudServiceImpl.TECHMD_PREFIX)) {
        System.out.println("    --> entry " + count + ": " + entry.getName());
        if (count != 7) {
          System.out.println("TECHMD ZIP entry not at position 7, it's on " + count);
          assertTrue(false);
        }
      }

      // Look for DATA.
      else if (e.contains(DHCrudServiceImpl.DATA_PREFIX)) {
        System.out.println("    --> entry " + count + ": " + entry.getName());
        if (count != 8) {
          System.out.println("DATA ZIP entry not at position 8, it's on " + count);
          assertTrue(false);
        }
      }

      else {
        System.out.println("        entry " + count + ": " + entry.getName());
      }
    }

    System.out.println("\tClosing THE BAG stream...");
    bagStream.close();

    System.out.println(OK);
  }

  /**
   * @param theUri
   * @param theDMD
   * @param theADMMD
   * @param theTECHMD
   * @param theData
   * @return
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  protected static DHCrudDariahObject createInitialCrudObject(URI theUri, String theDMD,
      String theADMMD, String theTECHMD, DataHandler theData)
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    DHCrudDariahObject result = new DHCrudDariahObject(theUri);

    // Set date and metadata.
    result.setMetadata(RDFUtils.readModel(theDMD, RDFConstants.TURTLE));
    result.setAdmMD(RDFUtils.readModel(theADMMD, RDFConstants.TURTLE));
    result.setTechMD(theTECHMD);
    result.setData(theData);

    return result;
  }

  /**
   * @param theBag
   */
  protected static void verifyBag(Bag theBag) {

    System.out.println("Verifying THE BAG...");

    boolean ignoreHiddenFiles = true;
    try {
      // Verify (COMPLETE) the bag.
      BagVerifier verifier = new BagVerifier();
      verifier.isComplete(theBag, ignoreHiddenFiles);

      // Verify (VALID) the bag.
      verifier.isValid(theBag, ignoreHiddenFiles);

      verifier.close();

    } catch (MissingPayloadManifestException | MissingBagitFileException
        | MissingPayloadDirectoryException | FileNotInPayloadDirectoryException
        | InterruptedException | MaliciousPathException | CorruptChecksumException
        | VerificationException | UnsupportedAlgorithmException | InvalidBagitFileFormatException
        | IOException e) {
      assertTrue(false);
    }

    System.out.println(OK);
  }

  /**
   * @param theNEW
   * @param theEXPECTED
   */
  private static void checkMD(Model theNEW, Model theEXPECTED, String theWHAT) {

    System.out.println("Checking " + theWHAT + "...");

    if (!theNEW.isIsomorphicWith(theEXPECTED)) {
      System.out.println(STARS);
      System.out.println(CREATED);
      System.out.println(STARS);
      System.out.println(RDFUtils.getTTLFromModel(theNEW));
      System.out.println(STARS);
      System.out.println(EXPECTED);
      System.out.println(STARS);
      System.out.println(RDFUtils.getTTLFromModel(theEXPECTED));
      System.out.println(STARS);
      System.out.println(DIFFERENCE);
      System.out.println(STARS);
      System.out.println(theEXPECTED.difference(theNEW));
      assertTrue("Created " + theWHAT + " and expected " + theWHAT + " metadata differ!", false);
    }

    System.out.println(OK);
  }

  /**
   * @param theNEW
   * @param theEXPECTED
   */
  private static void checkTECHMD(String theNEW, String theEXPECTED) {

    System.out.println("Checking TECHMD...");

    String wen = theNEW.replaceAll("\\s", "").trim();
    String exp = theEXPECTED.replaceAll("\\s", "").trim();
    if (!wen.equals(exp)) {
      System.out.println(STARS);
      System.out.println(CREATED);
      System.out.println(STARS);
      System.out.println(wen);
      System.out.println(STARS);
      System.out.println(EXPECTED);
      System.out.println(STARS);
      System.out.println(exp);
      System.out.println(wen.length() + " = " + exp.length());
      assertTrue("Created TECHMD and expected TECHMD metadata differ!", false);
    }

    System.out.println(OK);
  }

  /**
   * @param theUri
   * @param theDATA
   * @param theDMD
   * @param theADMMD
   * @param theTECHMD
   * @throws NoSuchAlgorithmException
   * @throws IOException
   * @throws TikaException
   * @throws IoFault
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws KeyStoreException
   * @throws KeyManagementException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static void createTestBag(URI theUri, DataHandler theDATA, String theDMD, String theADMMD,
      String theTECHMD)
      throws NoSuchAlgorithmException, IOException, TikaException, IoFault, KeyManagementException,
      KeyStoreException, URISyntaxException, org.json.simple.parser.ParseException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    String meth = ".createTestBag()";
    String logID = "JUNIT_" + System.currentTimeMillis();

    // Create CRUD object.
    System.out.println("Creating CRUD object for URI " + theUri + "...");

    CrudObject<Model> object =
        createInitialCrudObject(theUri, theDMD, theADMMD, theTECHMD, theDATA);

    System.out.println(OK);

    // Create THE BAG.
    System.out.println("Creating THE BAG...");

    Bag bag = DHCrudServiceStorageDariahImpl.createDHREPBagit(object, DONT_EXTRACT_TECHMD,
        NO_LOCATION, NO_TIMEOUT, CRUD_SERVICE_VERSION, LOGID);

    System.out.println("\tBAG root folder: " + bag.getRootDir());
    System.out.println(OK);

    // Verify THE BAG.
    verifyBag(bag);

    // Write THE BAG to ZIP file.
    System.out.println("Writing THE BAG to ZIP file... ");

    Path zipName = bag.getRootDir().getFileName();
    if (zipName == null) {
      throw new IOException("No ZIP name!");
    }
    Path zippedBagPath =
        Paths.get(new File("./src/test/resources/" + zipName.toString() + ".zip").toURI());

    System.out.println("\tZIP file name: " + zippedBagPath);

    DHCrudServiceUtilities.createZipBag(bag.getRootDir(), zippedBagPath);

    System.out.println(OK);

    // Checking order of zipped items! (We need to have metadata files FIRST!)
    checkEntriesOrder(new DHCrudDariahObject(theUri), zippedBagPath.toFile());

    // Create new object from ZIP file and check content.
    System.out.println("Creating CRUD object from ZIP file...");

    DHCrudDariahObject newObject = new DHCrudDariahObject(theUri);
    File tempDataFile = addMetadataAndData(zippedBagPath.toFile(), newObject, LOGID);

    System.out.println(OK);

    // Check metadata and ADMMD.
    checkMD(newObject.getMetadata(), RDFUtils.readModel(theDMD, RDFConstants.TURTLE), "DMD");

    checkMD(newObject.getAdmMD(), RDFUtils.readModel(theADMMD, RDFConstants.TURTLE), "ADMMD");

    checkTECHMD(newObject.getTechMD(), theTECHMD);

    // TODO Check more things here?

    boolean deleted = tempDataFile.delete();
    if (!deleted) {
      assertTrue(false);
    }

    // Delete ZIP file.
    if (DELETE) {
      System.out.println("Deleting ZIP file...");

      if (!zippedBagPath.toFile().delete()) {
        assertTrue("Could not delete ZIP file!", false);
      } else {
        System.out.println(OK);
      }

      // Delete temp BAG folder recursively.
      System.out.println("Deleting BAG folder...");

      File bagFile = bag.getRootDir().toFile();
      File bagParent = bagFile.getParentFile();

      DHCrudServiceUtilities.deleteTempFolder(bagParent, meth, logID);

      if (bagFile.exists() || bagParent.exists()) {
        throw new IOException("bag file or parent is still existing!");
      }

      System.out.println(OK);

    } else {
      System.out.println("Deleting NOTHING! Please set DELETE to TRUE!");
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Tests the creation of an RDA WG Repository Interoperability bag. Needs datacite.org to be
   * reachable, please comment in this test if needed!
   * </p>
   *
   * FIXME! Seems not to work! Seems, data can not be obtained...??
   *
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ParseException
   * @throws XMLStreamException
   * @throws IoFault
   * @throws MetatdataValueIsNotRepeatableException
   * @throws RequiredTagFileNotPresentException
   * @throws BagitVersionIsNotAcceptableException
   * @throws RequiredManifestNotPresentException
   * @throws MetatdataValueIsNotAcceptableException
   * @throws RequiredMetadataFieldNotPresentException
   * @throws FetchFileNotAllowedException
   * @throws org.json.simple.parser.ParseException
   * @throws URISyntaxException
   * @throws KeyStoreException
   * @throws NoSuchAlgorithmException
   * @throws KeyManagementException
   * @throws MetadataParseFault
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  @Test
  @Ignore
  public void testCreateBagPack() throws FileNotFoundException, IOException, ParseException,
      XMLStreamException, IoFault, FetchFileNotAllowedException,
      RequiredMetadataFieldNotPresentException, MetatdataValueIsNotAcceptableException,
      RequiredManifestNotPresentException, BagitVersionIsNotAcceptableException,
      RequiredTagFileNotPresentException, MetatdataValueIsNotRepeatableException,
      KeyManagementException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException,
      org.json.simple.parser.ParseException, MetadataParseFault,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    System.out.println(STARS);
    System.out.println("Test creating RDA WG Repository Interoperability BagPack");
    System.out.println(STARS);

    // Create CRUD object.
    System.out.println("Creating CRUD object...");

    URI uri = URI.create(RDFConstants.HDL_NAMESPACE + "21.11113/0000-000B-CAC6-2");
    DHCrudDariahObject object = new DHCrudDariahObject(uri);

    File zip = new File("./src/test/resources/dhrep_21.11113_0000-000B-CAC6-2.bagit.zip");

    System.out.println(OK);

    // Create ZIP stream. Reading.
    long start = System.currentTimeMillis();
    System.out.println("Reading bag...");

    System.out.println("PID: " + uri);

    // Get metadata from public DARIAH storage using the PID.
    ZipInputStream zipStream = new ZipInputStream(new FileInputStream(zip));
    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zipStream, object,
        LOGID);
    Model admMd = object.getAdmMD();
    Model dmd = object.getMetadata();
    String techMd = object.getTechMD();

    // Check for empty metadata!
    if (dmd == null || dmd.isEmpty()) {
      assertTrue("DMD empty!", false);
    }
    System.out.println("DMD-------------\n" + RDFUtils.getTTLFromModel(dmd).trim());

    if (admMd == null || admMd.isEmpty()) {
      assertTrue("ADM_MD empty!", false);
    }
    System.out.println("ADMMD-----------\n" + RDFUtils.getTTLFromModel(admMd).trim());

    if (techMd == null || techMd.isEmpty()) {
      assertTrue("TECH_MD empty!", false);
    }
    System.out.println("TECHMD----------\n" + techMd.trim());

    // Check for empty data.
    if (object.getData() == null || object.getData().getInputStream().available() <= 0) {
      assertTrue("Data null or empty!", false);
    }
    // String data = IOUtils.toString(object.getData().getInputStream());
    // System.out.println("DATA-----------\n" + data);

    // Export RDA WG RI BagPack.
    // NOTE In createRDAWGRIBagit an online request for DOI XML metadata is being made!
    Path theBagPackPath = DHCrudServiceStorageDariahImpl.createRDAWGRIBagit(uri.toString(), object,
        LOGID, DOI_API_LOCATION);

    System.out.println("BagPack file location: " + theBagPackPath.toAbsolutePath());

    System.out.print(OK);
    System.out.println("  >>  " + (System.currentTimeMillis() - start) + " millis");
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Get the DARIAH object, metadata first, then the data stream.
   * </p>
   *
   * @param theFile
   * @param theObject
   * @param theLogID
   * @return
   * @throws IOException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws IoFault
   * @throws URISyntaxException
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static File addMetadataAndData(File theFile, CrudObject<Model> theObject,
      String theLogID) throws IOException, KeyManagementException, NoSuchAlgorithmException,
      KeyStoreException, IoFault, URISyntaxException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File result = null;

    addMetadataOnly(theFile, theObject, theLogID);
    result = addDataOnly(theFile, theObject, theLogID);

    return result;
  }

  /**
   * <p>
   * Get the DARIAH object, metadata first, then the collection stream.
   * </p>
   *
   * @param theFile
   * @param theObject
   * @param theLogID
   * @return
   * @throws IOException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws IoFault
   * @throws URISyntaxException
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static File addMetadataAndCollectionData(File theFile, CrudObject<Model> theObject,
      String theLogID) throws IOException, KeyManagementException, NoSuchAlgorithmException,
      KeyStoreException, IoFault, URISyntaxException, ParseException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    File result = null;

    addMetadataOnly(theFile, theObject, theLogID);
    if (theObject.isCollection()) {
      result = addDataOnly(theFile, theObject, theLogID);
    }

    return result;
  }

  /**
   * <p>
   * Add metadata only.
   * </p>
   *
   * @param theFile
   * @param theObject
   * @param theLogID
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static void addMetadataOnly(File theFile, CrudObject<Model> theObject,
      String theLogID) throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    ZipInputStream zin = new ZipInputStream(new FileInputStream(theFile));
    DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadataFromZIPStream(zin, theObject,
        theLogID);
    zin.close();
  }

  /**
   * <p>
   * Add Data only.
   * </p>
   *
   * @param theFile
   * @param theObject
   * @param theLogID
   * @throws IOException
   */
  private static File addDataOnly(File theFile, CrudObject<Model> theObject, String theLogID)
      throws IOException {

    File result = DHCrudServiceStorageDariahImpl.storeDATAToTempFile(theFile,
        new CrudServiceMethodInfo(CrudOperations.READ), theLogID);
    InputStream in = new FileInputStream(result);
    theObject.setData(new DataHandler(new FileDataSource(result)));
    in.close();

    return result;
  }

}
