package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.cxf.helpers.IOUtils;
import org.junit.Test;
import jakarta.ws.rs.core.Response.Status;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class TestDHCrudServiceGenericExceptionMapper {

  /**
   * @throws IOException
   * @throws FileNotFoundException
   */
  @Test
  public void testCreatingXHTMLErrorPage() throws FileNotFoundException, IOException {

    String expectedErrorHTML = IOUtils
        .toString(new FileInputStream(new File("./src/test/resources/EXPECTED_ERROR_HTML.html")));

    DHCrudServiceGenericExceptionMapper dhMapper = new DHCrudServiceGenericExceptionMapper();

    String htmlFromMapper =
        dhMapper.prepareXHTMLMessage(Status.NOT_FOUND, "java.io.FileNotFoundException",
            "The request https://hdl.handle.net/api/handles/21.T11991/000-0011-B8C6-E?type=BAG for DARIAH-DE Repository BagIt bag is not valid!",
            "dhcrud-base-9.0.7-DH+201809201218", TestDHCrudServiceGenericExceptionMapper.getConf());

    if (!expectedErrorHTML.trim().equals(htmlFromMapper.trim())) {
      // System.out.println("EXPECTED ERROR HTML:\n" + expectedErrorHTML);
      System.out.println("CREATED ERROR HTML:\n" + htmlFromMapper);
      assertTrue(false);
    }
  }

  // **
  // PRIVATES
  // **

  /**
   * @return
   * @throws IOException
   */
  private static CrudServiceConfigurator getConf() throws IOException {

    CrudServiceConfigurator result = CrudServiceConfigurator.getInstance("");
    result.setProperty("CRUD_LOCATION", "https://localhost/dhcrud/1.0/");
    result.setProperty("MENU_HEADER_COLOR", "#112233");
    result.setProperty("BADGE_TEXT", "URGL ARGL AUA");

    return result;
  }

}
