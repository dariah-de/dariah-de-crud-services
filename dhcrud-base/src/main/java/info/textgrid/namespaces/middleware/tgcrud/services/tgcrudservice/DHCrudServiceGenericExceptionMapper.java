/**
 * This software is copyright (c) 2022 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2022-05-24 - Funk - Fix some SpotBugs issues.
 * 
 * 2018-10-26 - Funk - First version.
 */

/**
 * <p>
 * Writes DHCrud exception information in HTML bodies.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2022-05-24
 * @since 2018-10-26
 */

public class DHCrudServiceGenericExceptionMapper extends CrudServiceGenericExceptionMapper {

  private static DHCrudServiceVersion dhcrudServiceVersion = new DHCrudServiceVersion();

  /**
   * CONSTRUCTOR
   */
  public DHCrudServiceGenericExceptionMapper() {
    this.setHtmlTemplateFile("templates/dhrep-error.tmpl.html");
  }

  /**
   * @return
   */
  @Override
  public String getServiceVersion() {
    return dhcrudServiceVersion.getFULLVERSION();
  }

}
