/**
 * This software is copyright (c) 2023 by
 * 
 * DARIAH-DE Consortium (https://dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import java.text.ParseException;
import java.util.Date;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.jena.rdf.model.Model;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2023-02-15 - Funk - Refactor date and time things.
 *
 * 2017-11-22 - Funk - Added LogID.
 * 
 * 2017-09-26 - Funk - Added test for collection.
 * 
 * 2017-09-07 - Funk - Added wrapper checksum methods and implementation. Removed wrapper size from
 * ADM_MD.
 * 
 * 2017-08-13 - Funk - Using "hdl:" prefix now for superclass call.
 * 
 * 2017-08-02 - Funk - Using all prefixes and element names from RDFUtils now.
 * 
 * 2015-10-22 - Funk - Added mimetype.
 * 
 * 2015-08-07 - Funk - Added administrative metadata.
 * 
 * 2015-02-10 - Funk - Added file size and checksum.
 * 
 * 2015-02-02 - Funk - Added location.
 * 
 * 2015-01-30 - Funk - Copied from TGCrudTextGridObject.
 * 
 */

/**
 * <p>
 * This DHCrudDariahObject just binds together a metadata ObjectType (a Jena RDF model for
 * administrative, technical, and descriptive metadata) and a DataHandler for the storage interface.
 * </p>
 * 
 * <p>
 * PLEASE NOTE: The checksum in DARIAH objects is the checksums of THE BAG itself, because the
 * objects' checksums inside the bag already are computed creating THE BAG!
 * </p>
 * 
 * <p>
 * ANOTHER NOTE: We put the checksums of the data file into the ADM_MD metadata file!
 * </p>
 * 
 * <p>
 * TODO Check checksum handling! :-)
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */

public class DHCrudDariahObject extends CrudObject<Model> {

  // **
  // CLASS VARIABLES
  // **

  private String storageToken;
  private String resolvedUri;
  private String logID;

  private long wrapperSize;
  private String wrapperChecksum;
  private String wrapperChecksumOrigin;
  private String wrapperChecksumType;

  // **
  // CONSTRUCTORS
  // **

  /**
   * @param theUri
   */
  public DHCrudDariahObject(final URI theUri) {
    // URI must have Handle prefix!
    super(URI.create(RDFConstants.HDL_PREFIX + ":" + LTPUtils.omitHdlPrefix(theUri.toString())));
    // Use the resolved URI (http://hdl.handle.net/[theUri] as internal URI, because we gave the
    // resource that URL!
    this.resolvedUri = LTPUtils.resolveHandlePid(theUri);
  }

  // **
  // IMPLEMENTED ABSTRACTS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getFilesize()
   */
  @Override
  public long getFilesize() {
    return Long.parseLong(RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri,
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_EXTENT));
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setFilesize(long)
   */
  @Override
  public void setFilesize(final long theFilesize) {
    Model m = this.getAdmMD();
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_EXTENT, String.valueOf(theFilesize));
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getWrapperSize()
   */
  @Override
  public long getWrapperSize() {
    return this.wrapperSize;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setFilesize(long)
   */
  @Override
  public void setWrapperSize(final long theSize) {
    this.wrapperSize = theSize;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getChecksum()
   */
  @Override
  public String getChecksum() {
    return RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri, RDFConstants.PREMIS_PREFIX,
        RDFConstants.ELEM_PREMIS_MD);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setChecksum(java.lang.String)
   */
  @Override
  public void setChecksum(final String theChecksum) {
    Model m = this.getAdmMD();
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.PREMIS_PREFIX,
        RDFConstants.ELEM_PREMIS_MD,
        theChecksum);
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getChecksumType()
   */
  @Override
  public String getChecksumType() {
    return RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri, RDFConstants.PREMIS_PREFIX,
        RDFConstants.ELEM_PREMIS_MDT);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setChecksumType(java.lang.String)
   */
  @Override
  public void setChecksumType(final String theType) {
    Model m = this.getAdmMD();
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.PREMIS_PREFIX,
        RDFConstants.ELEM_PREMIS_MDT, theType);
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getChecksumOrigin()
   */
  @Override
  public String getChecksumOrigin() {
    return RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri, RDFConstants.PREMIS_PREFIX,
        RDFConstants.ELEM_PREMIS_MDO);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setChecksumOrigin(java.lang.String)
   */
  @Override
  public void setChecksumOrigin(final String theOrigin) {
    Model m = this.getAdmMD();
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.PREMIS_PREFIX,
        RDFConstants.ELEM_PREMIS_MDO, theOrigin);
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudObject#getWrapperChecksum()
   */
  @Override
  public String getWrapperChecksum() {
    return this.wrapperChecksum;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudObject#setWrapperChecksum(java.lang.String)
   */
  @Override
  public void setWrapperChecksum(String theChecksum) {
    this.wrapperChecksum = theChecksum;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudObject#getWrapperChecksumType()
   */
  @Override
  public String getWrapperChecksumType() {
    return this.wrapperChecksumType;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudObject#setWrapperChecksumType(java.lang.String)
   */
  @Override
  public void setWrapperChecksumType(String theChecksumType) {
    this.wrapperChecksumType = theChecksumType;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudObject#getWrapperChecksumOrigin()
   */
  @Override
  public String getWrapperChecksumOrigin() {
    return this.wrapperChecksumOrigin;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudObject#setWrapperChecksumOrigin(java.lang.String)
   */
  @Override
  public void setWrapperChecksumOrigin(String theChecksumOrigin) {
    this.wrapperChecksumOrigin = theChecksumOrigin;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setCreationDate(java.util.Date)
   */
  @Override
  public void setCreationDate(final Date theDate) throws IoFault {
    Model m = this.getAdmMD();
    XMLGregorianCalendar xcal = CrudServiceUtilities.getXMLGregorianCalendar(theDate.getTime());
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_CREATED, CrudServiceUtilities.getCETDateString(xcal));
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getCreationDate()
   */
  @Override
  public Date getCreationDate() throws ParseException {
    String date = RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri,
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_CREATED);
    return CrudServiceUtilities.getDateFromCETString(date);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setLastModifiedDate(java.util.Date)
   */
  @Override
  public void setLastModifiedDate(final Date theDate) throws IoFault {
    Model m = this.getAdmMD();
    XMLGregorianCalendar xcal = CrudServiceUtilities.getXMLGregorianCalendar(theDate.getTime());
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_MODIFIED, CrudServiceUtilities.getCETDateString(xcal));
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getLastModifiedDate()
   */
  @Override
  public Date getLastModifiedDate() throws ParseException {
    String date = RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri,
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_MODIFIED);
    return CrudServiceUtilities.getDateFromCETString(date);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #setMimetype(java.lang.String)
   */
  @Override
  public void setMimetype(String theMimetype) {
    Model m = this.getAdmMD();
    RDFUtils.addLiteralToModel(m, this.resolvedUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_FORMAT, theMimetype);
    this.setAdmMD(m);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject
   * #getMimetype()
   */
  @Override
  public String getMimetype() {
    return RDFUtils.findFirstObject(this.getAdmMD(), this.resolvedUri, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_FORMAT);
  }

  /**
   * @return Returns the current storage token belonging to this object.
   */
  public String getStorageToken() {
    return this.storageToken;
  }

  /**
   * @param theStorageToken
   */
  public void setStorageToken(String theStorageToken) {
    this.storageToken = theStorageToken;
  }

  /**
   * @return
   */
  public String getLogID() {
    if (this.logID != null) {
      return this.logID;
    } else {
      return "";
    }
  }

  /**
   * @param theLogID
   */
  public void setLogID(String theLogID) {
    this.logID = theLogID;
  }

  /**
   * @return Returns TRUE if this object is of type DARIAH-DE collection, FALSE otherwise.
   */
  @Override
  public boolean isCollection() {
    return RDFUtils.isCollection(this.getAdmMD(), this.getUri().toString());
  }

}
