/**
 * This software is copyright (c) 2024 by
 *
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.DigestOutputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import javax.xml.stream.XMLStreamException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.tika.exception.TikaException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import gov.loc.repository.bagit.conformance.BagProfileChecker;
import gov.loc.repository.bagit.creator.BagCreator;
import gov.loc.repository.bagit.domain.Bag;
import gov.loc.repository.bagit.domain.Metadata;
import gov.loc.repository.bagit.exceptions.CorruptChecksumException;
import gov.loc.repository.bagit.exceptions.FileNotInPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.InvalidBagitFileFormatException;
import gov.loc.repository.bagit.exceptions.MaliciousPathException;
import gov.loc.repository.bagit.exceptions.MissingBagitFileException;
import gov.loc.repository.bagit.exceptions.MissingPayloadDirectoryException;
import gov.loc.repository.bagit.exceptions.MissingPayloadManifestException;
import gov.loc.repository.bagit.exceptions.UnsupportedAlgorithmException;
import gov.loc.repository.bagit.exceptions.VerificationException;
import gov.loc.repository.bagit.exceptions.conformance.BagitVersionIsNotAcceptableException;
import gov.loc.repository.bagit.exceptions.conformance.FetchFileNotAllowedException;
import gov.loc.repository.bagit.exceptions.conformance.MetatdataValueIsNotAcceptableException;
import gov.loc.repository.bagit.exceptions.conformance.MetatdataValueIsNotRepeatableException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredManifestNotPresentException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredMetadataFieldNotPresentException;
import gov.loc.repository.bagit.exceptions.conformance.RequiredTagFileNotPresentException;
import gov.loc.repository.bagit.hash.StandardSupportedAlgorithms;
import gov.loc.repository.bagit.hash.SupportedAlgorithm;
import gov.loc.repository.bagit.verify.BagVerifier;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.Response;

/**
 * TODOLOG
 *
 **
 * CHANGELOG
 *
 *
 * 2024-09-30 - Funk - Fix all try-with-resource warnings. Add DEFAULT_CHARSET for resolving
 * deprecated IOUtils methods.
 *
 * 2022-05-24 - Funk - Fix some SpotBugs issues.
 *
 * 2020-11-25 - Funk - Add deletion of ZIP file for BAGPACKing data.
 *
 * 2020-06-18 - Funk - Add new RDA_BAGIT_PROFILE_ID.
 *
 * 2018-04-20 - Funk - Added streaming for reading data objects.
 *
 * 2018-03-20 - Funk - Finalised RDAWGRI BagIt export.
 *
 * 2018-03-15 - Funk - Changed RDAWGRI BagIt checksum type to SHA-256.
 *
 * 2018-03-15 - Funk - Added BagIt RDAWGRI profile URL to bags.
 *
 * 2017-12-01 - Funk - Using new storage client.
 *
 * 2017-11-22 - Funk - Added log ID.
 *
 * 2017-10-12 - Funk - Fixed reading metadata first and reading collection data only if needed!
 *
 * 2017-09-28 - Funk - Fixed reading metadata first and reading data only if needed!
 *
 * 2017-08-28 - Funk - Added BagIt DARIAH-DE Profile.
 *
 */

/**
 * <p>
 * This storage implementation grants CRUD operations via DARIAH Storage API.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-10-01
 * @since 2014-09-12
 */

public class DHCrudServiceStorageDariahImpl extends CrudStorageAbs<Model> {

  // **
  // FINAL STATICS
  // **

  private static final String UNIT_NAME = "dariah";

  private static final String MIMETYPE_ZIP = "application/x-zip-compressed";
  private static final String MESSAGE_DIGEST_TYPE = "MD5";
  private static final String DEFAULT_CHARSET = "UTF-8";

  private static final boolean DO_NOT_EXTRACT_TECHMD = false;
  private static final String NO_LOCATION = "";
  private static final int NO_TIMEOUT = 0;
  private static final String NO_TOK = null;
  protected static final String BAGPACK_FILE_SUFFIX = ".bagpack";

  protected static final String BAGIT_FILE_SUFFIX = ".bagit";
  protected static final String ZIP_FILE_SUFFIX = ".zip";

  private static final String DATA_MISSING =
      "Please provide valid DHCrudServiceObject! URI, metadata, or data missing!";
  private static final String BAGPACK_FAILURE = "Failed to create the Bagpack";
  private static final String FAILURE_STORING = "Failure storing to the DARIAH storage due to a ";
  private static final String FAILURE_READING = "Failure reading from the DARIAH storage";

  // Generic BagIt Profile keys.
  private static final String BAGIT_PROFILE_KEY_SRC_ORG = "Source-Organization";
  private static final String BAGIT_PROFILE_KEY_ORG_ADDR = "Organization-Address";
  private static final String BAGIT_PROFILE_KEY_CONTACT_NAME = "Contact-Name";
  private static final String BAGIT_PROFILE_KEY_CONTACT_EMAIL = "Contact-Email";
  private static final String BAGIT_PROFILE_KEY_EXT_DESCR = "External-Description";
  private static final String BAGIT_PROFILE_KEY_ID = "BagIt-Profile-Identifier";
  private static final String BAGIT_PROFILE_KEY_BAG_SIZE = "Bag-Size";

  // Generic BagIt Profile values for DH-rep and RDA WG RI bags.
  private static final String BAGIT_PROFILE_SRC_ORG = "DARIAH-DE";
  private static final String BAGIT_PROFILE_ORG_ADDR =
      "DARIAH-DE, Papendiek 14, 37073 Göttingen, Germany";
  private static final String BAGIT_PROFILE_CONTACT_NAME = "Stefan E. Funk";
  private static final String BAGIT_PROFILE_CONTACT_EMAIL = "funk@sub.uni-goettingen.de";
  private static final String BAGIT_PROFILE_EXT_DESCR =
      "BagIt bag recommended by the RDA Research Data Repository Interoperability WG";

  // Specific RDA WG RI BagIt settings.
  // TODO Change back to RDAResearchDataRepositoryInteropWG's version if BagIt 1.0 is included in
  // BagIt version acceptance list!
  // private static final String RDA_BAGIT_PROFILE_ID =
  // "https://raw.githubusercontent.com/RDAResearchDataRepositoryInteropWG/bagit-profiles/master/generic/0.1/profile.json";
  private static final String RDA_BAGIT_PROFILE_ID =
      "https://raw.githubusercontent.com/fungunga/bagit-profiles/master/generic/0.2/profile.json";

  // Specific DH-rep BagIt settings.
  // private static final String DH_BAGIT_PROFILE_ID =
  // "https://repository.de.dariah.eu/schemas/bagit/profiles/dhrep_0.2.json";

  // **
  // STATICS
  // **

  // The DARIAH-DE storage client bean.
  private static DariahStorageClient storageClient = new DariahStorageClient();
  private static File baseTempDir = new File(System.getProperty("java.io.tmpdir"));

  private static DHCrudServiceVersion dhCrudServiceVersion = new DHCrudServiceVersion();

  // **
  // IMPLEMENTED METHODS
  // **

  /**
   *
   */
  @Override
  public void create(final CrudObject<Model> theObject, final String theStorageToken)
      throws IoFault {

    String meth = UNIT_NAME + ".create()";

    // Check incoming data for existence.
    if (theObject.getUri() == null || theObject.getMetadata() == null
        || theObject.getData() == null) {
      CrudServiceExceptions.ioFault(DATA_MISSING);
    }

    // Get DHCrudObject, cast, get log ID.
    String logID = ((DHCrudDariahObject) theObject).getLogID();
    String logIDString = "[" + logID + "] ";

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "URI: " + theObject.getUri(), logID);

    // Read descriptive metadata string as RDF model.
    Model metadataModel = theObject.getMetadata();

    // Re-set descriptive metadata.
    theObject.setMetadata(metadataModel);

    try {
      // Create THE BAG.
      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Starting THE BAG creation", logID);

      String serviceVersion = dhCrudServiceVersion.getFULLVERSION();
      Bag bag = createDHREPBagit(theObject, this.conf.getEXTRACTtECHMD(),
          this.conf.getFITSlOCATION().toString(), this.conf.getFITScLIENTsTUBtIMEOUT(),
          serviceVersion, logID);

      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "THE BAG creation complete", logID);

      // Write THE BAG to DARIAH Public Storage.
      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Writing THE BAG to DARIAH PublicStorage",
          logID);

      // NOTE: BAG temp folder and BAG ZIP file are deleted within writeToDariahStorage() method!
      URL location = writeToDariahStorage(theObject, bag, theStorageToken, logID);

      // Final error check.
      if (location == null) {
        throw new IoFault(logIDString + FAILURE_STORING + ": Something went wrong!");
      }

      DHCrudServiceImpl.log(CrudService.INFO, meth, "THE BAG for URI " + theObject.getUri()
          + " was savely written to DARIAH PublicStorage: " + location, logID);

      // Add location to DARIAH object.
      theObject.setLocation(location);

    } catch (IOException | TikaException | NoSuchAlgorithmException e) {
      throw new IoFault(logIDString + FAILURE_STORING + ": " + e.getMessage(), e);
    }
  }

  /**
   *
   */
  @Override
  public void create(final CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void createMetadata(final CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void update(final CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void updateMetadata(final CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public CrudObject<Model> retrieve(final URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public DHCrudDariahObject retrievePublic(final URI thePid) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public Model retrieveMetadata(final URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public Model retrieveMetadataPublic(URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public Model retrieveAdmMD(final URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }


  /**
   *
   */
  @Override
  public Model retrieveAdmMDPublic(final URI thePid) throws IoFault {

    Model result = null;

    DHCrudDariahObject bag = new DHCrudDariahObject(thePid);
    String logID = bag.getLogID();
    try {
      addDHCrudDariahObjectMetadata(thePid, bag, this.conf.getPIDrESOLVER(), logID);

      result = bag.getAdmMD();

    } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException
        | URISyntaxException | ParseException | IOException
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      throw new IoFault("[" + logID + "] " + FAILURE_READING + ": " + e.getMessage(), e);
    }

    return result;
  }

  /**
   *
   */
  @Override
  public String retrieveTechMD(final URI theUri) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public String retrieveTechMDPublic(final URI thePid) throws IoFault {

    String result = "";

    DHCrudDariahObject bag = new DHCrudDariahObject(thePid);
    String logID = bag.getLogID();
    try {
      addDHCrudDariahObjectMetadata(thePid, bag, this.conf.getPIDrESOLVER(), logID);

      result = bag.getTechMD();

    } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException
        | URISyntaxException | ParseException | IOException
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      throw new IoFault("[" + logID + "] " + FAILURE_READING + ": " + e.getMessage(), e);
    }

    return result;
  }

  /**
   *
   */
  @Override
  public void delete(final CrudObject<Model> theObject) throws IoFault, RelationsExistFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void addKeyValuePair(final URI theUri, final String theKey, final Object theValue)
      throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deleteMetadata(final CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public int getLatestRevision(final URI theUri, final boolean unfiltered)
      throws IoFault, ObjectNotFoundFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public int getLatestRevisionPublic(final URI theUri, boolean unfiltered)
      throws IoFault, ObjectNotFoundFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void move(final URI theUri) throws IoFault, ObjectNotFoundFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void copy(final URI theUri) throws IoFault, ObjectNotFoundFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public URI resolve(final URI theUri) throws IoFault, ObjectNotFoundFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deletePublic(CrudObject<Model> theObject) throws IoFault, RelationsExistFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   *
   */
  @Override
  public void deleteMetadataPublic(CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /**
   * @param thePid
   * @param thePidResolver
   * @param theLogID
   * @return
   * @throws IoFault
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   */
  public static DHCrudDariahObject retrieveMetadataPublicBag(final URI thePid, URL thePidResolver,
      String theLogID) throws IoFault, org.apache.jena.riot.lang.extra.javacc.ParseException,
      KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException,
      URISyntaxException, ParseException {

    DHCrudDariahObject result = new DHCrudDariahObject(thePid);

    addDHCrudDariahObjectMetadata(thePid, result, thePidResolver, theLogID);

    return result;
  }

  // **
  // PROTECTED METHODS
  // **

  /**
   * @param theObject
   * @param extractTechMd
   * @param theFitsLocation
   * @param theFitsClientTimeout
   * @param theServiceVersion
   * @param theLogID
   * @return
   * @throws NoSuchAlgorithmException
   * @throws IOException
   * @throws TikaException
   * @throws IoFault
   */
  protected static Bag createDHREPBagit(CrudObject<Model> theObject, final boolean extractTechMd,
      final String theFitsLocation, final long theFitsClientTimeout, String theServiceVersion,
      String theLogID) throws NoSuchAlgorithmException, IOException, TikaException, IoFault {

    Bag result;

    // Create the bag temp file folder and the bag.
    File bagRoot = createBasicBagFolder(theObject, extractTechMd, theFitsLocation,
        theFitsClientTimeout, theServiceVersion, theLogID);

    result = createTheBag(bagRoot, StandardSupportedAlgorithms.MD5, null);

    verifyBagCompleteValid(result, theLogID);
    // TODO Use DH-rep BagIt profile for creation and validation!
    // verifyBagProfile(bag, DH_BAGIT_PROFILE_ID, logID);

    return result;
  }

  /**
   * @param thePid
   * @param theObject
   * @param theLogID
   * @param theDataciteLocation
   * @return
   * @throws XMLStreamException
   * @throws IOException
   * @throws IoFault
   */
  protected static Path createRDAWGRIBagit(String thePid, DHCrudDariahObject theObject,
      String theLogID, String theDataciteLocation) throws XMLStreamException, IOException, IoFault {

    String meth = UNIT_NAME + ".createRDAWGRIBagit()";

    // Get the DOI.
    String doi = RDFUtils.getFirstProperty(theObject.getAdmMD(), LTPUtils.resolveHandlePid(thePid),
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "DOI: " + doi, theLogID);

    // Get DataCite XML metadata.
    String dataciteXML = DHCrudServiceUtilities.getDOIMetadataXML(LTPUtils.omitDoiNamespace(doi),
        theLogID, theDataciteLocation);

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "DataCite XML String: " + dataciteXML, theLogID);

    // Create RDA Repository Interoperability WG metadata for BagIt info file.

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Starting BagPack BAG creation", theLogID);

    Metadata bagMetadata = new Metadata();
    bagMetadata.add(BAGIT_PROFILE_KEY_CONTACT_NAME, BAGIT_PROFILE_CONTACT_NAME);
    bagMetadata.add(BAGIT_PROFILE_KEY_CONTACT_EMAIL, BAGIT_PROFILE_CONTACT_EMAIL);
    bagMetadata.add(BAGIT_PROFILE_KEY_SRC_ORG, BAGIT_PROFILE_SRC_ORG);
    bagMetadata.add(BAGIT_PROFILE_KEY_ORG_ADDR, BAGIT_PROFILE_ORG_ADDR);
    bagMetadata.add(BAGIT_PROFILE_KEY_EXT_DESCR, BAGIT_PROFILE_EXT_DESCR);
    bagMetadata.add(BAGIT_PROFILE_KEY_BAG_SIZE,
        FileUtils.byteCountToDisplaySize(theObject.getFilesize()));
    bagMetadata.add(BAGIT_PROFILE_KEY_ID, RDA_BAGIT_PROFILE_ID);

    Bag bag = null;
    String serviceVersion = "";
    File bagRootFolder = null;
    try {
      // Create the bag temp file folder.
      serviceVersion = dhCrudServiceVersion.getFULLVERSION();
      bagRootFolder = createBasicBagFolder(theObject, DO_NOT_EXTRACT_TECHMD, NO_LOCATION,
          NO_TIMEOUT, serviceVersion, theLogID);
    } catch (IOException | TikaException | IoFault | NoSuchAlgorithmException e) {
      throw new IoFault("[" + theLogID + "] " + BAGPACK_FAILURE + ": " + e.getMessage(), e);
    }

    try {
      // Create THE BAG.
      bag = createTheBag(bagRootFolder, StandardSupportedAlgorithms.SHA256, bagMetadata);

      // TODO Use fetch file to have small BagPack bags? Maybe optionally?

      // TODO Copy DC metadata to metadata folder, add checksum to tag manifest file.

      // Add DataCite metadata folder and file AFTER creating the bag.
      File meta = new File(bagRootFolder, "metadata");
      boolean metadataFolderCreated = meta.mkdir();
      if (!metadataFolderCreated) {
        throw new IoFault("[" + theLogID + "] " + BAGPACK_FAILURE
            + ": BagPack metadata folder could not be created!");
      }
      File dataciteMetadata = new File(meta, "datacite.xml");
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      try (FileOutputStream dataciteStream = new FileOutputStream(dataciteMetadata);
          DigestOutputStream digest = new DigestOutputStream(dataciteStream, md)) {
        digest.write(dataciteXML.getBytes(DEFAULT_CHARSET));
      }
      String sha = new String(Hex.encodeHex(md.digest()));

      DHCrudServiceImpl.log(CrudService.DEBUG, meth,
          "SHA256 checksum of DataCite metadata file: " + sha, theLogID);

      // Add metadata/datacite.xml to tag manifest file. Throw away existing entries!
      // FIXME Must we really delete the existing entries?
      // String existingTagManifest = IOUtils.toString(new FileInputStream(tagManifestFile));
      File tagManifestFile = new File(bag.getRootDir().toFile(), "tagmanifest-sha256.txt");
      String newTagManifest = sha + "  " + "metadata/datacite.xml";
      try (FileOutputStream fos = new FileOutputStream(tagManifestFile)) {
        IOUtils.write(newTagManifest, fos, DEFAULT_CHARSET);
      }

      DHCrudServiceImpl.log(CrudService.DEBUG, meth,
          "Added DataCite metadata to BagPack tag manifest (replacing existing!)", theLogID);

      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "BagPack creation complete", theLogID);

    } catch (IOException | NoSuchAlgorithmException e) {
      throw new IoFault("[" + theLogID + "] " + BAGPACK_FAILURE + ": " + e.getMessage(), e);
    }

    // Verify THE BAG.
    verifyBagCompleteValid(bag, theLogID);
    verifyBagProfile(bag, new URL(RDA_BAGIT_PROFILE_ID), theLogID);

    // Create ZIP temp file and DO ZIP the bag.
    Path zippedBag = Paths.get(
        File.createTempFile(DHCrudServiceImpl.DHREP_PREFIX, BAGPACK_FILE_SUFFIX + ZIP_FILE_SUFFIX)
            .toURI());
    DHCrudServiceUtilities.createZipBag(bag.getRootDir(), zippedBag);

    // Remove temp folder and temp files.
    File bagTempRootFolderParent = bagRootFolder.getParentFile();
    DHCrudServiceUtilities.deleteTempFolder(bagTempRootFolderParent, meth, theLogID);

    // Return the path to the bag.
    return zippedBag;
  }

  /**
   * @param thePid
   * @param theHandleResolverUrl
   * @param theLogID
   * @return
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws KeyManagementException
   * @throws IoFault
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   */
  protected static Response getBagResponsePublic(final URI thePid, URL theHandleResolverUrl,
      String theLogID) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException,
      IoFault, URISyntaxException, ParseException, IOException {

    // Get relocated object response.
    Response result =
        getPublicStorageBagResponse(getBagURL(thePid, theHandleResolverUrl, theLogID), theLogID);

    // Add filename header.
    String filename =
        DHCrudServiceUtilities.getFilenameFromUri(thePid, BAGIT_FILE_SUFFIX, ZIP_FILE_SUFFIX);
    result.getHeaders().add(HttpHeaders.CONTENT_DISPOSITION,
        "inline; filename=\"" + filename + "\"");

    return result;
  }

  /**
   * <p>
   * Get THE BAG as a ZIP input stream.
   * </p>
   *
   * @param theLocation
   * @param theLogID
   * @return
   * @throws IOException
   */
  protected static InputStream getBagStreamPublic(final URI theLocation, String theLogID)
      throws IOException {
    return storageClient
        .setStorageUri(URI.create(theLocation.getScheme() + "://" + theLocation.getHost()))
        .readFile(theLocation.getPath(), NO_TOK, theLogID);
  }

  /**
   * <p>
   * Gets THE BAG's URI directly from Handle resolver (URL).
   * </p>
   *
   * TODO: Check if we can implement this in the PID implementation as checkTimeDeleted(), or even
   * implement a getMetadata() method for the PID service to retrieve all Handle metadata pairs as
   * JSON.
   * 
   * @param thePid
   * @param theHandleResolverUrl
   * @param theLogID
   * @return
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws KeyManagementException
   * @throws IoFault
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   * @throws JSONException
   */
  protected static URI getBagURL(final URI thePid, URL theHandleResolverUrl, String theLogID)
      throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IoFault,
      IOException, URISyntaxException, ParseException {

    String meth = UNIT_NAME + ".getBagURL()";

    URI result;

    // Get BAG location from HDL metadata (BAG).
    // <http://hdl.handle.net/api/handles/21.T11998/0000-0001-F6BE-4?type=BAG>
    String pidWithoutPrefix = LTPUtils.omitHdlPrefix(thePid.toString());

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "PID to get THE BAG from: " + pidWithoutPrefix,
        theLogID);

    String handleResolverUrl =
        theHandleResolverUrl.getProtocol() + "://" + theHandleResolverUrl.getHost();
    String bagRequest = handleResolverUrl + "/api/handles/" + pidWithoutPrefix + "?type=BAG";

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "THE BAG request URL: " + bagRequest, theLogID);

    try (CloseableHttpClient client = HttpClientBuilder.create().build();
        InputStream content = client.execute(new HttpGet(bagRequest)).getEntity().getContent()) {
      result = getLocationFromJSON(bagRequest, content);
    }

    DHCrudServiceImpl.log(CrudService.INFO, meth,
        "PublicStorage URL of THE BAG: " + result.toString(), theLogID);

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Adds metadata included in the ZIP BAG to the given DHCrudDariahObject. Add data only if object
   * is a collection and/or configured.
   * </p>
   *
   * @param thePid
   * @param theObject
   * @param theHandleResolverUrl
   * @param theLogID
   * @throws IOException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws IoFault
   * @throws URISyntaxException
   */
  protected static void addDHCrudDariahObjectMetadata(URI thePid, CrudObject<Model> theObject,
      final URL theHandleResolverUrl, String theLogID) throws IOException, KeyManagementException,
      NoSuchAlgorithmException, KeyStoreException, IoFault, URISyntaxException,
      java.text.ParseException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    URI location = getBagURL(thePid, theHandleResolverUrl, theLogID);

    ZipInputStream zipStream =
        new ZipInputStream(DHCrudServiceStorageDariahImpl.getBagStreamPublic(location, theLogID));

    addDHCrudDariahObjectMetadataFromZIPStream(zipStream, theObject, theLogID);
  }

  /**
   * <p>
   * Writes a bag to the DARIAH Public Storage.
   * </p>
   *
   * @param theObject
   * @param theBag
   * @param theStorageToken
   * @param theLogID
   * @return
   * @throws IoFault
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  private URL writeToDariahStorage(final CrudObject<Model> theObject, final Bag theBag,
      final String theStorageToken, String theLogID)
      throws IoFault, IOException, NoSuchAlgorithmException {

    String meth = UNIT_NAME + ".writeToDariahStorage()";

    URL result = null;

    // Create ZIP temp file.
    Path zippedBag = Paths.get(
        File.createTempFile(DHCrudServiceImpl.DHREP_PREFIX, BAGIT_FILE_SUFFIX + ZIP_FILE_SUFFIX)
            .toURI());

    // ZIP THE BAG!
    DHCrudServiceUtilities.createZipBag(theBag.getRootDir(), zippedBag);

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "THE BAG written to temp ZIP file: "
        + zippedBag.getFileName() + " (" + zippedBag.toFile().length() + ")", theLogID);

    // Get the DARIAH-DE PublicStorage client.
    DariahStorageClient publicStorageClient =
        storageClient.setStorageUri(this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC());

    // Get THE BAG's file size...
    try (CountingInputStream count =
        new CountingInputStream(new FileInputStream(zippedBag.toFile()))) {

      // ...and checksum.
      MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST_TYPE);
      try (DigestInputStream digest = new DigestInputStream(count, md)) {

        // Write THE BAG to DARIAH PublicStorage using the digest stream using the counting stream
        // using our OAuth token :-)
        String id = publicStorageClient.createFile(digest, MIMETYPE_ZIP, theStorageToken, theLogID);

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Storage ID: " + id, theLogID);

        // Do count and get the message digest (of THE BAG!) and set wrapper checksum data.
        long bagSize = count.getByteCount();
        theObject.setWrapperSize(bagSize);
        String bagChecksum = new String(Hex.encodeHex(md.digest()));
        theObject.setWrapperChecksumType(MESSAGE_DIGEST_TYPE.toLowerCase());
        theObject.setWrapperChecksum(bagChecksum);
        theObject.setWrapperChecksumOrigin(dhCrudServiceVersion.getSERVICENAME() + " "
            + dhCrudServiceVersion.getVERSION() + "." + dhCrudServiceVersion.getBUILDDATE());

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Filesize of bag: " + bagSize, theLogID);
        DHCrudServiceImpl.log(CrudService.DEBUG, meth,
            MESSAGE_DIGEST_TYPE + " digest of bag: " + bagChecksum, theLogID);

        if (this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC().toString().endsWith("/")) {
          result = new URL(this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC() + id);
        } else {
          result = new URL(this.conf.getDEFAULTdATAsTORAGEuRIpUBLIC() + "/" + id);
        }

        digest.close();
      }

      count.close();
    }

    // Delete temp ZIP file.
    DHCrudServiceUtilities.deleteTempFile(zippedBag.toFile(), meth, theLogID);

    // Delete temp ZIP source folder.
    Path tempFolderPath = theBag.getRootDir().getParent();
    if (tempFolderPath != null) {
      DHCrudServiceUtilities.deleteTempFolder(tempFolderPath.toFile(), meth, theLogID);
    }

    return result;
  }

  /**
   * @param theBagRootFolder
   * @param theAlgorithm
   * @param theBagMetadata
   * @return
   * @throws NoSuchAlgorithmException
   * @throws IOException
   */
  private static Bag createTheBag(File theBagRootFolder, SupportedAlgorithm theAlgorithm,
      Metadata theBagMetadata) throws NoSuchAlgorithmException, IOException {

    Collection<SupportedAlgorithm> algorithm = new ArrayList<SupportedAlgorithm>();
    algorithm.add(theAlgorithm);

    if (theBagMetadata == null) {
      return BagCreator.bagInPlace(Paths.get(theBagRootFolder.toURI()), algorithm, false);
    } else {
      return BagCreator.bagInPlace(Paths.get(theBagRootFolder.toURI()), algorithm, false,
          theBagMetadata);
    }
  }

  /**
   * <p>
   * Get THE BAG as Response. We do not stream here! Use for data file download only.
   * </p>
   *
   * @param theLocation
   * @param theLogID
   * @return
   * @throws IOException
   */
  private static Response getPublicStorageBagResponse(URI theLocation, String theLogID)
      throws IOException {
    return storageClient
        .setStorageUri(URI.create(theLocation.getScheme() + "://" + theLocation.getHost()))
        .readResponse(theLocation.getPath(), NO_TOK, theLogID);
  }

  /**
   * <p>
   * Checks THE BAG for validity and completeness.
   * </p>
   *
   * @param theBag
   * @param theLogID
   * @throws IOException
   */
  private static void verifyBagCompleteValid(Bag theBag, String theLogID) throws IOException {

    String meth = UNIT_NAME + ".verifyBagCompleteValid()";

    try (BagVerifier verifier = new BagVerifier()) {
      // Verify and ignore hidden files.
      try {
        verifier.isComplete(theBag, true);
        verifier.isValid(theBag, true);

        DHCrudServiceImpl.log(CrudService.DEBUG, meth,
            "THE BAG has been verified: complete and valid", theLogID);

      } catch (MissingPayloadManifestException | MissingBagitFileException
          | MissingPayloadDirectoryException | FileNotInPayloadDirectoryException
          | InterruptedException | MaliciousPathException | UnsupportedAlgorithmException
          | InvalidBagitFileFormatException | CorruptChecksumException | VerificationException e) {
        // Verification ERROR!
        String message = "THE BAG verification error!";
        DHCrudServiceImpl.log(CrudService.ERROR, meth, message, theLogID);
        throw new IOException(message, e);
      } finally {
        verifier.close();
      }
    }
  }

  /**
   * <p>
   * Validates THE BAG against the given BagIt profile.
   * </p>
   *
   * @param theBag
   * @param theProfileLocation
   * @param theLogID
   * @throws IOException
   */
  private static void verifyBagProfile(Bag theBag, URL theProfileLocation, String theLogID)
      throws IOException {

    String meth = UNIT_NAME + ".verifyBagProfile()";

    try {
      BagProfileChecker.bagConformsToProfile(theProfileLocation.openStream(), theBag);

      DHCrudServiceImpl.log(CrudService.DEBUG, meth,
          "THE BAG is conform to <" + theProfileLocation + "> specs", theLogID);

    } catch (FetchFileNotAllowedException | RequiredMetadataFieldNotPresentException
        | MetatdataValueIsNotAcceptableException | RequiredManifestNotPresentException
        | BagitVersionIsNotAcceptableException | RequiredTagFileNotPresentException
        | MetatdataValueIsNotRepeatableException e) {

      // Profile validation ERROR!
      String message = "Bag is not conform to BagIt profile specifications: " + theProfileLocation;
      DHCrudServiceImpl.log(CrudService.ERROR, meth, message, theLogID);
      throw new IOException(message, e);
    }
  }

  /**
   * <p>
   * Creates A BAG temp file from metadata and data files.
   * </p>
   *
   * @param theObject
   * @param extractTechMd
   * @param theFitsLocation
   * @param theFitsClientTimeout
   * @param theServiceVersion
   * @param theLogID
   * @return
   * @throws IOException
   * @throws TikaException
   * @throws IoFault
   * @throws NoSuchAlgorithmException
   */
  private static File createBasicBagFolder(CrudObject<Model> theObject, final boolean extractTechMd,
      final String theFitsLocation, final long theFitsClientTimeout, String theServiceVersion,
      String theLogID) throws IOException, TikaException, IoFault, NoSuchAlgorithmException {

    String meth = UNIT_NAME + ".createBasicBagFolder()";

    File result;

    // **
    // Create temp folder for creating THE BAG's folder.
    // **

    File tempFolder = new File(baseTempDir,
        DHCrudServiceImpl.DHREP_PREFIX + UUID.randomUUID().toString().replaceAll("-", ""));
    if (!tempFolder.mkdir()) {
      String message =
          "Bagit temp folder " + tempFolder.getCanonicalPath() + " could not be created!";
      throw new IOException(message);
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth,
        "THE BAG's temp folder name: " + tempFolder.getCanonicalPath(), theLogID);

    // **
    // Create THE BAG's folder.
    // **

    String bagFilename =
        DHCrudServiceUtilities.getFilenameFromUri(theObject.getUri(), BAGIT_FILE_SUFFIX, "");
    result = new File(tempFolder, bagFilename);
    if (!result.mkdir()) {
      String message = "Bag folder " + result.getCanonicalPath() + " could not be created!";
      throw new IOException(message);
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "THE BAG's filename: " + bagFilename, theLogID);

    // **
    // Create descriptive metadata file.
    // **

    Model metadataModel = theObject.getMetadata();

    // Purge unused namespaces from DMD.
    RDFUtils.removeUnusedNamespacesFromModel(metadataModel);

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Purged unused namespaces from DMD metadata",
        theLogID);

    File meta = new File(result, DHCrudServiceImpl.DMD_PREFIX + RDFConstants.TTL_FILE_SUFFIX);
    try (BufferedWriter metaWriter = new BufferedWriter(new FileWriter(meta))) {
      metadataModel.write(metaWriter, RDFConstants.TURTLE);
      metaWriter.close();
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "METADATA filename: " + meta.getName(),
        theLogID);
    DHCrudServiceImpl.log(CrudService.DEBUG, meth,
        "METADATA string:\n" + RDFUtils.getTTLFromModel(metadataModel), theLogID);

    // **
    // Create data file.
    // **

    File data = new File(result, DHCrudServiceImpl.DATA_PREFIX);

    // Create a counting stream around a file output stream.
    try (CountingOutputStream count = new CountingOutputStream(new FileOutputStream(data))) {

      // Create a message digest stream around the counting stream.
      MessageDigest md = MessageDigest.getInstance(MESSAGE_DIGEST_TYPE);
      try (DigestOutputStream digest = new DigestOutputStream(count, md)) {

        // Write TO THE STORAGE using the digest stream using the counting stream :-)
        theObject.getData().writeTo(digest);

        // Close both streams.
        count.close();
        digest.close();
      }

      // Do count, get the message digest, and set things to get it into the metadata later!
      theObject.setFilesize(count.getByteCount());
      theObject.setChecksum(new String(Hex.encodeHex(md.digest())));
      theObject.setChecksumOrigin(theServiceVersion);
      theObject.setChecksumType(MESSAGE_DIGEST_TYPE.toLowerCase());
    }

    // Get data file's file extension.
    String dataSuffix = LTPUtils.extractFileExtension(data);
    if (dataSuffix == null) {
      dataSuffix = "";
    }
    File newData = new File(data.getAbsolutePath() + dataSuffix);
    boolean renamed = data.renameTo(newData);

    if (!renamed) {
      DHCrudServiceImpl.log(CrudService.WARN, meth,
          "This warning should actually be an error and the error should not have occurred! The data file could not internally be renamed to newData file! Please check THE BAG creation process!",
          theLogID);
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "DATA filename: " + newData.getName(), theLogID);

    // **
    // Create administrative metadata file.
    // **

    Model admmdModel = theObject.getAdmMD();

    // Purge unused namespaces from ADM_MD.
    RDFUtils.removeUnusedNamespacesFromModel(admmdModel);

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Purged unused namespaces from ADM_MD metadata",
        theLogID);

    File adm = new File(result, DHCrudServiceImpl.ADMMD_PREFIX + RDFConstants.TTL_FILE_SUFFIX);
    try (BufferedWriter admWriter = new BufferedWriter(new FileWriter(adm))) {
      admmdModel.write(admWriter, RDFConstants.TURTLE);
      admWriter.close();
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "ADM_MD filename: " + adm.getName(), theLogID);
    DHCrudServiceImpl.log(CrudService.DEBUG, meth,
        "ADM_MD string:\n" + RDFUtils.getTTLFromModel(admmdModel), theLogID);

    // **
    // Create technical metadata file, if configured.
    // **

    File tech =
        new File(result, DHCrudServiceImpl.TECHMD_PREFIX + DHCrudServiceImpl.XML_FILE_SUFFIX);
    String techString = "";
    if (extractTechMd) {

      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "EXTRACT_TECHMD: " + extractTechMd, theLogID);
      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "FITS_LOCATION: " + theFitsLocation, theLogID);

      // Extract technical metadata to string.
      techString = LTPUtils.extractTechMD(theFitsLocation, theFitsClientTimeout, newData);

      // Set techmd to storage object.
      theObject.setTechMD(techString);

    }
    // Do use given TECHMD if provided... just used for testing at the moment...
    else {
      if (theObject.getTechMD() != null && !theObject.getTechMD().isEmpty()) {
        techString = theObject.getTechMD();
      }
    }

    // Write TECHMD string to file, if not empty.
    if (!techString.isEmpty()) {
      try (BufferedWriter techWriter = new BufferedWriter(new FileWriter(tech))) {
        techWriter.write(techString);
        techWriter.close();
      }

      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "TECH_MD filename: " + tech.getName(),
          theLogID);
      DHCrudServiceImpl.log(CrudService.DEBUG, meth,
          "TECH_MD string:\n" + IOUtils.toString(new FileInputStream(tech), DEFAULT_CHARSET),
          theLogID);
    }

    // **
    // TODO Handle provenance metadata file!
    // **

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Data and metadata temp file creation complete",
        theLogID);

    // **
    // Return the bag temp folder.
    // **

    return result;
  }

  /**
   * @param thePid
   * @param theMethodInfo
   * @param theLogID
   * @param theConf
   * @return
   * @throws IOException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws IoFault
   * @throws URISyntaxException
   * @throws java.text.ParseException
   */
  protected static File storeDATAFromZIPToTempFile(URI thePid, CrudServiceMethodInfo theMethodInfo,
      String theLogID, CrudServiceConfigurator theConf)
      throws IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException,
      IoFault, URISyntaxException, java.text.ParseException {

    File result = null;

    File zipTempFile = File.createTempFile("dhcrud_", ".zip");
    long start = System.currentTimeMillis();

    URI location = getBagURL(thePid, theConf.getPIDrESOLVER(), theLogID);

    DHCrudServiceImpl.log(CrudService.DEBUG, theMethodInfo,
        "Downloading temp ZIP bag to " + zipTempFile.getCanonicalPath(), theLogID);

    long bytesRead = 0;
    try (FileOutputStream out = new FileOutputStream(zipTempFile)) {
      try (InputStream in = DHCrudServiceStorageDariahImpl.getBagStreamPublic(location, theLogID)) {
        bytesRead = IOUtils.copyLarge(in, out);
        in.close();
      }
      out.close();
    }

    DHCrudServiceImpl.log(
        CrudService.DEBUG, theMethodInfo, "ZIP download complete ("
            + (System.currentTimeMillis() - start) + " millis --> read " + bytesRead + " bytes)",
        theLogID);

    result = storeDATAToTempFile(zipTempFile, theMethodInfo, theLogID);

    // Delete ZIP file.
    DHCrudServiceUtilities.deleteTempFile(zipTempFile, theMethodInfo.getName(), theLogID);

    return result;
  }

  /**
   * @param theZIPFile
   * @param theMethodInfo
   * @param theLogID
   * @return
   * @throws IOException
   */
  protected static File storeDATAToTempFile(File theZIPFile,
      CrudServiceMethodInfo theMethodInfo, String theLogID) throws IOException {

    File result = File.createTempFile("dhcrud_", ".data");

    try (ZipFile zipTempBag = new ZipFile(theZIPFile)) {
      Enumeration<? extends ZipEntry> entries = zipTempBag.entries();
      ZipEntry entry = null;
      while (entries.hasMoreElements()) {
        entry = entries.nextElement();
        // Look for data entry, store it!
        if (entry.getName().substring(entry.getName().lastIndexOf(File.separatorChar) + 1)
            .startsWith("data")) {
          long start = System.currentTimeMillis();

          DHCrudServiceImpl.log(CrudService.DEBUG, theMethodInfo,
              "Storing temp data file to " + result.getCanonicalPath(), theLogID);

          try (FileOutputStream out = new FileOutputStream(result)) {
            try (InputStream in = zipTempBag.getInputStream(entry)) {
              long bytes = IOUtils.copyLarge(in, out);
              out.close();
              in.close();

              DHCrudServiceImpl.log(CrudService.DEBUG, theMethodInfo, "Data download complete ("
                  + (System.currentTimeMillis() - start) + " millis -- > read " + bytes + " bytes)",
                  theLogID);
            }
          }
        }
      }

      zipTempBag.close();
    }

    return result;
  }

  /**
   * @param theZIPStream
   * @param theObject
   * @param theLogID
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  protected static void addDHCrudDariahObjectMetadataFromZIPStream(ZipInputStream theZIPStream,
      CrudObject<Model> theObject, String theLogID)
      throws IOException, org.apache.jena.riot.lang.extra.javacc.ParseException {

    String meth = UNIT_NAME + "addDHCrudDariahObjectMetadataFromZIPStream";

    // Go through every ZIP entry: Read metadata first! We assume, we always get metadata entries
    // (DMD, ADMMD, TECHMD) before getting the data entry!
    ZipEntry entry;
    boolean gotDMD = false;
    boolean gotADMMD = false;
    boolean gotTECHMD = false;
    while ((entry = theZIPStream.getNextEntry()) != null) {

      String e = entry.getName().substring(entry.getName().lastIndexOf(File.separatorChar) + 1);

      // Break loop if metadata has already been read completely.
      if (gotDMD && gotADMMD && gotTECHMD) {
        break;
      }

      // Look for descriptive metadata file.
      if (e.startsWith(DHCrudServiceImpl.DMD_PREFIX)) {

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Reading DMD: " + entry.getName(), theLogID);

        // Give back only entry's size of the bag's metadata input stream.
        String metadata = IOUtils.toString(new BoundedInputStream(theZIPStream, entry.getSize()),
            DEFAULT_CHARSET);
        theObject.setMetadata(RDFUtils.readModel(metadata, RDFConstants.TURTLE));

        gotDMD = true;

        theZIPStream.closeEntry();
        continue;
      }

      // Look for administrative metadata file.
      if (e.startsWith(DHCrudServiceImpl.ADMMD_PREFIX)) {

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Reading ADMMD: " + entry.getName(),
            theLogID);

        // Give back only entry's size of the bag's administrative metadata input stream.
        String adm = IOUtils.toString(new BoundedInputStream(theZIPStream, entry.getSize()),
            DEFAULT_CHARSET);
        theObject.setAdmMD(RDFUtils.readModel(adm, RDFConstants.TURTLE));

        gotADMMD = true;

        theZIPStream.closeEntry();
        continue;
      }

      // Look for TECHMD metadata file.
      if (e.startsWith(DHCrudServiceImpl.TECHMD_PREFIX)) {

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Reading TECHMD: " + entry.getName(),
            theLogID);

        // Give back only entry's size of the bag's TECHMD metadata input stream.
        String tech = IOUtils.toString(new BoundedInputStream(theZIPStream, entry.getSize()),
            DEFAULT_CHARSET);
        theObject.setTechMD(tech);

        gotTECHMD = true;

        theZIPStream.closeEntry();
        continue;
      }
    }

    theZIPStream.close();

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Stream closed", theLogID);
  }

  /**
   * @param theRequest
   * @param theContent
   * @return
   * @throws IOException
   * @throws JSONException
   */
  private static URI getLocationFromJSON(String theRequest, InputStream theContent)
      throws IOException {

    URI result = null;

    String message =
        "The object for request " + theRequest + " could not be found in the DARIAH-DE Repository!";

    JSONObject jo;
    try {
      jo = new JSONObject(IOUtils.toString(theContent, DEFAULT_CHARSET));

      // FIXME Check responseCode here! If not 1 (or if 100), BAG metadata is not existing! See
      // <https://hdl.handle.net/api/handles/21.11113/0000-000B-C8EF-7?type=BAG> and
      // <https://hdl.handle.net/api/handles/21.11113/0000-000B-C8ED-9?type=BAG>!
      JSONArray ja = (JSONArray) jo.get("values");
      if (ja == null || ja.length() == 0) {
        throw new FileNotFoundException(message);
      }
      JSONObject jo2 = (JSONObject) ja.get(0);
      if (jo2 == null || jo2.length() == 0) {
        throw new FileNotFoundException(message);
      }
      JSONObject jo3 = (JSONObject) jo2.get("data");
      if (jo3 == null || jo3.length() == 0) {
        throw new FileNotFoundException(message);
      }
      result = URI.create(jo3.get("value").toString());
      if (result == null) {
        throw new FileNotFoundException(message);
      }
    } catch (JSONException e) {
      throw new FileNotFoundException(
          message + " [" + e.getClass().getName() + "] " + e.getMessage());
    }

    return result;
  }

}
