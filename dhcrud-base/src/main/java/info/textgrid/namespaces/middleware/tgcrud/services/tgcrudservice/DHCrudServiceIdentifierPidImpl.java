/**
 * This software is copyright (c) 2021 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.naming.AuthenticationException;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.codehaus.jettison.json.JSONException;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.pid.api.TGPidService;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2021-04-16 - Funk - Add Handle metadata method.
 * 
 * 2021-04-07 - Funk - reformat.
 * 
 * 2016-05-31 - Funk - Added PID service auth secret.
 * 
 * 2014-12-22 - Funk - First version.
 */

/**
 * <p>
 * This identifier implementation creates PIDs from the GWDG Handle Service (EPIC2).
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-04-26
 * @since 2014-12-22
 */

public class DHCrudServiceIdentifierPidImpl extends CrudServiceIdentifierAbs {

  // **
  // FINALS
  // **

  private static final String UNIT_NAME = "handle";

  // **
  // CLASS VARIABLES
  // **

  protected static TGPidService thePidService = null;

  // **
  // IMPLEMENTED METHODS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#getUris
   * (int)
   */
  @Override
  public List<URI> getUris(int theAmount) throws IoFault {

    String meth = UNIT_NAME + ".getUris()";

    List<URI> result = new ArrayList<URI>();

    try {
      // Initialise the DARIAH PID Service client.
      init();

      // Loop.
      for (int i = 0; i < theAmount; i++) {
        // Call PID service.
        String pid = thePidService.getDariahPid(this.conf.getIDsERVICEpASS());

        result.add(URI.create(pid));

      }
    } catch (IOException | AuthenticationException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "PIDs: " + result.toString());

    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * updatePidMetadata(java.net.URI)
   */
  @Override
  public void updatePidMetadata(URI theUri) throws IoFault {

    String meth = UNIT_NAME + ".updatePidMetadata()";

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Updating PID metadata");

    // Initialise the PID service client.
    init();

    String uriWithoutPrefix = LTPUtils.omitHdlPrefix(theUri.toString());

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "URI: " + theUri.toString());
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Key/values: " + this.keyValueMap);

    // Call PID service.
    Response response = thePidService.updateMetadata(uriWithoutPrefix, convert(this.keyValueMap),
        this.conf.getIDsERVICEpASS());

    int statusCode = response.getStatus();
    String reasonPhrase = response.getStatusInfo().getReasonPhrase();

    if (statusCode != Status.NO_CONTENT.getStatusCode()) {

      String message = "Error updating PID metadata: " + statusCode + " " + reasonPhrase;

      CrudServiceUtilities.serviceLog(CrudService.ERROR, meth, message);

      throw new IoFault(message);
    }

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Update complete");
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * uriExists(java.net.URI)
   */
  @Override
  public boolean uriExists(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#lock(
   * java.net.URI, java.lang.String)
   */
  @Override
  public boolean lock(URI theUri, String theUser) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * isLockedBy(java.net.URI)
   */
  @Override
  public String isLockedBy(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * lockInternal(java.net.URI)
   */
  @Override
  public boolean lockInternal(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#unlock(
   * java.net.URI, java.lang.String)
   */
  @Override
  public boolean unlock(URI theUri, String theUser) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * unlockInternal(java.net.URI)
   */
  @Override
  public boolean unlockInternal(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.CrudServiceIdentifier#
   * getHandleMetadata(java.net.URI, java.lang.String)
   */
  @Override
  public String getHandleMetadata(URI theUri, String theType) throws IoFault, MetadataParseFault {

    String meth = UNIT_NAME + ".getHandleMetadata()";

    String result = null;

    // Initialise the PID service client.
    init();

    String uriWithoutPrefix = LTPUtils.omitHdlPrefix(theUri.toString());

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Fetching PID metadata of " + uriWithoutPrefix + " for " + theType + " metadata");

    // Call PID service.
    try {
      result =
          thePidService.getHandleMetadata(uriWithoutPrefix, this.conf.getIDsERVICEpASS(), theType);

      // Look for type metadata, throw error if not existing.
      boolean typeExisting = CrudServiceUtilities.checkTypeInHandleMetadata(result, theType);
      if (typeExisting) {
        return result;
      } else {
        String message = "No Handle metadata type=" + theType + " existing for " + theUri;
        throw new MetadataParseFault(message);
      }

    } catch (AuthenticationException | IOException | JSONException | ParseException e) {
      String message = "Error checking PID metadata type=" + theType + " for " + theUri.toString()
          + " due to a " + e.getClass().getName() + ": " + e.getMessage();

      CrudServiceUtilities.serviceLog(CrudService.ERROR, meth, message);

      throw new IoFault(message);
    }
  }

  // **
  // INTERNAL METHODS
  // **

  /**
   * <p>
   * Create key/value list from existing map.
   * </p>
   * 
   * @param theMap
   * @return
   */
  private ArrayList<String> convert(Map<String, String> theMap) {

    ArrayList<String> result = new ArrayList<String>();

    for (String s : theMap.keySet()) {
      result.add(s + "," + this.keyValueMap.get(s));
    }

    return result;
  }

  /**
   * @throws IoFault
   */
  private void init() throws IoFault {

    String meth = UNIT_NAME + ".init()";

    String pidServiceEndpoint = this.conf.getIDsERVICEuRL().toString();

    if (thePidService == null) {
      thePidService = JAXRSClientFactory.create(pidServiceEndpoint, TGPidService.class);

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Creating new PID service: " + pidServiceEndpoint);

    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Using existing PID service: " + pidServiceEndpoint);
    }
  }

}
