/**
 * This software is copyright (c) 2024 by
 *
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerConfigurationException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.InputStreamDataSource;
import org.apache.jena.rdf.model.Model;
import org.apache.tika.exception.TikaException;
import org.apache.tika.mime.MimeType;
import org.codehaus.jettison.json.JSONException;
import org.xml.sax.SAXException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import info.textgrid.middleware.common.CrudOperations;
import info.textgrid.middleware.common.DariahStorageClient;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.common.json.AuthInfo;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 *
 * 2018-05-04 - FIXME Check mimetype of ADM entries! In all methods! (see #35617)
 *
 **
 * CHANGELOG
 *
 * 2024-01-17 - Funk - Add new message producer.
 * 
 * 2023-07-06 - Funk - Add not found fault if error in reading ElasticSearch metadata. Fix NPE here.
 * 
 * 2022-05-24 - Funk - Fix some SpotBugs issues.
 *
 * 2021-06-29 - Funk - Always use mimetype extracted from tika (could also put into ADMMD right from
 * the start? (fixes #35556)
 *
 * 2021-05-04 - Funk - Add correct error handling for HTTP responses.
 * 
 * 2021-04-19 - Funk - Refactor citation for tombstone page to get from handle metadata.
 * 
 * 2021-04-13 - Funk - Add tombstone page and 410 GONE responses for all deleted objects.
 *
 * 2020-12-01 - Funk - Add a DHCrudServiceFileInputStream to delete temp files after close.
 *
 * 2020-09-24 - Funk - Implement control over mimetype (dcterms:format) in ADMMD --> Take it from
 * DMD if existing and correct, get it from Tika if not.
 *
 * 2020-07-02 - Funk - Fixed #32946. Using streaming now always in read() and readBagPack(). Both
 * are using now the new class DHCrudServiceWildcardFileInputStreamMessageBodyWriter (see
 * beans.xml).
 *
 * 2020-03-04 - Funk - Remove special header.
 *
 * 2019-03-25 - Funk - Implemented re-cache for PIDs.
 *
 * 2018-06-29 - Funk - Fixed #25068.
 *
 * 2018-05-15 - Funk - Added message producer.
 *
 * 2018-05-03 - Funk - Exception handling fixed, using RuntimeExceptions now.
 *
 * 2018-04-25 - Funk - Added streaming to read() method.
 *
 * 2018-04-23 - Funk - Fixed empty accept header bug for readRoot().
 *
 * 2018-03-09 - Funk - Added DOI support for reading.
 *
 * 2017-12-01 - Funk - Using new storage client.
 *
 * 2017-11-20 - Funk - Added logID.
 *
 * 2017-11-09 - Funk - Added content negotiation for root access.
 *
 * 2017-10-20 - Funk - Added technical metadata before adding metadata to ElasticSearch.
 *
 * 2017-10-17 - Funk - Added check for public crud to getPids method. Refactored create method using
 * data handler for metadata, too.
 *
 * 2017-08-24 - Funk - Refactored LastModified things.
 *
 * 2017-08-22 - Funk - Added direct TTL, JSON-LD, RDF, and NTRIPLES access for metadata and ADMMD.
 *
 * 2017-01-20 - Funk - Added PND and GND creator URLs.
 */

/**
 * <p>
 * <b>The DHCrudService implementation class.</b>
 * </p>
 *
 * <p>
 * This class implements the DARIAH methods of the CrudServiceGeneric class.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-10-02
 * @since 2014-09-09
 */

public class DHCrudServiceImpl extends CrudService implements DHCrudService {

  // **
  // FINAL STATICS
  // **

  protected static final String DMD_PREFIX = "01_metadata";
  protected static final String ADMMD_PREFIX = "02_adm";
  protected static final String TECHMD_PREFIX = "03_tech";
  protected static final String PROVMD_PREFIX = "04_prov";

  protected static final String DATA_PREFIX = "data";
  protected static final String DHREP_PREFIX = "dhrep_";
  protected static final String XML_FILE_SUFFIX = ".xml";

  protected static final String NOT_CREATED = "INDEX HTML could not be created!";
  protected static final String NO_PUBLIC_CRUD = "GO AWAY! THIS IS NO PUBLIC CRUD!";
  private static final String NO_EPPN =
      "The user's could not be extracted from the storage info call!";
  private static final String ERROR_READING_FROM_STORAGE = "Error reading from storage!";
  protected static final String ERROR_GONE =
      "Handle metadata DELETED exists, so this object has been deleted! Returning HTTP Status 410 GONE!";

  protected static final String HDL_RESPONSIBLE = "RESPONSIBLE";
  protected static final String HDL_URL = "URL";
  protected static final String HDL_BAG = "BAG";
  protected static final String HDL_DATA = "DATA";
  protected static final String HDL_METADATA = "METADATA";
  protected static final String HDL_ADMMD = "ADM_MD";
  protected static final String HDL_TECHMD = "TECH_MD";
  protected static final String HDL_PROVMD = "PROV_MD";
  protected static final String HDL_INDEX = "INDEX";
  protected static final String HDL_LANDING = "LANDING";
  protected static final String HDL_CHECKSUM = "CHECKSUM";
  protected static final String HDL_FILESIZE = "FILESIZE";
  protected static final String HDL_PUBDATE = "PUBDATE";
  protected static final String HDL_SOURCE = "SOURCE";
  protected static final String HDL_DOI = "DOI";
  protected static final String HDL_DELETED = "DELETED";

  private static final String LANDING_PAGE_TEMPLATE_LOCATION = "templates/landing.tmpl.html";
  private static final String TOMBSTONE_PAGE_TEMPLATE_LOCATION = "templates/tombstone.tmpl.html";
  // Use "inline" or "attachment" for unknown mimetype? "attachment" will always download from
  // browser, so we'll take it for the time being! "inline" is being used for downloading known
  // mimetypes as TTL or XML files.
  private static final String CONTENT_DISPOSITION_ATTACHMENT = "attachment";
  private static final String CONTENT_DISPOSITION_INLINE = "inline";

  // **
  // STATICS
  // **

  // Implementation classes.
  protected static CrudStorage<Model> dataStorageImplementation;
  protected static CrudStorage<Model> idxdbImplementation;
  private static ObjectMapper jsonObjectMapper = new ObjectMapper();
  private static DHCrudServiceVersion dhcrudServiceVersion = new DHCrudServiceVersion();

  // The DARIAH-DE storage client bean.
  private static DariahStorageClient storageClient = new DariahStorageClient();

  // **
  // CONSTRUCTOR
  // **

  /**
   * <p>
   * CXF calls this constructor just once at service start time... we call init() with every method
   * call. The configuration file location is set in the beans.xml configuration file as constructor
   * argument.
   * </p>
   *
   * @param theConfLocation
   * @throws IoFault
   */
  public DHCrudServiceImpl(final String theConfLocation) throws IoFault {

    // Set the services' version.
    new DHCrudServiceVersion();

    // TODO Check if we need a static reference for this configuration objects! Could be tested with
    // multiple DH-crud instances! Normally each DH-crud instance (if there were, we have got
    // singleton defined with CXF) should have it's own configuration, I suppose?
    this.configFileLocation = theConfLocation;

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.INIT);

    try {
      init(methodInfo, dhcrudServiceVersion);

      initImplementationClasses();

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));
    }

    // Catch any error that may occur to prevent SoapFaultExceptions.
    catch (RuntimeException r) {
      printExceptionInfos(r, methodInfo);
      throw CrudServiceExceptions.ioFault(r,
          INTERNAL_SERVICE_ERROR + (r.getCause() != null ? ": " + r.getCause().getMessage() : ""));
    }
  }

  /**
   * <p>
   * #CREATE
   * </p>
   */
  @Override
  @Hidden
  public Response create(DataHandler theMetadata, URI theDataUri, URI thePid,
      String theStorageToken, String theSeafileToken, String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.CREATE);

    try {
      // Check if we have a public crud. If not, #CREATE is not allowed!
      if (!conf.getDIRECTLYpUBLISHwITHcREATE()) {
        log(ERROR, methodInfo, NO_PUBLIC_CRUD, theLogID);
        throw new RuntimeException(new IOException(NO_PUBLIC_CRUD));
      }

      init(methodInfo, dhcrudServiceVersion);

      URI pid = DHCrudServiceUtilities.checkPid(thePid, methodInfo, conf.getPIDpREFIX(),
          conf.getDOIpREFIX(), theLogID);

      return createInternal(theMetadata, theDataUri, pid, theStorageToken, theSeafileToken,
          methodInfo, theLogID);

    } catch (IoFault | ParseException | IOException
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #CREATEBINARY
   * </p>
   */
  @Override
  @Hidden
  public Response createBinary(DataHandler theMetadata, DataHandler theData, final URI thePid,
      final String theStorageToken, final String theSeafileToken, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.CREATE);

    try {
      // Check if we have a public crud. If not, #CREATEBINARY is not allowed!
      // TODO That seems to be the other way around here in DH-crud! Please check!
      if (!conf.getDIRECTLYpUBLISHwITHcREATE()) {
        log(ERROR, methodInfo, NO_PUBLIC_CRUD, theLogID);
        throw new RuntimeException(NO_PUBLIC_CRUD);
      }

      init(methodInfo, dhcrudServiceVersion);

      URI pid = DHCrudServiceUtilities.checkPid(thePid, methodInfo, conf.getPIDpREFIX(),
          conf.getDOIpREFIX(), theLogID);

      return createInternal(theMetadata, theData, pid, theStorageToken, theSeafileToken, methodInfo,
          theLogID);

    } catch (IoFault | ParseException | IOException
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #GETPIDS
   * </p>
   */
  @Override
  @Hidden
  public Response getPids(final int howMany, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.GETURI);

    try {
      // Check if we have a public crud. If not, #GETPIDS is not allowed!
      if (!conf.getDIRECTLYpUBLISHwITHcREATE()) {
        log(ERROR, methodInfo, NO_PUBLIC_CRUD, theLogID);
        throw new RuntimeException(NO_PUBLIC_CRUD);
      }

      init(methodInfo, dhcrudServiceVersion);

      // Get PIDs, store PID metadata later (create PID at first without PID metadata key/values).

      log(INFO, methodInfo,
          "Requesting " + howMany + " PID" + (howMany != 1 ? "s" : "") + " from Handle service",
          theLogID);

      List<URI> pids = identifierImplementation.getUris(howMany);

      log(INFO, methodInfo, "PID creation complete", theLogID);

      // Create response with PID list.
      ResponseBuilder rBuilder = Response.ok(pids.toString(), MediaType.TEXT_PLAIN);

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #GETDOIS
   * </p>
   */
  @Override
  @Hidden
  public Response getDois(final int howMany, final String theLogID) {
    return null;
  }

  /**
   * <p>
   * #READ
   * </p>
   */
  @Override
  @Operation(
      tags = "Data",
      summary = "Retrieve data from DARIAH-DE Repository.",
      description = "DH-crud response delivering the data part of a DARIAH-DE Repository Object.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.WILDCARD)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theOffset", required = false, example = "64738"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_0")
      })
  public Response read(final URI thePID, final long theOffset, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      init(methodInfo, dhcrudServiceVersion);

      URI pid = DHCrudServiceUtilities.checkPid(thePID, methodInfo, conf.getPIDpREFIX(),
          conf.getDOIpREFIX(), theLogID);

      // Get THE metadata BAG from public DARIAH storage using the PID, add log ID.
      DHCrudDariahObject bag = null;
      try {
        bag = (DHCrudDariahObject) DHCrudServiceStorageDariahImpl.retrieveMetadataPublicBag(pid,
            conf.getPIDrESOLVER(), theLogID);
        // TODO LogID already set here? SpotBugs says so!
        // bag.setLogID(theLogID);
      } catch (IOException e) {

        log(DEBUG, methodInfo, ERROR_READING_FROM_STORAGE + " " + e.getMessage(), theLogID);

        // Check for DELETED flag in Handle metadata.
        try {
          String deletedResponse = identifierImplementation.getHandleMetadata(pid,
              DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
          boolean objectDeleted =
              CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse, HDL_DELETED);

          log(DEBUG, methodInfo, "Checking Handle metadata DELETED flag...");

          if (objectDeleted) {

            log(INFO, methodInfo, ERROR_GONE, theLogID);

            return deliverGONEResponse(pid, deletedResponse, theLogID);
          } else {
            throw e;
          }
        }
        // If not DELETED, just re-throw error!
        catch (MetadataParseFault | JSONException e1) {
          throw e;
        }
      }

      // Check for empty bag or empty ADM_MD!
      if (bag == null || bag.getAdmMD() == null || bag.getAdmMD().isEmpty()) {
        String message = "BAG or ADM_MD empty!";
        log(WARN, methodInfo, message, theLogID);
        throw new FileNotFoundException(message);
      }

      log(DEBUG, methodInfo, "ADM_MD string:\n" + RDFUtils.getTTLFromModel(bag.getAdmMD()),
          theLogID);

      // Get ZIP bag temp file from public DARIAH storage.
      final File tempData = DHCrudServiceStorageDariahImpl.storeDATAFromZIPToTempFile(pid,
          methodInfo, theLogID, conf);

      // Extract the most correct media type for the HTTP response.
      String mimetype = getCorrectMimetype(bag, tempData, methodInfo, theLogID);

      log(DEBUG, methodInfo, "Data is being STREAMED! YEAH!", theLogID);

      // We use here the newly implemented class
      // DHCrudServiceWildcardFileInputStreamMessageBodyWriter for writing data streams with correct
      // Content-Type HTTP header (fixes #32946), we always do STREAM here now!
      ResponseBuilder rBuilder = Response.ok(new DHCrudServiceFileInputStream(tempData));

      // Get file suffix from mimetype and assemble filename.
      String fileSuffix = LTPUtils.getFileExtension(mimetype);
      String responseFilename =
          DHCrudServiceUtilities.getFilenameFromUri(pid, DATA_PREFIX, fileSuffix);

      log(DEBUG, methodInfo, "Filename: " + responseFilename, theLogID);
      // Filesize must be put in, because we do STREAM!
      log(DEBUG, methodInfo, "Filesize: " + bag.getFilesize(), theLogID);
      log(DEBUG, methodInfo, "Mimetype: " + mimetype, theLogID);
      log(DEBUG, methodInfo, "Last-modified: " + bag.getLastModifiedDate(), theLogID);

      rBuilder.lastModified(bag.getLastModifiedDate())
          .header(HttpHeaders.CONTENT_TYPE, MediaType.valueOf(mimetype))
          .header(HttpHeaders.CONTENT_DISPOSITION,
              CONTENT_DISPOSITION_ATTACHMENT + "; filename=\"" + responseFilename + "\"")
          .header(HttpHeaders.CONTENT_LENGTH, bag.getFilesize());

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (IOException | ParseException | KeyManagementException | NoSuchAlgorithmException
        | KeyStoreException | URISyntaxException | IoFault | TikaException
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #READMETADATA
   * </p>
   */
  @Override
  @Operation(
      tags = "Descriptive Metadata (DMD)",
      summary = "Retrieve descriptive metadata from DARIAH-DE Repository (DMD).",
      description = "DH-crud response delivering the descriptive metadata of a DARIAH-DE Repository Object (TTL).",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_PLAIN)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_1")
      })
  public Response readMetadata(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readMetadata(thePID, RDFConstants.TURTLE, MediaType.TEXT_PLAIN,
          RDFConstants.TTL_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException | KeyManagementException
        | NoSuchAlgorithmException | KeyStoreException | URISyntaxException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READMETADATARDF
   * </p>
   */
  @Override
  @Operation(
      tags = "Descriptive Metadata (DMD)",
      summary = "Retrieve descriptive metadata from DARIAH-DE Repository (DMD).",
      description = "DH-crud response delivering the descriptive metadata of a DARIAH-DE Repository Object (RDF XML).",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_XML)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_2")
      })
  public Response readMetadataRDF(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readMetadata(thePID, RDFConstants.RDF_XML, MediaType.TEXT_XML,
          RDFConstants.RDFXML_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException | KeyManagementException
        | NoSuchAlgorithmException | KeyStoreException | URISyntaxException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READMETADATATTL
   * </p>
   */
  @Override
  @Operation(
      tags = "Descriptive Metadata (DMD)",
      summary = "Retrieve descriptive metadata from DARIAH-DE Repository (DMD).",
      description = "DH-crud response delivering the descriptive metadata of a DARIAH-DE Repository Object (TTL).",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_PLAIN)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_3")
      })
  public Response readMetadataTTL(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readMetadata(thePID, RDFConstants.TURTLE, MediaType.TEXT_PLAIN,
          RDFConstants.TTL_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException | KeyManagementException
        | NoSuchAlgorithmException | KeyStoreException | URISyntaxException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READMETADATANTRIPLES
   * </p>
   */
  @Override
  @Operation(
      tags = "Descriptive Metadata (DMD)",
      summary = "Retrieve descriptive metadata from DARIAH-DE Repository (DMD).",
      description = "DH-crud response delivering the descriptive metadata of a DARIAH-DE Repository Object (NTRIPLES).",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_PLAIN)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_4")
      })
  public Response readMetadataNTRIPLES(final URI thePID, final String theLogID) {
    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readMetadata(thePID, RDFConstants.NTRIPLES, MediaType.TEXT_PLAIN,
          RDFConstants.NTRIPLES_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException | KeyManagementException
        | NoSuchAlgorithmException | KeyStoreException | URISyntaxException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READMETADADTAJSON
   * </p>
   */
  @Override
  @Operation(
      tags = "Descriptive Metadata (DMD)",
      summary = "Retrieve descriptive metadata from DARIAH-DE Repository (DMD).",
      description = "DH-crud response delivering the descriptive metadata of a DARIAH-DE Repository Object (JSON).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.APPLICATION_JSON)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_5")
      })
  public Response readMetadataJSON(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readMetadata(thePID, RDFConstants.RDF_JSON, MediaType.APPLICATION_JSON,
          RDFConstants.RDFJSON_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException | KeyManagementException
        | NoSuchAlgorithmException | KeyStoreException | URISyntaxException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READMETADADTAJSONLD
   * </p>
   */
  @Override
  @Operation(
      tags = "Descriptive Metadata (DMD)",
      summary = "Retrieve descriptive metadata from DARIAH-DE Repository (DMD).",
      description = "DH-crud response delivering the descriptive metadata of a DARIAH-DE Repository Object (JSONLD).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.APPLICATION_JSON)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_6")
      })
  public Response readMetadataJSONLD(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readMetadata(thePID, RDFConstants.JSON_LD, MediaType.APPLICATION_JSON,
          RDFConstants.JSONLD_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException | KeyManagementException
        | NoSuchAlgorithmException | KeyStoreException | URISyntaxException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READADMMD
   * </p>
   */
  @Override
  @Operation(
      tags = "Administrative Metadata (ADMMD)",
      summary = "Retrieve administrative metadata from DARIAH-DE Repository (ADMMD).",
      description = "DH-crud response delivering the administrative metadata of a DARIAH-DE Repository Object (TTL).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.TEXT_PLAIN)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_7")
      })
  public Response readAdmMD(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readAdmMD(thePID, RDFConstants.TURTLE, MediaType.TEXT_PLAIN,
          RDFConstants.TTL_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READADMMDTTL
   * </p>
   */
  @Override
  @Operation(
      tags = "Administrative Metadata (ADMMD)",
      summary = "Retrieve administrative metadata from DARIAH-DE Repository (ADMMD).",
      description = "DH-crud response delivering the administrative metadata of a DARIAH-DE Repository Object (TTL).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.TEXT_PLAIN)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_8")
      })
  public Response readAdmMDTTL(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readAdmMD(thePID, RDFConstants.TURTLE, MediaType.TEXT_PLAIN,
          RDFConstants.TTL_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READADMMDRDF
   * </p>
   */
  @Override
  @Operation(
      tags = "Administrative Metadata (ADMMD)",
      summary = "Retrieve administrative metadata from DARIAH-DE Repository (ADMMD).",
      description = "DH-crud response delivering the administrative metadata of a DARIAH-DE Repository Object (RDF XML).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.TEXT_XML)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_9")
      })
  public Response readAdmMDRDF(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readAdmMD(thePID, RDFConstants.RDF_XML, MediaType.TEXT_XML,
          RDFConstants.RDFXML_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READADMMDJSON
   * </p>
   */
  @Override
  @Operation(
      tags = "Administrative Metadata (ADMMD)",
      summary = "Retrieve administrative metadata from DARIAH-DE Repository (ADMMD).",
      description = "DH-crud response delivering the administrative metadata of a DARIAH-DE Repository Object (JSON).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.APPLICATION_JSON)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_10")
      })
  public Response readAdmMDJSON(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readAdmMD(thePID, RDFConstants.RDF_JSON, MediaType.APPLICATION_JSON,
          RDFConstants.RDFJSON_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READADMMDNTRIPLES
   * </p>
   */
  @Override
  @Operation(
      tags = "Administrative Metadata (ADMMD)",
      summary = "Retrieve administrative metadata from DARIAH-DE Repository (ADMMD).",
      description = "DH-crud response delivering the administrative metadata of a DARIAH-DE Repository Object (NTRIPLES).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.TEXT_PLAIN)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_11")
      })
  public Response readAdmMDNTRIPLES(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readAdmMD(thePID, RDFConstants.NTRIPLES, MediaType.TEXT_PLAIN,
          RDFConstants.NTRIPLES_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READADMMDJSONLD
   * </p>
   */
  @Override
  @Operation(
      tags = "Administrative Metadata (ADMMD)",
      summary = "Retrieve administrative metadata from DARIAH-DE Repository (ADMMD).",
      description = "DH-crud response delivering the administrative metadata of a DARIAH-DE Repository Object (JSONLD).",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = MediaType.APPLICATION_JSON)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_12")
      })
  public Response readAdmMDJSONLD(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    Response result = null;

    try {
      init(methodInfo, dhcrudServiceVersion);

      result = readAdmMD(thePID, RDFConstants.JSON_LD, MediaType.APPLICATION_JSON,
          RDFConstants.JSONLD_FILE_SUFFIX, theLogID, methodInfo);

    } catch (IOException | ParseException | IoFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }

    return result;
  }

  /**
   * <p>
   * #READTECHMD
   * </p>
   */
  @Override
  @Operation(
      tags = "Technical Metadata (TECHMD)",
      summary = "Retrieve technical metadata from DARIAH-DE Repository (TECHMD).",
      description = "DH-crud response delivering the technical metadata of a DARIAH-DE Repository Object (XML).",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_XML)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_13")
      })
  public Response readTechMD(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READMETADATA);

    try {
      init(methodInfo, dhcrudServiceVersion);

      URI pid = DHCrudServiceUtilities.checkPid(thePID, methodInfo, conf.getPIDpREFIX(),
          conf.getDOIpREFIX(), theLogID);

      // Get metadata from public DARIAH storage using the PID, add log ID.
      DHCrudDariahObject bag = new DHCrudDariahObject(pid);
      bag.setLogID(theLogID);

      // Get metadata from public storage using PID.
      try {
        DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadata(pid, bag,
            conf.getPIDrESOLVER(), theLogID);
      } catch (IOException e) {

        log(DEBUG, methodInfo, ERROR_READING_FROM_STORAGE + " " + e.getMessage(), theLogID);

        // Check for DELETED flag in Handle metadata.
        try {
          String deletedResponse = identifierImplementation.getHandleMetadata(pid,
              DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
          boolean objectDeleted =
              CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse, HDL_DELETED);

          log(DEBUG, methodInfo, "Checking Handle metadata DELETED flag...");

          if (objectDeleted) {

            log(INFO, methodInfo, ERROR_GONE, theLogID);

            return deliverGONEResponse(pid, deletedResponse, theLogID);
          } else {
            throw e;
          }
        }
        // If not DELETED, just re-throw error!
        catch (MetadataParseFault | JSONException e1) {
          throw e;
        }
      }

      // Check for empty metadata!
      if (bag.getMetadata() == null || bag.getMetadata().isEmpty() || bag.getAdmMD() == null
          || bag.getAdmMD().isEmpty()) {
        String message = NOT_CREATED + " No metadata access!";
        log(WARN, methodInfo, message, theLogID);
        throw new FileNotFoundException(message);
      }

      // Get TECHMD string and compute size.
      String techMd = bag.getTechMD().trim();
      int techFileSize = DHCrudServiceUtilities.getHTMLContentLength(techMd);

      // Create response with TECH_MD and mimetype XML.
      ResponseBuilder rBuilder = Response.ok(techMd);

      log(DEBUG, methodInfo, "TECH_MD string:\n" + techMd, theLogID);

      // Set header metadata.
      String filename =
          DHCrudServiceUtilities.getFilenameFromUri(pid, TECHMD_PREFIX, XML_FILE_SUFFIX);
      log(DEBUG, methodInfo, "Filename: " + filename, theLogID);
      log(DEBUG, methodInfo, "Mimetype: " + MediaType.TEXT_XML, theLogID);
      log(DEBUG, methodInfo, "Last-modified: " + bag.getLastModifiedDate(), theLogID);
      log(DEBUG, methodInfo, "Content-Length: " + techFileSize, theLogID);

      rBuilder.type(MediaType.TEXT_XML).lastModified(bag.getLastModifiedDate())
          .header(HttpHeaders.CONTENT_DISPOSITION,
              CONTENT_DISPOSITION_INLINE + "; filename=\"" + filename + "\"")
          .header(HttpHeaders.CONTENT_LENGTH, techFileSize);

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException
        | URISyntaxException | IoFault | IOException | ParseException
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #READPROVMD
   * </p>
   */
  @Override
  @Hidden
  public Response readProvMD(final URI pid, final String theLogID) {
    throw new UnsupportedOperationException(CrudStorageAbs.NOT_IMPLEMENTED);
  }

  /**
   * <p>
   * #READLANDING
   * </p>
   */
  @Override
  @Operation(
      tags = "HTML Pages",
      summary = "Retrieve landig page from DARIAH-DE Repository.",
      description = "DH-crud response delivering the landing page of a DARIAH-DE Repository Object (HTML), containing DOI, basic metadata and a download link of this object.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_HTML)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_15")
      })
  public Response readLanding(final URI thePID, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      init(methodInfo, dhcrudServiceVersion);

      URI pid = DHCrudServiceUtilities.checkPid(thePID, methodInfo, conf.getPIDpREFIX(),
          conf.getDOIpREFIX(), theLogID);

      // Create storage object, add log ID.
      DHCrudDariahObject bag = new DHCrudDariahObject(pid);
      bag.setLogID(theLogID);

      // Get metadata from public storage using PID.
      try {
        DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadata(pid, bag,
            conf.getPIDrESOLVER(), theLogID);
      } catch (IOException e) {

        log(DEBUG, methodInfo, e.getMessage(), theLogID);

        // Check for DELETED flag in Handle metadata.
        try {
          String deletedResponse = identifierImplementation.getHandleMetadata(pid,
              DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
          boolean objectDeleted = CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse,
              DHCrudServiceImpl.HDL_DELETED);
          if (objectDeleted) {

            log(INFO, methodInfo, ERROR_GONE, theLogID);

            return deliverTombstonePage(pid, deletedResponse, theLogID);
          } else {
            throw e;
          }
        }
        // If not DELETED, just re-throw error!
        catch (MetadataParseFault | JSONException e1) {
          throw e;
        }
      }

      // Check for empty metadata!
      if (bag.getMetadata() == null || bag.getMetadata().isEmpty() || bag.getAdmMD() == null
          || bag.getAdmMD().isEmpty()) {
        String message = NOT_CREATED + " No metadata access!";
        log(WARN, methodInfo, message, theLogID);
        throw new FileNotFoundException(message);
      }

      // Get data, if type is DARIAH-DE collection.
      String dataString = "";
      if (bag.isCollection()) {
        File theData = DHCrudServiceStorageDariahImpl.storeDATAFromZIPToTempFile(pid, methodInfo,
            theLogID, conf);
        bag.setData(new DataHandler(new FileDataSource(theData)));
        dataString = IOUtils.toString(bag.getData().getInputStream());
        if (bag.getData() == null || bag.getData().getInputStream() == null) {
          String message = NOT_CREATED + " No collection data access!";
          log(WARN, methodInfo, message, theLogID);
          throw new FileNotFoundException(message);
        }
        if (theData != null) {
          DHCrudServiceUtilities.deleteTempFile(theData, methodInfo.getName(), theLogID);
        }
      }

      // Create response with HTML and mimetype XML.
      String version = dhcrudServiceVersion.getFULLVERSION();

      HashMap<String, Object> templateMap = null;
      templateMap = DHCrudServiceUtilities.createHTMLPageHashMap(pid, bag.getMetadata(),
          bag.getAdmMD(), dataString, version, conf, theLogID);
      String landingHTML = getLandingHTMLFromTemplate(templateMap);

      ResponseBuilder rBuilder = Response.ok(landingHTML, MediaType.TEXT_HTML);

      // Get last modified date from ADM_MD.
      String date = RDFUtils.findFirstObject(bag.getAdmMD(), LTPUtils.resolveHandlePid(pid),
          RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_MODIFIED);
      Date lastModified = CrudServiceUtilities.getDateFromCETString(date);

      log(DEBUG, methodInfo, "Last-Modified: " + lastModified, theLogID);

      // Set last modified date for client caching reasons.
      rBuilder.lastModified(lastModified);

      // Set length.
      rBuilder.header(HttpHeaders.CONTENT_LENGTH,
          DHCrudServiceUtilities.getHTMLContentLength(landingHTML));

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException
        | URISyntaxException | IoFault | XMLStreamException | ParseException
        | org.apache.jena.riot.lang.extra.javacc.ParseException | IOException
        | TransformerConfigurationException | JSONException | SAXException
        | ParserConfigurationException | DatatypeConfigurationException | ObjectNotFoundFault e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #READROOT
   * </p>
   */
  @Override
  @Operation(
      tags = "Content Negotiation",
      summary = "Retrieve content negotiated object from DARIAH-DE Repository.",
      description = "DH-crud response delivering either a complete object package (metadata and data in form of a Bagit ZIP package), the HTML landing page, or the data object only of the repository object.",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = DHCrudService.APPLICATION_ZIP)),
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_HTML)),
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.WILDCARD)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theAcceptHeader", required = false,
              example = DHCrudService.APPLICATION_ZIP),
          @Parameter(name = "theAcceptHeader", required = false, example = MediaType.TEXT_HTML),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_16")
      })
  public Response readRoot(final URI thePID, final String theAcceptHeader, final String theLogID) {

    // Do negotiate content!
    if (theAcceptHeader != null && theAcceptHeader.contains("html")) {
      return readLanding(thePID, theLogID);
    } else if (theAcceptHeader != null && theAcceptHeader.contains("zip")) {
      return readBag(thePID, theLogID);
    } else {
      // Otherwise do return the data object only.
      return read(thePID, 0, theLogID);
    }
  }

  /**
   * <p>
   * #READBAG
   * </p>
   */
  @Override
  @Operation(
      tags = "Bags",
      summary = "Retrieve complete Bagit object package from DARIAH-DE Repository.",
      description = "DH-crud response delivering complete object package, including all metadata and data in form of a Bagit ZIP package.",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = DHCrudService.APPLICATION_ZIP)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_17")
      })
  public Response readBag(final URI thePid, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      init(methodInfo, dhcrudServiceVersion);

      return readBag(thePid, conf, theLogID, methodInfo);

    } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException
        | URISyntaxException | ParseException | IoFault | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #READBACKPACK
   * </p>
   */
  @Override
  @Operation(
      tags = "Bags",
      summary = "Retrieve complete RDA BagPack object package from DARIAH-DE Repository.",
      description = "DH-crud response delivering complete RDA Research Data Repository Interoperability WG BagPack object package in form of a Bagit ZIP package.",
      responses = {
          @ApiResponse(responseCode = "200",
              content = @Content(mediaType = DHCrudService.APPLICATION_ZIP)),
          @ApiResponse(responseCode = "400"),
          @ApiResponse(responseCode = "404")
      },
      parameters = {
          @Parameter(name = "thePID", required = true, example = "21.11113/0000-000B-C8F0-4"),
          @Parameter(name = "theLogID", required = false, example = "openapi_53280_18")
      })
  public Response readBagPack(final URI thePID, final String theLogID) {

    Response result = null;

    // TODO ADD HEADER METADATA AS IN READ AND READMETADATA METHODS!

    // TODO USE EXISTING BAG TO ONLY ADD BAGPACK PARTS!!

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      init(methodInfo, dhcrudServiceVersion);

      URI pid = DHCrudServiceUtilities.checkPid(thePID, methodInfo, conf.getPIDpREFIX(),
          conf.getDOIpREFIX(), theLogID);

      // Read DARIAH-DE bag and create DHCrudDariahObject.
      DHCrudDariahObject bag = new DHCrudDariahObject(pid);
      bag.setLogID(theLogID);

      // Get metadata from public storage using PID.
      try {
        DHCrudServiceStorageDariahImpl.addDHCrudDariahObjectMetadata(pid, bag,
            conf.getPIDrESOLVER(), theLogID);
      } catch (IOException e) {

        log(DEBUG, methodInfo, ERROR_READING_FROM_STORAGE + " " + e.getMessage());

        // Check for DELETED flag in Handle metadata.
        try {
          String deletedResponse = identifierImplementation.getHandleMetadata(pid,
              DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
          boolean objectDeleted =
              CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse, HDL_DELETED);

          log(DEBUG, methodInfo, "Checking Handle metadata DELETED flag...");

          if (objectDeleted) {

            log(INFO, methodInfo, ERROR_GONE, theLogID);

            return deliverGONEResponse(pid, deletedResponse, theLogID);
          } else {
            throw e;
          }
        }
        // If not DELETED, just re-throw error!
        catch (MetadataParseFault | JSONException e1) {
          throw e;
        }
      }

      // Check for empty metadata.
      if (bag.getMetadata() == null || bag.getMetadata().isEmpty() || bag.getAdmMD() == null
          || bag.getAdmMD().isEmpty()) {
        String message = NOT_CREATED + " No metadata access!";
        log(WARN, methodInfo, message, theLogID);
        throw new FileNotFoundException(message);
      }

      // Get data.
      File theData = DHCrudServiceStorageDariahImpl.storeDATAFromZIPToTempFile(pid, methodInfo,
          theLogID, conf);
      bag.setData(new DataHandler(new FileDataSource(theData)));
      if (bag.getData() == null || bag.getData().getInputStream() == null) {
        String message = NOT_CREATED + " No data access!";
        log(WARN, methodInfo, message, theLogID);
        throw new FileNotFoundException(message);
      }

      // Prepare BagPack ZIP file.
      Path theBagPackPath = DHCrudServiceStorageDariahImpl.createRDAWGRIBagit(pid.toString(), bag,
          theLogID, conf.getDOIaPIlOCATION().toString());

      Long backPackLength = theBagPackPath.toFile().length();

      // Create response with data stream.
      ResponseBuilder rBuilder =
          Response.ok(new DHCrudServiceFileInputStream(theBagPackPath.toFile()));

      // Add filename and other headers.
      String filename = DHCrudServiceUtilities.getFilenameFromUri(pid,
          DHCrudServiceStorageDariahImpl.BAGPACK_FILE_SUFFIX,
          DHCrudServiceStorageDariahImpl.ZIP_FILE_SUFFIX);
      rBuilder
          .header(HttpHeaders.CONTENT_DISPOSITION,
              CONTENT_DISPOSITION_INLINE + "inline; filename=\"" + filename + "\"")
          .header(HttpHeaders.CONTENT_LENGTH, backPackLength)
          .lastModified(bag.getLastModifiedDate());
      result = rBuilder.build();

      // Delete data temp file.
      if (theData != null) {
        DHCrudServiceUtilities.deleteTempFile(theData, methodInfo.getName(), theLogID);
      }

      // Do log.
      log(CrudService.DEBUG, methodInfo, "THE BAG's headers:" + result.getHeaders().toString(),
          theLogID);
      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      // Return response.
      return result;

    } catch (KeyManagementException | NoSuchAlgorithmException | KeyStoreException | IOException
        | URISyntaxException | XMLStreamException | ParseException | IoFault
        | org.apache.jena.riot.lang.extra.javacc.ParseException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * #GETVERSION
   * </p>
   */
  @Override
  @Operation(
      tags = "Version",
      summary = "Retrieve DH-crud service's version.",
      description = "DH-crud response delivering the service's version.",
      responses = {
          @ApiResponse(responseCode = "200", content = @Content(mediaType = MediaType.TEXT_PLAIN))
      })
  public String getVersion() {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.GETVERSION);

    log(DEBUG, methodInfo,
        CrudServiceUtilities.getVersionMethodLog(methodInfo, dhcrudServiceVersion));

    return dhcrudServiceVersion.getFULLVERSION();
  }

  /**
   * <p>
   * #RECACHE
   * </p>
   */
  @Override
  @Hidden
  public Response recache(final URI thePid, final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.RECACHE);

    log(INFO, methodInfo, "recache() called for PID " + thePid);

    // Send UPDATE message via message producer, if configured.
    try {
      if (conf.getUSEmESSAGING()) {

        DHCrudDariahObject dariahStorageObject = new DHCrudDariahObject(thePid);
        Model admMd = dataStorageImplementation.retrieveAdmMDPublic(thePid);
        dariahStorageObject.setAdmMD(admMd);

        log(DEBUG, methodInfo, "Retrieved ADMMD", theLogID);

        // TODO Check if public?

        // Get format.
        String format = dariahStorageObject.getMimetype();

        // TODO Get root collection?

        // Assemble correct PID.
        URI hdl = URI
            .create(RDFConstants.HDL_PREFIX + ":" + LTPUtils.omitHdlNamespace(thePid.toString()));

        // Produce UPDATE message :-)
        CrudServiceMessage message = new CrudServiceMessage()
            .setOperation(CrudOperations.UPDATE)
            .setObjectId(hdl)
            .setRootId("")
            .setFormat(format)
            .setPublic(true);
        messageProducer.sendMessage(conf, message);

        log(DEBUG, methodInfo, "UPDATE Message sent: " + message.toString(), theLogID);
      }

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      ResponseBuilder rBuilder = Response.ok();

      return rBuilder.build();

    } catch (IoFault f) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, f), theLogID);
      throw new RuntimeException(f);
    }
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Creates a new DARIAH object from String source.
   * </p>
   *
   * @param theMetadata
   * @param theData
   * @param thePid
   * @param theStorageToken
   * @param theSeafileToken
   * @param theMethodInfo
   * @param theLogID
   * @return
   * @throws IoFault
   * @throws ParseException
   * @throws IOException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   */
  private static Response createInternal(final DataHandler theMetadata, final Object theData,
      final URI thePid, final String theStorageToken, final String theSeafileToken,
      final CrudServiceMethodInfo theMethodInfo, final String theLogID)
      throws IoFault, ParseException, IOException,
      org.apache.jena.riot.lang.extra.javacc.ParseException {

    // Get ePPN from storage and check content.
    String tokenInfo = storageClient.setStorageUri(conf.getDEFAULTdATAsTORAGEuRI())
        .authInfo(theStorageToken, theLogID);
    String eppn = jsonObjectMapper.readValue(tokenInfo, AuthInfo.class).principal.name;
    if (eppn == null || "".equals(eppn)) {
      log(ERROR, theMethodInfo, NO_EPPN, theLogID);
      throw new RuntimeException(new IOException(NO_EPPN));
    }

    // **
    // Read data from DARIAH OwnStorage or given data string
    // **

    // Create DARIAH object for creation and add log ID for logging.
    DHCrudDariahObject dariahStorageObject = new DHCrudDariahObject(thePid);
    dariahStorageObject.setLogID(theLogID);

    // Put data into the object.
    String resource = LTPUtils.resolveHandlePid(thePid);

    log(DEBUG, theMethodInfo, "Using resource: " + resource, theLogID);

    if (theData instanceof DataHandler) {

      log(DEBUG, theMethodInfo, "Reading data from binary input", theLogID);

      dariahStorageObject.setData((DataHandler) theData);

    } else if (theData instanceof URI) {

      URI uri = (URI) theData;

      log(DEBUG, theMethodInfo, "Reading data from URI: " + uri.toString(), theLogID);

      String token = theStorageToken;
      if (uri.getHost().startsWith("seafile")) {
        token = theSeafileToken;
      }

      log(DEBUG, theMethodInfo, "Host: " + uri.getHost(), theLogID);
      log(DEBUG, theMethodInfo, "Token: " + (token != null ? "existing" : "NULL"), theLogID);
      log(DEBUG, theMethodInfo, "Path: " + uri.getPath(), theLogID);
      log(DEBUG, theMethodInfo, "Default storage URI: " + conf.getDEFAULTdATAsTORAGEuRI(),
          theLogID);

      URI ownStorageHost = URI.create(conf.getDEFAULTdATAsTORAGEuRI().getScheme() + "://"
          + conf.getDEFAULTdATAsTORAGEuRI().getHost());

      log(DEBUG, theMethodInfo, "OwnStorage host: " + ownStorageHost, theLogID);

      // Get Create web target and add token.
      Response response =
          storageClient.setStorageUri(ownStorageHost).readResponse(uri.getPath(), token, theLogID);
      int statusCode = response.getStatus();
      String reasonPhrase = response.getStatusInfo().getReasonPhrase();

      log(DEBUG, theMethodInfo, "Status: " + statusCode + " " + reasonPhrase, theLogID);

      // Everything is fine, if OK (correct file) or NO CONTENT (empty file).
      if (statusCode != Status.OK.getStatusCode()
          && statusCode != Status.NO_CONTENT.getStatusCode()) {
        throw new IOException("Error reading file: " + statusCode + " " + reasonPhrase);
      }

      InputStreamDataSource dataSource =
          new InputStreamDataSource(response.readEntity(InputStream.class), "");

      dariahStorageObject.setData(new DataHandler(dataSource));

    } else {
      throw new IOException("Wrong data type: Only DataHandler and URI are allowed!");
    }

    // Get metadata model from crud object.
    Model metadata =
        RDFUtils.readModel(IOUtils.toString(theMetadata.getInputStream()), RDFConstants.TURTLE);

    // Check for type metadata! Crud must get an appropriate type setting with dc:format.
    List<String> formatValues =
        RDFUtils.findAllObjects(metadata, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_FORMAT);

    log(DEBUG, theMethodInfo, "Format values: " + formatValues, theLogID);

    // Get DOI from dcterms:identifier, we have only one resource, and we do not know the resources
    // name, so we are looking for all DOI occurrences.

    log(DEBUG, theMethodInfo,
        "METADATA TTL FROM PUBLISH CALL:\n" + RDFUtils.getTTLFromModel(metadata), theLogID);

    String doiValue = RDFUtils.findFirstObject(metadata, null, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);
    String pidValue = RDFUtils.findFirstObject(metadata, null, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.HDL_NAMESPACE);

    log(DEBUG, theMethodInfo, "DOI: " + doiValue + " | PID: " + pidValue, theLogID);

    // **
    // Create administrative metadata (ADMMD).
    //
    // NOTE for URI handling (such as data contributor): If we have problems with metadata-core JAXB
    // mappings: We have got TWO different mappings for the XSD URI type. One from tgcrud-base and
    // it's WSDL mapping (XSD URI --> Java URI), and one from the esutils metadata-core-jaxb package
    // (XSD URI --> Java String). I excluded the one from esutils, so we can use URIs here!
    // **

    String rootCollection =
        RDFUtils.findFirstObject(metadata, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_RELATION);

    // Get source value from dcterms:source in metadata.
    String sourceValue = DHCrudServiceUtilities.getSourceFromRDF(metadata);

    log(DEBUG, theMethodInfo,
        "Value for dc:source: " + (sourceValue != null ? sourceValue : "null"), theLogID);

    String formatValue = "";
    Model admMd = null;
    if (formatValues.contains(TextGridMimetypes.DARIAH_COLLECTION)) {

      // Set format value to DARIAH collection!
      formatValue = TextGridMimetypes.DARIAH_COLLECTION;

      // Create collection's ADM_MD.
      admMd = DHCrudServiceUtilities.createAdministrativeMetadata(resource, eppn, doiValue,
          pidValue, rootCollection, formatValue);

      // Add source only if existing.
      if (!"".equals(sourceValue)) {
        RDFUtils.addLiteralToModel(admMd, resource, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DC_SOURCE, sourceValue);
      }

      log(DEBUG, theMethodInfo, "ADMMD dcterms:format [coll]: " + formatValue, theLogID);
      log(DEBUG, theMethodInfo, "ADMMD dcterms:source [coll]: " + sourceValue, theLogID);

      // Delete dcterms:source, dcterms:format, and dcterms:hasPart from collection DMD metadata.
      RDFUtils.removeFromModel(metadata, resource, RDFConstants.DCTERMS_PREFIX,
          RDFConstants.ELEM_DC_SOURCE);
      RDFUtils.removeFromModel(metadata, resource, RDFConstants.DCTERMS_PREFIX,
          RDFConstants.ELEM_DC_FORMAT);
      RDFUtils.removeHasPartListFromModel(metadata, resource);

      log(DEBUG, theMethodInfo, "Deletion of dcterms values from collection DMD complete",
          theLogID);
    }
    // We have got a data object!
    else {
      // Add first correct mimetype from DMD (dc:format)
      for (String format : formatValues) {
        // If not empty and valid, take it!
        if (!format.isEmpty() && MimeType.isValid(format)) {
          formatValue = format;

          log(DEBUG, theMethodInfo, "Using first valid format for ADMMD mimetype: " + formatValue,
              theLogID);

          break;
        }
      }

      // If format still empty, (FIXME Extract using Tika?!) or otherwise take default value!
      if (formatValue.isEmpty()) {
        formatValue = MediaType.APPLICATION_OCTET_STREAM;

        log(DEBUG, theMethodInfo, "Using DEFAULT mimetype: " + formatValue, theLogID);
      }

      // Create DataObject's ADM_MD.
      admMd = DHCrudServiceUtilities.createAdministrativeMetadata(resource, eppn, doiValue,
          pidValue, rootCollection, formatValue);

      RDFUtils.addLiteralToModel(admMd, resource, RDFConstants.DCTERMS_PREFIX,
          RDFConstants.ELEM_DC_FORMAT,
          formatValue);

      log(DEBUG, theMethodInfo, "dc:format [file]: " + formatValue, theLogID);
    }

    // Delete dcterms:identifier from object's and collection's metadata.
    RDFUtils.removeFromModel(metadata, resource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER);

    log(DEBUG, theMethodInfo, "Deletion of dcterms:identifier from DMD complete", theLogID);

    log(INFO, theMethodInfo, "Object's administrative metadata is being set", theLogID);

    dariahStorageObject.setAdmMD(admMd);

    log(DEBUG, theMethodInfo,
        "ADM_MD string:\n" + RDFUtils.getTTLFromModel(dariahStorageObject.getAdmMD()), theLogID);

    // Add format to object's mimetype.
    dariahStorageObject.setMimetype(formatValue);

    // Add creation and modified dates.
    Date date = new GregorianCalendar().getTime();
    dariahStorageObject.setCreationDate(date);
    dariahStorageObject.setLastModifiedDate(date);

    // **
    // Set metadata (DMD).
    // **

    log(INFO, theMethodInfo, "Object's descriptive metadata is being set", theLogID);

    dariahStorageObject.setMetadata(metadata);

    log(DEBUG, theMethodInfo,
        "METADATA string:\n" + RDFUtils.getTTLFromModel(dariahStorageObject.getMetadata()),
        theLogID);

    // **
    // Store object using storage implementation. Create technical metadata (TECHMD).
    // **

    log(INFO, theMethodInfo, "Object is being created", theLogID);

    dataStorageImplementation.create(dariahStorageObject, theStorageToken);

    log(INFO, theMethodInfo, "Object creation complete", theLogID);

    // **
    // Update PID metadata. Do add:
    //
    // -- RESPONSIBLE: creator (user's ePPN)
    // -- URL: Link to the CRUD's root bag page, so we can use HDL templates to add @data to the
    // PID, so we get the data file /data from DH-crud then.
    // http://hdl.handle.net/21.T11998/0000-0001-6E73-3@data {HDL}@data then goes to
    // http://box.dariah.local/1.0/dhcrud/21.T11998/0000-0001-6E73-3/data {URL/data}.
    // -- INDEX: link to index page
    // -- LANDING: link to landing page
    // -- DATA: data URL (DH-crud#READ)
    // -- METADATA: metadata URL (DH-crud#READMETADATA)
    // -- ADM_MD: URL to administrative metadata
    // -- TECH_MD: URL to technical metadata
    // -- PROV_MD: URL to provenance metadata
    // -- BAG: URL directly to DARIAH-DE Storage Service: THE BAG
    // -- CHECKSUM: checksum of THE BAG
    // -- FILESIZE: file size of THE BAG
    // -- PUBDATE: publication date
    // -- SOURCE: source URI for collections in DARIAH-DE OwnStorage
    // -- DOI: the DOI of the object
    //
    // NOTE: Update deletes old entries, so we have to add everything again.
    // **

    log(INFO, theMethodInfo, "Handle PID metadata is being updated", theLogID);

    // Get object's location for CRUD calls.
    String objectLocation = conf.getCRUDlOCATION().toString();
    if (!objectLocation.endsWith("/")) {
      objectLocation += "/";
    }
    String objectPidWithoutPrefix = LTPUtils.omitHdlPrefix(dariahStorageObject.getUri().toString());
    objectLocation += objectPidWithoutPrefix;
    // Get object's location in DARIAH-DE PublicStorage.
    String objectStorageLocation =
        LTPUtils.omitHdlPrefix(dariahStorageObject.getLocation().toString());

    // Create and set PID HashMap.
    HashMap<String, String> keyValuePairs = new HashMap<String, String>();
    keyValuePairs.put(HDL_RESPONSIBLE, eppn);
    keyValuePairs.put(HDL_URL, objectLocation);
    keyValuePairs.put(HDL_BAG, objectStorageLocation);
    keyValuePairs.put(HDL_DATA, objectLocation + "/data");
    keyValuePairs.put(HDL_METADATA, objectLocation + "/metadata");
    keyValuePairs.put(HDL_ADMMD, objectLocation + "/adm");
    keyValuePairs.put(HDL_TECHMD, objectLocation + "/tech");
    keyValuePairs.put(HDL_PROVMD, objectLocation + "/prov");
    keyValuePairs.put(HDL_INDEX, objectLocation + "/index");
    keyValuePairs.put(HDL_LANDING, objectLocation + "/landing");
    keyValuePairs.put(HDL_CHECKSUM, dariahStorageObject.getWrapperChecksumType().toLowerCase() + ":"
        + dariahStorageObject.getWrapperChecksum());
    keyValuePairs.put(HDL_FILESIZE, Long.toString(dariahStorageObject.getWrapperSize()));
    keyValuePairs.put(HDL_PUBDATE, new SimpleDateFormat(CrudServiceUtilities.DATE_FORMAT)
        .format(dariahStorageObject.getCreationDate()));
    identifierImplementation.setKeyValuePairs(keyValuePairs);
    // Add SOURCE only if existing (only is existing for collections).
    if (!"".equals(sourceValue)) {
      keyValuePairs.put(HDL_SOURCE, sourceValue);
    }
    keyValuePairs.put(HDL_DOI, doiValue);

    // Update PID metadata.
    identifierImplementation.updatePidMetadata(dariahStorageObject.getUri());

    log(INFO, theMethodInfo, "Handle PID metadata update complete", theLogID);

    // **
    // Store all the metadata to the ElasticSearch database (DMD, ADMMD, and TECHMD are already in
    // DARIAH storage object!)
    // **

    idxdbImplementation.createMetadata(dariahStorageObject);

    // **
    // Send #CREATE message to messaging system
    // **
    if (conf.getUSEmESSAGING()) {
      String rootCollectionHdl = "hdl:"
          + LTPUtils.omitNamespace(rootCollection, RDFConstants.DARIAH_COLLECTION);
      URI objectId =
          URI.create(RDFConstants.HDL_PREFIX + ":" + LTPUtils.omitHdlNamespace(pidValue));
      CrudServiceMessage message = new CrudServiceMessage()
          .setOperation(CrudOperations.CREATE)
          .setObjectId(objectId)
          .setRootId(rootCollectionHdl)
          .setFormat(formatValue)
          .setPublic(true);
      messageProducer.sendMessage(conf, message);
    }

    // **
    // Return HTTP response including:
    // -- location (URL to PID resolver)
    // -- lastModified (from generated metadata)
    // **

    ResponseBuilder rBuilder = Response.ok();
    rBuilder.location(URI.create(conf.getPIDrESOLVER().toString() + thePid));
    rBuilder.lastModified(dariahStorageObject.getCreationDate());

    log(INFO, theMethodInfo, CrudServiceUtilities.endMethodLog(theMethodInfo));

    return rBuilder.build();
  }

  /**
   * @param thePid
   * @param theFormat
   * @param theMediaType
   * @param theFileSuffix
   * @param theLogID
   * @param theMethodInfo
   * @return
   * @throws IoFault
   * @throws ParseException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws URISyntaxException
   * @throws IOException
   * @throws JSONException
   */
  private static Response readMetadata(final URI thePid, String theFormat, String theMediaType,
      String theFileSuffix, String theLogID, CrudServiceMethodInfo theMethodInfo)
      throws IoFault, ParseException, org.apache.jena.riot.lang.extra.javacc.ParseException,
      KeyManagementException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException,
      IOException, JSONException {

    URI pid = DHCrudServiceUtilities.checkPid(thePid, theMethodInfo, conf.getPIDpREFIX(),
        conf.getDOIpREFIX(), theLogID);

    // Get object including DMD, ADM_MD, and TECH_MD, set log ID.
    DHCrudDariahObject bag = null;
    try {
      bag = (DHCrudDariahObject) DHCrudServiceStorageDariahImpl.retrieveMetadataPublicBag(pid,
          conf.getPIDrESOLVER(), theLogID);
    } catch (IOException e) {

      log(DEBUG, theMethodInfo, ERROR_READING_FROM_STORAGE + " " + e.getMessage());

      // Check for DELETED flag in Handle metadata.
      try {
        String deletedResponse = identifierImplementation.getHandleMetadata(pid,
            DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
        boolean objectDeleted =
            CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse, HDL_DELETED);

        log(DEBUG, theMethodInfo, "Checking Handle metadata DELETED flag...");

        if (objectDeleted) {

          log(INFO, theMethodInfo, ERROR_GONE, theLogID);

          return deliverGONEResponse(pid, deletedResponse, theLogID);
        } else {
          throw e;
        }
      }
      // If not DELETED, just re-throw error!
      catch (MetadataParseFault | JSONException e1) {
        throw e;
      }
    }

    // Check for empty metadata!
    if (bag == null
        || bag.getMetadata() == null
        || bag.getMetadata().isEmpty()
        || bag.getAdmMD() == null
        || bag.getAdmMD().isEmpty()) {
      String message = "METADATA or ADM_MD empty!";
      log(WARN, theMethodInfo, message, theLogID);
      throw new FileNotFoundException(message);
    }

    log(DEBUG, theMethodInfo,
        "DMD string:\n" + RDFUtils.getStringFromModel(bag.getMetadata(), theFormat), theLogID);

    // Create response with DMD.
    ResponseBuilder rBuilder =
        Response.ok(RDFUtils.getStringFromModel(bag.getMetadata(), theFormat));

    // Set header metadata.
    String filename = DHCrudServiceUtilities.getFilenameFromUri(pid, DMD_PREFIX, theFileSuffix);

    log(DEBUG, theMethodInfo, "Filename: " + filename, theLogID);
    log(DEBUG, theMethodInfo, "Mimetype: " + theMediaType, theLogID);
    log(DEBUG, theMethodInfo, "Last-modified: " + bag.getLastModifiedDate(), theLogID);

    rBuilder.type(theMediaType).lastModified(bag.getLastModifiedDate()).header(
        HttpHeaders.CONTENT_DISPOSITION,
        CONTENT_DISPOSITION_INLINE + "inline; filename=\"" + filename + "\"");

    log(INFO, theMethodInfo, CrudServiceUtilities.endMethodLog(theMethodInfo));

    // Return response.
    return rBuilder.build();
  }

  /**
   * @param thePid
   * @param theFormat
   * @param theMediaType
   * @param theFileSuffix
   * @param theLogID
   * @param theMethodInfo
   * @return
   * @throws ParseException
   * @throws IoFault
   * @throws FileNotFoundException
   */
  private static Response readAdmMD(URI thePid, String theFormat, String theMediaType,
      String theFileSuffix, String theLogID, CrudServiceMethodInfo theMethodInfo)
      throws ParseException, IoFault, FileNotFoundException {

    URI pid = DHCrudServiceUtilities.checkPid(thePid, theMethodInfo, conf.getPIDpREFIX(),
        conf.getDOIpREFIX(), theLogID);

    // Get ADMMD and create object.
    DHCrudDariahObject bag = new DHCrudDariahObject(pid);
    try {
      Model admMd = dataStorageImplementation.retrieveAdmMDPublic(pid);
      bag.setAdmMD(admMd);
    } catch (IoFault e) {

      log(DEBUG, theMethodInfo, ERROR_READING_FROM_STORAGE + " " + e.getMessage());

      // Check for DELETED flag in Handle metadata.
      try {
        String deletedResponse = identifierImplementation.getHandleMetadata(pid,
            DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
        boolean objectDeleted =
            CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse, HDL_DELETED);

        log(DEBUG, theMethodInfo, "Checking Handle metadata DELETED flag...");

        if (objectDeleted) {

          log(INFO, theMethodInfo, ERROR_GONE, theLogID);

          return deliverGONEResponse(pid, deletedResponse, theLogID);
        } else {
          throw e;
        }
      }
      // If not DELETED, just re-throw error!
      catch (MetadataParseFault | JSONException e1) {
        throw e;
      }
    }

    // Check for empty ADM_MD!
    if (bag.getAdmMD() == null || bag.getAdmMD().isEmpty()) {
      String message = "ADM_MD empty!";
      throw new FileNotFoundException(message);
    }

    log(DEBUG, theMethodInfo,
        "ADM_MD string:\n" + RDFUtils.getStringFromModel(bag.getAdmMD(), theFormat), theLogID);

    // Create response with ADM_MD.
    ResponseBuilder rBuilder = Response.ok(RDFUtils.getStringFromModel(bag.getAdmMD(), theFormat));

    // Set header metadata.
    String filename = DHCrudServiceUtilities.getFilenameFromUri(pid, ADMMD_PREFIX, theFileSuffix);

    log(DEBUG, theMethodInfo, "Filename: " + filename, theLogID);
    log(DEBUG, theMethodInfo, "Mimetype: " + theMediaType, theLogID);
    log(DEBUG, theMethodInfo, "Last-modified: " + bag.getLastModifiedDate(), theLogID);

    rBuilder.type(theMediaType).lastModified(bag.getLastModifiedDate()).header(
        HttpHeaders.CONTENT_DISPOSITION,
        CONTENT_DISPOSITION_INLINE + "inline; filename=\"" + filename + "\"");

    log(INFO, theMethodInfo, CrudServiceUtilities.endMethodLog(theMethodInfo));

    // Return response.
    return rBuilder.build();
  }

  /**
   * <p>
   * Read bag basic method, because we have to cover /bag and / (root) access!
   * </p>
   *
   * @param thePid
   * @param theConfig
   * @param theLogID
   * @param theMethodInfo
   * @return
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   * @throws IoFault
   * @throws IOException
   * @throws URISyntaxException
   * @throws ParseException
   * @throws JSONException
   */
  private static Response readBag(final URI thePid, CrudServiceConfigurator theConfig,
      String theLogID, CrudServiceMethodInfo theMethodInfo) throws KeyManagementException,
      NoSuchAlgorithmException, KeyStoreException, IoFault, IOException, URISyntaxException,
      ParseException, JSONException {

    Response result = null;

    URI pid = DHCrudServiceUtilities.checkPid(thePid, theMethodInfo, conf.getPIDpREFIX(),
        conf.getDOIpREFIX(), theLogID);

    try {
      result = DHCrudServiceStorageDariahImpl.getBagResponsePublic(pid, theConfig.getPIDrESOLVER(),
          theLogID);
      int statusCode = result.getStatus();

      // Check status code
      if (statusCode == Status.NOT_FOUND.getStatusCode()) {
        throw new FileNotFoundException();
      } else if (statusCode != Status.OK.getStatusCode()
          && statusCode != Status.NO_CONTENT.getStatusCode()) {
        throw new IOException();
      }
    } catch (FileNotFoundException e) {

      log(DEBUG, theMethodInfo, "Object " + pid + " is not existing: " + e.getMessage()
          + "! Checking Handle metadata DELETED flag...", theLogID);

      // Check for DELETED flag in Handle metadata, if object is not existing.
      try {
        String deletedResponse = identifierImplementation.getHandleMetadata(pid,
            DHCrudServiceUtilities.HANDLE_DELETED_TYPE);
        boolean objectDeleted =
            CrudServiceUtilities.checkTypeInHandleMetadata(deletedResponse, HDL_DELETED);
        if (objectDeleted) {
          log(INFO, theMethodInfo, ERROR_GONE, theLogID);
          return deliverGONEResponse(pid, deletedResponse, theLogID);
        } else {
          throw e;
        }
      }
      // If not DELETED, just re-throw error!
      catch (MetadataParseFault e1) {
        throw e;
      }
    }

    return result;
  }

  /**
   * @param theInfo
   * @param theE
   * @return
   */
  private static String errorMethodLog(CrudServiceMethodInfo theInfo, Exception theE) {
    return CrudServiceUtilities.START_LOGCHARS + "ERROR: " + theE.getMessage() + " ["
        + theE.getClass().getName() + "]";
  }

  /**
   * @param theMap
   * @return
   * @throws IOException
   */
  protected static String getLandingHTMLFromTemplate(HashMap<String, Object> theMap)
      throws IOException {
    return getHTMLFromTemplate(LANDING_PAGE_TEMPLATE_LOCATION, theMap);
  }

  /**
   * @param theMap
   * @return
   * @throws IOException
   */
  protected static String getTombstoneHTMLFromTemplate(HashMap<String, Object> theMap)
      throws IOException {
    return getHTMLFromTemplate(TOMBSTONE_PAGE_TEMPLATE_LOCATION, theMap);
  }

  /**
   * <p>
   * Takes a String for template file and HashMap for inserting data.
   * </p>
   *
   * @param theTemplateLocation
   * @param theMap
   * @return HTML page as a string.
   * @throws IOException
   */
  private static String getHTMLFromTemplate(String theTemplateLocation,
      HashMap<String, Object> theMap) throws IOException {

    String result = "";

    MustacheFactory mf = new DefaultMustacheFactory();
    Mustache m = mf.compile(theTemplateLocation);

    try (Writer writer = new StringWriter()) {
      m.execute(writer, theMap);
      writer.flush();
      writer.close();
      result = writer.toString();
    }

    return result;
  }

  /**
   * @param theLogLevel
   * @param theMethodName
   * @param theLogMessage
   * @param theLogID
   */
  protected static void log(final int theLogLevel, final String theMethodName,
      final String theLogMessage, final String theLogID) {

    String message = theLogMessage;
    if (theLogID != null && !theLogID.isEmpty()) {
      message = "[" + theLogID + "] " + message;
    }

    CrudServiceUtilities.serviceLog(theLogLevel, theMethodName, message);
  }

  /**
   * @param theLogLevel
   * @param theMethodInfo
   * @param theLogMessage
   * @param theLogID
   */
  protected static void log(final int theLogLevel, final CrudServiceMethodInfo theMethodInfo,
      final String theLogMessage, final String theLogID) {
    log(theLogLevel, theMethodInfo.getName(), theLogMessage, theLogID);
  }

  /*
   * (non-Javadoc)
   *
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudService#initImplementationClasses()
   */
  @Override
  protected void initImplementationClasses() throws IoFault {
    identifierImplementation = CrudServiceUtilities.getIdImplementation(conf);
    dataStorageImplementation = DHCrudServiceUtilities.getDataStorageImplementation(conf);
    idxdbImplementation = DHCrudServiceUtilities.getIndexStorageImplementation(conf);
  }

  /**
   * <p>
   * Always returns Tika mimetype to be independent of the dc:format in DMD, because this value/s
   * is/are served by the operating system of the user's machine and only seldom trustworthy. For
   * returning mimetype and file extension in HTTP responses we MUST HAVE correct data.must have and
   * file extensionTries to determine the correct Mimetype to return in HTTP response. Finally use
   * the default type OCTET_STREAM.
   * </p>
   *
   * @param theBag
   * @param theTempData
   * @param theMethodInfo
   * @param theLogID
   * @return
   * @throws TikaException
   * @throws IOException
   */
  private static String getCorrectMimetype(final DHCrudDariahObject theBag,
      final File theTempData, final CrudServiceMethodInfo theMethodInfo, final String theLogID)
      throws TikaException, IOException {

    String result = LTPUtils.extractMimetype(theTempData);

    log(DEBUG, theMethodInfo, "Mimetype from ADMMD: " + theBag.getMimetype(), theLogID);
    log(DEBUG, theMethodInfo, "Mimetype from TIKA: " + result, theLogID);

    // If Tika mimetype is empty or not valid, use default mimetype (was: workaround for #25068).
    if (result.isEmpty() || !MimeType.isValid(result)) {
      result = MediaType.APPLICATION_OCTET_STREAM;

      log(DEBUG, theMethodInfo,
          "Mimetype extracted from Tika is empty or invalid, using default mimetype: " + result);
    }

    return result;
  }

  /**
   * <p>
   * Delivers a tombstone page for the given PID. Please check before, if this object really has
   * been declared DELETED=true in Handle metadata!
   * </p>
   * 
   * @param thePid The (Handle PID) prefix-checked PID without Handle prefix (hdl:)!
   * @param theDELETEDMetadata The complete DELETED Handle metadata JSON String.
   * @param theLogID The log ID.
   * @return
   * @throws MetadataParseFault
   */
  private static Response deliverTombstonePage(final URI thePid,
      final String theHandleDeletedMetadata, final String theLogID) throws MetadataParseFault {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      // Extrapolate DOI from Handle PID and institutional DOI prefix.
      // TODO Get DOI from Handle metadata type=DOI instead??
      // String doi = DHCrudServiceUtilities.getDOIfromPID(thePid, conf.getDOIpREFIX());

      DHCrudServiceImpl.log(CrudService.DEBUG, methodInfo,
          "Handle DELETED JSON metadata metadata string: " + theHandleDeletedMetadata, theLogID);

      // Create HTML response with status 410 GONE.
      String version = dhcrudServiceVersion.getSERVICENAME() + "-"
          + dhcrudServiceVersion.getVERSION() + "+" + dhcrudServiceVersion.getBUILDDATE();
      HashMap<String, Object> templateMap = DHCrudServiceUtilities
          .createTombstoneHTMLPageHashMap(thePid, conf, version, theHandleDeletedMetadata);
      String tombstoneHTML = getTombstoneHTMLFromTemplate(templateMap);

      ResponseBuilder rBuilder = Response.ok(tombstoneHTML, MediaType.TEXT_HTML);
      rBuilder.status(410);

      // Set last modified date for client caching reasons.
      rBuilder.lastModified(
          DHCrudServiceUtilities.getTimestampFromHandleMetadata(theHandleDeletedMetadata));

      // Set length.
      rBuilder.header(HttpHeaders.CONTENT_LENGTH,
          DHCrudServiceUtilities.getHTMLContentLength(tombstoneHTML));

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (IoFault | IOException | ParseException | JSONException
        | DatatypeConfigurationException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

  /**
   * <p>
   * Delivers a tombstone page for the given PID.
   * </p>
   * 
   * @param theCheckedPid The prefix-checked PID.
   * @param theDeletedJSON The value of the Handle metadata key DELETED.
   * @param theLogID
   * @return
   */
  private static Response deliverGONEResponse(final URI theCheckedPid, final String theDeletedJSON,
      final String theLogID) {

    CrudServiceMethodInfo methodInfo = new CrudServiceMethodInfo(CrudOperations.READ);

    try {
      // Get timestamp from JSON.
      Date lastModified = DHCrudServiceUtilities.getTimestampFromHandleMetadata(theDeletedJSON);

      log(DEBUG, methodInfo, "Deleted timestamp used for Last-Modified: " + lastModified, theLogID);

      // Create response with status 410 GONE.
      ResponseBuilder rBuilder = Response.status(Status.GONE);

      // Set last modified date for client caching reasons.
      rBuilder.lastModified(lastModified);

      log(INFO, methodInfo, CrudServiceUtilities.endMethodLog(methodInfo));

      return rBuilder.build();

    } catch (ParseException | JSONException e) {
      log(ERROR, methodInfo, errorMethodLog(methodInfo, e), theLogID);
      throw new RuntimeException(e);
    }
  }

}
