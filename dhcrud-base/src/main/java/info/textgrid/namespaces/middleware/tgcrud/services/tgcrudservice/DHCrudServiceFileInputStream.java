/**
 * This software is copyright (c) 2020 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2020-11-30 - Funk - First version.
 */

/**
 * <p>
 * Just override close() here to delete temp file. Use own implementation that our self implemented
 * DHCrudServiceWildcardDHCrudServiceFileInputStreamMessageBodyWriter is taken for delivering object
 * data!
 * 
 * Fixes: https://projects.gwdg.de/projects/dariah-de-repository/work_packages/34267 (If temp file
 * is deleted on close, the wrong MessageBodyWriter is being used for delivering objects on read)
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-12-01
 * @since 2020-11-30
 */

public class DHCrudServiceFileInputStream extends FileInputStream {

  // The file that belongs to the created FileInputStream.
  private File file;

  /**
   * @param file
   * @throws FileNotFoundException
   */
  public DHCrudServiceFileInputStream(File theFile) throws FileNotFoundException {
    super(theFile);
    // Set file from constructor to be able to delete at close().
    this.file = theFile;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.io.FileInputStream#close()
   */
  @Override
  public void close() throws IOException {
    super.close();
    this.file.delete();
  }

}
