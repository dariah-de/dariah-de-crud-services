/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * TODOLOG
 * 
 * TODO Generalise the JSON PID extraction with DHPublishUtils from koLibRI.
 * 
 * TODO Generalise the class loading of the storage classes with the TGCrudServiceUtilities class!
 * 
 **
 * CHANGELOG
 * 
 * 2021-08-30 - Funk - Fix ORCID parsing: Remove resolving for the moment, as orcid.org changed
 * their metadata in HTML pages.
 * 
 * 2021-04-07 - Funk - Refactor some HTML hash map creation methods for tombstone hash map creation.
 * 
 * 2020-11-25 - Funk - Removed deleteFolder method.
 * 
 * 2020-09-17 - Funk - Add instance name and menu header color to menu header.
 * 
 * 2020-09-11 - Funk - Add method for getting name from ORCID ID.
 * 
 * 2020-03-31 - Funk - Add LRS to templates.
 * 
 * 2017-11-06 - Funk - More index and landing page functionality added.
 * 
 * 2015-02-16 - Funk - Copied from TGrudServiceUtilities.
 * 
 */

/**
 * <p>
 * The DHCrudServiceUtilities class provides some utilities, needed by all the DHCrudService
 * classes.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-08-30
 * @since 2010-06-24
 */

public class DHCrudServiceUtilities extends CrudServiceUtilities {

  // **
  // STATIC FINALS
  // **

  public static final String ORCID_PREFIX = "orcid:";
  public static final String GND_PREFIX = "gnd:";
  public static final String PND_PREFIX = "pnd:";
  public static final String DEPRECATED_DOI_PREFIX = "10.5072";
  public static final int ORCID_NAME_HASHMAP_MAX_SIZE = 111;
  public static final String HANDLE_DELETED_TYPE = "DELETED";

  // **
  // STATICS
  // **

  // private static CloseableHttpClient httpClient = HttpClients.createDefault();
  // private static HashMap<String, String> orcidNameMap = new HashMap<String, String>();
  private static Client dataciteClient = null;

  // **
  // CLASS LOADING
  // **

  /**
   * <p>
   * Instantiates the DATA storage to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The data storage implementation class instance.
   * @throws IoFault
   */
  public static CrudStorageAbs<Model> getDataStorageImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return DHCrudServiceUtilities.getStorageImplementation(theConfiguration,
        theConfiguration.getDATAsTORAGEiMPLEMENTATION());
  }

  /**
   * <p>
   * Instantiates the Index storage to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The index storage implementation class instance.
   * @throws IoFault
   */
  public static CrudStorage<Model> getIndexStorageImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return DHCrudServiceUtilities.getStorageImplementation(theConfiguration,
        theConfiguration.getIDXDBsTORAGEiMPLEMENTATION());
  }

  /**
   * <p>
   * Instantiates the RDF storage to use.
   * </p>
   * 
   * @param theConfiguration
   * @return The RDF storage implementation class instance.
   * @throws IoFault
   */
  public static CrudStorageAbs<Model> getRdfStorageImplementation(
      CrudServiceConfigurator theConfiguration) throws IoFault {
    return DHCrudServiceUtilities.getStorageImplementation(theConfiguration,
        theConfiguration.getRDFDBsTORAGEiMPLEMENTATION());
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * <p>
   * Instantiates the storage implementation class to use.
   * </p>
   * 
   * @param theConfiguration
   * @param theClassName
   * @return The storage implementation.
   * @throws IoFault
   */
  private static CrudStorageAbs<Model> getStorageImplementation(
      CrudServiceConfigurator theConfiguration, String theClassName) throws IoFault {

    String meth = UNIT_NAME + ".getStorageImplementation()";

    Class<?> storageImplementation;
    try {
      storageImplementation = DHCrudServiceUtilities.class.getClassLoader().loadClass(theClassName);
      @SuppressWarnings("unchecked")
      CrudStorageAbs<Model> result =
          (CrudStorageAbs<Model>) storageImplementation.getDeclaredConstructor().newInstance();
      result.setConfiguration(theConfiguration);

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Instantiated Storage implementation: " + theClassName);

      return result;

    } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
        | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
        | SecurityException e) {
      throw CrudServiceExceptions.ioFault(e, "DH-Crud service storage implementation class ["
          + theConfiguration.getDATAsTORAGEiMPLEMENTATION() + "] can  not be instanciated due to a "
          + e.getClass().getName() + ": " + e.getMessage());
    }
  }

  /**
   * <p>
   * Using Jettison for parsing JSON. We are getting something like this:
   * {"responseCode":1,"handle":"11022/0000-0000-8487-2",
   * "values":[{"index":14,"type":"COLREG_ID","data":{"format":"string",
   * "value":"5746ff8a7c8dec05e679dece"},"ttl":86400,"timestamp": "2015-08-18T13:11:56Z"}]}
   * </p>
   * 
   * @param thePidServiceResponse
   * @param theKey
   * @return The value of a PID metadata key.
   * @throws JSONException
   */
  public static String extractPidValue(String thePidServiceResponse, String theKey)
      throws JSONException {

    String result = "";

    if (thePidServiceResponse != null && !"".equals(thePidServiceResponse)) {

      JSONObject jo = new JSONObject(thePidServiceResponse);
      if (jo.has("values")) {
        JSONArray ja = jo.getJSONArray("values");
        for (int i = 0; i < ja.length(); i++) {
          if (ja.getJSONObject(i).has("type") && ja.getJSONObject(i).get("type").equals(theKey)) {
            result = ja.getJSONObject(i).getJSONObject("data").getString("value");
          }
        }
      }
    }

    return result;
  }

  /**
   * @param thePid
   * @param theConf
   * @param theVersion
   * @param theJSONDeletedMetadata
   * @return
   * @throws UnsupportedEncodingException
   * @throws IoFault
   * @throws JSONException
   * @throws ParseException
   * @throws DatatypeConfigurationException
   */
  public static HashMap<String, Object> createTombstoneHTMLPageHashMap(final URI thePid,
      final CrudServiceConfigurator theConf, final String theVersion,
      final String theJSONDeletedMetadata)
      throws UnsupportedEncodingException, IoFault, JSONException, ParseException,
      DatatypeConfigurationException {

    // Add basic values, deletion date from Handle metadata, and CRUD version.
    HashMap<String, Object> result = createBasicHTMLPageHashMap(theConf);
    result.put("dhcrud_version", theVersion);

    // Add DOI.
    String doi = getDOIfromPID(thePid, theConf.getDOIpREFIX());
    result.put("doi", doi);

    // Add citation.
    String citation = getCitationFromHandleMetadata(theJSONDeletedMetadata);
    result.put("citation", citation);

    return result;
  }

  /**
   * @param thePid
   * @param theMetadata
   * @param theAdmMD
   * @param theCollectionData
   * @param theVersion
   * @param theConf
   * @param theLogID
   * @return
   * @throws IOException
   * @throws IoFault
   * @throws XMLStreamException
   * @throws URISyntaxException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   * @throws TransformerConfigurationException
   * @throws JSONException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws DatatypeConfigurationException
   * @throws ObjectNotFoundFault
   */
  public static HashMap<String, Object> createHTMLPageHashMap(final URI thePid,
      final Model theMetadata, final Model theAdmMD, final String theCollectionData,
      final String theVersion, final CrudServiceConfigurator theConf, final String theLogID)
      throws IOException, IoFault, XMLStreamException, URISyntaxException,
      org.apache.jena.riot.lang.extra.javacc.ParseException, TransformerConfigurationException,
      JSONException, SAXException, ParserConfigurationException, DatatypeConfigurationException,
      ObjectNotFoundFault {
    // Add basic values.
    HashMap<String, Object> result = createBasicHTMLPageHashMap(theConf);

    // Get the DOI.
    String doi = RDFUtils.findFirstObject(theMetadata, LTPUtils.resolveHandlePid(thePid),
        RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_IDENTIFIER, RDFConstants.DOI_NAMESPACE);
    doi = doi.replace(RDFConstants.DOI_NAMESPACE, "");

    // Get all titles, creators, identifiers, relations, formats, rights, and descriptions from DC
    // metadata.
    List<String> titles =
        RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_TITLE);
    List<String> creators =
        RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_CREATOR);
    Collections.sort(creators);
    List<String> identifiers = RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER);
    List<String> relations = RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_RELATION);
    List<String> formats =
        RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_FORMAT);
    List<String> rights =
        RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX, RDFConstants.ELEM_DC_RIGHTS);
    List<String> descriptions = RDFUtils.findAllObjects(theMetadata, RDFConstants.DC_PREFIX,
        RDFConstants.ELEM_DC_DESCRIPTION);

    // Get relation and type from ADM metadata.
    String relation =
        RDFUtils.findFirstObject(theAdmMD, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DC_RELATION);
    String mimetype =
        RDFUtils.findFirstObject(theAdmMD, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DC_FORMAT);
    String created =
        RDFUtils.findFirstObject(theAdmMD, RDFConstants.DCTERMS_PREFIX,
            RDFConstants.ELEM_DCTERMS_CREATED);
    String extent = RDFUtils.findFirstObject(theAdmMD, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DCTERMS_EXTENT);
    String relationWithoutPrefix = relation.replaceAll(RDFConstants.DARIAH_COLLECTION + "/", "");

    // Build citation string.
    String citation = getCitation(creators, titles, created,
        omitTrailingSlash(theConf.getDOIrESOLVER().toString()), doi.toLowerCase(),
        theConf.getORCIDrESOLVER().toString());

    // Create extended HashMap with content.
    result.put("doi", doi);
    result.put("hdl", LTPUtils.omitHdlPrefix(thePid.toString()));
    result.put("titles", titles);
    result.put("creators", getCreators(creators, theConf.getGNDrESOLVER().toString(),
        theConf.getORCIDrESOLVER().toString()));
    result.put("citation", citation);
    result.put("rights", rights);
    result.put("formats", formats);
    result.put("type", mimetype);
    result.put("type_encoded", URLEncoder.encode(mimetype, "UTF-8"));
    result.put("date", created);
    result.put("extent", humaniseExtent(extent));
    String hdlEncoded =
        "hdl:" + URLEncoder.encode(LTPUtils.omitHdlPrefix(thePid.toString()), "UTF-8");
    result.put("hdl_encoded", hdlEncoded);
    // Replace "%2F" to "%252F" for use in TIFY and Mirador calls!
    result.put("hdl_double_encoded", hdlEncoded.replace("%", "%25"));
    if (mimetype.startsWith("image") || mimetype.startsWith("img")) {
      result.put("show_image", true);
      result.put("thumb_size", theConf.getTHMUBsIZE());
    }
    if (!descriptions.isEmpty()) {
      result.put("descriptions", descriptions);
    }
    if (!identifiers.isEmpty()) {
      result.put("identifiers", getIdentifiers(identifiers, theConf));
    }
    if (!relations.isEmpty()) {
      result.put("relations", getRelations(relations, theConf));
    }
    // If no ADMMD relation is given, we use the object's PID as OAI-PMH set reference.
    if (relation.isEmpty()) {
      result.put("oaipmh_set", LTPUtils.omitHdlPrefix(thePid.toString()));
    } else {
      result.put("oaipmh_set", relationWithoutPrefix);
    }
    result.put("root_collection", getRootCollection4HTML(thePid, (String) result.get("hdl_url"),
        relationWithoutPrefix, theLogID));
    result.put("root_encoded", "hdl:" + URLEncoder.encode(relationWithoutPrefix, "UTF-8"));
    List<String> embeddedObjects = getEmbeddedObjects4HTML(mimetype, theCollectionData, thePid,
        (String) result.get("hdl_url"), theLogID);
    if (!embeddedObjects.isEmpty()) {
      result.put("has_embedded", true);
      result.put("embedded_objects", embeddedObjects);
    }
    result.put("dhcrud_version", theVersion);

    return result;
  }

  /**
   * @param theBagRoot
   * @param theZippedBagPath
   * @throws IOException
   */
  public static void createZipBag(final Path theBagRoot, final Path theZippedBagPath)
      throws IOException {

    try (FileOutputStream fout = new FileOutputStream(theZippedBagPath.toFile());
        ZipOutputStream zout = new ZipOutputStream(fout);) {
      // Walk the files. Sort.
      Files.walk(theBagRoot).sorted()
          .forEach(path -> addEntry(path, zout, theBagRoot.getFileName().toString()));
      zout.close();
      fout.close();
    }

    // Recursively delete ZIP source folder.
    FileUtils.deleteDirectory(theBagRoot.toFile());
  }

  /**
   * @param thePid
   * @param thePrefix
   * @param theSuffix
   * @return
   */
  public static String getFilenameFromUri(URI thePid, String thePrefix, String theSuffix) {
    return DHCrudServiceImpl.DHREP_PREFIX
        + LTPUtils.omitHdlNamespace(LTPUtils.omitHdlPrefix(thePid.toString())).replaceAll("/", "_")
        + (thePrefix.equals("") || thePrefix.startsWith(".") ? "" : ".") + thePrefix
        + (theSuffix.equals("") || theSuffix.startsWith(".") ? "" : ".") + theSuffix;
  }

  /**
   * @param theResource
   * @param theEppn
   * @param theDoiValue
   * @param thePidValue
   * @param theRootCollection
   * @param theFormat
   * @return
   */
  public static Model createAdministrativeMetadata(String theResource, String theEppn,
      String theDoiValue, String thePidValue, String theRootCollection, String theFormat) {

    Model result =
        RDFUtils.newModel(theResource, RDFConstants.RDF_PREFIX, RDFConstants.RDF_NAMESPACE);

    // Add DARIAH type dariah:Collection or dariah:DataObject.
    if (theFormat.equals(TextGridMimetypes.DARIAH_COLLECTION)) {
      result.getResource(theResource).addProperty(result.createProperty(RDFConstants.RDF_TYPE),
          result.createProperty(RDFConstants.DARIAH_COLLECTION));
    } else {
      result.getResource(theResource).addProperty(result.createProperty(RDFConstants.RDF_TYPE),
          result.createProperty(RDFConstants.DARIAH_DATAOBJECT));
    }

    // Set PREMIS and DARIAH namespace prefix.
    result.setNsPrefix(RDFConstants.PREMIS_PREFIX, RDFConstants.PREMIS_NAMESPACE);
    result.setNsPrefix(RDFConstants.DARIAH_PREFIX, RDFConstants.DARIAH_NAMESPACE);
    result.setNsPrefix(RDFConstants.DCTERMS_PREFIX, RDFConstants.DCTERMS_NAMESPACE);

    // Set format.
    RDFUtils.addLiteralToModel(result, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_FORMAT, theFormat);

    // Add all the values we already got: dc:creator, dc:identifiers (HDL
    // and DOI), and dc:relation.
    RDFUtils.addLiteralToModel(result, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_CREATOR, theEppn);
    RDFUtils.addResourceToModel(result, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, result.createResource(thePidValue));
    RDFUtils.addResourceToModel(result, theResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_IDENTIFIER, result.createResource(theDoiValue));

    // Add dcterms:relation only if dc:relation is existing and contains
    // value starting with collection namespace!
    if (theRootCollection != null && theRootCollection.startsWith(RDFConstants.DARIAH_NAMESPACE)) {
      RDFUtils.addResourceToModel(result, theResource, RDFConstants.DCTERMS_PREFIX,
          RDFConstants.ELEM_DC_RELATION, result.createResource(theRootCollection));
    }

    return result;
  }

  /**
   * @param theModel
   * @return
   */
  public static String getSourceFromRDF(Model theModel) {

    // Get source resource value from rdf:about in metadata.
    String sourceResource =
        RDFUtils.findFirstSubject(theModel, RDFConstants.DARIAH_COLLECTION, "",
            RDFConstants.RDF_TYPE);

    return RDFUtils.findFirstObject(theModel, sourceResource, RDFConstants.DCTERMS_PREFIX,
        RDFConstants.ELEM_DC_SOURCE);
  }

  /**
   * @param theString
   * @return
   */
  public static String omitTrailingSlash(String theString) {
    return (!theString.endsWith("/") ? theString
        : theString.substring(0, theString.lastIndexOf("/")));
  }

  /**
   * <p>
   * Checks if the correct PID prefixes are used in requested URIs. So trep.de.dariah.eu is not
   * allowed to deliver productive HDL or DOI objects! Rewrites DOIs to Handle PIDs, too, for
   * correct DOI usage in crud requests! Omits "hdl:" and "doi:" prefixes!
   * </p>
   * 
   * @param theUri
   * @param theMethodInfo
   * @param thePidPrefix
   * @param theDoiPrefix
   * @param checkWrongPrefix
   * @return Handle URI without prefix, unchanged if not a DOI or HDL.
   * @throws ParseException
   */
  public static URI checkPid(final URI theUri, final CrudServiceMethodInfo theMethodInfo,
      String thePidPrefix, String theDoiPrefix, String theLogID) throws ParseException {

    DHCrudServiceImpl.log(CrudService.DEBUG, theMethodInfo, "PID: " + theUri, theLogID);

    URI result;

    // Check for valid PID prefix and omit "hdl:"...
    if (LTPUtils.omitHdlPrefix(theUri.toString()).startsWith(thePidPrefix)) {
      result = URI.create(LTPUtils.omitHdlPrefix(theUri.toString()));
    }
    // ...then check for valid DOI prefix and omit "doi:"!
    else if (LTPUtils.omitDoiPrefix(theUri.toString()).startsWith(theDoiPrefix)) {
      result =
          URI.create(LTPUtils.omitDoiPrefix(theUri.toString().replace(theDoiPrefix, thePidPrefix)));
    }
    // Handle deprecated test DOI prefix.
    else if (LTPUtils.omitDoiPrefix(theUri.toString()).startsWith(DEPRECATED_DOI_PREFIX)) {
      result = URI.create(
          LTPUtils.omitDoiPrefix(theUri.toString().replace(DEPRECATED_DOI_PREFIX, thePidPrefix)));
    }
    // Else throw exception.
    else {
      String message =
          "WRONG PID PREFIX! " + theUri + " is not a HDL or DOI of this service, only prefixes "
              + thePidPrefix + " or " + theDoiPrefix + " are served here!";
      throw new ParseException(message, 42);
    }

    // Log PID adaptations.
    if (!theUri.toString().equals(result.toString())) {
      DHCrudServiceImpl.log(CrudService.DEBUG, theMethodInfo, "PID adapted to: " + result,
          theLogID);
    }

    return result;
  }

  /**
   * <p>
   * Test the PID for correct GWDG Handle PID syntax as following: 21.T11991/0000-0008-BB04-1
   * </p>
   * 
   * @param thePid
   * @return
   */
  public static boolean checkHandlePIDSyntax(URI thePid) {
    return thePid.toString().matches(DHCrudService.GWDG_HANDLE_PID_REGEXP);
  }

  /**
   * <p>
   * Test the DOI for correct syntax as following: 10.20375/0000-0008-BB04-1
   * </p>
   * 
   * @param thePid
   * @return
   */
  public static boolean checkPIDAndDOISyntax(URI thePid) {
    return thePid.toString().matches(DHCrudService.PID_AND_DOI_REGEXP);
  }

  /**
   * <p>
   * Computing the length for HTML header Content-Length from A STRING!
   * </p>
   * 
   * @param theString
   * @return
   */
  public static int getHTMLContentLength(String theString) {
    return theString.getBytes().length;
  }

  /**
   * @param theCreatorEntry
   * @param theORCIDResolver
   * @return
   */
  public static String getNameFromORCIDCreatorEntry(String theCreatorEntry,
      String theORCIDResolver) {

    // FIXME Get names from ORCID via ORCID public API!

    String result = "";

    // // Test if HashMap is too large. Reset HashMap if > ORCID_NAME_HASHMAP_MAX_SIZE entries.
    // // PLEASE NOTE If entries were changes by the authors on their ORCID dataset, it will only be
    // // refreshed if crud restarts or the hash map is emptied!! TODO Go on and do implement a
    // // time-based hash map if you like! :-D
    // if (orcidNameMap.size() > ORCID_NAME_HASHMAP_MAX_SIZE) {
    // orcidNameMap.clear();
    // }
    //
    // // Get ORCID ID from entry.
    // String orcidID = getORCIDFromCreatorEntry(theCreatorEntry);
    // if (orcidID.isEmpty()) {
    // return result;
    // }
    //
    // // Get name, if ID already existing in HashMap.
    // result = orcidNameMap.get(orcidID);
    //
    // // Get name from ORCID HTML page.
    // // TODO Use ORCID client API if needed to be faster.
    // try {
    // if (result == null || result.isEmpty()) {
    // if (theCreatorEntry.startsWith(ORCID_PREFIX)
    // || theCreatorEntry.startsWith("http://orcid.org/")
    // || theCreatorEntry.startsWith("https://orcid.org/")
    // || theCreatorEntry.startsWith("orcid.org/")) {
    // CloseableHttpResponse response =
    // httpClient.execute(new HttpGet(theORCIDResolver + orcidID + "/print"));
    // int statusCode = response.getStatusLine().getStatusCode();
    // if (statusCode == 200) {
    // File f = File.createTempFile("dhcrud_", "_orcid");
    // FileOutputStream fos = new FileOutputStream(f);
    // response.getEntity().writeTo(fos);
    // fos.close();
    // String html = IOUtils.readStringFromStream(new FileInputStream(f));
    // // Dirty string work done here! Shame on me!
    // int titleIndex = html.indexOf("og:title");
    // if (titleIndex != -1) {
    // result = html.substring(titleIndex + 19, html.indexOf("(0000") - 1);
    // orcidNameMap.put(orcidID, result);
    // } else {
    // result = "";
    // }
    // f.delete();
    // }
    // }
    // }
    // } catch (IOException e) {
    // // Do nothing if not OK.
    // }

    return result;
  }

  /**
   * @return
   */
  public static String getORCIDFromCreatorEntry(String theCreatorEntry) {

    String result = "";

    // Get ORCID ID from entry, if certain patterns apply.
    if (theCreatorEntry.startsWith(ORCID_PREFIX)) {
      result = theCreatorEntry.substring(theCreatorEntry.lastIndexOf(":") + 1);
    } else if (theCreatorEntry.startsWith("http://orcid.org/")
        || theCreatorEntry.startsWith("https://orcid.org/")
        || theCreatorEntry.startsWith("orcid.org/")) {
      result = theCreatorEntry.substring(theCreatorEntry.lastIndexOf("/") + 1);
    }

    return result;
  }

  /**
   * <p>
   * Deleting the temp file.
   * </p>
   * 
   * @param theMethodName
   * @param theLogID
   */
  public static void deleteTempFile(File theTempFile, String theMethodName, String theLogID) {

    boolean dataDeleted = theTempFile.delete();
    if (dataDeleted) {
      DHCrudServiceImpl.log(CrudService.DEBUG, theMethodName,
          "Temp file deleted: " + theTempFile.getAbsolutePath(), theLogID);
    } else {
      DHCrudServiceImpl.log(CrudService.WARN, theMethodName,
          "Temp file NOT deleted: " + theTempFile.getAbsolutePath(), theLogID);
    }
  }

  /**
   * <p>
   * Deleting the temp folder.
   * </p>
   * 
   * @param theTempFolder
   * @param theMethodName
   * @param theLogID
   */
  public static void deleteTempFolder(File theTempFolder, String theMethodName, String theLogID) {

    try {
      FileUtils.deleteDirectory(theTempFolder);

      DHCrudServiceImpl.log(CrudService.DEBUG, theMethodName,
          "Temp source folder deleted: " + theTempFolder.getAbsolutePath(), theLogID);

    } catch (IOException e) {
      DHCrudServiceImpl.log(CrudService.WARN, theMethodName,
          "Temp source folder NOT deleted: " + theTempFolder.getAbsolutePath(), theLogID);
    }
  }

  /**
   * @param theDOI The DOI without prefix or namespace!
   * @param theLogID
   * @param theDataciteLocation
   * @return
   * @throws IoFault
   */
  public static String getDOIMetadataXML(final String theDOI, String theLogID,
      String theDataciteLocation) throws IoFault {

    String result = "";

    // https://api.datacite.org/dois/application/vnd.datacite.datacite+xml/10.20375/0000-000b-c8ef-7
    String datacitePath = "dois/application/vnd.datacite.datacite+xml/";

    Response doiMetadataResponse =
        getDOIMetadataResponse(theDOI, theDataciteLocation, datacitePath, theLogID);

    int statusCode = doiMetadataResponse.getStatus();
    String reasonPhrase = doiMetadataResponse.getStatusInfo().getReasonPhrase();
    if (statusCode != Status.OK.getStatusCode()) {
      String message = "Failed to get DataCite XML metadata: " + statusCode + " " + reasonPhrase;
      throw new IoFault(message);
    }

    try {
      result = IOUtils.toString(doiMetadataResponse.readEntity(InputStream.class));
    } catch (IOException e) {
      String message =
          "Failed to get DataCite XML metadata: [" + e.getClass().getName() + "] " + e.getMessage();
      throw new IoFault(message);
    }

    return result;
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param thePath
   * @param theZipStream
   * @param theParentPathNotToZip
   */
  private static void addEntry(Path thePath, ZipOutputStream theZipStream,
      String theParentPathNotToZip) {

    BasicFileAttributes attributes = null;
    try {
      attributes = Files.readAttributes(thePath, BasicFileAttributes.class);
    } catch (java.io.IOException ex) {
      throw new RuntimeException(ex);
    }

    // Get name of the ZIP entry. We do not want to put ALL the pathes into
    // THE ZIP, but only the ones after the root bag content folder!
    File file = thePath.toFile();
    String name =
        file.getAbsolutePath().substring(file.getAbsolutePath().indexOf(theParentPathNotToZip));

    // Create entry.
    ZipEntry entry = null;
    if (file.isDirectory()) {
      entry = new ZipEntry(name + "/");
    } else {
      entry = new ZipEntry(name);
    }

    // Set metadata.
    entry.setCreationTime(attributes.creationTime()).setLastAccessTime(attributes.lastAccessTime())
        .setLastModifiedTime(attributes.lastModifiedTime());

    // Put entry.
    try {
      theZipStream.putNextEntry(entry);
      if (!file.isDirectory()) {
        try (FileInputStream in = new FileInputStream(thePath.toFile());) {
          byte[] buf = new byte[4096];
          int len;
          while ((len = in.read(buf)) != -1)
            theZipStream.write(buf, 0, len);
        }
      }
    } catch (java.io.IOException e) {
      // TODO Do something here?
      throw new RuntimeException(e);
    } finally {
      try {
        theZipStream.closeEntry();
      } catch (java.io.IOException e) {
        // TODO Do something here?
        throw new RuntimeException(e);
      }
    }
  }

  /**
   * <p>
   * Get all the PIDs (and titles) from RDF file and children, if mimetype is collection.
   * </p>
   * 
   * @param theMimetype
   * @param theCollectionData
   * @param thePid
   * @param theHDLURL
   * @param theLogID
   * @return
   * @throws XMLStreamException
   * @throws IOException
   * @throws URISyntaxException
   * @throws org.apache.jena.riot.lang.extra.javacc.ParseException
   * @throws IoFault
   * @throws TransformerConfigurationException
   * @throws JSONException
   * @throws SAXException
   * @throws ParserConfigurationException
   * @throws ObjectNotFoundFault
   */
  private static List<String> getEmbeddedObjects4HTML(String theMimetype, String theCollectionData,
      URI thePid, String theHDLURL, String theLogID) throws XMLStreamException, IOException,
      URISyntaxException, org.apache.jena.riot.lang.extra.javacc.ParseException, IoFault,
      TransformerConfigurationException, JSONException, SAXException, ParserConfigurationException,
      ObjectNotFoundFault {

    List<String> result = new ArrayList<String>();

    String meth = "getEmbeddedObjects4HTML()";

    DHCrudServiceImpl.log(CrudService.DEBUG, meth,
        "Collection data [" + thePid + "]: " + theCollectionData, theLogID);

    // Get embedded titles only if we have a collection type.
    if (theMimetype.equals(TextGridMimetypes.DARIAH_COLLECTION)) {
      Model model = RDFUtils.readModel(theCollectionData, RDFConstants.TURTLE);
      List<String> partPids = RDFUtils.getProperties(model, LTPUtils.resolveHandlePid(thePid),
          RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_HASPART);

      // Prevent JUnit testing errors!
      if (DHCrudServiceImpl.idxdbImplementation == null) {
        for (String part : partPids) {
          String hdlWithoutPrefix = part.replaceAll(RDFConstants.HDL_NAMESPACE, "");
          String htmlEmbeddedPart = "<a href=\"" + theHDLURL + "/" + hdlWithoutPrefix + "\">hdl:"
              + hdlWithoutPrefix + "</a>";
          result.add(htmlEmbeddedPart);
        }
      }

      // Cast to SuperSpecialDHCrudTitleAndMimetypeMapGeneration! (FIXME Maybe? URGS?)
      else {

        DHCrudServiceImpl.log(CrudService.DEBUG, meth,
            "Get titles and mimetypes from ES for parts [" + thePid + "]: " + partPids, theLogID);

        MultiGetItemResponse metadataResponse[] =
            ((DHCrudServiceStorageElasticSearchImpl) DHCrudServiceImpl.idxdbImplementation)
                .retrieveMultiMetadataFromElasticSearch(partPids);

        // Get both titles and mimetype.
        Map<String, List<String>> titleMap =
            retrieveTitlesFromPartsMetadataPublic(partPids, metadataResponse);

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Title map: " + titleMap, theLogID);

        Map<String, String> mimetypeMap =
            retrieveMimetypesFromPartsMetadataPublic(partPids, metadataResponse);

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Mimetype map: " + mimetypeMap, theLogID);

        // Iterate over the the part list, because we do NEED the order of elements here!
        for (String part : partPids) {
          String hdlWithoutPrefix = part.replaceAll(RDFConstants.HDL_NAMESPACE, "");
          String htmlEmbeddedPart = "<a href=\"" + theHDLURL + "/" + hdlWithoutPrefix + "\">";

          if (titleMap.get(hdlWithoutPrefix).isEmpty()) {
            htmlEmbeddedPart += "hdl:" + hdlWithoutPrefix + "</a>";
          } else {
            // Loop list, concatenate title string(s), and add mimetype.
            String titleString = "";
            for (String title : titleMap.get(hdlWithoutPrefix)) {
              titleString += title + ", ";
            }
            titleString = titleString.substring(0, titleString.length() - 2);
            htmlEmbeddedPart += titleString + "</a> (" + mimetypeMap.get(hdlWithoutPrefix) + ")";
          }

          // Add ID or title string to result list.
          result.add(htmlEmbeddedPart);
        }
      }
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Embedded HTML result: " + result, theLogID);

    return result;
  }

  /**
   * <p>
   * Get the root collection title and mimetype.
   * </p>
   * 
   * @param thePid
   * @param theHDLURL
   * @param theRootCollection
   * @param theLogID
   * @return
   * @throws ParserConfigurationException
   * @throws SAXException
   * @throws IoFault
   * @throws IOException
   * @throws TransformerConfigurationException
   * @throws JSONException
   * @throws ObjectNotFoundFault
   */
  private static String getRootCollection4HTML(URI thePid, String theHDLURL,
      String theRootCollection, String theLogID) throws TransformerConfigurationException,
      IOException, IoFault, SAXException, ParserConfigurationException, JSONException,
      ObjectNotFoundFault {

    String result = "";

    String meth = "getRootCollection4HTML()";

    // Get title of root collection.
    if (theRootCollection != null && !theRootCollection.isEmpty()) {

      DHCrudServiceImpl.log(CrudService.DEBUG, meth,
          "Get title for root collection: " + theRootCollection, theLogID);

      // Prevent JUnit testing errors!
      if (DHCrudServiceImpl.idxdbImplementation == null) {
        String htmlRootCollection = "<a href=\"" + theHDLURL + "/" + theRootCollection + "\">hdl:"
            + theRootCollection + "</a>";
        result = htmlRootCollection;
      }

      // Cast to SuperSpecialDHCrudTitleAndMimetypeGeneration! (FIXME Maybe? URGS?)
      else {
        GetResponse metadataResponse =
            ((DHCrudServiceStorageElasticSearchImpl) DHCrudServiceImpl.idxdbImplementation)
                .retrieveItemMetadataFromElasticSearch(theRootCollection);

        // Get titles.
        List<String> titleList =
            retrieveTitleFromMetadataPublic(theRootCollection, metadataResponse);

        DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Title list: " + titleList, theLogID);

        result = "<a href=\"" + theHDLURL + "/" + theRootCollection + "\">";

        if (titleList.isEmpty()) {
          result += "hdl:" + theRootCollection + "</a>";
        } else {
          // Loop list, concatenate title string(s).
          for (String title : titleList) {
            result += title + ", ";
          }
          result = result.substring(0, result.length() - 2);
          result += "</a>";
        }
      }
    }

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Embedded root collection result: " + result,
        theLogID);

    return result;
  }

  /**
   * <p>
   * Get all the identifiers including HDL and DOI resolver references.
   * </p>
   * 
   * @param theIdentifiers
   * @param theConf
   * @return
   * @throws IoFault
   */
  protected static Set<String> getIdentifiers(final List<String> theIdentifiers,
      final CrudServiceConfigurator theConf) throws IoFault {

    Set<String> result = new TreeSet<String>();

    for (String id : theIdentifiers) {

      // Check for external ID.
      boolean isExternal = false;
      if (!id.contains(theConf.getPIDpREFIX()) && !id.contains(theConf.getDOIpREFIX())
          && !id.contains(DEPRECATED_DOI_PREFIX)) {
        isExternal = true;
      }

      String idContent = id;
      // Wrap HREF.
      if (id.startsWith("http://") || id.startsWith("https://")) {
        String idSans = id;
        if (!isExternal) {
          int startOfPID = id.indexOf(theConf.getPIDpREFIX());
          int startOfDOI = id.indexOf(theConf.getDOIpREFIX());
          int startOfDeprecatedDOI = id.indexOf(DEPRECATED_DOI_PREFIX);
          if (startOfPID != -1) {
            idSans = RDFConstants.HDL_PREFIX + ":" + id.substring(startOfPID);
          } else if (startOfDOI != -1) {
            idSans = RDFConstants.DOI_PREFIX + ":" + id.substring(startOfDOI);
          } else if (startOfDeprecatedDOI != -1) {
            idSans = RDFConstants.DOI_PREFIX + ":" + id.substring(startOfDeprecatedDOI);
          }
        }
        idContent = "<a href=\"" + id + "\">" + idSans + "</a>";
      }
      // Add HDL resolver.
      else if (id.startsWith(RDFConstants.HDL_PREFIX)) {
        idContent = "<a href=\"" + LTPUtils.resolveHandlePid(id) + "\">" + id + "</a>";
      }
      // Add DOI resolver.
      else if (id.startsWith(RDFConstants.DOI_PREFIX)) {
        idContent = "<a href=\"" + LTPUtils.resolveDoiPid(id) + "\">" + id + "</a>";
      }

      // Make non-internal identifiers visible as it and add linkage and boldness.
      if (isExternal) {
        idContent = "[external] " + idContent;
      }

      result.add(idContent);
    }

    return result;
  }

  /**
   * @param theRelations
   * @param theConf
   * @return
   * @throws IoFault
   */
  protected static Set<String> getRelations(final List<String> theRelations,
      final CrudServiceConfigurator theConf) throws IoFault {

    Set<String> result = new TreeSet<String>();

    for (String id : theRelations) {

      // Check for external ID.
      boolean isExternal = false;
      if (!id.contains(theConf.getPIDpREFIX()) && !id.contains(theConf.getDOIpREFIX())
          && !id.contains(DEPRECATED_DOI_PREFIX)) {
        isExternal = true;
      }

      String idContent = id;
      // Wrap HREF.
      if (id.startsWith("http://") || id.startsWith("https://")) {
        String idSans = id;
        if (!isExternal) {
          int isCollection = id.indexOf(RDFConstants.DARIAH_COLLECTION);
          int startOfPID = id.indexOf(theConf.getPIDpREFIX());
          int startOfDOI = id.indexOf(theConf.getDOIpREFIX());
          int startOfDeprecatedDOI = id.indexOf(DEPRECATED_DOI_PREFIX);
          // First check for DARIAH-DE collections!
          if (isCollection != -1) {
            int startOfCollection = isCollection + RDFConstants.DARIAH_COLLECTION.length();
            idSans =
                RDFConstants.ELEM_DARIAH_COLLECTION + ":" + id.substring(startOfCollection + 1);
            id = LTPUtils.resolveHandlePid(id.substring(startOfCollection + 1));
          } else if (startOfPID != -1) {
            idSans = RDFConstants.HDL_PREFIX + ":" + id.substring(startOfPID);
          } else if (startOfDOI != -1) {
            idSans = RDFConstants.DOI_PREFIX + ":" + id.substring(startOfDOI);
          } else if (startOfDeprecatedDOI != -1) {
            idSans = RDFConstants.DOI_PREFIX + ":" + id.substring(startOfDeprecatedDOI);
          }
        }
        idContent = "<a href=\"" + id + "\">" + idSans + "</a>";
      }
      // Add HDL resolver.
      else if (id.startsWith(RDFConstants.HDL_PREFIX)) {
        idContent = "<a href=\"" + LTPUtils.resolveHandlePid(id) + "\">" + id + "</a>";
      }
      // Add DOI resolver.
      else if (id.startsWith(RDFConstants.DOI_PREFIX)) {
        idContent = "<a href=\"" + LTPUtils.resolveDoiPid(id) + "\">" + id + "</a>";
      }

      // Make non-internal identifiers visible as it and add linkage and boldness.
      if (isExternal) {
        idContent = "[external] " + idContent;
      }

      result.add(idContent);
    }

    return result;
  }

  /**
   * <p>
   * Handle OCRIDs and GNDs the easy way :-)
   * </p>
   * 
   * @param theCreators
   * @param theGNDResolver
   * @param theORCIDResolver
   * @return
   */
  private static List<String> getCreators(List<String> theCreators, String theGNDResolver,
      String theORCIDResolver) {

    List<String> result = new ArrayList<String>();

    for (String creator : theCreators) {
      String orcNAME = getNameFromORCIDCreatorEntry(creator, theORCIDResolver);
      if (!orcNAME.isEmpty()) {
        String orcID = getORCIDFromCreatorEntry(creator);
        result.add("<i class=\"icon-user\"></i> " + orcNAME + " (<a href=\"" + theORCIDResolver
            + orcID + "\" target=\"_blank\">" + creator + "</a>)");
      } else if (creator.startsWith(GND_PREFIX) || creator.startsWith(PND_PREFIX)) {
        String indexCreator = creator.substring(creator.lastIndexOf(":") + 1);
        result.add("<i class=\"icon-user\"></i> <a href=\"" + theGNDResolver + indexCreator
            + "\" target=\"_blank\">" + creator + "</a>");
      } else if (creator.contains("://d-nb.info/gnd/")) {
        String indexCreator = creator.substring(creator.lastIndexOf("/") + 1);
        result.add("<i class=\"icon-user\"></i> <a href=\"" + theGNDResolver + indexCreator
            + "\" target=\"_blank\">" + creator + "</a>");
      } else {
        result.add(creator);
      }
    }

    return result;
  }

  /**
   * @param theCreators
   * @param theTitles
   * @param theCreated
   * @param theDoiUrl
   * @param theDoi
   * @param theORCIDResolver
   * @return
   */
  private static String getCitation(List<String> theCreators, List<String> theTitles,
      String theCreated, String theDoiUrl, String theDoi, String theORCIDResolver) {

    String result = "";

    // Add creators,check for ORCID ID.
    for (String c : theCreators) {
      String orcidName = getNameFromORCIDCreatorEntry(c, theORCIDResolver);
      result += (orcidName.isEmpty() ? c : orcidName) + ", ";
    }
    result = result.substring(0, result.length() - 2) + " (" + theCreated.substring(0, 4) + "). ";

    // Add titles.
    for (String t : theTitles) {
      result += t + ". ";
    }
    result += "DARIAH-DE. " + theDoiUrl + "/" + theDoi;

    return result;
  }

  /**
   * @param theConf
   * @return
   * @throws UnsupportedEncodingException
   * @throws IoFault
   * @throws DatatypeConfigurationException
   */
  private static HashMap<String, Object> createBasicHTMLPageHashMap(
      final CrudServiceConfigurator theConf)
      throws UnsupportedEncodingException, IoFault, DatatypeConfigurationException {

    HashMap<String, Object> result = new HashMap<String, Object>();

    result.put("hdl_url", omitTrailingSlash(theConf.getPIDrESOLVER().toString()));
    result.put("doi_url", omitTrailingSlash(theConf.getDOIrESOLVER().toString()));
    result.put("oaipmh_url", omitTrailingSlash(theConf.getOAIPMHlOCATION().toString()));
    result.put("dhcrud_url", omitTrailingSlash(theConf.getCRUDlOCATION().toString()));
    result.put("dhcrud_url_encoded",
        URLEncoder.encode(omitTrailingSlash(theConf.getCRUDlOCATION().toString()), "UTF-8"));
    result.put("publikator_url", omitTrailingSlash(theConf.getPUBLIKATORlOCATION().toString()));
    result.put("datacite_url", omitTrailingSlash(theConf.getDATACITElOCATION().toString()));
    result.put("digilib_url", omitTrailingSlash(theConf.getDIGILIBlOCATION().toString()));
    result.put("manifest_url", omitTrailingSlash(theConf.getMANIFESTlOCATION().toString()));
    result.put("mirador_url", omitTrailingSlash(theConf.getMIRADORlOCATION().toString()));
    result.put("digilib_url", omitTrailingSlash(theConf.getDIGILIBlOCATION().toString()));
    result.put("tify_url", omitTrailingSlash(theConf.getTIFYlOCATION().toString()));
    result.put("switchboard_url", omitTrailingSlash(theConf.getSWITCHBOARDlOCATION().toString()));
    result.put("mail_of_contact", theConf.getMAILoFcONTACT());
    result.put("name_of_contact", theConf.getNAMEoFcONTACT());
    result.put("documentation", theConf.getDOCUMENTATIONlOCATION().toString());
    result.put("api_documentation", theConf.getAPIdOCUMENTATIONlOCATION().toString());
    result.put("faq", theConf.getFAQlOCATION().toString());
    result.put("imprint_url", theConf.getIMPRINTuRL().toString());
    result.put("privacypolicy_url", theConf.getPRIVACYpOLICYuRL().toString());
    result.put("contact_url", theConf.getCONTACTuRL().toString());
    String menuHeaderColor = "";
    if (!theConf.getMENUhEADERcOLOR().equals("none")) {
      menuHeaderColor = theConf.getMENUhEADERcOLOR();
    }
    result.put("menu_header_color", menuHeaderColor);
    String badgeText = "";
    if (!theConf.getBADGEtEXT().equals("none")) {
      badgeText = " [" + theConf.getBADGEtEXT() + "]";
    }
    result.put("badge_text", badgeText);
    result.put("year", CrudServiceUtilities.getYearFromSystemTime());

    return result;
  }

  /**
   * @param theMethodInfo
   * @param theLogID
   */
  protected static void initDataciteClient(String theLogID) {

    String meth = UNIT_NAME + ".initDataciteClient()";

    // Create DataCite HTTP service client, if not existing yet.
    if (dataciteClient == null) {
      dataciteClient = ClientBuilder.newClient().property("thread.safe.client", "true");

      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "New DataCite HTTP client created", theLogID);
    } else {
      DHCrudServiceImpl.log(CrudService.DEBUG, meth, "Using existing DataCite HTTP client",
          theLogID);
    }
  }

  /**
   * @param theDOI The DOI without prefix or namespace!
   * @param theDataciteLocation
   * @param theDatacitePath
   * @param theLogID
   * @return
   * @throws IoFault
   */
  private static Response getDOIMetadataResponse(String theDOI, final String theDataciteLocation,
      final String theDatacitePath, String theLogID) throws IoFault {

    String meth = UNIT_NAME + ".getDOIMetadataResponse()";

    Response result = null;

    initDataciteClient(theLogID);

    // Get DataCite metadata response.
    // TODO Move method to PID service?
    String request = theDataciteLocation + theDatacitePath + theDOI;

    DHCrudServiceImpl.log(CrudService.DEBUG, meth, "DataCite client request: " + request,
        theLogID);

    result = dataciteClient.target(request).request().get();

    if (result.getStatus() != Status.OK.getStatusCode()) {
      DHCrudServiceImpl.log(CrudService.WARN, meth, "DOI metadata status: " + result.getStatus()
          + " " + result.getStatusInfo().getReasonPhrase(), theLogID);
    }
    return result;
  }

  /**
   * @param thePid
   * @param theDOIPrefix
   * @return
   */
  protected static String getDOIfromPID(final URI thePid, final String theDOIPrefix) {

    // Prepare the handle PID, remove hdl prefix and namespace.
    String pid = LTPUtils.omitHdlNamespace(thePid.toASCIIString());
    pid = LTPUtils.omitHdlPrefix(pid);

    // Get the PID without institutional prefix.
    String neededPidFragment = pid.substring(pid.lastIndexOf("/") + 1);

    return theDOIPrefix + "/" + neededPidFragment;
  }

  /**
   * @param theJSONDeletedMetadata
   * @return
   * @throws JSONException
   * @throws ParseException
   */
  protected static Date getTimestampFromHandleMetadata(final String theJSONDeletedMetadata)
      throws JSONException, ParseException {

    Date result = null;

    SimpleDateFormat handleMetadataTimestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    JSONObject jsonDeletedMetadata = new JSONObject(theJSONDeletedMetadata);
    if (jsonDeletedMetadata.has("values")) {
      JSONArray values = jsonDeletedMetadata.getJSONArray("values");
      for (int i = 0; i < values.length(); i++) {
        JSONObject o = values.getJSONObject(i);
        if (o.has("timestamp")) {
          result = handleMetadataTimestampFormat.parse(o.getString("timestamp"));
          break;
        }
      }
    }

    return result;
  }

  /**
   * @param theJSONDeletedMetadata
   * @return
   * @throws JSONException
   * @throws ParseException
   */
  protected static String getCitationFromHandleMetadata(final String theJSONDeletedMetadata)
      throws JSONException, ParseException {

    String result = null;

    JSONObject jsonDeletedMetadata = new JSONObject(theJSONDeletedMetadata);
    if (jsonDeletedMetadata.has("values")) {
      JSONArray values = jsonDeletedMetadata.getJSONArray("values");
      for (int i = 0; i < values.length(); i++) {
        JSONObject o = values.getJSONObject(i);
        if (o.has("data")) {
          result = o.getJSONObject("data").getString("value");
          break;
        }
      }
    }

    return result;
  }

  /**
   * <p>
   * Gets a title list from a JSON metadata ES object, containing string and/or array. Please use
   * one of:
   * <ul>
   * <li>{"descriptiveMetadata":{"dc:title":["WESPE!","GiantWasp.gif"]}}</li>
   * <li>{descriptiveMetadata={dc:title=Ghost.gif}}</li>
   * </ul>
   * </p>
   * 
   * @param theJSON The JSON ES metadata string.
   * @return The list of titles contained in the JSON object or array.
   * @throws ObjectNotFoundFault
   * @throws JSONException
   */
  protected static List<String> getTitlesFromMetadataJSON(String theJSONString)
      throws ObjectNotFoundFault, JSONException {

    List<String> result = new ArrayList<String>();

    // No object found in ES index: Maybe already deleted in ES and not yet in storage!
    if (theJSONString == null || theJSONString.isEmpty()) {
      String message = "Could not obtain metadata from ElasticSearch!";
      throw new ObjectNotFoundFault(message);
    }

    try {
      JSONObject o = new JSONObject(theJSONString);
      JSONObject dmd = o.getJSONObject("descriptiveMetadata");

      try {
        // Get JSON array from titles.
        JSONArray titleArray =
            dmd.getJSONArray(RDFConstants.DC_PREFIX + ":" + RDFConstants.ELEM_DC_TITLE);
        for (int i = 0; i < titleArray.length(); i++) {
          result.add(titleArray.getString(i));
        }
      } catch (JSONException e) {
        // It's not an array, we try getting an object.
        String titleObject =
            dmd.getString(RDFConstants.DC_PREFIX + ":" + RDFConstants.ELEM_DC_TITLE);
        result.add(titleObject);
      }

    } catch (JSONException e) {
      throw e;
    }

    return result;
  }

  /**
   * <p>
   * Gets a mimetype list from a JSON metadata ES object, containing string and/or array. Please
   * use:
   * <ul>
   * <li>{administrativeMetadata={dcterms:format=image/tiff}}</li>
   * </ul>
   * </p>
   * 
   * @param theJSON The JSON ES metadata string.
   * @return The mimetype contained in the JSON object.
   * @throws JSONException
   */
  protected static String getMimetypeFromMetadataJSON(String theJSONString)
      throws JSONException {

    String result = "";

    try {
      JSONObject o = new JSONObject(theJSONString);
      JSONObject dmd = o.getJSONObject("administrativeMetadata");

      // Get JSON string from metadata JSON.
      result = dmd.getString(RDFConstants.DCTERMS_PREFIX + ":" + RDFConstants.ELEM_DCTERMS_FORMAT);

    } catch (JSONException e) {
      throw e;
    }

    return result;
  }

  /**
   * <p>
   * Gets all titles from all parts of the given part ID list.
   * </p>
   * 
   * @param thePartIDs
   * @param theESResponse
   * @return
   * @throws JSONException
   * @throws ObjectNotFoundFault
   */
  protected static Map<String, List<String>> retrieveTitlesFromPartsMetadataPublic(
      List<String> thePartIDs, MultiGetItemResponse theESResponse[])
      throws JSONException, ObjectNotFoundFault {

    Map<String, List<String>> result = new HashMap<String, List<String>>();

    for (MultiGetItemResponse item : theESResponse) {
      List<String> dmdTitleList = getTitlesFromMetadataJSON(item.getResponse().getSourceAsString());
      result.put(item.getId(), dmdTitleList);
    }

    return result;
  }

  /**
   * <p>
   * Gets the title list for the root collection.
   * </p>
   * 
   * @param theRootCollection
   * @param theESResponse
   * @return
   * @throws JSONException
   * @throws ObjectNotFoundFault
   */
  protected static List<String> retrieveTitleFromMetadataPublic(String theRootCollection,
      GetResponse theESResponse) throws JSONException, ObjectNotFoundFault {
    return getTitlesFromMetadataJSON(theESResponse.getSourceAsString());
  }

  /**
   * <p>
   * Gets all mimetypes from all parts of the given part ID list.
   * </p>
   * 
   * @param thePartIDs
   * @param theESResponse
   * @return
   * @throws JSONException
   */
  protected static Map<String, String> retrieveMimetypesFromPartsMetadataPublic(
      List<String> thePartIDs, MultiGetItemResponse theESResponse[]) throws JSONException {

    Map<String, String> result = new HashMap<String, String>();

    for (MultiGetItemResponse item : theESResponse) {
      String mimetype = getMimetypeFromMetadataJSON(item.getResponse().getSourceAsString());
      result.put(item.getId(), mimetype);
    }

    return result;
  }

  /**
   * <p>
   * Computes a humanised extent for given extent in bytes.
   * </p>
   * 
   * @param theBytes
   * @return
   */
  protected static String humaniseExtent(String theBytes) {
    return FileUtils.byteCountToDisplaySize(Long.valueOf(theBytes));
  }

}
