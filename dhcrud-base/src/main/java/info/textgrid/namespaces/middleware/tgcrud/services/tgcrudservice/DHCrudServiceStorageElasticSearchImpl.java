/**
 * This software is copyright (c) 2023 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import org.apache.http.HttpHost;
import org.apache.jena.rdf.model.Model;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.elasticsearch.ESJsonBuilder;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2021-03-05 - Funk - Adapt queries to ElasticSearch6.
 * 
 * 2019-10-25 - Funk - Using RDFUtils now for transformation of RDF model to RDF ElasticSearch
 * compliant model.
 * 
 * 2017-09-07 - Funk - Fixed URI usage in metadata conversion: We must check which URI is used
 * where!
 * 
 * 2017-09-05 - Funk - Adapted to new RDF resources instead of literals in URI elements.
 * 
 * 2017-08-22 - Funk - Adapted to new metadata model/TTL things.
 * 
 * 2016-01-29 - Funk - Adapted to new esutils classes.
 * 
 * 2015-03-03 - Funk - Copied from TGCrudServiceStorageElasticSearchImplementation.
 */

/**
 * <p>
 * This storage implementation grants CRUD operations to the DARIAH ElasticSearch database.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2021-03-05
 * @since 2010-09-09
 */

public class DHCrudServiceStorageElasticSearchImpl extends CrudStorageAbs<Model> {

  // **
  // STATIC FINALS
  // **

  private static final String UNIT_NAME = "esdb";

  private static final String ERROR_STORING_METADATA =
      "Failure storing metadata to ElasticSearch database";
  private static final String NULL_URI = "URI must not be null";
  private static final String METADATA_CREATION_STARTED = "ElasticSearch metadata creation started";
  private static final String METADATA_CREATION_COMPLETE =
      "ElasticSearch metadata creation complete";
  private static final String DURATION_LITERAL = "Duration";
  private static String DC_FIELDS[] =
      {"descriptiveMetadata.dc:title", "administrativeMetadata.dcterms:format"};

  // **
  // PRIVATES
  // **

  private RestHighLevelClient client = null;
  private ESJsonBuilder esJson = null;
  private String indexName = null;

  // **
  // IMPLEMENTED METHODS
  // **

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#create(info.textgrid.namespaces.middleware.tgcrud.services.
   * tgcrudservice.CrudObject)
   */
  @Override
  public void create(CrudObject<Model> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#createMetadata(info.textgrid.namespaces.middleware.tgcrud.
   * services.tgcrudservice.CrudObject)
   */
  @Override
  public void createMetadata(CrudObject<Model> theObject) throws IoFault {

    String meth = UNIT_NAME + ".createMetadata()";
    long startTime = System.currentTimeMillis();

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, METADATA_CREATION_STARTED);

    try {
      if (theObject.getUri() == null) {
        throw CrudServiceExceptions.ioFault(NULL_URI);
      }
      URI prefixUri = theObject.getUri();
      String namespaceUri = LTPUtils.resolveHandlePid(prefixUri);

      init();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "URI for ES: " + prefixUri);
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "Namepsace URI: " + namespaceUri);

      // Prepare DMD for ElasticSearch JSON creation.
      Model dmd = RDFUtils.prepareDMDforJSONCreation(prefixUri, theObject.getMetadata());

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "DMD XML for ES:\n" + RDFUtils.getStringFromModel(dmd));

      // Prepare ADMMD for ElasticSearch JSON creation.
      Model admmd = RDFUtils.prepareADMMDforJSONCreation(prefixUri, theObject.getAdmMD());

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "ADM_MD XML for ES:\n" + RDFUtils.getStringFromModel(admmd));

      String dariahMetadataJson = this.esJson
          .buildDariahMetadataObject(RDFUtils.getStringFromModel(dmd),
              RDFUtils.getStringFromModel(admmd), theObject.getTechMD(), theObject.getProvMD())
          .toJSONString();

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "JSON for ES:\n" + dariahMetadataJson);

      checkAndCreateDocument(prefixUri, dariahMetadataJson);

    } catch (TransformerException | IOException | ParseException | SAXException
        | ParserConfigurationException | XMLStreamException | DatatypeConfigurationException e) {
      throw CrudServiceExceptions.ioFault(e, ERROR_STORING_METADATA);
    } catch (TransformerFactoryConfigurationError e) {
      throw CrudServiceExceptions.ioFault(e.getException(), ERROR_STORING_METADATA);
    }

    CrudServiceUtilities.serviceLog(CrudService.INFO, meth, METADATA_CREATION_COMPLETE);
    String duration = CrudServiceUtilities.getDuration(System.currentTimeMillis() - startTime);
    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, DURATION_LITERAL + ": " + duration);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#delete(info.textgrid.namespaces.middleware.tgcrud.services.
   * tgcrudservice.CrudObject)
   */
  @Override
  public void delete(CrudObject<Model> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#deleteMetadata(info.textgrid.namespaces.middleware.tgcrud.
   * services.tgcrudservice.CrudObject)
   */
  @Override
  public void deleteMetadata(CrudObject<Model> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#getLatestRevision(java.net.URI, boolean)
   */
  @Override
  public int getLatestRevision(URI theUri, boolean unfiltered) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #getLatestRevisionPublic(java.net.URI)
   */
  @Override
  public int getLatestRevisionPublic(URI theUri, boolean unfiltered) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieve(java.net.URI)
   */
  @Override
  public CrudObject<Model> retrieve(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrievePublic(java.net.URI)
   */
  @Override
  public CrudObject<Model> retrievePublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveMetadata(java.net.URI)
   */
  @Override
  public Model retrieveMetadata(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveMetadataPublic(java.net.URI)
   */
  @Override
  public Model retrieveMetadataPublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveAdmMD(java.net.URI)
   */
  @Override
  public Model retrieveAdmMD(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveAdmMDPublic(java.net.URI)
   */
  @Override
  public Model retrieveAdmMDPublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveTechMD(java.net.URI)
   */
  @Override
  public String retrieveTechMD(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #retrieveTechMDPublic(java.net.URI)
   */
  @Override
  public String retrieveTechMDPublic(URI theUri) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage #
   * update(info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudObject)
   */
  @Override
  public void update(CrudObject<Model> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #updateMetadata(info.textgrid.namespaces.middleware.tgcrud.services. tgcrudservice.CrudObject)
   */
  @Override
  public void updateMetadata(CrudObject<Model> theObject) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #move(java.net.URI)
   */
  @Override
  public void move(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #copy(java.net.URI)
   */
  @Override
  public void copy(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #addKeyValuePair(java.net.URI, java.lang.String, java.lang.Object)
   */
  @Override
  public void addKeyValuePair(URI theUri, String theKey, Object theValue) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice. CrudStorage
   * #resolve(java.net.URI)
   */
  @Override
  public URI resolve(URI theUri) throws IoFault, ObjectNotFoundFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#create(info.textgrid.namespaces.middleware.tgcrud.services.
   * tgcrudservice.CrudObject, java.lang.String)
   */
  @Override
  public void create(CrudObject<Model> theObject, String theStorageToken) throws IoFault {
    throw CrudServiceExceptions.ioFault(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#deletePublic(info.textgrid.namespaces.middleware.tgcrud.
   * services.tgcrudservice.CrudObject)
   */
  @Override
  public void deletePublic(CrudObject<Model> theObject) throws IoFault, RelationsExistFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  /*
   * (non-Javadoc)
   * 
   * @see info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.
   * CrudStorage#deleteMetadataPublic(info.textgrid.namespaces.middleware.
   * tgcrud.services.tgcrudservice.CrudObject)
   */
  @Override
  public void deleteMetadataPublic(CrudObject<Model> theObject) throws IoFault {
    throw new UnsupportedOperationException(NOT_IMPLEMENTED);
  }

  // **
  // INTERNAL CLASSES FOR ES COMMUNICATION.
  // **

  /**
   * <p>
   * Updates an object if it is already existing, creates a new one if not.
   * </p>
   * 
   * @param theUri
   * @param theJson
   * @throws IoFault
   * @throws IOException
   */
  private void checkAndCreateDocument(URI theUri, String theJson) throws IoFault, IOException {
    if (idExists(theUri)) {
      throw new IoFault("No ElasticSearch updates are allowed in public DARIAH-DE Repository!");
    } else {
      createObject(theUri, theJson);
    }
  }

  // **
  // DEEP ES METHODS (USING THE ES CLIENT)
  // **

  /**
   * <p>
   * Creates an object in the ES database.
   * </p>
   * 
   * @param theUri
   * @param theJson
   * @throws IoFault
   * @throws IOException
   */
  private void createObject(URI theUri, String theJson) throws IoFault, IOException {

    String meth = UNIT_NAME + ".createObject()";

    String uriWithoutPrefix = LTPUtils.omitHdlPrefix(theUri.toString());

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Creating ElasticSearch object: " + uriWithoutPrefix);

    IndexRequest indexRequest = new IndexRequest().index(this.indexName).id(uriWithoutPrefix)
        .source(theJson, XContentType.JSON);
    IndexResponse response = this.client.index(indexRequest, RequestOptions.DEFAULT);

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth, "ElasticSearch index/id: "
        + response.getIndex() + "/" + response.getId());
  }

  // **
  // UTILITY METHODS
  // **

  /**
   * <p>
   * Gets all needed metadata from all parts of the given part ID list.
   * </p>
   * 
   * @param thePartIDs
   * @return
   * @throws IOException
   * @throws TransformerConfigurationException
   * @throws IoFault
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public MultiGetItemResponse[] retrieveMultiMetadataFromElasticSearch(List<String> thePartIDs)
      throws IOException, TransformerConfigurationException, IoFault, SAXException,
      ParserConfigurationException {

    MultiGetItemResponse result[];

    // Get default DMD metadata model from string.
    init();

    // Include all needed metadata fields.
    String[] includes = DC_FIELDS;
    String[] excludes = Strings.EMPTY_ARRAY;
    FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

    // Create a multi title request.
    MultiGetRequest titleRequest = new MultiGetRequest();
    for (String id : thePartIDs) {
      titleRequest.add(new MultiGetRequest.Item(this.indexName, LTPUtils.omitHdlNamespace(id))
          .fetchSourceContext(fetchSourceContext));
    }

    result = this.client.mget(titleRequest, RequestOptions.DEFAULT).getResponses();

    return result;
  }

  /**
   * <p>
   * Gets all needed metadata from all parts of the given part ID list.
   * </p>
   * 
   * @param theRootCollection
   * @return
   * @throws IOException
   * @throws TransformerConfigurationException
   * @throws IoFault
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  public GetResponse retrieveItemMetadataFromElasticSearch(
      String theRootCollection) throws IOException, TransformerConfigurationException, IoFault,
      SAXException, ParserConfigurationException {

    GetResponse result;

    // Get default DMD metadata model from string.
    init();

    // Include all needed metadata fields.
    String[] includes = DC_FIELDS;
    String[] excludes = Strings.EMPTY_ARRAY;
    FetchSourceContext fetchSourceContext = new FetchSourceContext(true, includes, excludes);

    // Create a title request.
    GetRequest titleRequest = new GetRequest(this.indexName, theRootCollection);
    titleRequest.fetchSourceContext(fetchSourceContext);

    // result = this.client.mget(titleRequest, RequestOptions.DEFAULT).getResponses();
    result = this.client.get(titleRequest, RequestOptions.DEFAULT);

    return result;
  }

  /**
   * <p>
   * Checks if URI already exists.
   * </p>
   * 
   * @param theUri
   * @return Is the URI already existing?
   * @throws IoFault
   * @throws IOException
   */
  private boolean idExists(URI theUri) throws IoFault, IOException {

    String uriWithoutPrefix = LTPUtils.omitHdlPrefix(theUri.toString());
    GetRequest getRequest = new GetRequest().index(this.indexName).id(uriWithoutPrefix);
    boolean res = this.client.exists(getRequest, RequestOptions.DEFAULT);

    return res;
  }

  /**
   * <p>
   * Initialise the ES client and the ES JSON objects once.
   * </p>
   * 
   * TODO Put all init() classes in a TextGrid/DARIAH utility module!? (as already done in
   * TG-publish service!)
   * 
   * @throws TransformerConfigurationException
   * @throws IoFault
   * @throws SAXException
   * @throws ParserConfigurationException
   */
  private void init() throws TransformerConfigurationException, IoFault, SAXException,
      ParserConfigurationException {

    String meth = UNIT_NAME + ".init()";

    // Get host, port, and path config.
    String host = this.conf.getIDXDBsERVICEuRL().getHost();
    String path = this.conf.getIDXDBsERVICEuRL().getPath();
    List<String> esPorts = this.conf.getIDXDBsERVICEpORTlIST();

    // Set index name (MUST be NOT NULL after INIT taken from URL path as:
    // http://searchindex-main:9200/dariah-public!
    String index[] = path.split("/");
    this.indexName = index[1];

    CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
        "Extracted index name [" + this.indexName + "]");

    // Get client as singleton.
    if (this.client == null) {

      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Creating new ElasticSearch client: " + host + " " + esPorts.toString());

      // Extract ports, create host list.
      List<HttpHost> hosts = new ArrayList<HttpHost>();
      for (String p : esPorts) {
        hosts.add(new HttpHost(host, Integer.valueOf(p), "http"));
      }

      // Create ElasticSearch client.
      this.client =
          new RestHighLevelClient(RestClient.builder(hosts.toArray(new HttpHost[hosts.size()])));

    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Using existing ElasticSearch client: " + host + " " + esPorts.toString());
    }

    // Get JSON builder as singleton.
    if (this.esJson == null) {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Creating new ElasticSearch builder");
      this.esJson = new ESJsonBuilder();
    } else {
      CrudServiceUtilities.serviceLog(CrudService.DEBUG, meth,
          "Using existing ElasticSearch builder");
    }
  }

}
