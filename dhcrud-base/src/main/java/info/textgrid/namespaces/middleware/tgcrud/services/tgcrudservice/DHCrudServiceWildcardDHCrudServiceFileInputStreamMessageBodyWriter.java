/**
 * This software is copyright (c) 2020 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import org.apache.cxf.helpers.IOUtils;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.ext.MessageBodyWriter;
import jakarta.ws.rs.ext.Provider;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2020-11-30 - Funk - First version.
 */

/**
 * <p>
 * Use this special MessageBodyWriter to write WILDCARD mimetype files.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-11-30
 * @since 2020-11-30
 */

@Provider
@Produces(MediaType.WILDCARD)
public class DHCrudServiceWildcardDHCrudServiceFileInputStreamMessageBodyWriter
    implements MessageBodyWriter<DHCrudServiceFileInputStream> {

  /**
   *
   */
  @Override
  public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations,
      MediaType mediaType) {
    return type == DHCrudServiceFileInputStream.class;
  }

  /**
   *
   */
  @Override
  public long getSize(DHCrudServiceFileInputStream stream, Class<?> type, Type genericType,
      Annotation[] annotations, MediaType mediaType) {
    // Deprecated by JAX-RS 2.0.
    return 0;
  }

  /**
   *
   */
  @Override
  public void writeTo(DHCrudServiceFileInputStream stream, Class<?> type, Type genericType,
      Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders,
      OutputStream entityStream) throws IOException, WebApplicationException {
    IOUtils.copyAndCloseInput(stream, entityStream);
    entityStream.flush();
  }

}
