#!/bin/bash

# Generates a GRID Proxy file out of an CERTIFICATE (e.g. a ROBOT certificate)

# TODO Catch any errors, make the script robust in any way possible.

# Some references.
export GAT_LOCATION=/usr/local/javagat
TGCRUD_LOCATION=/usr/local/tgcrud
CERT_LOCATION=$TGCRUD_LOCATION/certs
INFO_MAIL=tgcrud@daasi.de

# At first get the password from the user.
echo "This is the proxy generator! Please state the certificate passphrase below:"
read -s CERT_PASSPHRASE

# Now start generating a grid proxy every 24 hours, that is valid for 26 hours.
while [ 1 ]
do
    # Backup the old proxy file, if existing.
    if test -e $CERT_LOCATION/tgcrud.proxy
    then
	cp $CERT_LOCATION/tgcrud.proxy $CERT_LOCATION/tgcrud.proxy_OLD
    fi
    
    # Generate the grid proxy.
    echo $CERT_PASSPHRASE | $GAT_LOCATION/bin/grid-proxy-init -pwstdin -cert $CERT_LOCATION/usercert.pem -key $CERT_LOCATION/userkey.pem -valid 26 -out $CERT_LOCATION/tgcrud.proxy >> $CERT_LOCATION/proxy.log
    echo "Proxy generated for textgrid-ws3.sub.uni-goettingen.de" | mail -s "Proxy successfully generated" $INFO_MAIL
    sleep 86400
    #sleep 4
done
