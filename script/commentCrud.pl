#!/usr/bin/perl -w 

# This script is called via cron to create the logcomment.log file each day after tgcrud has logged the daily rollback.log file.

@a = gmtime (time - 60*60*24 ); 
$yesterday = sprintf ("%04d-%02d-%02d", $a[5]+1900, $a[4]+1, $a[3]); 
system "/root/crudAna -l /var/log/textgrid/tgcrud/rollback.log.${yesterday} -c /var/log/textgrid/tgcrud/logcomments.${yesterday}.log";
system "/root/crudAna -l /var/log/textgrid/tgcrud-public/rollback.log.${yesterday} -c /var/log/textgrid/tgcrud-public/logcomments.${yesterday}.log";
