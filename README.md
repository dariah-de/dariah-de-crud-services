# The DARIAH-DE {TG|DH}-crud Services

## Overview

TG-crud and DH-crud are the services for all the CREATE, RETRIEVE, UPDATE and DELETE operations of the TextGrid and the DARIAH-DE Repository. Both the TextGrid and the DARIAH-DE CRUD Services are offering all it's methods via REST protocols. SOAP is implemented only for TG-crud Service methods.

## Technical Information

The package *crud-base* provides general CRUD Service classes, including the TGCrudServiceImpl class for the TextGrid CRUD service. The DARIAH-DE CRUD Service classes can be found in module *dhcrud-base*, and the web service implamentations are covered by the *{tg|dh}crud-webapp* and *{tg|dh}crud-webapp-public* modules.

The classes {TG|DH}CrudServiceImpl implement the service interface created by the CXF web service implementation. All service methods can be used with the Java client classes that are provided in the middleware.tgcrud.clients.tgcrudclient folder, using the JAXB data binding, or using every client to be build to serve the TG-crud's WSDL, or WADL file, or using the tgcrud-client Maven module provided with the parent TG-crud module. The DH-crud Service only offers a REST API.

## Documentation

For service and API documentation please have a look at the [TG-crud Documentation](https://textgridlab.org/doc/services//submodules/tg-crud/tgcrud-webapp/docs/index.html) and the [DH-crud Documentation](https://repository.de.dariah.eu/doc/services/submodules/tg-crud/dhcrud-webapp-public/docs/index.html).

## Installation

Both services are provided with a Gitlab CI workflow to build Docker containers to be deployed and installed on TextGrid and DARIAH-DE servers.

### Building from GIT using Maven

You can check out the {TG|DH}-crud from our [GIT Repository](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services), develop or main branch, or get yourself a certain tag, and then use Maven to build the {TG|DH}-crud Service WAR file:

    git clone https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services.git

...build the WAR package via:

    mvn clean package

You will get a {TG|DH}-crud WAR file in the folders ./{tg|dh}crud-webapp/target and ./{tg|dh}crud-webapp-public/target.

## Deploying the {TG|DH}-crud Service

The service is deployed just by installing the appropriate Docker container.

## Releasing a new version

For releasing a new version of TG-crud or DH-crud, please have a look at the [DARIAH-DE Release Management Page](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management) or see the [Gitlab CI file](.gitlab-ci.yml).

## Large file handling

Some large (test) files > 100 MB are handled with git-lfs, so please have a look at <https://gitlab.gwdg.de/help/topics/git/lfs/index>. All files with file name endings *_LARGE.tif* and *_LARGE.zip* are handles with git-lfs.

## Badges

[![OpenSSF Best Practices](https://www.bestpractices.dev/projects/3885/badge)](https://www.bestpractices.dev/projects/3885)
[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/dariah-de/dariah-de-crud-services)](https://api.reuse.software/info/gitlab.gwdg.de/dariah-de/dariah-de-crud-services)
[![Dependency Track Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/dariah-de-crud-services/develop)](https://deps.sub.uni-goettingen.de/projects/5b09cf22-0d55-405d-aec4-8e29d0cea707)
[![Dependency Track Container Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/dariah-de-crud-services-container/develop)](https://deps.sub.uni-goettingen.de/projects/6cd8e931-940a-480b-ac25-3ded73e33038)
