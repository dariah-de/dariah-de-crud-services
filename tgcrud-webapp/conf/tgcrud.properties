# Special TG-crud feature: Directly publish objects created with #CREATE.
# Used for the second (public) TG-crud instance used by TG-publish.
DIRECTLY_PUBLISH_WITH_CREATE = FALSE

# The TextGrid metadata namespace TG-crud is working with (mandatory).
METADATA_NAMESPACE = http://textgrid.info/namespaces/metadata/core/2010

# The path to the log4j config file (mandatory).
LOG4J_CONFIGFILE = /etc/textgrid/tgcrud/tgcrud.log4j

# The Handle resolver URL (resolving only!).
PID_RESOLVER = https://hdl.handle.net/

# The ID implementation to use for URI creation.
ID_IMPLEMENTATION = info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceIdentifierUuidImpl
# The security implementation (AAI) to use for authentification and authorisation.
AAI_IMPLEMENTATION = info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceAaiTgextraImpl
# GRID storage implementation class.
DATA_STORAGE_IMPLEMENTATION = info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceStorageLocalFileImpl
# XMLDB storage implementation class.
IDXDB_STORAGE_IMPLEMENTATION = info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceStorageExistImpl
# RDFDB storage implementation class.
RDFDB_STORAGE_IMPLEMENTATION = info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudServiceStorageSesameImpl

# The default encoding of all used XML operations (default: UTF-8).
DEFAULT_ENCODING = UTF-8

# The path, all files will be stored to, including the protocol to use. Only
# "gsiftp://" and "file://" are supported at the moment (mandatory).
DEFAULT_DATA_STORAGE_URI = gsiftp://host-1.de//data/
DEFAULT_DATA_STORAGE_URI_PUBLIC = gsiftp://host-public.de//data/
# The pathes of all needed replica locations, separated by whitespaces
# (optional).
REPLICA_DATA_STORAGE_URIS = gsiftp://host-1-replica.de//data/
REPLICA_DATA_STORAGE_URIS_PUBLIC = gsiftp://host-public-replica.de//data/

# Which DATA STORAGE authentication shall be used (e.g. JavaGAT)? Allowed is one
# of MYPROXY, PROXY, CERT, SLC or NONE. The needed values have to be set
# accordingly (mandatory).
DATA_STORAGE_AUTHENTICATION = NONE
# A valid grid proxy certificate to access the above given Grid location
# (mandatory if GRID_AUTHENTICATION is set to PROXY).
#GRID_PROXY_FILE = /usr/local/tgcrud/tgcrud.proxy
# A valid GRID certificate to access the above given Grid location
# (mandatory if GRID_AUTHENTICATION is set to CERT).
#GRID_CERT_FILE = ***
#GRID_KEY_FILE = ***
#GRID_CERT_CN = *** 
#GRID_CERT_PASSWORD = ***
# Some values to use MyProxy certificates, default port is -1.
# (mandatory if GRID_AUTHENTICATION is set to MYPROXY)
#MYPROXY_SERVER_HOSTNAME = ***
#MYPROXY_SERVER_PORT = ***
#MYPROXY_USERNAME = ***
#MYPROXY_PASSWORD = ***
# TODO To be included: SLC
# Values for DARIAH Storage API access
#DARIAH_USER = ***
#DARIAH_PW = ***
#DARIAH_BASE_URL = *** 
#DARIAH_IDP_URL = ***

# The prefix of the URI created for TextGrid objects (mandatory).
URI_PREFIX = textgrid
# The suffix for the TextGrid metadata files stored TO THE GRID (mandatory).
METADATA_FILE_SUFFIX = .meta
# The suffix for the TextGrid rollback temporary files (mandatory).
ROLLBACK_FILE_SUFFIX = .rollback
# Shall the baseline encoded objects be validated by the AdaptorManager?
# (default: FALSE).
VALIDATE_BASELINE = FALSE

# The URI of the Tgextra service endpoint (mandatory).
AAI_SERVICE_URL = http://localhost/tgauth/tgextra.php                   
# The URI of the TG-crud specific Tgextra service endpoint (mandatory).
AAI_CRUD_SERVICE_URL =	http://localhost/tgauth/tgextra-crud.php
# The secret for registering/unregistering resources to/from the RBAC
# (mandatory).
AAI_SPECIAL_SECRET = *
# The TG-auth client stub timeout (default: 30000). TG-lab timeout is 60000, so
# so we should use less.
AAI_CLIENT_STUB_TIMEOUT = 45000

# The RDF database namespace (mandatory).
RDFDB_NAMESPACE = http://textgrid.info/relation-ns
# The RDF database service URL, non-public instance (e.g. Sesame).
RDFDB_SERVICE_URL = http://textgridlab.org/1.0/openrdf-sesame/repositories/textgrid/
# The RDF database username and password.
RDFDB_USER = *
RDFDB_PASS = *
# The RDF public database service URL, public instance (e.g. Sesame).
RDFDB_SERVICE_URL_PUBLIC = http://textgridlab.org/1.0/openrdf-sesame/repositories/textgrid-public/
# The RDF public database username and password.
RDFDB_USER_PUBLIC = *
RDFDB_PASS_PUBLIC = *
# The SPARQL namespace (mandatory).
SPARQL_NAMESPACE = http://www.w3.org/2005/sparql-results

# The Index DB namespace, if XML.
IDXDB_NAMESPACE = urn:exist
# The Index database service URL (e.g. ElasticSearch).
IDXDB_SERVICE_URL = http://textgridlab.org/1.0/exist/rest/
# The index database cluster name (if needed).
IDXDB_CLUSTER_NAME = tg-search-es-ws4
# The Index database username and password (mandatory).
IDXDB_USER = *
IDXDB_PASS = *
# The public Index database service URL (e.g. ElasticSearch).
IDXDB_SERVICE_URL_PUBLIC = http://textgridlab.org/1.0/exist/rest/public/
# The public Index database username and password (mandatory).
IDXDB_USER_PUBLIC = *
IDXDB_PASS_PUBLIC = *
# The Index database pathes to store metadata, baseline encoded files, and
# original files (if needed).
IDXDB_METADATA_PATH = metadata/
IDXDB_BASELINE_PATH = structure/baseline/
IDXDB_ORIGINAL_PATH = structure/original/
IDXDB_AGGREGATION_PATH = structure/aggregation/
IDXDB_QUERY_PATH = db/generic/query/
# Pairtree configuration for Index database.
IDXDB_PAIRTREE_USAGE = FALSE

# The URI settings.
ID_SERVICE_URL = http://textgrid-java2.sub.uni-goettingen.de/nd/noidu_textgrid/
# The NOID username and password.
ID_SERVICE_USER = *
ID_SERVICE_PASS = *
# The duration an URI stays locked without re-locking (in minutes).
ID_AUTOMAGIC_UNLOCKING_TIME = 1800000

# Password for the #MOVE method.
PUBLISH_SECRET = *

# Password for the #REINDEXMETADATA method.
REINDEX_SECRET = *

# Settings for the message producer.
USE_MESSAGING = FALSE
MESSAGING_SERVICE_URL = http://localhost:5672
MESSAGING_EVENT_NAME = tgcrud-event
MESSAGING_USER = *
MESSAGING_PASS = *
