# How to contribute to DARIAH-DE CRUD Services

Please read the [RDD Technical Reference Manual](https://gitlab.gwdg.de/fe/technical-reference/-/releases) before proceeding!

If you want to contribute to the DARIAH-DE-CRUD Services, please

1. Fork the GIT repository: `git clone https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services.git`

2. Do install the needed packages for commiting (you may to have use --force to avoid some unmatched dependencies): `npm i (--force)`

3. Create an new issue for your feature/fix in the [GitLab GUI issue page](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues).

4. Create a new branch for your feature/fix.

5. Develop your feature/fix in the new branch and commit and push acording to the [sesmantic release rules](https://semantic-release.gitbook.io/semantic-release/)

6. Husky and commitizen installation should force you to provide correct commit messages.

7. Then file a [merge request in the GitLab GUI](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/merge_requests/new) from your new branch (src) to the develop branch (dest).

8. If you are a maintainer, please take the [DARIAH-DE Release Workflow](https://projects.academiccloud.de/projects/dariah-de-intern/wiki/dariah-de-release-management) into account.

## Reporting an issue

To just report an issue or bug please write to mailto:textgrid-support@gwdg.de.
