## [12.2.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.2.0...v12.2.1) (2024-12-11)


### Bug Fixes

* add doc ([0f6f1bc](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0f6f1bc9ed7d300f657ec0378d2f06af38aab8cd))
* add real isPublic value to all messages sent by message producer ([31288ad](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/31288ad072a8d4334228955f41f37cba672884a7))
* fix message acknowledgement message ([bcb5a63](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bcb5a631cc92b2a96f1e822cea61a981ac92f03c))
* increase cxf version to 4.0.6 ([470cc96](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/470cc968cacd586769269336eee1a97d5106750f))

# [12.2.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.7...v12.2.0) (2024-11-25)


### Bug Fixes

* add alias hdl to consistency object (i would bet) ([6a8442a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6a8442aa7843a09251d2485a22db3cc04bcf7a8e))
* add authoritative query to checkConsistency method ([56980c2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/56980c24d7d76a5b01e8b57221d1365034361779))
* add logging ([ba8fb0d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ba8fb0d7f1cc99afcff53c525f270b1ded5a549a))
* add logging for ES and Storage metadata ([8504023](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/850402376f6526569deec3b5ebaeacf60ada5746))
* add NOT_IN_METADATA if pid not found in metadata ([8081166](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8081166acb89ce8fb11d17cd9406d4adfa5f7945))
* add orig pid if redirected from pid service ([bbe6645](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bbe66457b4f31ef9cf4e910544f4b1c57ee05292))
* add reindex secret ([5c19491](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5c194918bf338472b29b2b9ebf0dfa0d4d47dc4d))
* add writing to storage to re-index method, we need the new relations in there! ([734e884](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/734e884da9450ceb28e27dc75652cabb6eabedec))
* checking correct pid type now ([e28a637](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e28a6373489322e9875ba813213d9250ccdb116b))
* correct test ([7f27e33](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7f27e3321f677aba229d16891b4b218ffc8b408b))
* fix  auth hdl metadata call ([719256d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/719256df973bcf537f96abc30b474b12bea1254d))
* fix base64 decoding ([a7e3689](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a7e36898e2f8b0e850f5e5e2925802da26c456ac))
* fix error fixing issue :-D ([0256c41](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0256c419cb5320947ede07b85ad42b1d47abd26e))
* fix filesize not existing fault ([37498b2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/37498b2315995188acf4b865afd5421e6640f71f))
* fix method header ([2dfcdb6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2dfcdb6d213bc079c625e74453acfaa08d266c30))
* fix re-idex end method logging ([8bd9718](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8bd9718e5b881e609f5524d9a1f7b284792b3f63))
* fix the fix of the fix: decode base64 correctly ([06ae311](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/06ae3115cd7a6c7c2ad92c89455c36442d435da7))
* fix typo ([d30f291](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d30f29123f8e8e86d2e0dadd8b0f6b357dd57406))
* get original metadata from elasticsearch ([5c8a045](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5c8a045e1e3c88f53410a35d6bd0d753430c5758))
* handle exception ([e287dd9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e287dd9773e7ab08ea0a1588bf2835d47d2d7846))
* increase to next common release version ([8089da7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8089da732f7012e634555f4efa3f86008cefe0cb))
* pid was already rewritten ([a9a67be](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a9a67be661ccdf6c7cf6444a1bf8f3f3ea5fa790))
* re-fetch handle metadata if HS_ALIAS is set ([81a0a7a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/81a0a7ae8f89e3828b95437d40f5469017ac4934))
* refactor method reindex ([4c2110a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4c2110aa38b8f61d8c7bbb31069eadffe3c6d2a8))
* remove some todos ([e8931c9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e8931c91ca453af569b31f85280f49b0ec4cfded))
* set reindex secret name in tgcrud api correctly ([2a4e77d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2a4e77d781dbadd99f6b8034468c452a5601774e))
* use correct retrieval method for metatata retrieval now ([310cbb4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/310cbb4a4b9940c9f61cf8dd464f42cce3741465))
* use reindex_secret in reindex method, too :-) ([4b2f9fe](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4b2f9feb877cfa5153227f2ff2f31b2d912db0c3))
* use update metadata for es in reindex ([dbb2dff](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/dbb2dfff3de3b0ff1b1b3bae1fe37c1d8c20c918))


### Features

* add retrieve method to elasticsearch storage method ([bbc4f65](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bbc4f6506fb9cd497972111b4649444a8b7fde73))
* new method REINDEXMETADATA for reindexing storage metadata to elasticsearch ([779bff1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/779bff1ec508471ca3b5849997514ca69b96c039))

## [12.1.7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.6...v12.1.7) (2024-10-23)


### Bug Fixes

* add new snapshot common version ([7cf9f38](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7cf9f38bb0b5288187769bb220e3b54d4d269aab))
* use new common release version ([d7f80f5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d7f80f576e09474afe934e56ccdf036abf25e6a6))

## [12.1.6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.5...v12.1.6) (2024-10-07)


### Bug Fixes

* add more logging ([7a840ff](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7a840ff8a04ab75c0b11d57066aa251cd9775764))
* add some debug logging ([2e7fc83](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2e7fc83ac580e5386d90bae4c36bbd3fc289741e))
* change date ([54f498e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/54f498e237852f5445914ae6e7d4eca7a17df741))
* fix  try-with-resource warnings, add default charset final ([10ad6e3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/10ad6e30a3eeb3ff16c9313f4ccb62b23879ff47))
* fix some findbugs issues ([0202ced](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0202cedc6d8511b7cdb709666f134aec591ba96e))
* fix some more warnings, refactor method headers ([add636f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/add636f3ff377e19a86dbc8d922137f5d46e32d9))
* get http headers too ([57cb460](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/57cb460c881888737da26c43560c67d68bf8136d))
* merging ([de64e69](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/de64e69f5d0ebf3323a308a24fdc5a0bb5b3464e))
* more debugging ([a1edc14](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a1edc144c052c4b7ea5aa699371e54c8408bf2e8))
* more debugging ([9aa427c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9aa427c1d59c787e0c59db5b02feed65ff18dbf5))
* remove latest debug logging ([d222330](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d222330121693a176494dfd271ef4fdb7a43667e))
* remove SNAPSHOT deps ([8f1bf63](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8f1bf639214b8c84877763b5249062623bd2d925))
* shorten readroot ([a1403ee](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a1403eea3c9b93f39eb6acf2e11d5641bd243b84))
* using new common snapshot version ([3f7b4a5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3f7b4a5c2ca8d627e112339dc811fec76d39290d))

## [12.1.5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.4...v12.1.5) (2024-10-02)


### Bug Fixes

* fix indexOutOfBound on different dc:relation ([6f55d32](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6f55d32ed75d7bf136152a9aa1f153b0645958d9))
* stop deleting the relation type, we only need to delete and re-write the RDF type ([ecf7342](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ecf7342473265e38584e3b2e2e0e1d05e4a51352))

## [12.1.4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.3...v12.1.4) (2024-09-26)


### Bug Fixes

* add relation metadata to landing page, sort identifiers and relations on page ([2aa8152](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2aa81527629fefee3becabcc9162d31a6b80de34))
* fix cts ref ([321056c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/321056ccf2cd68b43850b006594da8f6ad762a5a))
* fix error page logos and doc references ([a45b142](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a45b142b18d3dca31da9b8b875869ba134eaedc5))

## [12.1.3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.2...v12.1.3) (2024-09-05)


### Bug Fixes

* rename concistency check method ([ae36a83](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ae36a83e38ea202e5afd584d85b081e2ce4de3e6))

## [12.1.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.1...v12.1.2) (2024-08-22)


### Bug Fixes

* add relations RDF check for #create and  #update ([c8b906a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c8b906a01f934b2d3eb20331b2980e3efd6ce0c2))
* catch RiotException only ([9c72153](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9c721533c8fddcf3742db384b7d3113a0d6adf85))
* fix rdf relations bug, ignoring invalid rdf ([0901416](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/09014163a167ee71362b40400970671b9fb37624))
* re-adapt exif info if RDF is invalid, only for images! ([a78c214](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a78c2141dc8fb00fd23915ead23960c4f4d89b03))

## [12.1.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.1.0...v12.1.1) (2024-07-11)


### Bug Fixes

* add missing item, collection, edition, and work metadata ([953e478](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/953e478b0a9db2de32aeb1d38129969ee4964107))
* add more npe checks ([a610c40](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a610c40056da8633118637ecd677950cb03bc61f))
* aRG! add more npe checks! ARG! ([291c553](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/291c5531238898c2c1c11c4817d3d31dde2a61fa))
* do not add item object on existing editon, collection, and work tags ([1dbd828](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1dbd828377292eca2b345fa44209865e64b3bdb1))

# [12.1.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.0.2...v12.1.0) (2024-07-05)


### Bug Fixes

* add api classes ([ebeaa96](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ebeaa9693b2a1f61b0cda0f8ae3efa8d49bf9b31))
* add checksum and filesize from storage ([5228829](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5228829d185d6c4967d0170a3455ee2e8830b438))
* add config ([ac01124](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ac011245b886abb81cf92d23b70440f11366941e))
* add constructor to Consistency object ([f224fd5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f224fd53d0fa3c8a772b475074dc70e36c8919ef))
* add general values to response ([81d1bef](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/81d1bef0bb03a0e7e8831956eb57f08a1ce7d4b5))
* add hdl service call directly ([1082710](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/10827107531016783e886c08bffc4111df1b25fa))
* add JSON message body writer ([8ae9a36](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8ae9a36085a7972404bc3eb46713854ca8d75237))
* add k8s storage request ([7dca7e1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7dca7e11fbc51494780effba47d5be12c27e9629))
* add logging ([2d37dde](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2d37dde75e7eafdf7787b942f138cce180018383))
* add method for checking consistency on uri ([2277fd9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2277fd9657fd096c6684cf294a75c2570517f7e6))
* add mimeytpe and created date to consistency object ([41d7012](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/41d70121210041175bc7cb5cbe02a71a0b32c124))
* add more tests ([6b58bcb](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6b58bcb827dbf068227761d2381175dfef87a391))
* add more tests ([68cf844](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/68cf844e8dae9d1324ff1d1ba832334c9a15f89e))
* add needed new class :-D ([89cd1ae](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/89cd1aeb9657c210b763dde0f8392a499ac3e3cf))
* add new test ([688cab4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/688cab4989a98e2a13f899b402b5509f9db4b231))
* arg! add conf ([18cabcf](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/18cabcffd5a0fd85a775e66ce95bc0875a33420f))
* change json output ([671d972](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/671d9723c212426f838de7dd31fee8d006ca505f))
* correct hdl value ([85a7b3b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/85a7b3b0e2c5a2dbbe971d3149c0d39acc99f85a))
* deliver filesize ([fdc4b1b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/fdc4b1b04456bdf3810e51d0b4295f45cce39dfb))
* fix -q param ([a51170e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a51170e140574b01fd251fb0cc72bfda3347fb7e))
* fix equality check ([a29e5c1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a29e5c10d62d3a82c08d5abadb818b17817e2423))
* fix json output ([36c07cc](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/36c07cc51076777870379dd4d1016e3539d70ead))
* fix last typos ([5884ac2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5884ac28a06f141b8606d33e9161836b8c54558b))
* fix response type ([99dc55e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/99dc55ead6ca25a60a9d5fd72d71743090d43976))
* fix rest api ([3708262](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3708262345b83bb6f2eda849fa6f9c615d85bfa7))
* fix storage limits for k8s runners ([fefbd27](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/fefbd271803078994710efb35076edfa8d88f5a2))
* fix ubbos remarks ([1f552a7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1f552a7498651db17121265a5390d7ac0d4b5f40))
* fixxxxxxing...... arg! ich will endlich ein BIER! ([0bf034d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0bf034da799a5af15330f4282ca80002a0cdaa4f))
* go to next common release version ([e4e62eb](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e4e62eb9681653055eb37a92febdef41182458e0))
* hmpf! more logging ([e043b88](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e043b88c350452cc1124706e900eabd1d3df5652))
* just testing ([7d7c944](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7d7c94466de114636ac573b86197b5f6f6048354))
* make it more robust! yeah! ([75d7dfe](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/75d7dfe01231adfbea7cfa103df2760f2e289373))
* more adaptations ([9a8f700](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9a8f700d14a71a4e14fdb763be8d344949ca301a))
* more testing ([5d71635](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5d71635e0a96c9538fb937a250900ecf1e91c671))
* remove things and add things ([6411aef](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6411aef57f47f842c51101b4e56557ba1723355a))
* return no html anymore ([ee3b10e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ee3b10edfb6cacbed601d72a66d602bf552f4fbd))
* some more json ([61e7c8b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/61e7c8bbf936aa69a0ed519ad8c9eeb7920d1e0e))
* take public uris only ([467c6b8](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/467c6b897449ab170073cb58e75f2ce1f41c6649))
* testitest ([40b31c5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/40b31c520e1fd09d71a07e9d2bec56d833eb5e26))
* use pojos for consistency json build ([5a5646f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5a5646fed3a6677b382501a438d4b741635d8c8e))
* use prefix exchange for deprecated handle prefixes ([28b4969](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/28b49694960536f10f923111ccabef4104a59b6c))
* using json for response ([1824120](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1824120f08b7dcefba63a90ab7ccee9dfbb1e380))


### Features

* Resolve "implement filesize and checksum check method for one object" ([0053a9b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0053a9be1105ffd1f8d88264279239683f54f2b3))

## [12.0.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.0.1...v12.0.2) (2024-06-05)


### Bug Fixes

* fix deleting relation data on update ([9f494be](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9f494be0771d86339b1c987a358c88df2fc3f7db))
* fix some warning calls ([300e138](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/300e1382b9701504066f44ab74b78f678401ef58))

## [12.0.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v12.0.0...v12.0.1) (2024-05-23)


### Bug Fixes

* add editorconfig ([7bc5a44](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7bc5a441ff06aa7f9da6a1290de08745b694e63c))
* increase tg-pid version ([c71c314](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c71c31476fecbe4b3793626004779a1fd6bdfdbc))

# [12.0.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.9.4...v12.0.0) (2024-05-23)


### Bug Fixes

* aahhhhh!!!! re-ignore online tests (again...) ([c5447f2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c5447f223d45321ac3ab7d82f6e623daedb1d38f))
* adapt config for messaging ([1193398](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1193398828e799ce15b7892645a35be8e7980d12))
* adapt dhcrud to es7: type removal ([43305c6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/43305c610929fdba78851d8e18cc8d3dfd9fe562))
* add all jarxs annotations again ([7020b2c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7020b2cc80145a7b02edbcb71671afadd5ca0e9e))
* add badges to readme ([bdad1f8](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bdad1f81a1558c8097f82c139d39844d963c4a46))
* add correct logo test pages ([f5d3916](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f5d3916079aaaf068b04089c036f4032beec37c2))
* add cts logo, monochromize logos ([ffc4c5a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ffc4c5ab5b12570779af1f2d9c56b6b3d7ee7f54))
* add fast transition and coloured logos on hover ([88d21cd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/88d21cd244367960a644c5f4f0b535b404fd9f29))
* add fine grained error message to HTML error response ([ad971ca](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ad971cab6e4bc882841a76808765633684168ac4))
* add jaxrs annotations for implementing methods again ([5499d4f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5499d4fd5a217d7772f7df7142105ffe199d5c05))
* add junit test cases for new metadata checks ([0ac0c60](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0ac0c6078e69a1cff052b2873c7c1bcaefeb81e4))
* add logging ([4111439](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4111439b6d6ead091e8d1658007e9a504243f8e3))
* add logos to footer for html pages ([5fdc31b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5fdc31b439a38a612fc814921cd94bb7a33242f0))
* add messaging initialisation ([88057ed](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/88057ed999a2ec01d9c739f9494e0eb6ae439601))
* add more ([9f20e7d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9f20e7dca097ecf03135bcd2a89735dffc991387))
* add more error handling ([4d50ef4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4d50ef43fec6bec76dc76a7e3462a221612b8654))
* add more es logs ([7ea1ad6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7ea1ad691f6ea9018327f7d4b6d0d1561b2e1aac))
* add more logging ([65bd4da](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/65bd4daaab9bbba0c3cfcd87592616a336a2fd40))
* add more logging ([2ad9b3f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2ad9b3f127536ac9c85ab2586a802e57d8b76025))
* add more logs ([8ce40bd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8ce40bd109021fa2e09d445e40a88de816c85d6d))
* add new class files, finally... :-) ([8952c40](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8952c40496dad572865446bd30d014016a46a2a3))
* add new test files ([decb94d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/decb94d5692f3d5e91aaf9facfe0f35434521ac1))
* add new URL for datacite doc call from landing page ([317c8b5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/317c8b562e43e129a51b1a62e0c53a89acd70cea))
* add removing last "/" if existing ([a6f35da](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a6f35da92875027ea011830299301feff31dcb9e))
* add test files ([c95914e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c95914e99cb802e066e621c1f5bbfd4437fd26c3))
* add to remote connection factory ([6e984c9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6e984c9671c85d02db200a9d1891f9ad6a7f1c3f))
* add warnings to metadata on metadata validation issues ([b536944](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b536944d4097ed4c6318afdb8e9cdde60ab4855b))
* add warnings to metadata, finally... :-D ([134e11c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/134e11c72e087df2d1308c73b9e23b9b3b37700c))
* add warnings to metadata, finally... finally ([f049fb5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f049fb58b80dc6b9fdff3e1392e512424653c723))
* add web application errors for each rest method it was missing ([3300691](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3300691b82224b3646083cd3aacc06a080b1faed))
* add wildfly ejb bom ([d636ca4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d636ca4c84cf22d62d4a50306c50cc4163209727))
* add wildfly jms again ([5343021](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/534302136f8c30e07aabeed395b4f47a5c9ab47c))
* add wildfly naming client ([1d1885d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1d1885d35fc369ce858835d64b6a36113d6da507))
* add wildfly naming client package ([2d0dfd8](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2d0dfd81320334bf9083c1fa3904d67536aa50e0))
* akk! ([99d039c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/99d039c4448ec00a66d59a5106822e29686ac146))
* another factory test: java:jboss/exported/jms/RemoteConnectionFactory ([a2348ba](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a2348ba680ce81582da44aa03e23d0a2145bd1ce))
* arg! ([2818ac3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2818ac3d1109df543cffde39f819ad75c498d7d0))
* aRG! ([b45a422](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b45a422272cbeda9a4daa946289fb5260fbbd0f1))
* arg! re-ignore online tests ([921237f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/921237f857c7ca385d55e5dd042c549c4325b128))
* back to jboss connection factory ([9985b1e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9985b1eb02d50cf204efe2d73caffc41eb650115))
* change connection factory ([ea0183a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ea0183a8de29ead8147d4bdd9a8dd4ef4ddf2f49))
* check message event names ([28bb9c1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/28bb9c1a776178c934d0b7e1ce75cd27bc9c6166))
* checking conditional ([f8c60b0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f8c60b05c3fd4d74832849c9a10f158497322f1f))
* decrease jakarta-bind version ([e142511](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e142511670e90155312e569efc0dfa4f1d9a87d1))
* devide messaging in tg and dh message producers ([0cabd54](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0cabd5401a549d7c56c988908bb3bab3a2b12765))
* doofe online tests! ork! ([a7d0644](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a7d0644787153e87a2db60bd3b3fc8521f7159ca))
* first create new rdf data, then delete old data from rdfdb! ([7c98437](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7c984374b7aed60c321e8fb17ca4e5e9244a93e4))
* fix all still missing refs ([dece133](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/dece133385a74f5414cbbff324a0e61fc09b4b6f))
* fix error in noid creation ([2f87bbd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2f87bbd9d98f4f41b0370c86a6f4965737424301))
* fix getting the index name from the IDX service URL ([fab863a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/fab863a6dd46c24e41c4a2b12f2a42feaccebb8f))
* fix html error page parsing ([60f1d1a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/60f1d1a646119dc2b725d4b5e8bf386795064144))
* fix init of messaging ([5252268](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/525226862b2bde4d3d003579246723f8be8d269f))
* fix link to terms of use in tombstone page ([f7f0d09](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f7f0d09be0e2544e3c77775503d043545c473c0d))
* fix logging with uri instead of configuration ([afa8c69](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/afa8c69c7770fea983d60366b329d0fdd653f8df))
* fix noid http queries ([9404c7c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9404c7c9ced1227bef613173ecb6ebadef4c8f93))
* fix noid http queries, finally ([e7d41a6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e7d41a699d0368ea3cab4e0c0b3aec66d07f52b5))
* fix online tests ([6eb3e12](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6eb3e1238cd0fc9b5bfff12036516b1023afd3ec))
* fix test for invalid hdl regexp from 404 to 500 ([1df84d1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1df84d192e9946328a29820c876998ca1f6857ef))
* fix tgauth workaround for #TG-635, error message string has changed! ([346acac](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/346acacf02ec890dabe678c98f962794b7130b4d)), closes [#TG-635](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/issues/TG-635)
* fix typo in config class ([5c8f9f7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5c8f9f7c0ac3e8290a759303ec8d82675dc505e8))
* fix var names, change logging to INFO level ([acfc237](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/acfc2379acd01773877fe73a99932ebe459cf3e4))
* fix webserviceexception issue ([3da4f46](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3da4f46c381ef77a66f0120b540ff528544dc326))
* fixing message sender creation ([413394c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/413394cb77d3a1a112763a86a9b9cf4b0970b3a7))
* ignore online tests ([2a217c1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2a217c1da78e4b1b32587c848a0ef3469c06880d))
* increase antrun plugin version and config params ([72c776c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/72c776c10052b742bd67ffae3a0116880ed9af0f))
* increase common version ([6985986](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6985986a8af91ce3dd57490c985510706033940a))
* increase common version to newest SNAPSHOT ([c6b17a7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c6b17a7f2202f6580ca4cc218454a9601c17db5a))
* increase commons version to 5.0.0 (es7) release ([68b1af9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/68b1af9eaa0d7a71ab4a5a6f4a0a244f4377a1ee))
* increase many things :-) spring and tomcat10 mainly, i would think ([5fdb1ec](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5fdb1eca9558f9eb935b5407397423d06d61700d))
* increase module versions, especially elasticsearch ([2ba7c59](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2ba7c59f4cf913f70236b52a8d22fa16e1174e5d))
* increase tgauth version ([61c65a1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/61c65a10ff10a6504123efcde6e9221670c3c9e0))
* increase tgauth version (java17) ([aacc2e3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/aacc2e354a7cc1fcf0a12be887326cdae17e42a0))
* increase wildfly client ersion ([1f0895b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1f0895b99f14de119a6fe68234dc01c5460c3f4d))
* last cosmetic things ([67c2f9a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/67c2f9aad5e24b9f6ffd489d258930f0b58d5c0b))
* log connection factory ([70513a4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/70513a4b78c0673eda042b5b5c81ae0b8237f0e2))
* make ObjectMapper reusable ([eed8f3a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/eed8f3a84770d7a44473a808360995929fedbc98))
* merge develop for correct main pom file ([4512579](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4512579aa7f6b0716d500f64852a38a68da9357e))
* more output ([d687c3e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d687c3e20aaabf92de491355737a44c54e7822f2))
* more testing ([1a22333](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1a22333d90a3c63a19571954f617b5c163dd20ea))
* more testing ([bc5e81f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bc5e81fbb92c4b8ce02e70b42ddf4a09d42f5ad8))
* more type removals ([1236064](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1236064d2ca4301b14955a507f04c026fd011f5d))
* move event config into crud config file ([dedc009](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/dedc009b8c8bd014e066f556249178121c020187))
* only take the first path as index name from crud configuration ([860327a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/860327a2f5adb1ac41493f9a8abbae2b5faeb4cc))
* re-ignore also dhrep online testing :-D ([4ee6f24](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4ee6f24f3bded36b8d99a8c82bbaafcb237ea862))
* re-ignore online testing ([2ec1f47](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2ec1f47a47b363188777c36f10bfa83f9f2243c3))
* re-ignore online testing ([549418a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/549418aaff62bf814c02472501541814a573dad6))
* re-set formatting... :-D ([fc45537](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/fc4553710f0f7fd81c259f7e167633ff7ab3d25b))
* remove apache httpclient ([0227c64](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0227c6454d7869e5f64e67f7e92eae2d85f1a69e))
* remove code of crud hotfix branch... whatwhatwhat? ([a48c1b3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a48c1b3cba9de2835494e10904d5ede237521625))
* remove ejb-bom from pom file ([b45bca2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b45bca29a00d5cd78a40c4f2cd9584a6e02217a2))
* remove generated type only for metadata validation ([5639cec](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5639cecdc7b3e8f23c2e90a73e1038dc5b99bf48))
* remove merkwürdische validation class! ([0c431d2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0c431d21ccd8aff8b638cf1728cf0c71c9afa091))
* remove ssl for messaging, seems not to be needed for local messaging, no? ([157e2bd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/157e2bdeb5b76122acbf7cfcda9c68baa76c7ed5))
* remove types from es init and config ([14f1b29](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/14f1b29b112551907ce2615dfc8326a0d086e917))
* remove unused javatar version in main pom ([7464d51](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7464d5110ea1e976d27ce84036dd5454e0cd98bc))
* remove unused mail dep ([110fc7f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/110fc7f514d6fa6e44536ca26058be8339961fcc))
* remove validation? ([e365661](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e365661757bb39cbb4e3986da14b56d82a639485))
* remove wildfly from poms ([c147ad5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c147ad5f05c9c1fa441f8f475e9e6e0f3674fb68))
* reset connector name ([c800437](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c800437aa6e49ba4fdee89a0c9d6af9b7ebb7dd9))
* retry connection factory ([b27e36c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b27e36c6a7358abbd4eaa64518838cd3279765d0))
* set ci and dockerfile to j17 ([95581b5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/95581b5bec1ba64fdea159e577532626886b097a))
* set CORRECT tgauth version this time! :-D ([0de5955](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0de5955e9e6598374bb46116b269849b5f8f77bf))
* set sharper log levels ([b1cebc6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b1cebc6c9416a81284fb9cb95dec26ef35c3ec59))
* set ssl for messaging in crud message producer ([c61e2de](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c61e2deb853660145f6b7b38d892c5afea7c29d9))
* testing another connection factory ([28d38f2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/28d38f2e773b66a99e7c44d1c608fc8db9a9fe09))
* update crud config files, and configurator settings for messaging ([eab635c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/eab635c0808701bf36cc684c3f9b5058950602a9))
* update exception for workaround to WebServiceException ([84e3b53](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/84e3b5309e181dd02c30d65f8ee3a3021deec35f))
* update jackson version, include jackson for dhcrud-base ([2607386](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2607386a7af203703373c8f14f290533aaee8e92))
* update link to faq (junit test only) ([029181c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/029181c4250c7aab297da59c5472e263f650397e))
* update readme ([f45dd17](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f45dd1772952bb5bb69e599d28b00368ea8b3377))
* update to java:comp/DefaultJMSConnectionFactory ([1c6dbf1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1c6dbf1dc7927c247b07b0341996985536106df8))
* update wiki links on error page ([bf0c96e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bf0c96ec759f6fea116e50e6790d7cc175948d92))
* use final static object mapper now ([b88fc9d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b88fc9d68d76edbe885be22ddab2c87f431e8ea7))
* use jaxb cloning with marshal/unmarshal ([5228411](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/52284118ef17b2242acb372fa96ea3363a05a0cd))
* use microprofile webservice for tgsearch online testing ([92aef5d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/92aef5dd03860519a21793610654cb834605060b))
* yeah, best is testing locally first :-D ([aff7019](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/aff7019f06efad2e1f43143aa7b883e30ac3065d))


### Features

* add drunken owl to html error page ([6a1861f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6a1861facd24bb6f64017ac5941766f6d682bfd1))
* increase cxf and java, fix pom files ([f3608cd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f3608cd1d8f70571bf334a8844ec5df7b2fb277d))
* increase to new common version ([7040c68](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7040c68966b26eb3863ff0ccdc1715e92be58a39))
* **messaging:** wildfly is obsolete now, we are using amqp! yeah! ([e877fb0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e877fb0e823f326f6bd527188abf5fc39f5d37b7))
* try to update to Java 17 and CXF 4 :-) ([3064d69](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3064d699dc3b2cfae38dd1d9ddf4caef4ad5ee7e))


### BREAKING CHANGES

* **messaging:** no wildfly, but ampq now!
* No TGHttpClients anymore to use

## [11.9.4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.9.3...v11.9.4) (2023-08-04)


### Bug Fixes

* add matomo to error and tombstone pages ([726cd01](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/726cd01f90bffee1b6858de9e2b13032b41ffd50))

## [11.9.3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.9.2...v11.9.3) (2023-08-03)


### Bug Fixes

* add not found fault if elasticsearch metadata is not found for deleted objects ([1a7573d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1a7573de240fa20a3545d352eedf711f9483c67d))

## [11.9.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.9.1...v11.9.2) (2023-07-03)


### Bug Fixes

* increase common and tgauth versions ([7127c87](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7127c87aa4ce7d8b28bb82bda8489310542af073))

## [11.9.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.9.0...v11.9.1) (2023-05-26)


### Bug Fixes

* add blobs folder for large test files ([44da5ca](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/44da5ca3c1343a750888f640b65d14e50b7ed937))
* add git lfs support and tracking for *_LARGE.tif and *_LARGE.zip files ([7221380](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7221380eb114ea429413140109d568855b3690bb))
* introduce lfs for large tif images and large zip files ([af759d3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/af759d33812ae6ef5b161957f41b852f1d927168))
* remove blob folder, makes no sense to sort files that way ([80ca637](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/80ca63741ec39ca21e98b9940417b1052fc5a9fb))

# [11.9.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.8.1...v11.9.0) (2023-05-03)


### Bug Fixes

* add TextGrid URI as base URI to RDF triples if about="" (and btw: remove last deb things) ([817c159](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/817c159ea7fcbed2fa1160df11a37580d979749e))
* add textgrid uri to subject resource to all anonymous rdf statements ([b9f8c89](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b9f8c89071c1ef696526b038ca38413f750d25db))
* increase common version to new snapshot ([a191196](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a19119600cd521243abc0399f3897fa9335f850a))
* remove hazelcast dependency ([4603b98](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4603b9876c9bbfcff546281b33a2a8070c7a419f))
* remove jdeb version from main pom ([9c9faf1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9c9faf1db9a9873db5a7ab1528904b1fca5d677a))
* remove online tests ([f1d3925](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f1d3925739ce9ce6cea8747ee2f4c2b022b0bb42))


### Features

* increase common version include tg-java-clients ([31b34e5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/31b34e50af7537c4bb9b902c399a02b0f47dbfba))

## [11.8.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.8.0...v11.8.1) (2023-04-13)


### Bug Fixes

* remove content-length feature ([515cf8d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/515cf8d371d88969b0357cc19e04fbf59a18b571))

# [11.8.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.7.5...v11.8.0) (2023-02-28)


### Bug Fixes

* use current jquery version 3.6.3 in crud html page generation ([f507030](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f507030a376033e42696434fdbc4b6a5ecd4170f))


### Features

* drop aptly deployment ([2a53ce6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2a53ce655a46d284b6efdbbf880adb33b678d227))

## [11.7.5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.7.4...v11.7.5) (2023-02-21)


### Bug Fixes

* add extent as content-length for tgcrud read requests ([69638d4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/69638d42eb26073fa6dd2aeb86b2f836519d456c))
* correct non-deletion of spinning flower on image landing page ([e704390](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e7043909d02c8ccf2ea953a39f6afa8dec761d27))
* correct some HTML issues and warnings from W3C validator ([ac114e7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ac114e7ddc669d3bccdbe924339042f7b99b52ad))
* remove spinning flower due to non-existing GUI concept ([f32619d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f32619dbf8146b2e529b24911cf41d229ab5c410))

## [11.7.4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.7.3...v11.7.4) (2023-02-16)


### Bug Fixes

* adapted expected error html ([1f944f6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1f944f63b0080668ba55880f0713f588237b3b38))
* add cookie-free matomo code to landing pages ([b244c34](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b244c343747f8b64c469f49534a3010999743ce4))
* add next audit betterings ([9dbfc73](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9dbfc73f51a387a22da9979d16f9a3bac045628e))
* direct fix of landing page ([0f1fe4e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0f1fe4ee883dea1f04760af2526389fd14e99e64))
* fix date delivery for both timestamp and non timestamp date formats ([c34bb5a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c34bb5ac4a22b984550adcb80c4c9283fdba1c35))
* fix test for remote machines with different default timezone ([cdeb264](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/cdeb2648223526da3cd955f955b7e81e997457f4))
* refactor timestamp issues ([4c8f001](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4c8f00176842a214d4e71502e9ea32ffb3a40e2e))
* resolved audit issues ([2266e70](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2266e7013f9aa8c3a2499dd76c6fb181ec3a2067))

## [11.7.3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.7.2...v11.7.3) (2022-11-24)


### Bug Fixes

* add correct references to IIIF, Mirador, and TIFY to landing page ([0cfa34a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0cfa34a2891587998e9742413bcd61bf172976cd))
* add new locations to landing page ([32da459](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/32da459ee8e2659e220d8e20bdc4f602b38aba41))
* add year automatically for landing pages copyright date ([de9d663](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/de9d66352b118ac4b88ee2c12f32ec04dec9e956))
* correct Mirador and TIFY URLs ([c0b90d0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c0b90d04101beef80fc0899ae323674a0202cb9b))
* merge develop into here ([de59001](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/de590012f98d6bdacb25c52b4d771e36898d11bb))
* re-assuring existing relation type in metadata (fixes [#302](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/issues/302)) ([33bf0e6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/33bf0e6c77a02343c7c563a7ca92f200c27c330b))
* remove iiif references for data objects/images ([0050808](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/0050808393bcaa37222d3179465469d390211ca5))

## [11.7.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.7.1...v11.7.2) (2022-10-13)


### Bug Fixes

* increase some dep versions ([f4226e1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/f4226e1cef604ffe301db8e93344c04e9b4f9a5c))
* remove N3 triple validation for all triples ([e011858](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e011858a23e8f72f33141a20858bbd170257b6a6))
* restrict N3 validation to create and update ([5ec80e4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5ec80e483194bb432c0848c8863acbd1d13664fa))
* validate N3 triples before uploaded into the RDF database ([11f9a7b](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/11f9a7b457e8af1f2910982111123201cbf78551))

## [11.7.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.7.0...v11.7.1) (2022-09-22)


### Bug Fixes

* create new BOM version for deps in gitlab CI file ([41ed0fd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/41ed0fd31ffa3e6afc0a00a3f160bee347411cad))
* fix es port configuration in config and in es module ([4d30405](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4d304058fe15136f29866f0d20aa84e08820ce12))
* fix port handling for dhrep, too ([c7f6b57](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/c7f6b5700a53e6614bfdb2c9cf55085d1de5839f))

# [11.7.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.6.5...v11.7.0) (2022-06-28)


### Bug Fixes

* Adapt logging for deleting files to avoid WARNING if file does not exist.xsxxsx ([1f8dc1a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/1f8dc1a45971bf31f77752739e57ec4b906c5cd4))
* Add and ignore test for bug [#277](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/issues/277) ([bb8bb46](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bb8bb466d39c6c09a7bb7fd04e4175d5ceee4a2d))
* Add commitizen configuration ([6430f8c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/6430f8c783131aab6cd687e84ce9efae297e6a10))
* Add CONTRIBUTING.md ([452f475](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/452f475bbdb6e25452af0f09dbf06352ea7c29b3))
* Add CONTRIBUTORS file ([a4cb841](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a4cb8410ca3ede6b5ed4cff41799a639354cfacf))
* Add CR to pom file ([668e012](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/668e01225b29dd70cc236bcf6ab3665e1a837416))
* Add dependency for CycloneDX maven plugin ([476e4b5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/476e4b58e36143d74fa55d425ff525b3b24e52ed))
* Add husky installation ([36919a2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/36919a2fd7137c524fcbff921510c51183e6d2a2))
* add more doc ([db5aa4d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/db5aa4d8c2750c7843f5ab64526bcc007771ec0a))
* Add new test for testing continuously update of XML files (not aggregations) ([d8646d9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d8646d92c797aea6162d98d30fc3baa92ba32c21))
* Add out-commented var ([3705fa6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3705fa669f3d6172102bb769a4eb653f7cdb1e69))
* Add test for locking with base URI ([b3e4ce2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b3e4ce2217c717d020fe5c6978a508c081a8b805))
* add to doc ([4b65fb0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4b65fb0121a88b5e6065945d8eb18f823991506d))
* And fix another SpotBugs issue ([2571aac](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2571aac3a77e448dfb913af9ff78414c1d4c2d26))
* Dix some SpotBugs issues ([85c013d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/85c013d5f6a14a2e3aaa15da9d1b571b252755b8))
* es client is created before loggiong! fixes [#2154](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/issues/2154) ([d86f8a6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d86f8a6a132e790f833d8b6676fbf146dbdb0ee2))
* Fix another SpotBugs issue ([8643009](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8643009e8c7b4ccb7e3e15c76412f24849f9e110))
* Fix some deprecated stream close issues. ([d6960df](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d6960df98c09ed23440f19f00ebbcf6519d1b46d))
* Fix typo in pom ([606e473](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/606e4735b9a943aeabcb4b1735509cfd8a9f495d))
* more ci changes ([ac3431c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ac3431c225350bd30ccbc88fc1eb433169aece10))
* Re-factor stages ([e528b7a](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e528b7acc0237a4bff9ceb690a6022ce362f7e65))
* Re-set test settings ([32d07f7](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/32d07f7fad0e21d97e5b0a0efff3db1f30ad7849))
* Remove online tests ([02fdb4d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/02fdb4d051e6bf8bc2c21edd7f74d609f0f88dc7))
* Testing fix for [#277](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/issues/277) ([2a96afc](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/2a96afcfcfc34eccfa8008e8f64de19a48218d96))
* update CONTRIBUTING.md ([cfe026d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/cfe026dfcd956f92886fb434fded3fa982c2bedb))


### Features

* Add BOM creation, update minor common version ([37538cf](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/37538cf022ac58a0959ffa2967b4dff222ee8cd2))
* Add CycloneDX BOM upload (increase minor swagger packages for testing) ([3da2eee](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3da2eeeac75f8ed55d6047613656edf58a497f93))

## [11.6.5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.6.4...v11.6.5) (2022-05-20)


### Bug Fixes

* Deactivate to-filter copy and rewriting for now due to new CI workflow! ([bd4734d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/bd4734db51f0dfb81afb0e91a65d96068e5c4f26))
* new restraint in ci rules for main commit and push, increase version ([b0b1acd](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b0b1acdc376b04eea5c5e1eb5e916f2037557fd4))

## [11.6.4](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.6.3...v11.6.4) (2022-05-20)


### Bug Fixes

* Increase SNAPSHOT version, add doc ([13a1d9f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/13a1d9f09424a70012633cf4bd5299764211ae6a))

## [11.6.3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.6.2...v11.6.3) (2022-05-20)


### Bug Fixes

* Adapt config logg messages ([7f6b9b8](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7f6b9b82ecb7d58917f13bdf835eff59c1aed8e9))
* Add some logging logs ([20b5df0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/20b5df07b867003b4dcdf72813262139b6901313))
* Fix more logging ([9dcd21c](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9dcd21c1a8b29e1ebc91584d66d2c191c5846a15))
* New SNAPSHOT version due to release failures... ([e880c6e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/e880c6e330dbdde276c492424c86eca949aa79b7))

## [11.6.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.6.1...v11.6.2) (2022-05-19)


### Bug Fixes

* Add deploying the SNAPSHOT JARS using --disable-snapshot-skip-ci on semantic-release call ([460c2af](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/460c2af63384c776929e983d56fe5e14ebafd066))
* Add TODO to gitlab-ci file ([344e2fb](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/344e2fb550bc935f9a0caca5a132c9ddd82113fc))
* Comment out online tests again ([d42b9d0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/d42b9d089a49f0b606964884de46802f227c9c81))
* just testing logging ([a167e3e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a167e3e9436da51abad92467ff8dab8afa7d0835))
* Remove rollback logging test ([ae9de08](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/ae9de08f207a3bd07321be899da1f661e8d372d7))
* Remove tests for index HTML page ([4486ad6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/4486ad64b7c267c977b6e0e195cc8b1cdb961021))
* Testing rollback logging ([572315f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/572315f5af22ab6fa55d2cd2737ce1b6b27e3ab5))

## [11.6.1](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.6.0...v11.6.1) (2022-05-13)


### Bug Fixes

* Really do test releasing now! ([cd0d0a5](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/cd0d0a57daf17da83a73b019e9b70f0bfe5712ee))

# [11.6.0](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/compare/v11.5.1...v11.6.0) (2022-05-13)


### Bug Fixes

* Add package files, and husky folder ([a1519df](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/a1519df54bb8b6b8756481bf09a01efd35126fcb))
* Add some doc ([5aa0d62](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5aa0d6291d7d670e2ce8b294f77d0a8d562e536d))
* another removal ([34a1c3f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/34a1c3f4e3ae98c8266b2924a8a6a92f4f74644e))
* just minor outcommenting ([079b3f2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/079b3f2a84d80bca760ae6d9abe163e4ccaae1c1))
* Remove another out-commented, eeh, comment? ([5884b42](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/5884b42ddc5aee0cb75162f941fa775949897d7c))
* Remove everything not needed for semantic-versioning testing ([9581059](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/958105971b91abc31c6ba4236224c21ca2229565))
* Remove mvn run for validating java ([7f981c9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7f981c9115204a09506d182cc81ad35bee39c80e))
* remove orig jobs, add test jobs ([8ec999e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/8ec999e8c5bf61f519e834e527be006c23375861))
* Testing first run of semantic-release without --verify-release ([b942412](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/b9424120b65b2519729c7353ae970d738816c5ae))
* Testing with ci after local success ([3dcd056](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/3dcd0563d58dddb6d6d036cbff3e70049db91f54))
* Trying tags on release using rules... ([505d893](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/505d893d830202c8b704b5444f073b9f7a3d7a1b))


### Features

* Add new Gitlab CI workflow, adapt CHANGELOG ([9c5b51e](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/9c5b51ee6af0023bdbd7e9b515a10baa86612f84))
* **cicd:** Resolve "Improve CI process" ([7ecd710](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/commit/7ecd7101a85dba6be9d924cefe9e4aa80e6de0e7))

# 11.2.0-TG-RELEASE

* Use Gitlab CI now for build and deployment of JARs and DEBs

# 11.1.0-SNAPSHOT

* Remove index page
* Add infos to landing page
* Metadata formatting for landing page
* Add humanised extent to landing page (KB, MB, etcpp.)
* Change DOI and HDL identifiers on landing page

# 11.0.0-SNAPSHOT

* Merge feature/elastic6

# 10.6.3-ES6-SNAPSHOT

* Fix #35572: TG-crud shall deliver filenames and mimetype as HTML headers
* Fix #35026: Check if dir exists before creating it using .mkdirs()

# 10.4.0-ES6-SNAPSHOT

* Increase CXF version to 3.3.9
* Adapt dhcrud classes and config files to ES6

# 10.3.0.1-DH

* Remove client folder and added online tests to crudclient-online module

# 10.2.5.7-SNAPSHOT

* Increase CXF version to 3.3.6
* Allowing BagIt 1.0 in RDAResearchDataRepositoryInteropWG new bagit-profile version 0.2

# 9.8.2

* Add RESTful method #recache for DH-crud only (for the moment)
* Increase CXF to version 3.2.8

# 9.4.1

* Add Matomo support for DARIAH-DE Repository index and landing pages (dhcrud)
