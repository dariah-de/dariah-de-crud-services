/**
 * This software is copyright (c) 2023 by
 *
 * DARIAH-DE Consortium (https://de.dariah.eu)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @copyright Göttingen State and University Library (https://sub.uni-goettingen.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice;

import java.net.URI;
import jakarta.activation.DataHandler;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 * 
 * 2021-10-13 - Funk - Mark index page as deprecated. We want only one landing page with all the
 * information.
 * 
 * 2021-07-15 - Funk - Add "hdl:" and "doi:" to expected DOI/Handle PID RegExp.
 *
 * 2019-05-15 - Funk - Removed ePPN from method parameters. We get it from storage now!
 * 
 * 2019-03-25 - Funk - Added re-cache for PIDs.
 *
 * 2017-11-06 - Funk - Added content negotiation for / access.
 * 
 * 2017-10-17 - Funk - Refactored QueryParams to FormParams for POST calls!
 * 
 * 2017-08-29 - Funk - Added some metadata and bag retrieval methods.
 *
 * 2017-01-18 - Funk - Added storage token param and DARIAH-DE storage token.
 *
 * 2016-08-31 - Funk - Added seafile token param.
 *
 * 2016-03-18 - Funk - Added PARAM_PID to create methods.
 *
 * 2016-03-16 - Funk - Added index support for collections, and bag support.
 *
 * 2015-10-06 - Funk - Added provenance metadata support.
 *
 * 2015-08-06 - Funk - Added some metadata reading methods, and offset parameter for streaming.
 *
 * 2014-12-22 - Funk - Adapted to new repository prototype plans.
 *
 * 2014-10-31 - Funk - Merged into tgcrud master branch.
 *
 * 2014-09-09 - Funk - First version.
 */

/**
 * <p>
 * This API provides REST access to TG-crud methods.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2020-06-22
 * @since 2014-09-09
 */

public interface DHCrudService {

  // **
  // STATIC FINALS
  // **

  public static final String PARAM_METADATA = "metadata";
  public static final String PARAM_DATA_URI = "dataUri";
  public static final String PARAM_DATA = "data";
  public static final String PARAM_PID = "pid";
  public static final String PARAM_HOW_MANY = "howMany";
  public static final String PARAM_OFFSET = "offset";
  public static final String STORAGE_TOKEN = "X-Storage-Token";
  public static final String SEAFILE_TOKEN = "X-Seafile-Token";
  public static final String TRANSACTION_ID = "X-Transaction-ID";
  public static final String APPLICATION_ZIP = "application/x-zip-compressed";
  // General PID and DOI syntax.
  public static final String PID_AND_DOI_REGEXP =
      "(hdl:|doi:)?[0-9]{2}\\.[a-zA-Z0-9]+\\/[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{1}";
  // The GWDG Handle PID syntax, using institution ID 21.
  public static final String GWDG_HANDLE_PID_REGEXP =
      "^21\\.[a-zA-Z0-9]+\\/[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{1}$";

  // **
  // METHOD DECLARATIONS
  // **

  /**
   * @param metadata
   * @param data
   * @param pid
   * @param storageToken
   * @param seafileToken
   * @param logID
   * @return A create HTTP response.
   */
  @POST
  @Path("/create")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response create(final @Multipart(value = PARAM_METADATA,
      type = MediaType.APPLICATION_OCTET_STREAM) DataHandler metadata,
      final @QueryParam(PARAM_DATA_URI) URI data, final @QueryParam(PARAM_PID) URI pid,
      final @HeaderParam(STORAGE_TOKEN) String storageToken,
      final @HeaderParam(SEAFILE_TOKEN) String seafileToken,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * @param metadata
   * @param data
   * @param pid
   * @param storageToken
   * @param seafileToken
   * @param logID
   * @return A create HTTP response.
   */
  @POST
  @Path("/createBinary")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response createBinary(final @Multipart(value = PARAM_METADATA,
      type = MediaType.APPLICATION_OCTET_STREAM) DataHandler metadata,
      final @Multipart(value = PARAM_DATA,
          type = MediaType.APPLICATION_OCTET_STREAM) DataHandler data,
      final @QueryParam(PARAM_PID) URI pid, final @HeaderParam(STORAGE_TOKEN) String storageToken,
      final @HeaderParam(SEAFILE_TOKEN) String seafileToken,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * @param howMany
   * @param logID
   * @return A getPids HTTP response containing howMany EPIC Handles.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getPids")
  public Response getPids(final @QueryParam(PARAM_HOW_MANY) int howMany,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * @param howMany
   * @param logID
   * @return A getPids HTTP response containing howMany DataCite DOIs.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/getDois")
  public Response getDois(final @QueryParam(PARAM_HOW_MANY) int howMany,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the data object out of the Bagit Bag.
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DARIAH data object.
   */
  @GET
  @Produces(MediaType.WILDCARD)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/data")
  public Response read(final @PathParam(PARAM_PID) URI pid,
      final @QueryParam(PARAM_OFFSET) long offset, final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the default descriptive metadata out of the Bagit Bag (TURTLE).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DC metadata TURTLE.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/metadata")
  public Response readMetadata(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the descriptive metadata out of the Bagit Bag (RDF/XML).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DC metadata RDF/XML.
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/metadata/xml")
  public Response readMetadataRDF(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the descriptive metadata out of the Bagit Bag (TURTLE).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DC metadata TURTLE.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/metadata/ttl")
  public Response readMetadataTTL(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the descriptive metadata out of the Bagit Bag (NTRIPLES).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DC metadata NTRIPLES.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/metadata/ntriples")
  public Response readMetadataNTRIPLES(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the descriptive metadata out of the Bagit Bag (JSON).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DC metadata JSON.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/metadata/json")
  public Response readMetadataJSON(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the descriptive metadata out of the Bagit Bag (JSONLD).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with DC metadata JSONLD.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/metadata/jsonld")
  public Response readMetadataJSONLD(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the default administrative metadata out of the Bagit Bag (TURTLE).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with administrative metadata TURTLE.
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/adm")
  public Response readAdmMD(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the administrative metadata out of the Bagit Bag (TURTLE).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with administrative metadata TURTLE.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/adm/ttl")
  public Response readAdmMDTTL(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the administrative metadata out of the Bagit Bag (RDF/XML).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with administrative metadata RDF/XML.
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/adm/xml")
  public Response readAdmMDRDF(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the administrative metadata out of the Bagit Bag (JSON).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with administrative metadata JSON.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/adm/json")
  public Response readAdmMDJSON(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the administrative metadata out of the Bagit Bag (NTRIPLES).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with administrative metadata NTRIPLES.
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/adm/ntriples")
  public Response readAdmMDNTRIPLES(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the administrative metadata out of the Bagit Bag (JSONLD).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with administrative metadata JSONLD.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/adm/jsonld")
  public Response readAdmMDJSONLD(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the technical metadata out of the Bagit Bag (FITS XML).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with technical metadata FITS XML.
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/tech")
  public Response readTechMD(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the provenance metadata out of the Bagit Bag.
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with provenance metadata.
   */
  @GET
  @Produces(MediaType.TEXT_XML)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/prov")
  public Response readProvMD(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Creates a object landing HTML page.
   * </p>
   * 
   * @param pid
   * @param logID
   * @return Landing page HTML.
   */
  @GET
  @Produces(MediaType.TEXT_HTML)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/landing")
  public Response readLanding(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the object as a Bagit Bag ZIP file.
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with the object as Bagit Bag ZIP.
   */
  @GET
  @Produces(APPLICATION_ZIP)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/bag")
  public Response readBag(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Exports the object as a BagPack export BagIt ZIP file.
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with the object as BagPack Bagit Bag ZIP.
   */
  @GET
  @Produces(APPLICATION_ZIP)
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/bag/pack")
  public Response readBagPack(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Reads the object as negotiated in the HTTP header:
   * <li>text/html - HTML landing page</li>
   * <li>application/x-zip-compressed - the Bagit bag as a ZIP file</li>
   * <li>other or not set - the object as it is (see /data)</li>
   * </ul>
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response with the object as Bagit Bag ZIP.
   */
  @GET
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}")
  public Response readRoot(final @PathParam(PARAM_PID) URI pid,
      final @HeaderParam("Accept") String accept, final @HeaderParam(TRANSACTION_ID) String logID);

  /**
   * <p>
   * Gets the version. Yeah! Nothing else!
   * </p>
   * 
   * @return DH-crud Version string
   */
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Path("/version")
  public String getVersion();

  /**
   * <p>
   * Checks and re-caches image file, if necessary. Sends an UPDATE message via DH-crud to messaging
   * service (Wildfly).
   * </p>
   * 
   * @param pid
   * @param logID
   * @return HTTP response of the update message sending call.
   */
  @GET
  @Path("/{" + PARAM_PID + ": " + PID_AND_DOI_REGEXP + "}/recache")
  public Response recache(@PathParam(PARAM_PID) URI pid,
      final @HeaderParam(TRANSACTION_ID) String logID);

}
