.. dhcrud documentation master file, created by
   sphinx-quickstart on Thu May 21 14:55:42 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


DH-crud Service
===============

The DARIAH-DE CRUD Service is the core storage service for the DARIAH-DE Repository. It talks to both the DARIAH-DE OwnStorage and PublicStorage. Two instances of the DH-crud are in use within the DARIAH-DE Repository: An only internally used instance that actually is creating and importing objects (via Publikator) and an externally used instance (the documented one) that is delivering publicly available data and metadata to the users.


API Documentation
-----------------

To retrieve data and metadata you would need the object's Handle PID or DataCite DOI, such as

::

    21.11113/0000-000B-C8EF-7

or

::

    10.20275/0000-000B-C8EF-7

All requests below using the Handle PID can be equally stated using the DataCite DOI of the object. Just exchange the Handle PID prefix with the DataCite DOI prefix, such as:

    `https://repository.de.dariah.eu/1.0/dhcrud/10.20375/0000-000B-C8EF-7 <https://repository.de.dariah.eu/1.0/dhcrud/10.20375/0000-000B-C8EF-7>`_


#VERSION
^^^^^^^^

To check the current productive DH-crud version simply try:

  	`https://repository.de.dariah.eu/1.0/dhcrud/version <https://repository.de.dariah.eu/1.0/dhcrud/version>`_

and you get the currently deployed version of the productive public DH-crud.


Content Negotiation
^^^^^^^^^^^^^^^^^^^

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7 <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7>`_

Access on root object level provides content negotiated responses depending on Request Accept Header:

- Accept: **text/html** will provide the HTML landing page,
- Accept: **application/zip** will provide the ZIPped BagIt bag, and finally
- **leaving out** the Accept-header will return the data object itself in it's original mimetype.


#GET Data
^^^^^^^^^

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/data <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/data>`_

RESPONSE: A file object with appropriate Content-Type.


#GET BagIt Bag
^^^^^^^^^^^^^^

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/bag <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/bag>`_

RESPONSE: A DARIAH-DE Repository BagIt ZIP file object with Content-Type application/zip.


#GET RDA BagPack (Research Data Repository Interoperability WG)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    `https://trep.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/bag/pack <https://trep.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/bag/pack>`_

RESPONSE: A RDA WG Repository Interoperability BagIt ZIP file object with Content-Type application/zip (see ´working group web site ´<https://www.rd-alliance.org/groups/research-data-repository-interoperability-wg.html>´_).


#GET Descriptive Metadata (DMD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Descriptive metadata are in DC namespace and to be added by the user importing data into the repository. Only the dc:identifier tags holding the URL, DOI, and Handle PID are added by the DH-crud service.

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata>`_ (same as metadata/ttl)

RESPONSE: text/plain (TURTLE)

.. literalinclude:: ./metadataResponse.ttl
   :language: ttl

Different formats can be requested using

- TURTLE: `/metadata/ttl <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata/ttl>`_ (text/plain)
- RDF/XML: `/metadata/xml <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata/xml>`_ (text/xml)
- JSON: `/metadata/json <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata/json>`_ (application/json)
- JSONLD: `/metadata/jsonld <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata/jsonld>`_ (application/json)
- NTRIPLES: `/metadata/ntriples <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/metadata/ntriples>`_ (text/plain)


#GET Administrative Metadata (ADMMD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Administrative metadata are in DCTERMS namespace and created by the DH-crud service itself at importing time.

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm>`_ (same as adm/ttl)

RESPONSE: text/plain (TURTLE)

.. literalinclude:: ./admmdResponse.ttl
    :language: ttl

Different formats can be requested using

- TURTLE: `/adm/ttl <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm/ttl>`_ (text/plain)
- RDF/XML: `/adm/xml <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm/xml>`_ (text/xml)
- JSON: `/adm/json <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm/json>`_ (application/json)
- JSONLD: `/adm/jsonld <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm/jsonld>`_ (application/json)
- NTRIPLES: `/adm/ntriples <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/adm/ntriples>`_ (text/plain)


#GET Technical Metadata (TECHMD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Technical metadata is extracted from each single object (content file) at import. A `File Information Tool Set (FITS) <http://projects.iq.harvard.edu/fits>`_ service is used by the DH-crud.

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/tech <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/tech>`_

RESPONSE: text/xml (FITS XML)

.. literalinclude:: ./techmdResponse.xml
    :language: xml


#GET Provenance Metadata (PROVMD)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The DARIAH-DE Repository is not yet using provenance matadata.

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/prov <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/prov>`_

RESPONSE: HTTP 500 – Internal Server Error (not yet implemented)


#GET Object Landing Page
^^^^^^^^^^^^^^^^^^^^^^^^

    `https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/landing <https://repository.de.dariah.eu/1.0/dhcrud/21.11113/0000-000B-C8EF-7/landing>`_

RESPONSE: text/html

.. image:: ./landingPage.png


Sources
-------
See dhcrud_sources_


Bugtracking
-----------
See dhcrud_bugtracking_


Licence
-------
See LICENCE_


.. _LICENCE: https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/blob/develop/LICENSE.txt
.. _dhcrud_sources: https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services
.. _dhcrud_bugtracking: https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/issues?scope=all&state=opened&label_name[]=dhcrud
